package com.cupvm.classbuilder;
import java.util.*;
import com.cupvm.jvm.constants.*;
import static com.cupvm.jvm.constants.Constant.*;
import com.cupvm.jvm._abstract.*;
import static com.cupvm.jvm.executor.OPs.*;

public class ClassBuilder implements ACCESS_MODIFIERS{
	private String name;
	private int thisClass;
	private String superClassName="java/lang/Object";
	private int superClass;
	private int accessFlags;
	private final ArrayList<Constant> cpool=new ArrayList<>();
	private final HashMap<String, Integer> strings=new HashMap<>();
	private final HashMap<String, Integer> stringRefs=new HashMap<>();
	private final HashMap<String, Integer> classRefs=new HashMap<>();
	private final ArrayList<AClass> interfaces=new ArrayList<>();
	private final ArrayList<AField> fields=new ArrayList<>();
	private AMethod clinit=null;
	private final ArrayList<AMethod> constructors=new ArrayList<>();
	private final ArrayList<AMethod> methods=new ArrayList<>();
	public ClassBuilder setName(String name){
		this.name=name;
		return this;
	}

	public ClassBuilder addConstructor(AMethod method){
		constructors.add(method);
		return this;
	}

	public ClassBuilder addMethod(AMethod method){
		methods.add(method);
		return this;
	}


	private int createUTF8Constant(String str){
		Integer index=strings.get(str);
		if(index==null){
			Constant utf8=new Constant(CUtf8);
			utf8.str=str;
			cpool.add(utf8);
			index=cpool.size();
			strings.put(str,index);
		}
		return index;
	}

	private int createStringConstant(String str){
		Integer index=stringRefs.get(str);
		if(index==null){
			Constant s=new Constant(CString);
			s.index1=createUTF8Constant(str);
			cpool.add(s);
			index=cpool.size();
			stringRefs.put(str,index);
		}
		return index;
	}

	public int createClassConstant(String clazz){
		Integer index=classRefs.get(clazz);
		if(index==null){
			Constant c=new Constant(CClass);
			c.index1=createUTF8Constant(clazz);
			cpool.add(c);
			index=cpool.size();
			classRefs.put(clazz,index);
		}
		return index;
	}

	public ClassBuilder createEmptyConstructor(){
		MethodBuilder mb=new MethodBuilder();
		mb.setName("<init>")
			.setType("()V")
			.setCode(new byte[]{(byte)RETURN});
		constructors.add(mb.build());
		return this;
	}

	public AClass build(){
		thisClass=createClassConstant(name);
		superClass=createClassConstant(superClassName);
		return null;
	}

	public ArrayList<Constant> getConstantPool(){
		return cpool;
	}
	public int getThisClass(){
		return thisClass;
	}
	public int getSuperClass(){
		if(thisClass==superClass) return 0;
		return superClass;
	}
	public int getAccessFlags(){
		return accessFlags;
	}
	public ArrayList<AClass> getInterfaces(){
		return interfaces;
	}
	public ArrayList<AField> getFields(){
		return fields;
	}

	public AMethod getCLInit(){
		return clinit;
	}

	public ArrayList<AMethod> getConstructors(){
		return constructors;
	}
	public ArrayList<AMethod> getMethods(){
		return methods;
	}
}
