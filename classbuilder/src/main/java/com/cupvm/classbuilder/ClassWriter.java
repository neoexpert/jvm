package com.cupvm.classbuilder;

import com.cupvm.jvm.JVM;
import com.cupvm.jvm._abstract.*;
import com.cupvm.jvm._class.SystemClassLoader;
import com.cupvm.jvm.constants.Constant;

import java.io.*;
import java.util.ArrayList;

import static com.cupvm.jvm.constants.Constant.*;

public class ClassWriter{
	public static void main(String ... args){
		JVM jvm=new JVM();
		SystemClassLoader classLoader = jvm.getClassLoader();
		try{
			JVM.verbose=true;
			classLoader.addToClasspath("./");
			classLoader.loadClass("java/lang/Object");
			ClassBuilder cb=new ClassBuilder();
			cb
				.setName("java/lang/Object")
				.build();
			FileOutputStream fos=new FileOutputStream("java/lang/Object.class");
			write(cb,fos);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public static final int version=52;
	public static final int minor=0;
	public static void write(ClassBuilder cb, OutputStream os) throws IOException{
		DataOutputStream dos=new DataOutputStream(os);
		dos.writeInt(SystemClassLoader.HEAD);
		dos.writeChar(minor);
		dos.writeChar(version);
		//constantpool length:
		ArrayList<Constant> constantPool=cb.getConstantPool();
		dos.writeChar(constantPool.size()+1);
		for(Constant c:constantPool){
			dos.writeByte(c.tag);
			switch (c.tag) {
				default:
					throw new RuntimeException("unknown pool item type " + c.tag);
				case CUtf8:
					dos.writeChar(c.str.length());
					dos.write(c.str.getBytes());
					continue;
				case CClass:
				case CString:
				case CMethodType:
					//s = "%s ref=%d%n";
					dos.writeChar(c.index1);
					continue;
				case CFieldRef:
				case CMethodRef:
				case CInterfaceMethodRef:
				case CNameAndType:
					dos.writeChar(c.index1);
					dos.writeChar(c.index2);
					continue;
				case CInteger:
					dos.writeInt((int)c.value);
					continue;
				case CFloat:
					dos.writeFloat((float)c.value);
					continue;
				case CDouble:
					dos.writeDouble((double) c.value);
					continue;
				case CLong:
					dos.writeLong((long) c.value);
					continue;
				case CMethodHandle:
					dos.writeByte(c.index1);
					dos.writeChar(c.index2);
					continue;
				case CInvokeDynamic:
					dos.writeChar(c.index1);
					dos.writeChar(c.index2);
					continue;
			}
		}
		dos.writeChar(cb.getAccessFlags());
		dos.writeChar(cb.getThisClass());
		dos.writeChar(cb.getSuperClass());
		ArrayList<AClass> interfaces=cb.getInterfaces();
		dos.writeChar(interfaces.size());

		ArrayList<AField> fields=cb.getFields();
		dos.writeChar(fields.size());

		AMethod clinit=cb.getCLInit();
		ArrayList<AMethod> constructors=cb.getConstructors();
		ArrayList<AMethod> methods=cb.getMethods();
		int methodCount=methods.size()+constructors.size();
		if(clinit!=null)
			methodCount++;
		dos.writeChar(methodCount);

		//attribites
		dos.writeChar(0);

		dos.flush();
		dos.close();
	}
}
