package com.cupvm.classbuilder;
import com.cupvm.jvm._abstract.*;
import java.util.*;

public class MethodBuilder implements ACCESS_MODIFIERS{
	private String name;
	private String type;
	private int accessFlags;
	private byte[] code;
	private final ArrayList<Type> params=new ArrayList<>();
	private int max_lv;
	private int max_stack;

	public MethodBuilder setName(String name){
		this.name=name;
		return this;
	}


	public MethodBuilder addParameter(String type){
		params.add(new Type(type));
		return this;
	}

	public MethodBuilder setType(String type){
		this.type=type;
		return this;
	}

	public MethodBuilder setCode(byte[] code){
		this.code=code;
		return this;
	}

	public AMethod build(){
		return null;
	}

	public int getAccessFlags(){
		return accessFlags;
	}
}
