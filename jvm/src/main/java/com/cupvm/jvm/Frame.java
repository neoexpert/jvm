package com.cupvm.jvm;


import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.method.Method;
import com.cupvm.jvm._class.Class;

import java.util.*;

public class Frame implements NativeFrame{
	public Number[] lvpt;

	public final Number get(int pos) {
		return stack[pos];
	}

	public final Number peek() {
		return stack[stackpos - 1];
	}
	public final void clearOperandStack(){
		stackpos=0;
	}

	public final Method getMethod() {
		return m;
	}


	public final int getCurrentLine() {
		return m.getCurrentLine(pc);
	}


	@Override
	public final String toString() {
		return m.getMyClass().getName() + "." + m.getSignature() + ": " + getCurrentLine();
	}

	public int pc = 0;
	public short[] code;
	public byte[] bcode;
	public int stackpos = 0;
	public Number[] stack;

	@Override
	public final void push(Number v) {
		stack[stackpos++] = v;
	}
	@Override
	public final void pushl(Number v) {
		stack[stackpos] = v;
		stackpos+=2;
	}

	public final Number pop() {
		return stack[--stackpos];
	}

	public Number[] lv;

	public Method m;
	public Class clazz;

	public Frame(int lvsize, int stacksize) {
		lv = new Number[lvsize];
		lvpt = new Number[lvsize];
		stack = new Number[stacksize];
	}

	public Frame(Method m) {
		this.m = m;
		this.clazz=m.clazz;
		stack = new Number[m.max_stack];
		code = m.code;
		if (code != null) {
			lv = new Number[m.max_locals];
			lvpt = new Number[m.max_locals];
		}
	}


	public final Frame newRefFrame(Method m) {
		Frame f = new Frame(m);
		int p = m.argsLength+1;
		Object[] lv=f.lv;
		if (lv == null) {
			f.lv = new Number[p];
			lv=f.lv;
			f.lvpt = new Number[p];
		}

		char[] args = m.args;
		Number[] stack=this.stack;
		stackpos-=p;
		Number[] lvpt=f.lvpt;
		Number v=stack[stackpos];
		lv[0]=v;
		lvpt[0]=v;
		for (int i = 1; i < p; ++i) {
			v=stack[stackpos+i];
			lv[i]=v;
			char pt = args[i-1];
			if(pt=='L'||pt=='[')
				lvpt[i]= v;
		}

		return f;
	}

	public final Frame newFrame(Method m) {
		Frame f = new Frame(m);
		int p = m.argsLength;
		Number[] lv = f.lv;
		if (lv == null) {
			f.lv = new Number[p];
			lv=f.lv;
			f.lvpt = new Number[p];
		}

		Number[] lvpt=f.lvpt;
		char[] args = m.args;
		stackpos-=p;
		for (int i = 0; i < p; ++i) {
			Number v=stack[stackpos+i];
			lv[i]=v;
			char pt = args[i];
			if(pt=='L'||pt=='[')
				lvpt[i] = v;
		}
		return f;
	}

	public final void setMethod(Method m) {
		this.m = m;
		this.clazz=m.clazz;
		code = m.code;
		stack = new Number[m.getMaxStack()];
	}

	/*
	public final void initLocals(Method m) {
		this.m = m;
		this.clazz=m.clazz;
		code = m.code;
		Number[] oldlv = lv;
		Number[] oldlvpt = lvpt;
		lv = new Number[m.getMaxLocals()];
		lvpt = new Number[m.getMaxLocals()];
		for (int i = 0; i < Math.min(lv.length, oldlv.length); ++i) {
			lv[i] = oldlv[i];
			lvpt[i]=oldlvpt[i];
		}
	}*/

	public final void checkReference(HashSet<Number> refs) {
		if(lvpt==null)
			return;
		for(Number a:lvpt)
			if(a!=null){
				refs.add(a);
			}
	}
}
