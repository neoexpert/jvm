package com.cupvm.jvm.instance.arrays;

public class CharArrayInstance extends ArrayInstance{
	public final char[] arr;
	public CharArrayInstance(int size){
		super(size);
		arr=new char[size];
		helper=arr;
	}

	public CharArrayInstance(final char[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new CharArrayInstance(arr.clone());
	}
}
