package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;

public class JFloat extends Instance {
    public final float value;
    public JFloat(AClass clazz, float value) {
        super(clazz);
        this.value=value;
    }
}
