package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;


public class JLong extends Instance {
    public final long value;
    public final int hash;

    public JLong(AClass clazz, long value){
        super(clazz);
        this.value=value;
        this.hash=(int)value;
    }

    @Override
    public final boolean equals(Object o) {
        JLong jLong = (JLong) o;
        return value == jLong.value;
    }

    @Override
    public int hashCode(){
    	return hash;
    }
}
