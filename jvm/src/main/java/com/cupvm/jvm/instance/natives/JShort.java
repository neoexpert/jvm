package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;

public class JShort extends Instance {
    public final short value;

    public JShort(AClass clazz, short value) {
        super(clazz);
        this.value=value;
    }
}
