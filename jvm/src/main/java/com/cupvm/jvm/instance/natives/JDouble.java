package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm._abstract.AClass;

public class JDouble extends Instance {
    public final double value;

    public JDouble(AClass clazz, double value) {
        super(clazz);
        this.value=value;
    }
}
