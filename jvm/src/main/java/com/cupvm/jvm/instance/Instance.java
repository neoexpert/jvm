package com.cupvm.jvm.instance;

import com.cupvm.jvm._abstract.AClass;
import java.util.concurrent.locks.ReentrantLock;

public class Instance{
	@Override
	public String toString() {
		return "Instance{}";
	}

	public long addr;
	public Number[] refs;
	public Number[] fields;
	//public volatile int ownerCount;
	public final AClass clazz;
	public Object helper;
	public volatile ReentrantLock lock=null;

	public void putField(String name, Number value){
		fields[clazz.getInstanceFields().get(name).index]=value;
	}

	public Number getField(String name){
		return fields[clazz.getInstanceFields().get(name).index];
	}

	public void putRef(String name, Number value){
		refs[clazz.getInstanceRefs().get(name).index]=value;
	}

	public Number getRef(String name){
		return refs[clazz.getInstanceRefs().get(name).index];
	}

	public Instance(){
		clazz=null;
	}

	public Instance(AClass clazz){
		this.clazz=clazz;
		refs=new Number[clazz.refsCount];
		Number[] fields=clazz.instanceTemplate;
		int length=fields.length;
		this.fields =new Number[length];
		if(length>0){
			System.arraycopy(fields, 0, this.fields, 0, fields.length);
		}
	}
}
