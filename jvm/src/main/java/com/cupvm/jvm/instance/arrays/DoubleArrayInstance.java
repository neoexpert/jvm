package com.cupvm.jvm.instance.arrays;

public class DoubleArrayInstance extends ArrayInstance{
	public final double[] arr;
	public DoubleArrayInstance(int size){
		super(size);
		arr=new double[size];
		helper=arr;
	}
	public DoubleArrayInstance(double[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new DoubleArrayInstance(arr.clone());
	}
}
