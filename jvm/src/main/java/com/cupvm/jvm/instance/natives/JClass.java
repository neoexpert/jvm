package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm.JVM;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.instance.Instance;

public class JClass extends Instance {
	public final String name;
	public final AClass the_clazz;

	public JClass(String name) {
		super(JVM.classClass);
		this.name=name;
		the_clazz=null;
	}

	public JClass(AClass the_clazz){
		super(JVM.classClass);
		this.name=clazz.getName();
		this.the_clazz=the_clazz;
	}

	public String getJNISignature(){
		switch(name){
			case "int": 
			case "I": 
				return "I";
			default:
				return "L"+name+";";
		}
	}
}
