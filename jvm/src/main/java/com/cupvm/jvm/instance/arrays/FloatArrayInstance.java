package com.cupvm.jvm.instance.arrays;

public class FloatArrayInstance extends ArrayInstance{
	public final float[] arr;

	public FloatArrayInstance(int size){
		super(size);
		arr=new float[size];
		helper=arr;
	}
	public FloatArrayInstance(float[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new FloatArrayInstance(arr.clone());
	}
}
