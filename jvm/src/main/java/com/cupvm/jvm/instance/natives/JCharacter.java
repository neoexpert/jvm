package com.cupvm.jvm.instance.natives;

import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm._abstract.AClass;

public class JCharacter extends Instance {
    private static AClass java_lang_Character;
		public static void init(AClass clazz){
			java_lang_Character=clazz;
		}
    public final char value;

    public JCharacter(char value) {
        super(java_lang_Character);
        this.value=value;
    }
}
