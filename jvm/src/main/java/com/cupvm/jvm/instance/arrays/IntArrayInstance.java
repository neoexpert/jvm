package com.cupvm.jvm.instance.arrays;

public class IntArrayInstance extends ArrayInstance{
	public final int[] arr;
	public IntArrayInstance(int size){
		super(size);
		arr=new int[size];
		helper=arr;
	}
	public IntArrayInstance(int[] arr){
		super(arr.length);
		this.arr=arr;
		helper=arr;
	}
	public ArrayInstance cloneme(){
		return new IntArrayInstance(arr.clone());
	}
}
