package com.cupvm.jvm.instance.arrays;

import com.cupvm.jvm.Heap;
import com.cupvm.jvm.instance.Instance;

public abstract class ArrayInstance extends Instance{
	public static final byte T_OBJECT = 3;
	public static final byte T_BOOLEAN = 4;
	public static final byte T_CHAR = 5;
	public static final byte T_FLOAT = 6;
	public static final byte T_DOUBLE = 7;
	public static final byte T_BYTE = 8;
	public static final byte T_SHORT = 9;
	public static final byte T_INT = 10;
	public static final byte T_LONG = 11;

	public final int size;
	public ArrayInstance(int size){
		super();
		this.size = size;
	}

	public static ArrayInstance create(int size, char l) {
		switch (l) {
			case 'I':
				return new IntArrayInstance(size);
			case 'Z':
				return new ByteArrayInstance(size);
			case 'B':
				return new ByteArrayInstance(size);
			case 'C':
				return new CharArrayInstance(size);
			case 'D':
				return new DoubleArrayInstance(size);
			case 'F':
				return new FloatArrayInstance(size);
			case 'J':
				return new LongArrayInstance(size);
			case 'S':
				return new ShortArrayInstance(size);
			case 'L':
			case ';':
				return new ObjectArrayInstance(size);
			default:
				throw new RuntimeException();
		}
	}


	public static ArrayInstance create(int dimensions, int dimension, int[] sizes, String classname) {
		int size=sizes[dimension];
		if(dimension==0)
		{
			return create(size,classname.charAt(classname.length()-1));
		}
		ObjectArrayInstance oarr=new ObjectArrayInstance(size);
		for(int i=0;i<size;++i){
			ArrayInstance ai = create(dimensions, dimension - 1, sizes, classname);
			oarr.refs[i]=Heap.add(ai);
		}
		return oarr;
	}

	public abstract ArrayInstance cloneme();
}
