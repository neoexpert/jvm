package com.cupvm.jvm.instance.natives;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.arrays.ByteArrayInstance;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

public class NInputStream extends Instance {
    private InputStream is;
    public NInputStream(ClassLoader classLoader, InputStream is) {
        super(classLoader.loadClass("neo/NInputStream"));
        this.is=is;
    }
    public static LinkedList<JNIMethod> gen() {
        LinkedList<JNIMethod> methods = new LinkedList<>();
        methods.add(new JNIMethod("neo/NInputStream.read()I", 0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                NInputStream nis= (NInputStream) Heap.get(lv[0]);
                try {
                    cF.push(nis.is.read());
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                return false;
            }
        });
        methods.add(new JNIMethod("neo/NInputStream.read([BII)I", 3) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                NInputStream nis= (NInputStream) Heap.get(lv[0]);
                ByteArrayInstance bai= (ByteArrayInstance) Heap.get(lv[1]);
                try {
                    cF.push(nis.is.read(bai.arr,lv[2].intValue(),lv[3].intValue()));
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                return false;
            }
        });
        return methods;
    }
}
