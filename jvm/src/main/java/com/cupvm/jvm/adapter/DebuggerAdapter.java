package com.cupvm.jvm.adapter;

import com.cupvm.jvm.Frame;
import com.cupvm.jvm.FrameStack;

public interface DebuggerAdapter {
	void setFrame(Frame f);

	void setFrameStack(FrameStack stack);
}
