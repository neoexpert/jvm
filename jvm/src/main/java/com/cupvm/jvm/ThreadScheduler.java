package com.cupvm.jvm;
import java.util.*;

public class ThreadScheduler{
	private final LinkedList<MyThread> threads=new LinkedList<>();
	private final LinkedList<MyThread> newthreads=new LinkedList<>();
	public final void notify(MyThread t){
		newthreads.add(t);
	}
	public final void setMainThread(MyThread main_thread){
		threads.add(main_thread);
	}
	public final void add(MyThread t){
		if(t.isDaemon()){}
		else
			newthreads.add(t);
	}

	public final void run(){
		while(!threads.isEmpty()){
			ListIterator<MyThread> it=threads.listIterator(0);
			while(it.hasNext()){
				if(it.next().prun())
				{
					it.remove();
				}
			}	
			if(!newthreads.isEmpty()){
				threads.addAll(newthreads);
				newthreads.clear();
			}
		}
	}
}
