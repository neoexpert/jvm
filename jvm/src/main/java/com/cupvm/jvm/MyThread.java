package com.cupvm.jvm;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jni.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.method.Method;
import com.cupvm.jvm.executor.*;

import java.util.*;

public class MyThread implements ThreadInterface{
	private static final Logger logger= ConsoleLogger.get();
	public static JNI jni=null;
	public final MyThread monitor;
	private final ClassLoader classLoader;

	public static void setJNI(JNI _jni){
		if(jni==null)
			jni=_jni;
	}

	public static void addNativeMethods(final LinkedList<JNIMethod> methods){
		jni.addAll(methods);
	}


	private String name;

	public final int getStackSize() {
		return cE.stack.size();
	}


	public final String getStackTrace() {
		Iterator<NativeFrame> it = cE.stack.iterator();
		StringBuilder sb = new StringBuilder();
		while (it.hasNext()) {
			sb.append(it.next());
			sb.append("\n");
		}
		return sb.toString();
	}

	public final void printStackTrace() {
		Iterator<NativeFrame> it = cE.stack.iterator();
		StringBuilder sb = new StringBuilder();
		while (it.hasNext()) {
			logger.error(it.next()+"");
		}
	}

	public final Frame getPrevFrame() {
		Frame f = cE.stack.pop();
		Frame pf = cE.stack.peek();
		cE.stack.push(f);
		return pf;
	}


	public final MyThread setMethodToRun(Method method, Number... args) {
		Frame cF = new Frame(method);
		for (int i = 0; i < args.length; ++i) {
			cF.lv[i] = args[i];
		}
		cE.stack.push(cF);
		return this;
	}

	public final MyThread setStaticMethodToRun(String method, Number... args) {
		Method m = (Method) cE.cC.getDeclaredMethod(method);
		if (m == null)
			return this;
		Frame cF = new Frame(m);
		char[] _args=m.args;
		for (int i = 0; i < args.length; ++i) {
			cF.lv[i] = args[i];
			char t = _args[i];
			if(t=='L'||t=='[')
				cF.lvpt[i]=args[i];
		}
		cE.stack.push(cF);
		return this;
	}

	public final int getCurrentLine() {
		if (cE.stack.isEmpty())
			return -1;
		return cE.stack.peek().getCurrentLine();
	}

	private static int counter=0;

	public MyThread(Class cC, ClassLoader classLoader, String name, MyThread monitor){
	    this.classLoader=classLoader;
		rbce=new RawShortCodeExecutor(this, jni);
		cE=rbce;
		this.name=name+" "+counter++;
		if(monitor==null)
			this.monitor=this;
		else
			this.monitor=monitor;

		cE.cC = cC;
			Heap.addThread(this);
	}

	private boolean daemon=false;
	private int psteps=1024;

	public final void setPriority(int priority){
		psteps=priority*1024;
	}

	public final void setDaemon(boolean on){
		this.daemon=on;
	}

	public final boolean isDaemon(){
		return daemon;
	}

	public final void _wait(){
		waiting=true;
	}

	private boolean waiting=false;
	public final boolean isWaiting(){
		return waiting;
	}

	public final void pushFrame(NativeFrame nFrame, Object o) {
		Frame prevFrame= (Frame) nFrame;
		Method m= (Method) o;
		Frame f = prevFrame.newRefFrame(m);
		cE.stack.push(f);
		cE.cC = m.clazz;
	}

	// int pc=0;
	public boolean debuggerStep() {
		return true;
	}

	public void destroy(){
		Heap.removeThread(this);
		synchronized(lock){
			isAlive=false;
			lock.notifyAll();
		}
	}
	private final RawShortCodeExecutor rbce;
	private ByteCodeExecutor cE;

	public final boolean prun() {
		/*
		cE.steps=psteps;
		while(true)
		switch(cE.step()){
			case 0:
				return false;
			case 1:
				destroy();
				return true;
		}*/
		return false;
	}

	public final void run() {
		try{
			cE.step();
		}
		finally{
			destroy();
		}
		/*
		cE.steps=Integer.MAX_VALUE;
		while(true)
			switch(cE.step()){
				case 0:
					continue;
				case 1:
					destroy();
					return;
			}*/
	}
	public void interrupt(){
		t.interrupt();
	}
	public void suspend(){
		t.suspend();
	}
	public void resume(){
		t.resume();
	}
	final Object lock=new Object();
	public final void join() throws InterruptedException{
			synchronized(lock){
				if(isAlive)
					lock.wait();
			}
		//t.join();
	}
	public void setName(String name){
		this.name=name;
	}
	volatile boolean isAlive;
	Thread t;
	public final void start(){
		isAlive=true;
		MyThread mt=this;
		t=new Thread(new Runnable(){
			@Override
			public void run(){
				try{
					cE.run();
				}
				finally{
					mt.destroy();
				}
			}
		});
		t.setName(this.name);
		t.start();
	}

	public static int s = 0;

	@Override
	public final String toString() {
		return name;
	}

	public final Frame getCurrentFrame() {
		return cE.stack.peek();
	}

	public final FrameStack getStack() {
		return cE.stack;
	}

	public final void checkReferences(HashSet<Number> refs) {
		Iterator<NativeFrame> it = cE.stack.iterator();
		NativeFrame f =  it.next();
		while(f!=null){
			f.checkReference(refs);
			f=it.next();
		}
	}

	public final String getName(){
		return name;
	}

	public ClassLoader getClassLoader() {
		return classLoader;
	}
}
