package com.cupvm.jvm;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.debugger.event.EventHandler;
import com.cupvm.debugger.event.EventModifier;
import com.cupvm.jni.ThreadInterface;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.SystemClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.method.Method;
import com.cupvm.jvm.instance.natives.*;

import java.io.*;
import java.util.*;


public class JVM implements EventHandler, ClassLoader.StaticCreator{
	public static int logColor;
	public static AClass stringClass;
	public static AClass classClass;

	public static void setLogColor(int color){
		logColor=color;
		logger=ConsoleLogger.get(color);
	}
	private static Logger logger= ConsoleLogger.get();
	SystemClassLoader classLoader;
	public JVM(){
		classLoader=new SystemClassLoader();

		classLoader.setStaticCreator(this);
	}
	@Override
	public int createStaticRef(Number v) {
		return Heap.addStaticRef(v);
	}

	@Override
	public int createStatic(Number v) {
			return Heap.addStaticField(v);
	}

	@Override
	public Number createStringInstance(String str) {
		JString jString=new JString(str);
		return Heap.addPermanent(jString);
	}


	public SystemClassLoader getClassLoader(){
		return classLoader;
	}

	public List<? extends ThreadInterface> getThreads(){
		return Heap.getThreads();
	}

	private int suspended=0;
	public void suspend(){
		if(suspended==0){
			List<MyThread> threads=Heap.getThreads();	
			synchronized(threads){
				for(MyThread t:threads){
					t.suspend();
				}
			}
		}
		suspended++;
	}

	public void resume(){
		suspended--;
		if(suspended==0){
			List<MyThread> threads=Heap.getThreads();	
			synchronized(threads){
				for(MyThread t:threads){
					t.resume();
				}
			}
		}
	}

	@Override
	public void addEventListener(byte suspendPolicy, byte eventKind, int requestID, EventModifier[] mods) {

	}

	@Override
	public void removeEventListener(byte eventKind, int requestID) {

	}



	@Override
	public void holdEvents() {

	}

	@Override
	public void releaseEvents() {

	}

	/*private static final ThreadScheduler ts=new ThreadScheduler();
	public static void notify(MyThread t){
		ts.notify(t);
	}
	public static void addThread(MyThread t){
		ts.add(t);
	}*/
	public static boolean verbose=false;
	/*
	public static String logPrefix="";
	private static String logPostfix="\u001b[0m";
	public static String warnPrefix="\u001b[33m";
	public static String errPrefix="\u001b[31m";
	 */
	public static int LINES=-1;
	public static int COLUMNS=-1;
	/*
	There's a trick that you can use based on ANSI Escape Codes. They don't provide a direct way to query the console size, but they do have a command for requesting the current position size. By moving the cursor to a really high row and column and then requesting the console size you can get an accurate measurement.

Send the following sequences to the terminal (stdout)

"\u001b[s"             // save cursor position
"\u001b[5000;5000H"    // move to col 5000 row 5000
"\u001b[6n"            // request cursor position
"\u001b[u"             // restore cursor position
Now watch stdin, you should receive a sequece that looks like \u001b[25;80R", where 25 is the row count, and 80 the columns.
	*/
	/*
	public static void log(String s){
		System.out.print(logPrefix);
		System.out.print(s);
		System.out.println(logPostfix);
	}
	public static void warn(String s){
		if(!verbose)return;
		System.err.print(logPrefix);
		System.err.print("WARN: ");
		System.err.print(warnPrefix);
		System.err.print(s);
		System.err.println(logPostfix);
	}

	public static void error(final String s){
		System.err.print(errPrefix);
		System.err.print(s);
		System.err.println(logPostfix);
	}

	public static void print(String s){
		System.out.print(logPrefix);
		System.out.print(s);
		System.out.print(logPostfix);
	}

	 */

	public static boolean debug;



	public static void main(String[] args) throws Exception {
		String cp=".";
		String s = new File(cp).getAbsolutePath();

		JVM jvm = new JVM();
		jvm.runMainClass(args[0], 0);
		//Heap.gc();
		//System.out.println(Heap.tostring());
		//print all loaded classes
	    /*
	       Set<String> classes=Class.classes.keySet();
	       for(String cl:classes)
	       System.out.println(cl);
	       */
		//System.out.println(OptimizedShortCodeExecutor.s);


	}
	public void runMainClass(String mainClass, Number aref){
		/*
		MyThread.setJNI(new JNI(){
			@Override
			public boolean invoke(String nativeDesc, NativeFrame cF, Object[] lv, ThreadInterface t){
				switch(nativeDesc){
					case "Main.print(Ljava/lang/String;)V":
						int oref = (int) lv[0];
						ClassInstance ci = (ClassInstance) Heap.get(oref);
						oref = (int) ci.getField("value");
						ArrayInstance ai = (ArrayInstance) Heap.get(oref);

						char[] ca = (char[]) ai.a;
						System.out.print(ca);
						return false;
					case "Main.print(I)V":
						System.out.println(lv[0]);
						return false;
					default:
						throw new RuntimeException("native method '"+nativeDesc+"' not found");
				}
			}
		});
		 */
		Class c = (Class)classLoader.loadClass(mainClass);
		if(c==null){
			logger.error(mainClass , " class not found");
			return;
		}
		MyThread t = new MyThread(c, classLoader,"main",null);
		try {
			t.setStaticMethodToRun("main([Ljava/lang/String;)V", aref);
			t.start();
			t.join();
			//ts.setMainThread(t);
			//ts.run();
		} catch (Exception e) {
			//t.getCurrentFrame();
			throw new RuntimeException(t.getStackTrace(), e);
		}
	}

	public void reset() {
		classLoader.reset();
		Heap.reset();
	}

	public void saveState(OutputStream os)throws IOException{
		suspend();
		resume();	
	}

	public static void clinit(AClass clazz, MyThread thread) {
		Method m= (Method)clazz.getDeclaredMethod("<clinit>()V");
		clazz.isInitialized = true;
		if(m==null)
			return;
		String name=clazz.getName();
		final MyThread t = new MyThread((Class)clazz, clazz.getClassLoader(),"clinit "+name,  thread)
				.setMethodToRun(m);
		try {
			t.run();
			//methodsbyname.remove("<clinit>()V");
		} catch (Exception e) {
			throw new RuntimeException(name + ".<clinit>()V" + t.getStackTrace(), e);
		}
	}

}
