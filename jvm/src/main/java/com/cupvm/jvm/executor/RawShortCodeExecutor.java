package com.cupvm.jvm.executor;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.*;
import com.cupvm.jni.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._class.BootstrapMethod;
import com.cupvm.jvm._abstract.*;
import com.cupvm.jvm._class.MethodHandleClass;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.field.FieldHead;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.arrays.*;
import com.cupvm.jvm.method.*;
import java.util.concurrent.locks.ReentrantLock;

import static com.cupvm.jvm.Globals.*;

public class RawShortCodeExecutor extends ByteCodeExecutor implements OPs{
	private static final Logger logger= ConsoleLogger.get(JVM.logColor);
	public RawShortCodeExecutor(MyThread thread, JNI jni){
		super(thread, jni);
	}

	public void run(){
		steps = Integer.MAX_VALUE;
		step();
	}

	@Override
	public final Object step(){
		if(stack.isEmpty()){
			//thread.destroy();
			return 1;
		}

		//current Frame
		Frame cF = stack.peek();
		return step(cF.stackpos, cF.pc, cF.stack, cF.code, cF, this, classLoader, env);
	}

	static int stepsdone = 0;

	//The Holy Grail
	public static Object step(int stackpos, int pc, Number[] opstack, short[] code, Frame cF, RawShortCodeExecutor $this, ClassLoader classLoader, JNIEnv env){
		//local variables are faster to load/store
		Number[] lv = cF.lv;
		//currentClass
		Class cC = cF.clazz;
		Constant[] constantPool = cC.constantPool;
		final FrameStack stack = $this.stack;
		int i1, i2, i3;
		Number oref;
		long l1, l2;
		float f1, f2;
		double d1, d2;
		Instance c;
		AClass cl;
		Method m;
		Number o1, o2, o3, o4;
		Constant c0;
		Constant c1;
		ArrayInstance array;
		CharArrayInstance carray;
		FloatArrayInstance farray;
		DoubleArrayInstance darray;
		ByteArrayInstance barray;
		ShortArrayInstance sarray;
		IntArrayInstance iarray;
		LongArrayInstance jarray;
		ObjectArrayInstance oarray;
		Integer iconst_m1 = Globals.iconst_m1;
		Integer iconst_0 = Globals.iconst_0;
		Number aconst_null = Globals.aconst_null;
		Integer iconst_1 = Globals.iconst_1;
		Integer iconst_2 = Globals.iconst_2;
		Integer iconst_3 = Globals.iconst_3;
		Integer iconst_4 = Globals.iconst_4;
		Integer iconst_5 = Globals.iconst_5;
		Number[] mlv;
		//Method[] methodCache = RawShortCodeExecutor.methodCache;
		//Number[] constantCache = RawShortCodeExecutor.constantCache;
		short s;
		JNIMethod handler;
		//byte op = code[pc];

		//if (cF.isThrowing())
		//op = (byte) 0xBF;

		int steps = $this.steps;
		mainLoop:
		while(true){
			/*
			if(JVM.verbose){
			   StringBuilder sb = new StringBuilder();
			   sb.append(stepsdone);
			   sb.append(": ");
			   ByteCodePrinter.parseOp(pc, code, sb);
			   logger.log(sb.toString());
			   stepsdone++;
			   if(stepsdone==166263)
			       $this.getClass();
			}
			//*/
			switch(code[pc]){
				case (short) NOP:
					pc += 1;
					continue;
				case (short) ACONST_NULL:
					opstack[stackpos++] = aconst_null;
					pc += 1;
					continue;
				case (short) ICONST_M1:
					opstack[stackpos++] = iconst_m1;
					pc += 1;
					continue;
				case (short) ICONST_0:
					opstack[stackpos++] = iconst_0;
					pc += 1;
					continue;
				case (short) ICONST_1:
					opstack[stackpos++] = iconst_1;
					pc += 1;
					continue;
				case (short) ICONST_2:
					opstack[stackpos++] = iconst_2;
					pc += 1;
					continue;
				case (short) ICONST_3:
					opstack[stackpos++] = iconst_3;
					pc += 1;
					continue;
				case (short) ICONST_4:
					opstack[stackpos++] = iconst_4;
					pc += 1;
					continue;
				case (short) ICONST_5:
					opstack[stackpos++] = iconst_5;
					pc += 1;
					continue;
				case (short) LCONST_0:
					opstack[stackpos++] = lconst_0;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) LCONST_1:
					opstack[stackpos++] = lconst_1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) FCONST_0:
					opstack[stackpos++] = fconst_0;
					pc += 1;
					continue;
				case (short) FCONST_1:
					opstack[stackpos++] = fconst_1;
					pc += 1;
					continue;
				case (short) FCONST_2:
					opstack[stackpos++] = 2f;
					pc += 1;
					continue;
				case (short) DCONST_0:
					opstack[stackpos++] = dconst_0;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) DCONST_1:
					opstack[stackpos++] = dconst_1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) BIPUSH:
					s = addConstantToCache(Integer.valueOf((byte) code[pc + 1]));
					//constantCache = RawShortCodeExecutor.constantCache;
					code[pc] = _BIPUSH;
					code[pc + 1] = s;
					continue;
				case (short) SIPUSH:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					s = addConstantToCache(Integer.valueOf(s));
					//constantCache = RawShortCodeExecutor.constantCache;
					code[pc] = _SIPUSH;
					code[pc + 1] = s;
					continue;
				case (short) LDC:
					c0 = constantPool[code[pc + 1]];
					o1 = c0.value;
					if(o1 == null){
						o1 = $this.ldc(constantPool, code[pc + 1]);
						c0.value = o1;
					}
					s = addConstantToCache(o1);
					//constantCache = RawShortCodeExecutor.constantCache;
					code[pc + 1] = s;
					code[pc] = _LDC;
					continue;
				case (short) LDC_W:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[s];
					o1 = c0.value;
					if(o1 == null){
						o1 = $this.ldc(constantPool, s);
						c0.value = o1;
					}
					code[pc] = _LDC_W;
					code[pc + 1] = s;
					continue;
				case (short) LDC2_W:
					//cF.stackpos = stackpos;
					opstack[stackpos++] = cC.ldc2((((code[pc + 1]) << 8) | (code[pc + 2])));
					opstack[stackpos++] = null;
					//stackpos = cF.stackpos;
					pc += 3;
					continue;
				case (short) ILOAD:
					opstack[stackpos++] = lv[code[pc + 1]];
					pc += 2;
					continue;
				case (short) LLOAD:
					opstack[stackpos] = lv[code[pc + 1]];
					stackpos+=2;
					//opstack[stackpos++] = lv[code[pc + 1] + 1];
					pc += 2;
					continue;
				case (short) FLOAD:
					opstack[stackpos++] = lv[code[pc + 1]];
					pc += 2;
					continue;
				case (short) DLOAD:
					opstack[stackpos++] = lv[code[pc + 1]];
					opstack[stackpos++] = lv[code[pc + 1] + 1];
					pc += 2;
					continue;
				case (short) ALOAD:
					opstack[stackpos++] = lv[code[pc + 1]];
					pc += 2;
					continue;
				case (short) ILOAD_0:
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (short) ILOAD_1:
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (short) ILOAD_2:
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (short) ILOAD_3:
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (short) LLOAD_0:
					opstack[stackpos] = lv[0];
					stackpos+=2;
					//opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (short) LLOAD_1:
					opstack[stackpos] = lv[1];
					stackpos+=2;
					//opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (short) LLOAD_2:
					opstack[stackpos] = lv[2];
					stackpos+=2;
					//opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (short) LLOAD_3:
					opstack[stackpos] = lv[3];
					stackpos+=2;
					//opstack[stackpos++] = lv[4];
					pc += 1;
					continue;
				case (short) FLOAD_0:
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (short) FLOAD_1:
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (short) FLOAD_2:
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (short) FLOAD_3:
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (short) DLOAD_0:
					opstack[stackpos++] = lv[0];
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (short) DLOAD_1:
					opstack[stackpos++] = lv[1];
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (short) DLOAD_2:
					opstack[stackpos++] = lv[2];
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (short) DLOAD_3:
					opstack[stackpos++] = lv[3];
					opstack[stackpos++] = lv[4];
					pc += 1;
					continue;
				case (short) ALOAD_0:
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (short) ALOAD_1:
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (short) ALOAD_2:
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (short) ALOAD_3:
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (short) IALOAD:
					iarray = (IntArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = iarray.arr[opstack[stackpos].intValue()];
					pc += 1;
					continue;
				case (short) LALOAD:
					jarray = (LongArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = jarray.arr[opstack[stackpos].intValue()];
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) FALOAD:
					farray = (FloatArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = farray.arr[opstack[stackpos].intValue()];
					pc += 1;
					continue;
				case (short) DALOAD:
					darray = (DoubleArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = darray.arr[opstack[stackpos].intValue()];
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) AALOAD:
					oref = Heap.get(opstack[--stackpos - 1]).refs[opstack[stackpos].intValue()];
					opstack[stackpos - 1] = oref == null ? aconst_null : oref;
					pc += 1;
					continue;
				case (short) BALOAD:
					barray = (ByteArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = Integer.valueOf(barray.arr[opstack[stackpos].intValue()]);
					pc += 1;
					continue;
				case (short) CALOAD:
					carray = (CharArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = Integer.valueOf(carray.arr[opstack[stackpos].intValue()]);
					pc += 1;
					continue;
				case (short) SALOAD:
					sarray = (ShortArrayInstance) Heap.get(opstack[--stackpos - 1]);
					opstack[stackpos - 1] = Integer.valueOf(sarray.arr[opstack[stackpos].intValue()]);
					pc += 1;
					continue;
				case (short) ISTORE:
					lv[code[pc + 1]] = opstack[--stackpos];
					pc += 2;
					continue;
				case (short) LSTORE:
					stackpos -= 2;
					//lv[code[pc + 1] + 1] = opstack[stackpos + 1];
					lv[code[pc + 1]] = opstack[stackpos];
					pc += 2;
					continue;
				case (short) FSTORE:
					lv[code[pc + 1]] = opstack[--stackpos];
					pc += 2;
					continue;
				case (short) DSTORE:
					lv[code[pc + 1] + 1] = opstack[--stackpos];
					lv[code[pc + 1]] = opstack[--stackpos];
					pc += 2;
					continue;
				case (short) ASTORE:
					oref = opstack[--stackpos];
					lv[code[pc + 1]] = oref;
					cF.lvpt[code[pc + 1]] = oref;
					pc += 2;
					continue;
				case (short) ISTORE_0:
					lv[0] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) ISTORE_1:
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) ISTORE_2:
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) ISTORE_3:
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) LSTORE_0:
					//lv[1] = opstack[--stackpos];
					stackpos-=2;
					lv[0] = opstack[stackpos];
					pc += 1;
					continue;
				case (short) LSTORE_1:
					//lv[2] = opstack[--stackpos];
					stackpos-=2;
					lv[1] = opstack[stackpos];
					pc += 1;
					continue;
				case (short) LSTORE_2:
					//lv[3] = opstack[--stackpos];
					stackpos-=2;
					lv[2] = opstack[stackpos];
					pc += 1;
					continue;
				case (short) LSTORE_3:
					//lv[4] = opstack[--stackpos];
					stackpos-=2;
					lv[3] = opstack[stackpos];
					pc += 1;
					continue;
				case (short) FSTORE_0:
					lv[0] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) FSTORE_1:
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) FSTORE_2:
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) FSTORE_3:
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) DSTORE_0:
					lv[1] = opstack[--stackpos];
					lv[0] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) DSTORE_1:
					lv[2] = opstack[--stackpos];
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) DSTORE_2:
					lv[3] = opstack[--stackpos];
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) DSTORE_3:
					lv[4] = opstack[--stackpos];
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (short) ASTORE_0:
					lv[0] = opstack[--stackpos];
					cF.lvpt[0] = lv[0];
					pc += 1;
					continue;
				case (short) ASTORE_1:
					lv[1] = opstack[--stackpos];
					cF.lvpt[1] = lv[1];
					pc += 1;
					continue;
				case (short) ASTORE_2:
					lv[2] = opstack[--stackpos];
					cF.lvpt[2] = lv[2];
					pc += 1;
					continue;
				case (short) ASTORE_3:
					lv[3] = opstack[--stackpos];
					cF.lvpt[3] = lv[3];
					pc += 1;
					continue;
				case (short) IASTORE:
					stackpos -= 3;
					iarray = (IntArrayInstance) Heap.get(opstack[stackpos]);
					iarray.arr[opstack[stackpos + 1].intValue()] = opstack[stackpos + 2].intValue();
					pc += 1;
					continue;
				case (short) LASTORE:
					--stackpos;
					oref = opstack[--stackpos];
					i3 = (int) opstack[--stackpos];
					jarray = (LongArrayInstance) Heap.get(opstack[--stackpos]);
					jarray.arr[i3] = oref.longValue();
					pc += 1;
					continue;
				case (short) FASTORE:
					stackpos -= 3;
					farray = (FloatArrayInstance) Heap.get(opstack[stackpos]);
					farray.arr[opstack[stackpos + 1].intValue()] = opstack[stackpos + 2].floatValue();
					pc += 1;
					continue;
				case (short) DASTORE:
					--stackpos;
					oref = opstack[--stackpos];
					i3 = (int) opstack[--stackpos];
					darray = (DoubleArrayInstance) Heap.get(opstack[--stackpos]);
					darray.arr[i3] = oref.doubleValue();
					pc += 1;
					continue;
				case (short) AASTORE:
					stackpos -= 3;
					c = Heap.get(opstack[stackpos]);
					c.refs[opstack[stackpos + 1].intValue()] = opstack[stackpos + 2];
					pc += 1;
					continue;
				case (short) BASTORE:
					stackpos -= 3;
					barray = (ByteArrayInstance) Heap.get(opstack[stackpos]);
					barray.arr[opstack[stackpos + 1].intValue()] = opstack[stackpos + 2].byteValue();
					pc += 1;
					continue;
				case (short) CASTORE:
					stackpos -= 3;
					carray = (CharArrayInstance) Heap.get(opstack[stackpos]);
					carray.arr[opstack[stackpos + 1].intValue()] = (char) opstack[stackpos + 2].intValue();
					pc += 1;
					continue;
				case (short) SASTORE:
					stackpos -= 3;
					sarray = (ShortArrayInstance) Heap.get(opstack[stackpos]);
					sarray.arr[opstack[stackpos + 1].intValue()] = opstack[stackpos + 2].shortValue();
					pc += 1;
					continue;
				case (short) POP:
					--stackpos;
					pc += 1;
					continue;
				case (short) POP2:
					stackpos -= 2;
					pc += 1;
					continue;
				case (short) DUP:
					opstack[stackpos] = opstack[stackpos - 1];
					++stackpos;
					pc += 1;
					continue;
				case (short) DUP_X1:
					oref = opstack[stackpos - 1];
					o1 = opstack[stackpos - 2];
					opstack[stackpos - 2] = oref;
					opstack[stackpos - 1] = o1;
					opstack[stackpos] = oref;
					++stackpos;
					pc += 1;
					continue;
				case (short) DUP_X2:
					o1 = opstack[stackpos - 1];
					o2 = opstack[stackpos - 2];
					o3 = opstack[stackpos - 3];
					opstack[stackpos - 3] = o1;
					opstack[stackpos - 2] = o3;
					opstack[stackpos - 1] = o2;
					opstack[stackpos] = o1;
					++stackpos;
					pc += 1;
					continue;
				case (short) DUP2:
					o2 = opstack[stackpos - 1];
					o1 = opstack[stackpos - 2];
					opstack[stackpos - 2] = o1;
					opstack[stackpos - 1] = o2;
					opstack[stackpos] = o1;
					opstack[stackpos + 1] = o2;
					stackpos += 2;
					pc += 1;
					continue;
				case (short) DUP2_X1:
					o2 = opstack[stackpos - 1];
					o1 = opstack[stackpos - 2];
					o3 = opstack[stackpos - 3];
					opstack[stackpos - 3] = o1;
					opstack[stackpos - 2] = o2;
					opstack[stackpos - 1] = o3;
					opstack[stackpos] = o1;
					opstack[stackpos + 1] = o2;
					stackpos += 2;
					pc += 1;
					continue;
				case (short) DUP2_X2:
					o2 = opstack[stackpos - 1];
					o1 = opstack[stackpos - 2];
					o4 = opstack[stackpos - 3];
					o3 = opstack[stackpos - 4];
					opstack[stackpos - 4] = o1;
					opstack[stackpos - 3] = o2;
					opstack[stackpos - 2] = o3;
					opstack[stackpos - 1] = o4;
					opstack[stackpos] = o1;
					opstack[stackpos + 1] = o2;
					stackpos += 2;
					pc += 1;
					continue;
				case (short) SWAP:
					o1 = opstack[stackpos - 1];
					o2 = opstack[stackpos - 2];
					opstack[stackpos - 2] = o1;
					opstack[stackpos - 1] = o2;
					pc += 1;
					continue;
				case (short) IADD:
					opstack[--stackpos - 1] = opstack[stackpos].intValue() + opstack[stackpos - 1].intValue();
					pc += 1;
					continue;
				case (short) LADD:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() + opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FADD:
					opstack[stackpos - 2] = opstack[stackpos - 1].floatValue() + opstack[stackpos - 2].floatValue();
					--stackpos;
					pc += 1;
					continue;
				case (short) DADD:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					d1 = d2 + d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) ISUB:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue() - opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LSUB:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() - opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FSUB:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].floatValue() - opstack[stackpos].floatValue();
					pc += 1;
					continue;
				case (short) DSUB:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					d1 = d2 - d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) IMUL:
					opstack[--stackpos - 1] = opstack[stackpos - 1].intValue() * opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LMUL:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() * opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FMUL:
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2 * f1;
					pc += 1;
					continue;
				case (short) DMUL:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					d1 = d2 * d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) IDIV:
					opstack[--stackpos - 1] = opstack[stackpos - 1].intValue() / opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LDIV:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() / opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FDIV:
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2 / f1;
					pc += 1;
					continue;
				case (short) DDIV:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					d1 = d2 / d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) IREM:
					opstack[--stackpos - 1] = opstack[stackpos - 1].intValue() % opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LREM:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() % opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FREM:
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2 % f1;
					pc += 1;
					continue;
				case (short) DREM:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					d1 = d2 % d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) INEG:
					opstack[stackpos - 1] = -(int) opstack[stackpos - 1];
					pc += 1;
					continue;
				case (short) LNEG:
					opstack[stackpos - 2] = -opstack[stackpos - 2].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) FNEG:
					opstack[stackpos - 1] = -(float) opstack[stackpos - 1];
					pc += 1;
					continue;
				case (short) DNEG:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					d1 = -d1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) ISHL:
					opstack[--stackpos - 1] = opstack[stackpos - 1].intValue() << opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LSHL:
					--stackpos;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() << opstack[stackpos].intValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) ISHR:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue() >> opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LSHR:
					--stackpos;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() >> opstack[stackpos].intValue();
					opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) IUSHR:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue() >>> opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LUSHR:
					--stackpos;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() >>> opstack[stackpos].intValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) IAND:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos].intValue() & opstack[stackpos - 1].intValue();
					pc += 1;
					continue;
				case (short) LAND:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() & opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) IOR:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue() | opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LOR:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() | opstack[stackpos].longValue();
					//opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) IXOR:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue() ^ opstack[stackpos].intValue();
					pc += 1;
					continue;
				case (short) LXOR:
					stackpos -= 2;
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue() ^ opstack[stackpos].longValue();
					opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) IINC:
					i1 = code[pc + 1];
					lv[i1] = (int) lv[i1] + (byte) code[pc + 2];
					pc += 3;
					continue;
				case (short) I2L:
					opstack[stackpos - 1] = opstack[stackpos - 1].longValue();
					opstack[stackpos] = null;
					++stackpos;
					pc += 1;
					continue;
				case (short) I2F:
					opstack[stackpos - 1] = (float) (int) opstack[stackpos - 1];
					pc += 1;
					continue;
				case (short) I2D:
					opstack[stackpos - 1] = opstack[stackpos - 1].doubleValue();
					opstack[stackpos] = null;
					++stackpos;
					pc += 1;
					continue;
				case (short) L2I:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue();
					pc += 1;
					continue;
				case (short) L2F:
					--stackpos;
					opstack[stackpos - 1] = opstack[stackpos - 1].floatValue();
					pc += 1;
					continue;
				case (short) L2D:
					opstack[stackpos - 2] = opstack[stackpos - 2].doubleValue();
					opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) F2I:
					opstack[stackpos - 1] = opstack[stackpos - 1].intValue();
					pc += 1;
					continue;
				case (short) F2L:
					opstack[stackpos - 1] = opstack[stackpos - 1].longValue();
					opstack[stackpos] = null;
					++stackpos;
					pc += 1;
					continue;
				case (short) F2D:
					f1 = (float) opstack[--stackpos];
					d1 = f1;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (short) D2I:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					opstack[stackpos++] = (int) d1;
					pc += 1;
					continue;
				case (short) D2L:
					opstack[stackpos - 2] = opstack[stackpos - 2].longValue();
					opstack[stackpos - 1] = null;
					pc += 1;
					continue;
				case (short) D2F:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					opstack[stackpos++] = (float) d1;
					pc += 1;
					continue;
				case (short) I2B:
					opstack[stackpos - 1] = (int) opstack[stackpos - 1].byteValue();
					pc += 1;
					continue;
				case (short) I2C:
					opstack[stackpos - 1] = Integer.valueOf((char)opstack[stackpos - 1].intValue());
					pc += 1;
					continue;
				case (short) I2S:
					opstack[stackpos - 1] = Integer.valueOf(opstack[stackpos - 1].shortValue());
					pc += 1;
					continue;
				case (short) LCMP:
					l1 = (long) opstack[stackpos - 2];
					l2 = (long) opstack[stackpos - 4];
					if(l2 == l1){
						opstack[stackpos - 4] = iconst_0;
					}else if(l2 > l1){
						opstack[stackpos - 4] = iconst_1;
					}else
						opstack[stackpos - 4] = iconst_m1;
					stackpos -= 3;
					pc += 1;
					continue;
				case (short) FCMPL:
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					if(f2 == f1){
						opstack[stackpos++] = iconst_0;
					}else if(f2 > f1){
						opstack[stackpos++] = iconst_1;
					}else if(f2 < f1)
						opstack[stackpos++] = iconst_m1;
					else
						opstack[stackpos++] = iconst_m1;
					pc += 1;
					continue;
				case (short) FCMPG:
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					if(f2 == f1){
						opstack[stackpos++] = iconst_0;
					}else if(f2 > f1){
						opstack[stackpos++] = iconst_1;
					}else if(f2 < f1)
						opstack[stackpos++] = iconst_m1;
					else
						opstack[stackpos++] = iconst_1;
					pc += 1;
					continue;
				case (short) DCMPL:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					if(d2 == d1){
						opstack[stackpos++] = iconst_0;
					}else if(d2 > d1){
						opstack[stackpos++] = iconst_1;
					}else if(d2 < d1)
						opstack[stackpos++] = iconst_m1;
					else
						opstack[stackpos++] = iconst_m1;
					pc += 1;
					continue;
				case (short) DCMPG:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					--stackpos;
					d2 = (double) opstack[--stackpos];
					if(d2 == d1){
						opstack[stackpos++] = iconst_0;
					}else if(d2 > d1){
						opstack[stackpos++] = iconst_1;
					}else if(d2 < d1)
						opstack[stackpos++] = iconst_m1;
					else
						opstack[stackpos++] = iconst_1;
					pc += 1;
					continue;
				case (short) IFEQ:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFEQ;
					code[pc + 1] = s;
					continue;
				case (short) IFNE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFNE;
					code[pc + 1] = s;
					continue;
				case (short) IFLT:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFLT;
					code[pc + 1] = s;
					continue;
				case (short) IFGE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFGE;
					code[pc + 1] = s;
					continue;
				case (short) IFGT:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFGT;
					code[pc + 1] = s;
					continue;
				case (short) IFLE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFLE;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPEQ:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPEQ;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPNE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPNE;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPLT:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPLT;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPGE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPGE;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPGT:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPGT;
					code[pc + 1] = s;
					continue;
				case (short) IF_ICMPLE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ICMPLE;
					code[pc + 1] = s;
					continue;
				case (short) IF_ACMPEQ:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ACMPEQ;
					code[pc + 1] = s;
					continue;
				case (short) IF_ACMPNE:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IF_ACMPNE;
					code[pc + 1] = s;
					continue;
				case (short) GOTO:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _GOTO;
					code[pc + 1] = s;
					continue;
				case (short) JSR:
					opstack[stackpos++] = pc + 3;
					//pc += i(code[pc + 1], code[pc + 2]);
					continue;
				case (short) RET:
					pc = (int) opstack[--stackpos];
					continue;
				case (short) TABLESWITCH:
					if(optimizeTableSwitch(code,pc))
						code[pc]=_TABLESWITCH;
					else
						code[pc]=TABLESWITCHSLOW;
					continue;
				case (short) LOOKUPSWITCH:
					i1 = (int) opstack[--stackpos];
					int pc2 = (pc + 4) & 0xfffffffc;
					int default_offset = ((code[pc2]) << 24) | ((code[++pc2]) << 16) |
							((code[++pc2]) << 8) | (code[++pc2]);//i(code[pc2], code[++pc2], code[++pc2], code[++pc2]);
					i2 = ((code[++pc2]) << 24) | ((code[++pc2]) << 16) | ((code[++pc2]) << 8) | (code[++pc2]);
					for(i3 = 0; i3 < i2; ++i3){
						int key = ((code[++pc2]) << 24) | ((code[++pc2]) << 16) | ((code[++pc2]) << 8) | (code[++pc2]);

						if(key == i1){
							int value = ((code[++pc2]) << 24) | ((code[++pc2]) << 16) | ((code[++pc2]) << 8) | (code[++pc2]);
							pc += value;
							continue mainLoop;
						}else
							pc2 += 4;
					}
					pc += default_offset;
					continue;
				case (short) IRETURN:
					o1 = opstack[--stackpos];
					//pc += 1;
					cF = stack.popAndPeek();
					opstack = cF.stack;
					stackpos = cF.stackpos;

					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack[stackpos++] = o1;
					code = cF.code;
					continue;
				case (short) LRETURN:
					--stackpos;
					l1 = (long) opstack[--stackpos];
					//pc += 1;
					cF = stack.popAndPeek();
					opstack = cF.stack;
					stackpos = cF.stackpos;
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;
					code = cF.code;
					continue;
				case (short) FRETURN:
					o1 = opstack[--stackpos];
					//pc += 1;
					cF = stack.popAndPeek();
					opstack = cF.stack;
					stackpos = cF.stackpos;
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack[stackpos++] = o1;
					code = cF.code;
					continue;
				case (short) DRETURN:
					--stackpos;
					d1 = (double) opstack[--stackpos];
					pc += 1;
					cF = stack.popAndPeek();
					opstack = cF.stack;
					stackpos = cF.stackpos;
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack[stackpos++] = d1;
					opstack[stackpos++] = null;
					code = cF.code;
					continue;
				case (short) ARETURN:
					o1 = opstack[--stackpos];
					pc += 1;
					cF = stack.popAndPeek();
					if(cF==null)
						return o1;
					opstack = cF.stack;
					stackpos = cF.stackpos;
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack[stackpos++] = o1;
					code = cF.code;
					continue;
				case (short) RETURN:
					cF = stack.popAndPeek();
					if(cF==null)
						return null;
					opstack = cF.stack;
					stackpos = cF.stackpos;
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					code = cF.code;
					continue;
				case (short) GETSTATIC:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					FieldHead fh= (FieldHead) c0.data;
					cl = classLoader.loadClass(c0.type);
					if(!cl.isInitialized)
						JVM.clinit(cl, $this.thread);
					AField f= cl.fields.get(c0.str);
					if(f.isObject()){
						code[pc]=_GETSTATICREF;
						code[pc+1]= (short) f.index;
						continue;
					}
					else{
						/*
						ValueHolder vh = c0.vh;
						if(vh == null){
							vh = cl.statics.get(c0.str);
							c0.vh = vh;
						}
						 */
						if(fh.wide)
							code[pc] = GETSTATIC64;
						else
							code[pc] = _GETSTATIC;
						code[pc + 1] = (short) f.index;
					}
					continue;
				case (short) PUTSTATIC:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					fh= (FieldHead) c0.data;
					cl = classLoader.loadClass(c0.type);
					if(!cl.isInitialized)
						JVM.clinit(cl, $this.thread);
					f= cl.fields.get(c0.str);
					if(f.isObject()){
						code[pc]=_PUTSTATICREF;
						code[pc+1]= (short) f.index;
						continue;
					}
					else{
						/*
						ValueHolder vh = c0.vh;
						if(vh == null){
							vh = ClassLoader.getClass(c0.type, true, $this.thread.monitor).statics.get(c0.str);
							c0.vh = vh;
						}*/
						if(fh.wide)
							code[pc] = PUTSTATIC64;
						else
							code[pc] = _PUTSTATIC;
						code[pc + 1] = (short) f.index;
					}
					continue;
				case (short) _GETSTATICREF:
					opstack[stackpos++] = Heap.static_refs[code[pc+1]];
					pc += 3;
					continue;
				case (short) _PUTSTATICREF:
					Heap.static_refs[code[pc+1]]=opstack[--stackpos];
					pc += 3;
					continue;
				case (short) GETFIELD:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					fh= (FieldHead) c0.data;
					f=classLoader.loadClass(c0.type).getInstanceRefs().get(c0.str);
					if(f!=null)
						if(f.isObject()){
							code[pc] = GETREFFIELD;
							code[pc+1]= (short) f.index;
							continue;
						}
					f=classLoader.loadClass(c0.type).getInstanceFields().get(c0.str);
					if(fh.wide)
						code[pc] = GETFIELD64;
					else
						code[pc] = _GETFIELD;
					//code[pc + 1] = (short) i1;
					code[pc + 1] = (short)f.index;
					continue;
				case (short) PUTFIELD:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					fh= (FieldHead) c0.data;
					f=classLoader.loadClass(c0.type).getInstanceRefs().get(c0.str);
					if(f!=null)
						if(f.isObject()){
							code[pc] = PUTREFFIELD;
							code[pc+1]= (short) f.index;
							continue;
						}
					f=classLoader.loadClass(c0.type).getInstanceFields().get(c0.str);
					if(fh.wide)
						code[pc] = PUTFIELD64;
					else
						code[pc] = _PUTFIELD;
					//code[pc + 1] = (short) i1;

					code[pc + 1] = (short)f.index;
					//System.out.println(f.index);
					continue;
				case (short) INVOKEVIRTUAL:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					m = c0.m;
					if(m == null){
						m = (Method) cC.getMethod(i1);
						if(m==null){
							if(c0.str.startsWith("invoke")){
								cF.stackpos=stackpos;
								m=resolveMethodHandle(constantPool,i1,cF);
							}
							else{
								MethodHead mh=cC.getMethodHead(i1);
									System.out.println(mh.signature);
							}
						}
						c0.m = m;
						s = addMethodToCache(m);
						//methodCache = RawShortCodeExecutor.methodCache;
						c0.index1 = s;
						code[pc + 1] = s;
					}else
						code[pc + 1] = (short) c0.index1;
					if(m.isFinal()){
						if(m.isNative){
							handler = MyThread.jni.get(m.nativeSignature);
							if(handler==null){
								cF.pc=pc;
								cF=$this.throwNewException(env, "java/lang/UnsatisfiedLinkError",  "static method not found: " + m.signature);
								if(cF==null)
									return 1;
								opstack = cF.stack;
								stackpos = 1;
								pc = cF.pc;
								code = cF.code;
								lv = cF.lv;
								cC = cF.clazz;
								constantPool = cC.constantPool;
								continue;
							}
							code[pc] = INVOKESPECIALNATIVE;
							s= (short) handler.getID();
							if(s==-1)
								code[pc+1]=addNativeToCache(handler);
							else code[pc+1]=s;
						}
						else
							code[pc] = _INVOKESPECIAL;
					}else
						code[pc] = _INVOKEVIRTUAL;
					continue;
				case (short) INVOKESPECIAL:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					m = c0.m;
					if(m == null){
						m = (Method) cC.getMethod(i1);
						c0.m = m;
						s = addMethodToCache(m);
						//methodCache = RawShortCodeExecutor.methodCache;
						c0.index1 = s;
						code[pc + 1] = s;
					}else
						code[pc + 1] = (short) c0.index1;
					if(m.isNative){
						handler = MyThread.jni.get(m.nativeSignature);
						if(handler==null){
							cF.pc=pc;
							cF=$this.throwNewException(env,"java/lang/UnsatisfiedLinkError", "static method not found: " + m.signature);
							if(cF==null)
								return 1;
							opstack = cF.stack;
							stackpos = 1;
							pc = cF.pc;
							code = cF.code;
							lv = cF.lv;
							cC = cF.clazz;
							constantPool = cC.constantPool;
							continue;
						}
						code[pc] = INVOKESPECIALNATIVE;
						s= (short) handler.getID();
						if(s==-1)
							code[pc+1]=addNativeToCache(handler);
						else
							code[pc+1]=s;
					}
					else{
						if(m.code.length==1)
							code[pc] = SKIP_INVOKESPECIAL;
						else
							code[pc] = _INVOKESPECIAL;
					}
					continue;
				case (short) INVOKESTATIC:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					m = c0.m;
					if(m == null){
						m = (Method) cC.getStaticMethod(i1);
						c0.m = m;
					}
					if(m.isNative){
						handler = MyThread.jni.get(m.nativeSignature);
						if(handler==null){
							cF.pc=pc;
							cF=$this.throwNewException(env,"java/lang/UnsatisfiedLinkError", "static method not found: " + m.signature);
							if(cF==null)
								return 1;
							opstack = cF.stack;
							stackpos = 1;
							pc = cF.pc;
							code = cF.code;
							lv = cF.lv;
							cC = cF.clazz;
							constantPool = cC.constantPool;
							continue;
						}
						code[pc] = INVOKESTATICNATIVE;
						s= (short) handler.getID();
						if(s==-1)
							code[pc+1]=addNativeToCache(handler);
						else
							code[pc+1]=s;
					}
					else {
						code[pc] = _INVOKESTATIC;
						code[pc + 1] = (short) i1;
					}
					continue;
				case (short) INVOKEINTERFACE:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[i1];
					m = c0.m;
					if(m == null){
						m = (Method) cC.getMethod(i1);
						c0.m = m;
					}
					code[pc] = _INVOKEINTERFACE;
					code[pc + 1] = (short) i1;
					continue;
				case (short) INVOKEDYNAMIC:
					i1 = (((code[pc + 1]) << 8) | (code[pc + 2]));
					pc += 5;
					cF.pc = pc;
					c0 = constantPool[i1];
					BootstrapMethod mh = cC.getMethodHandle(c0.index1);
					switch(mh.referenceKind){
						case BootstrapMethod.REF_invokeVirtual:
							break;
						case BootstrapMethod.REF_invokeStatic:
							c0 = constantPool[c0.index2];
							c1 = constantPool[c0.index1];
							Constant c2 = constantPool[c0.index2];
							oref=opstack[stackpos-1];
							c=Heap.get(oref);
							oref = mh.resolve(c1.str);
							Instance instance=Heap.get(oref);
							instance.refs=c.refs;
							instance.fields=c.fields;

							opstack[stackpos-1] = oref;
							break;
						default:
							throw new RuntimeException("referenceKind not supported");
					}
					/*
					AClass clazz = Class.getClass(mh.mclazz);
					Method method = (Method)clazz.getMethod(mh.mname + mh.mtype);
					fc=cC.getConstant(fc.index2);
					fname=cC.getConstant(fc.index1);
					ftype=cC.getConstant(fc.index2);
					System.out.println(fname.str+ftype.str);
					for(Constant arg:mh.arguments){
						switch (arg.getTag()){
							case Constant.CMethodType:
								Constant mt = cC.getConstant(arg.index1);
								break;
							case Constant.CMethodHandle:
								Constant mh1 = cC.getConstant(arg.index1);
								Constant methodref = cC.getConstant(arg.index2);
								Constant mref1 = cC.getConstant(methodref.index1);
								Constant mclazz= cC.getConstant(mref1.index1);
								Constant mref2 = cC.getConstant(methodref.index2);
								Constant fname1=cC.getConstant(mref2.index1);
								Constant ftype1=cC.getConstant(mref2.index2);
								clazz=Class.getClass(mclazz.str);
								method= (Method) clazz.getMethod(fname1.str+ftype1.str);
								DClass dclazz = new DClass("R");
								dclazz.addMethod("run()V",method);
								ci=new ClassInstance(dclazz);
								oref=Heap.add(ci);
								opstack[stackpos++]=oref);
								continue mainLoop;
						}
						System.out.println(arg);
					}
					 */
					//throw new RuntimeException("invokedynamic not implemented");
					continue;
				case (short) NEW:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _NEW;
					code[pc + 1] = s;
					c0 = constantPool[s];
					cl = c0.clazz;
					if(cl == null){
						cl = cC.getClass(s);
						if(!cl.isInitialized)
							JVM.clinit(cl, $this.thread.monitor);
						c0.clazz = cl;
					}
					continue;
				case (short) NEWARRAY:
					i1 = (int) opstack[--stackpos];
					switch((byte) code[pc + 1]){
						case ArrayInstance.T_BOOLEAN:
							array = new ByteArrayInstance(i1);
							break;
						case ArrayInstance.T_CHAR:
							array = new CharArrayInstance(i1);
							break;
						case ArrayInstance.T_FLOAT:
							array = new FloatArrayInstance(i1);
							break;
						case ArrayInstance.T_DOUBLE:
							array = new DoubleArrayInstance(i1);
							break;
						case ArrayInstance.T_BYTE:
							array = new ByteArrayInstance(i1);
							break;
						case ArrayInstance.T_SHORT:
							array = new ShortArrayInstance(i1);
							break;
						case ArrayInstance.T_INT:
							array = new IntArrayInstance(i1);
							break;
						case ArrayInstance.T_LONG:
							array = new LongArrayInstance(i1);
							break;
						default:
							throw new RuntimeException("unknown array type");
					}
					opstack[stackpos++] = Heap.gcadd(array);
					pc += 2;
					continue;
				case (short) ANEWARRAY:
					i1 = (int) opstack[--stackpos];
					oarray = new ObjectArrayInstance(i1);
					opstack[stackpos++] = Heap.gcadd(oarray);
					pc += 3;
					continue;
				case (short) ARRAYLENGTH:
					array = (ArrayInstance) Heap.get(opstack[--stackpos]);
					opstack[stackpos++] = array.size;
					pc += 1;
					continue;
				case (short) ATHROW:
					oref = opstack[--stackpos];
					cF.pc=pc;
					cF=$this.throwException(oref);
					if(cF==null)
						return 1;
					pc = cF.pc;
					cC = cF.clazz;
					constantPool = cC.constantPool;
					opstack = cF.stack;
					stackpos = 1;
					code = cF.code;
					lv = cF.lv;
					continue;
				case (short) CHECKCAST:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[s];
					AClass cl2 = c0.clazz;
					if(cl2 == null){
						c1 = constantPool[c0.index1];
						String classname = c1.str;
						if(classname.startsWith("[")){
							pc += 3;
							continue;
						}
						cl2 = classLoader.loadClass(classname);
						c0.clazz = cl2;
					}
					code[pc] = _CHECKCAST;
					code[pc + 1] = s;
					continue;
				case (short) INSTANCEOF:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					c0 = constantPool[s];
					cl2 = c0.clazz;
					if(cl2 == null){
						c1 = constantPool[c0.index1];
						String classname = c1.str;
						if(classname.startsWith("[")){
							pc += 3;
							continue;
						}
						cl2 = classLoader.loadClass(classname);
						c0.clazz = cl2;
					}
					code[pc] = _INSTANCEOF;
					code[pc + 1] = s;
					continue;
				case (short) MONITORENTER:
					oref = opstack[--stackpos];
					c = Heap.get(oref);
					
					if(c.lock==null)
						c.lock=new ReentrantLock();
					c.lock.lock();
					pc += 1;
					continue;
				case (short) MONITOREXIT:
					oref = opstack[--stackpos];
					c = Heap.get(oref);
					c.lock.unlock();
					pc += 1;
					continue;
				case (short) WIDE:
					pc += 1;
					switch(code[pc]){
						case (short) ILOAD:
							opstack[stackpos++] = lv[((code[pc + 1]) << 8) | (code[pc + 2])];
							pc += 3;
							continue;
						case (short) LLOAD:
							i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
							opstack[stackpos++] = lv[i1 + 1];
							opstack[stackpos++] = lv[i1];
							pc += 3;
							continue;
						case (short) FLOAD:
							opstack[stackpos++] = lv[((code[pc + 1]) << 8) | (code[pc + 2])];
							pc += 3;
							continue;
						case (short) DLOAD:
							i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
							opstack[stackpos++] = lv[i1 + 1];
							opstack[stackpos++] = lv[i1];
							pc += 3;
							continue;
						case (short) ALOAD:
							opstack[stackpos++] = lv[((code[pc + 1]) << 8) | (code[pc + 2])];
							pc += 3;
							continue;
						case (short) ISTORE:
							lv[((code[pc + 1]) << 8) | (code[pc + 2])] = opstack[--stackpos];
							pc += 3;
							continue;
						case (short) LSTORE:
							i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
							lv[i1] = opstack[--stackpos];
							lv[i1 + 1] = opstack[--stackpos];
							pc += 3;
							continue;
						case (short) FSTORE:
							lv[((code[pc + 1]) << 8) | (code[pc + 2])] = opstack[--stackpos];
							pc += 3;
							continue;
						case (short) DSTORE:
							i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
							lv[i1] = opstack[--stackpos];
							lv[i1 + 1] = opstack[--stackpos];
							pc += 3;
							continue;
						case (short) ASTORE:
							oref = opstack[--stackpos];
							lv[((code[pc + 1]) << 8) | (code[pc + 2])] = oref;
							cF.lvpt[((code[pc + 1]) << 8) | (code[pc + 2])] = oref;
							pc += 3;
							continue;
						case (short) IINC:
							i1 = (int) lv[((code[pc + 1]) << 8) | (code[pc + 2])];
							lv[(int) code[pc + 1]] = i1 + ((code[pc + 1]) << 8) | (code[pc + 2]);
							pc += 5;
							continue;

					}
					throw new RuntimeException("wide not implememted");
					//continue;
				case (short) MULTIANEWARRAY:
					i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
					c0 = constantPool[i1];
					c0 = constantPool[c0.index1];
					//dimensions
					i2 = code[pc + 3];
					int[] sizes = new int[i2];
					for(i3 = 0; i3 < i2; i3++){
						sizes[i3] = (int) opstack[--stackpos];
					}
					opstack[stackpos++] = Heap.gcadd(ArrayInstance.create(i2, i2 - 1, sizes, c0.str));
					pc += 4;
					continue;
				case (short) IFNULL:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFNULL;
					code[pc + 1] = s;
					continue;
				case IFNONNULL:
					s = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
					code[pc] = _IFNONNULL;
					code[pc + 1] = s;
					continue;
				case (short) GOTO_W:
					pc += ((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
							((code[pc + 3]) << 8) | (code[pc + 4]);
					continue;
				case (short) JSR_W:
					opstack[stackpos++] = pc + 5;
					pc += ((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
							((code[pc + 3]) << 8) | (code[pc + 4]);
					continue;
				case (short) _IFEQ:
					if(opstack[--stackpos] == iconst_0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFNE:
					if(opstack[--stackpos] != iconst_0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFLT:
					if((int) opstack[--stackpos] < 0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFGE:
					if((int) opstack[--stackpos] >= 0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFGT:
					if((int) opstack[--stackpos] > 0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFLE:
					if((int) opstack[--stackpos] <= 0)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPEQ:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] == (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPNE:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] != (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPLT:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] > (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPGE:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] <= (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPGT:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] < (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ICMPLE:
					stackpos -= 2;
					if((int) opstack[stackpos + 1] >= (int) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ACMPEQ:
					stackpos -= 2;
					if((long) opstack[stackpos + 1] == (long) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IF_ACMPNE:
					stackpos -= 2;
					if((long) opstack[stackpos + 1] != (long) opstack[stackpos])
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _GOTO:
					pc += code[pc + 1];
					continue;
				case (short) _BIPUSH:
					opstack[stackpos++] = constantCache[code[pc + 1]];
					pc += 2;
					continue;
				case (short) _SIPUSH:
					opstack[stackpos++] = constantCache[code[pc + 1]];
					pc += 3;
					continue;
				case (short) _LDC:
					opstack[stackpos++] = constantCache[code[pc + 1]];
					//opstack[stackpos++]=constantPool[code[pc + 1]].value;
					pc += 2;
					continue;
				case (short) _LDC_W:
					opstack[stackpos++] = constantPool[code[pc + 1]].value;
					pc += 3;
					continue;
				case _TABLESWITCH:
					pc2 = (pc + 4) & 0xfffffffc;
					int low=code[pc2+4];
					i1 = (int) opstack[--stackpos];
					if(i1 < low || i1 > code[pc2+8]){  // if its less than <low> or greater than <high>,
						// branch to default
						pc += code[pc2];
					}else{
						pc+=code[pc2 + ((i1 - low) * 4 + 12)];
					}
					continue;
				case TABLESWITCHSLOW:
					i1 = (int) opstack[--stackpos];
					pc2 = (pc + 4) & 0xfffffffc;
					int dfp = pc2;
					pc2 += 4;
					low = ((code[pc2]) << 24) | ((code[pc2 + 1]) << 16) | ((code[pc2 + 2]) << 8) | (code[pc2 + 3]);
					pc2 += 4;
					int high = ((code[pc2]) << 24) | ((code[pc2 + 1]) << 16) | ((code[pc2 + 2]) << 8) | (code[pc2 + 3]);
					pc2 += 4;
					//++pc2;
					if(i1 < low || i1 > high){  // if its less than <low> or greater than <high>,
						// branch to default
						pc += ((code[dfp]) << 24) | ((code[dfp + 1]) << 16) | ((code[dfp + 2]) << 8) | (code[dfp + 3]);
					}else{
						pc2 += (i1 - low) * 4;
						pc+=((code[pc2]) << 24) | ((code[pc2 + 1]) << 16) | ((code[pc2 + 2]) << 8) | (code[pc2 + 3]);
					}
					continue;
				case (short) _GETSTATIC:
					opstack[stackpos++] = Heap.static_fields[code[pc+1]];
					/*
					c0 = constantPool[code[pc + 1]];
					o1 = c0.vh.value;
					if(o1 == null)
						opstack[stackpos++] = iconst_0;
					else
						opstack[stackpos++] = o1;
					if(c0.wide){
						opstack[stackpos++] = iconst_0;
					}
					 */
					pc += 3;
					continue;
				case (short) _PUTSTATIC:
					Heap.static_fields[code[pc+1]]=opstack[--stackpos];
					/*
					c0 = constantPool[code[pc + 1]];
					o1 = opstack[--stackpos];
					if(c0.wide){
						o1 = opstack[--stackpos];
					}
					c0.vh.value = o1;
					 */
					pc += 3;
					continue;
				case (short) GETSTATIC64:
					opstack[stackpos] = Heap.static_fields[code[pc+1]];
					stackpos+=2;
					/*
					c0 = constantPool[code[pc + 1]];
					o1 = c0.vh.value;
					if(o1 == null)
						opstack[stackpos++] = iconst_0;
					else
						opstack[stackpos++] = o1;
					if(c0.wide){
						opstack[stackpos++] = iconst_0;
					}
					 */
					pc += 3;
					continue;
				case (short) PUTSTATIC64:
					stackpos-=2;
					Heap.static_fields[code[pc+1]]=opstack[stackpos];
					/*
					c0 = constantPool[code[pc + 1]];
					o1 = opstack[--stackpos];
					if(c0.wide){
						o1 = opstack[--stackpos];
					}
					c0.vh.value = o1;
					 */
					pc += 3;
					continue;
				case (short) GETREFFIELD:
					oref = opstack[stackpos - 1];
					if(oref == aconst_null){
						//JVM.log("field " + c0.str + " reference: " + oref);

						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "getfield: ");
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);
					o1 = c.refs[code[pc+1]];
					if(o1 == null) o1 = aconst_null;
					opstack[stackpos - 1] = o1;
					pc += 3;
					continue;
				case (short) PUTREFFIELD:
					stackpos-=2;
					o1 = opstack[stackpos+1];
					oref = opstack[stackpos];
					if(oref == aconst_null){
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "putfield: ");
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);
					c.refs[code[pc+1]]= o1;
					pc += 3;
					continue;
				case (short) _GETFIELD:
					//c0 = constantPool[code[pc + 1]];

					oref = opstack[stackpos - 1];
					if(oref == aconst_null){
						//JVM.log("field " + c0.str + " reference: " + oref);

						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "getfield: ");
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);
					o1 = c.fields[code[pc+1]];
					if(o1 == null) o1 = aconst_null;
					opstack[stackpos - 1] = o1;

					pc += 3;
					continue;
				case (short) _PUTFIELD:
					//c0 = constantPool[code[pc + 1]];
					stackpos-=2;
					o1 = opstack[stackpos+1];
					oref = opstack[stackpos];
					if(oref == aconst_null){
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "putfield: ");
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);

					c.fields[code[pc+1]]= o1;
					pc += 3;
					continue;
				case (short) GETFIELD64:
					c0 = constantPool[code[pc + 1]];

					oref = opstack[--stackpos];
					if(oref == aconst_null){
						//JVM.log("field " + c0.str + " reference: " + oref);
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "getfield: ");
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);
					o1 = c.fields[code[pc+1]];
					//o1 = c.fields.get(c0.str);
					if(o1 == null) o1 = iconst_0;
					opstack[stackpos++] = o1;
					opstack[stackpos++] = null;
					pc += 3;
					continue;
				case (short) PUTFIELD64:
					//c0 = constantPool[code[pc + 1]];
					stackpos -= 2;
					o1 = opstack[stackpos];
					oref = opstack[--stackpos];
					if(oref == aconst_null){
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException",  "putfield: " );
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					c = Heap.get(oref);

					//c.fields.put(c0.str, o1);
					c.fields[code[pc+1]]= o1;
					pc += 3;
					continue;
				case (short) _INVOKESPECIAL:
					m = methodCache[code[pc + 1]];
					pc += 3;
					cF.pc = pc;
					cF.stackpos = stackpos;
					Frame nf = cF.newRefFrame(m);
					//Heap.objectCreated(nf.lv[0]);
					cF = nf;
					pc = 0;
					lv = cF.lv;
					cC = m.clazz;
					constantPool = cC.constantPool;
					stack.push(cF);
					opstack = cF.stack;
					stackpos = cF.stackpos;
					code = cF.code;
					continue;
				case (short) INVOKESPECIALNATIVE:
					handler = nativesCache[code[pc + 1]];
					pc += 3;

					i1 = handler.argsLength+1;
					mlv=new Number[i1];
					stackpos-=i1;
					cF.stackpos = stackpos;
					for (i2 = 0; i2 < i1; ++i2) {
						mlv[i2]=opstack[stackpos+i2];
					}
					handler.invoke(cF, mlv, $this.env);
					stackpos = cF.stackpos;


					/*
					cF.stackpos = stackpos;
					nf = cF.newRefFrame(m);
					m.nativeHandler.invoke(cF, nf.lv, $this.thread);
					stackpos = cF.stackpos;
					*/
					continue;
				case SKIP_INVOKESPECIAL:
					--stackpos;
					pc += 3;
					continue;
				case (short) _INVOKESTATIC:
					m = constantPool[code[pc + 1]].m;
					pc += 3;
					cF.pc = pc;
					cF.stackpos = stackpos;
					nf = cF.newFrame(m);
					cF = nf;
					stack.push(cF);
					opstack = cF.stack;
					stackpos = 0;
					cC = m.clazz;
					constantPool = cC.constantPool;
					code = cF.code;
					pc = 0;
					lv = cF.lv;
					continue;
				case (short) INVOKESTATICNATIVE:
					//c0 = constantPool[code[pc + 1]];
					//m = c0.m;
					handler=nativesCache[code[pc + 1]];
					pc += 3;
					/*
					cF.stackpos = stackpos;
					nf = cF.newFrame(m);
					m.nativeHandler.invoke(cF, nf.lv, $this.thread);
					//*/
					///*
					i1 = handler.argsLength;
					mlv=new Number[i1];
					stackpos-=i1;
					cF.stackpos = stackpos;
					for (i2 = 0; i2 < i1; ++i2) {
						mlv[i2]=opstack[stackpos+i2];
					}
					handler.invoke(cF, mlv, $this.env);
					//*/
					stackpos = cF.stackpos;
					continue;
				case (short) _INVOKEINTERFACE:
					c0 = constantPool[code[pc + 1]];
					m = c0.m;
					short count = code[pc + 3];
					//short zero = code[pc + 4];
					pc += 5;
					cF.pc = pc;
					oref = opstack[stackpos-count];

					if(oref == aconst_null){
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "invokeinterface: " + m.signature);
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}
					AClass mc = Heap.get(oref).clazz;
					m = (Method) mc.getMethod(m.signature);
					nf = new Frame(m);


					if(m.isNative){
						//m.invoke(cF.stack, this);
						throw new RuntimeException("check.that");
						//continue;
					}
					nf.lv = new Number[m.max_locals];
					nf.lvpt = new Number[m.max_locals];

					char[] args = m.args;
					//int l = mlv.length;
					nf.lv[0]=oref;
					nf.lvpt[0]=oref;
					stackpos-=count;
					for(int ix = 1; ix < count; ++ix){
						o1 = opstack[stackpos+ix];
						char arg=args[ix-1];
						if(arg == 'L'||arg=='[')
							nf.lvpt[ix] = o1;
						nf.lv[ix] = o1;
					}
					/*
					for(int j = 0; j < l; ++j){
						if(j > 0)
							if(args[j - 1] == 'L')
								nf.lvpt[j] = mlv[j];
						nf.lv[j] = mlv[j];
					}*/

					cF.stackpos = stackpos;
					cF = nf;
					pc = cF.pc;
					lv = cF.lv;

					cC = m.clazz;
					constantPool = cC.constantPool;
					stack.push(cF);
					opstack = cF.stack;
					stackpos = cF.stackpos;
					code = cF.code;
					continue;
				case (short) _INVOKEVIRTUAL:
					//c0=constantPool[code[pc+1]];
					m = methodCache[code[pc + 1]];
					pc += 3;
					cF.pc = pc;
					cF.stackpos = stackpos;
					oref = opstack[stackpos - m.argsLength - 1];
					if(oref == aconst_null){
						cF=$this.throwNewException(env,"java/lang/NullPointerException", "invokevirtual: " + m.signature);
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
					}

					cl = Heap.get(oref).clazz;
					String mname = m.signature;
					m = (Method) cl.getMethod(mname);
					if(m == null)
						throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
					if(m.isNative){
						handler = MyThread.jni.get(m.nativeSignature);
						if(handler==null){
								cF.pc=pc;
								cF=$this.throwNewException(env,"java/lang/UnsatisfiedLinkError", "static method not found: " + m.clazz.getName()+"."+ m.signature);
								if(cF==null)
									return 1;
								opstack = cF.stack;
								stackpos = 1;
								pc = cF.pc;
								code = cF.code;
								lv = cF.lv;
								cC = cF.clazz;
								constantPool = cC.constantPool;
								continue;
							}
						nf = cF.newRefFrame(m);
						handler.invoke(cF, nf.lv, $this.env);
						stackpos = cF.stackpos;
						continue;
					}
					nf = cF.newRefFrame(m);

					cF = nf;
					pc = cF.pc;
					lv = cF.lv;
					cC = m.clazz;
					constantPool = cC.constantPool;
					stack.push(cF);
					opstack = cF.stack;
					stackpos = cF.stackpos;
					code = cF.code;
					continue;
				case (short) _NEW:
					c = new Instance(constantPool[code[pc + 1]].clazz);
					oref = Heap.gcadd(c);
					opstack[stackpos++] = oref;
					pc += 3;
					continue;
				case (short) _CHECKCAST:
					oref = opstack[stackpos - 1];
					if(oref == aconst_null){
						pc += 3;
						continue;
					}
					cl2 = constantPool[code[pc + 1]].clazz;
					cl = Heap.get(oref).clazz;
					if(cl == cl2){
						pc += 3;
						continue;
					}
					if(!cl.isInstance(cl2)){
						//ClassCastException
						cF.pc=pc;
						cF=$this.throwNewException(env,"java/lang/ClassCastException", cl.getName() + " is not " + cl2.getName());
						if(cF==null)
							return 1;
						opstack = cF.stack;
						stackpos = 1;
						pc = cF.pc;
						code = cF.code;
						lv = cF.lv;
						cC = cF.clazz;
						constantPool = cC.constantPool;
						continue;
						//throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
					}
					pc += 3;
					continue;
				case _INSTANCEOF:
					oref = opstack[--stackpos];
					if(oref==aconst_null){
						opstack[stackpos++] = iconst_0;
						pc += 3;
						continue;
					}
					c0 = constantPool[code[pc + 1]];
					cl2 = c0.clazz;
					cl = Heap.get(oref).clazz;
					if(cl == cl2){
						opstack[stackpos++] = iconst_1;
						pc += 3;
						continue;
					}
					if(cl.isInstance(cl2))
						opstack[stackpos++] = iconst_1;
					else
						opstack[stackpos++] = iconst_0;
					pc += 3;
					continue;
				case (short) _IFNULL:
					if(opstack[--stackpos] == aconst_null)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				case (short) _IFNONNULL:
					if(opstack[--stackpos] != aconst_null)
						pc += code[pc + 1];
					else
						pc += 3;
					continue;
				default:
					pc += 1;
					throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
					// cs+= byteToHex(code[i])+"\n";
			}
		}
	}

	private static Method resolveMethodHandle(Constant[] cpool,int index, Frame cF){
		Constant mref=cpool[index];
		Constant nameandtype = cpool[mref.index2];
		//Constant name= cpool[mref.index1];
		Constant type=cpool[nameandtype.index2];
		MethodType mt=new MethodType(type.str);
		Number oref = cF.stack[cF.stackpos-mt.args.length-1];
		Instance i=Heap.get(oref);
		return ((MethodHandleClass)i.clazz).getMethod();
	}

	@Override
	public final String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(stack.peek().toString());
		return sb.toString();
	}

	public final FrameStack getStack(){
		return stack;
	}

	//CACHES
	static Method[] methodCache = new Method[128];
	static short methodCacheIndex = 0;

	static short addMethodToCache(Method m){
		if(methodCache.length <= methodCacheIndex){
			int newSize = (methodCache.length * 3) / 2 + 1;
			if(JVM.verbose){
				logger.info("increasing methodCache to: " + newSize);
			}
			Method[] old = methodCache;
			methodCache = new Method[newSize];
			System.arraycopy(old, 0, methodCache, 0, old.length);
		}
		methodCache[methodCacheIndex] = m;
		return methodCacheIndex++;
	}

	static JNIMethod[] nativesCache = new JNIMethod[256];
	static short nativesCacheIndex = 0;

	static short addNativeToCache(JNIMethod m){
		if(nativesCache.length <= nativesCacheIndex){
			int newSize = (nativesCache.length * 3) / 2 + 1;
			if(JVM.verbose){
				logger.info("increasing nativesCache to: " + newSize);
			}
			JNIMethod[] old = nativesCache;
			nativesCache = new JNIMethod[newSize];
			System.arraycopy(old, 0, nativesCache, 0, old.length);
		}
		nativesCache[nativesCacheIndex] = m;
		return nativesCacheIndex++;
	}

	static Number[] constantCache = new Number[128];
	static short constantsCacheIndex = 0;

	static short addConstantToCache(Number c){
		if(constantCache.length <= constantsCacheIndex){
			int newSize = (constantCache.length * 3) / 2 + 1;
			if(JVM.verbose){
				logger.info("increasing constantCache to: " + newSize);
			}
			Number[] old = constantCache;
			constantCache = new Number[newSize];
			System.arraycopy(old, 0, constantCache, 0, old.length);
		}
		constantCache[constantsCacheIndex] = c;
		return constantsCacheIndex++;
	}
	private static boolean optimizeTableSwitch(short[] code, int pc){
		short[] backup=new short[code.length];
		System.arraycopy(code,0,backup,0,code.length);

		pc = (pc + 4) & 0xfffffffc;
		int default_offset = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
		if(default_offset<=Short.MIN_VALUE||default_offset>=Short.MAX_VALUE){
			return false;
		}
		code[pc]=(short)default_offset;
		pc += 4;
		int low = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
		if(low<=Short.MIN_VALUE||low>=Short.MAX_VALUE){
			System.arraycopy(backup,0,code,0,code.length);
			return false;
		}
		code[pc]=(short)low;
		pc += 4;
		int high = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
		if(high<=Short.MIN_VALUE||high>=Short.MAX_VALUE){
			System.arraycopy(backup,0,code,0,code.length);
			return false;
		}
		code[pc]=(short)high;
		pc += 4;
		int pc2=pc;

		for(int i=low;i<=high;++i){
			pc=pc2 + (i - low) * 4;
			int jump = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
			if(jump<=Short.MIN_VALUE||jump>=Short.MAX_VALUE){
				System.arraycopy(backup,0,code,0,code.length);
				return false;
			}
			code[pc]=(short)jump;
			//params.add(jump);
		}
		return true;
	}
}
