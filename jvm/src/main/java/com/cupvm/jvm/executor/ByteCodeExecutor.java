package com.cupvm.jvm.executor;
import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.adapter.DebuggerAdapter;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.JString;
import com.cupvm.jvm.method.MException;
import com.cupvm.jni.*;
import com.cupvm.jvm.natives.java.lang.NString;
import com.cupvm.jvm.instance.natives.*;
import java.util.HashMap;
import java.util.Iterator;
import static com.cupvm.jvm.Globals.aconst_null;

public abstract class ByteCodeExecutor implements Runnable{
	private static final Logger logger= ConsoleLogger.get(JVM.logColor);
	//public Frame last_frame;
	public MyThread thread;
	protected final JNIEnv env;
	private DebuggerAdapter da;
	public static JNI jni;
	protected ClassLoader classLoader;

	public ByteCodeExecutor(MyThread thread, JNI jni){
		this.thread=thread;
		classLoader=thread.getClassLoader();
		env=new JNIEnv(classLoader,thread);
		ByteCodeExecutor.jni =jni;
	}
	public final FrameStack stack = new FrameStack();
	public static final int psteps=4096*16;
	public int steps;
	// current class
	public Class cC;

	public static void fillInStackTrace(JNIEnv env, Instance ci, MyThread t) {
		StringBuilder stackTrace=new StringBuilder();
		NativeFrameStack stack = t.getStack();
		Iterator<NativeFrame> it = stack.iterator();
		while (it.hasNext()) {
			stackTrace
					.append(it.next())
					.append("\n");
		}
		Number addr = Heap.add(new JString(stackTrace.toString()));
		ci.putRef("stacktraceAsString",addr);
	}

	public abstract Object step();
	public Object prun() {
		this.steps=psteps;
		return step();
	}

	public Frame throwNewException(JNIEnv env, String clazz, String detailMessage){
		Class cl = (Class)classLoader.loadClass(clazz);
		if(cl.isInitialized)
			JVM.clinit(cl, thread);
		Instance ci=new Instance(cl);
		Number oref=Heap.add(ci);
		ci.putRef("detailMessage",Heap.add(new JString(detailMessage)));
		fillInStackTrace(env,ci,thread);
		return throwException(oref);
	}

	public Frame throwException(Number oref){
		if(oref==aconst_null){
			AClass cl = classLoader.loadClass("java/lang/NullPointerException");
			Instance ci=new Instance(cl);
			oref=Heap.add(ci);
		}
		//thread.printStackTrace();
		Frame cF=stack.peek();
		Instance c = Heap.get(oref);
		while (!stack.isEmpty()) {
			MException e = cF.m.checkException(cF.pc);
			AClass cl;
			boolean _catch=false;

			if(e!=null) {
				if(e.catch_type==0)_catch=true;
				else{
					cl = cC.getClass(e.catch_type);
					if(JVM.verbose)
						logger.info(c.clazz.getName() , " instanceof " , cl.getName());
					_catch = c.clazz.isInstance(cl);
				}
			}
			if (!_catch)
			{
				 stack.pop();
				if (stack.isEmpty()) {
					//thread.destroy();
					printStackTrace(c);
					return null;
					//throw new RuntimeException("HALT");
					/*
					System.out.println("Exception printing still not implemented");
					int outoref= (int) Class.getClass("java/lang/System").getStatic("out");
					AClass tcl = Class.getClass("java/lang/Throwable");
					new MyThread(tcl,"stacktrace_printer").setMethodToRun("printStackTrace(Ljava/io/PrintStream;)V",oref,outoref).run();
					throw new RuntimeException(c.getMyClass().getName());
					 */
				}
				cF = stack.peek();
				cC = cF.clazz;

			} else {
				cF.pc = e.handler_pc;
				cF.clearOperandStack();
				cF.push(oref);
				break;
			}
		}
		return cF;
	}

	private void printStackTrace(Instance c) {
		logger.error(c.clazz.getName());
		Number dmaddr = c.getRef("detailMessage");
		if(dmaddr!=null) {
			JString detailMessage = (JString) Heap.get(dmaddr);
			if(detailMessage!=null)
				logger.error(detailMessage.s);
		}
		String stacktrace= ((JString) Heap.get(c.getRef("stacktraceAsString"))).s;
		logger.error(stacktrace);
		Number causeaddr = c.getRef("cause");
		if(causeaddr!=null){
			c= Heap.get(causeaddr);
					if(c!=null) {
						logger.error("cause:");
						printStackTrace(c);
					}
		}
	}

	public void setDebuggerAdapter(DebuggerAdapter da) {
		this.da = da;
		da.setFrameStack(stack);
	}
	public Number ldc(Constant[] constantPool, int id) {
		Constant c = constantPool[id];
		Number oref;
		switch (c.tag) {
			case Constant.CString:
				oref=c.value;
				if(oref==null) {
					//throw new RuntimeException("look here");
					oref = NString.createInstance(c.str);
					c.value=oref;
				}
				return oref;
			case Constant.CFloat:
			case Constant.CInteger:
				return c.value;
			case Constant.CClass:
				Constant cref = constantPool[c.index1];
				String cname = cref.str;
				if (cname.charAt(0) == '[') {
					oref = classLoader.getArrayClass(cname).getMyInstanceRef();
					return oref;
				}
				AClass clazz=classLoader.loadClass(cref.str);
				oref = clazz.getMyInstanceRef();
				if(oref==null)
					oref=createClassClassInstance(classLoader, clazz);
				return oref;
			default:
				throw new RuntimeException("wrong constant type");
		}
	}

	public static Number createClassClassInstance(ClassLoader classLoader, AClass clazz){
		JClass ci=new JClass(clazz);
		final Number myoref = Heap.add(ci);
		clazz.setMyInstenceRef(myoref);
		Heap.makePermanent(myoref);
		//ClassInstance ci = (ClassInstance) Heap.get(myoref);
		HashMap<String, AField> clazzrefs = ci.clazz
				.getInstanceRefs();
		ci.refs[
			clazzrefs.get("code_source")
			.index]=
			Heap.add(new JString(clazz.getCodePath()));
		ci.refs[clazzrefs
				.get("name")
				.index]=
				Heap.add(new JString(clazz.getName()));
		return myoref;
	}
}
