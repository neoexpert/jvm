package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.arrays.CharArrayInstance;
import com.cupvm.jvm.instance.natives.JString;

import java.util.HashMap;
import java.util.LinkedList;
import static com.cupvm.jvm.Globals.*;

public class NString {
	private static final HashMap<String, Number> strings = new HashMap<>();
	public static Number createInstance(String str) {
		Number addr = strings.get(str);
		if(addr!=null)
			return addr;
		JString jString=new JString(str);
		addr = Heap.add(jString);
		strings.put(str, addr);
		Heap.makePermanent(addr);
		return addr;
	}

	public static LinkedList<JNIMethod> gen(AClass java_lang_String){
		LinkedList<JNIMethod> methods=new LinkedList<>();
		java_lang_String.getDeclaredMethod("<init>([CII)V").declareNative();
		java_lang_String.getDeclaredMethod("intern()Ljava/lang/String;").declareNative();
		java_lang_String.getDeclaredMethod("hashCode()I").declareNative();
		methods.add(new JNIMethod("java/lang/String.<init>([CII)V", 3){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				Number saddr = lv[0];
				Instance ci = Heap.get(saddr);
				CharArrayInstance cai= (CharArrayInstance) Heap.get(lv[1]);
				Heap.set(saddr,new JString(new String(cai.arr,lv[2].intValue(),lv[3].intValue())));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/String.intern()Ljava/lang/String;", 0){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				Number saddr = lv[0];
				JString ci = (JString) Heap.get(saddr);
				String s = ci.s.intern();
				Number addr = strings.get(s);
				if (addr == null) {
					strings.put(s, saddr);
					Heap.makePermanent(saddr);
					cF.push(saddr);
				} else
					cF.push(addr);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/String.hashCode()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.get(lv[0]).hashCode());
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("length()I").declareNative();
		methods.add(new JNIMethod("java/lang/String.length()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString jString = (JString) Heap.get(lv[0]);
				cF.push(jString.s.length());
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("equals(Ljava/lang/Object;)Z").declareNative();
		methods.add(new JNIMethod("java/lang/String.equals(Ljava/lang/Object;)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Number me=lv[0];
				Number other=lv[1];
				if(me.intValue()==other.intValue()){
					cF.push(iconst_1);
					return false;
				}
				JString jString = (JString) Heap.get(me);
				Instance ci= Heap.get(other);
				if(ci instanceof JString)
				{
					JString jString2= (JString) ci;
					cF.push(jString.s.equals(jString2.s)?iconst_1: iconst_0);
				}
				else cF.push(iconst_0);
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("charAt(I)C").declareNative();
		methods.add(new JNIMethod("java/lang/String.charAt(I)C",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString jString = (JString) Heap.get(lv[0]);
				cF.push((int)jString.s.charAt((Integer) lv[1]));
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("replace(CC)Ljava/lang/String;").declareNative();
		methods.add(new JNIMethod("java/lang/String.replace(CC)Ljava/lang/String;",2) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci= (JString) Heap.get(lv[0]);
				cF.push(Heap.add(new JString(ci.s.replace((char) lv[1].intValue(), (char) lv[2].intValue()))));
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("substring(I)Ljava/lang/String;").declareNative();
		methods.add(new JNIMethod("java/lang/String.substring(I)Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci= (JString) Heap.get(lv[0]);
				cF.push(Heap.add(new JString(ci.s.substring((Integer) lv[1]))));
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("indexOf(I)I").declareNative();
		methods.add(new JNIMethod("java/lang/String.indexOf(I)I",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci= (JString) Heap.get(lv[0]);
				cF.push(ci.s.indexOf((Integer) lv[1]));
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("startsWith(Ljava/lang/String;)Z").declareNative();
		methods.add(new JNIMethod("java/lang/String.startsWith(Ljava/lang/String;)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci= (JString) Heap.get(lv[0]);
				JString prefix= (JString) Heap.get(lv[1]);
				cF.push(ci.s.startsWith(prefix.s)?1:0);
				return false;
			}
		});
		java_lang_String.getDeclaredMethod("toCharArray()[C").declareNative();
		methods.add(new JNIMethod("java/lang/String.toCharArray()[C",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci= (JString) Heap.get(lv[0]);
				char[] arr=ci.s.toCharArray();
				CharArrayInstance ai=new CharArrayInstance(arr);
				cF.push(Heap.add(ai));
				return false;
			}
		});
		return methods;
	}



}
