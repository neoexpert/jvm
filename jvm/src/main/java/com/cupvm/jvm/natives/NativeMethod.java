package com.cupvm.jvm.natives;

import com.cupvm.jvm.Frame;
import com.cupvm.jvm.MyThread;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;

public class NativeMethod extends AMethod {

	public NativeMethod(AClass cl, String nativeType) {
		super(nativeType,cl);
		this.nativeSignature =nativeType;
		isNative=true;
	}
	public void invoke(Frame cF, Object[] lv, MyThread t){}

	public void setParamsArrayLength(int argsLength) {
		this.argsLength=argsLength;
	}
}
