package com.cupvm.jvm;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.NativeFrameStack;

import java.util.Iterator;

public class FrameStack implements NativeFrameStack {
	private int pos=1;
	private int capacity=300;
	private Frame[] elements=new Frame[capacity];

	public final Frame get(int position){
		return elements[position];
	}

	public final int size() {
		return pos;
	}

	public final Frame pop(){
		Frame f=elements[--pos];
		elements[pos]=null;
		return f;
	}

	public final Frame popAndPeek(){
		elements[--pos]=null;
		return elements[pos-1];
	}

	public final Frame peek(){
		return elements[pos-1];
	}

	public final void push(Frame f){
		if(pos>=capacity)
			grow();
		elements[pos++]=f;
	}

	private void grow(){
		capacity=(capacity*3)/2+1;
		Frame[] old=elements;
		elements=new Frame[capacity];
		System.arraycopy(old,0,elements,0,pos);
	}

	public final boolean isEmpty() {
		return pos<=1;
	}

	public final Iterator<NativeFrame> iterator() {
		return new StackIterator();
	}

	private final class StackIterator implements Iterator<NativeFrame>
	{
		private final int n;
		public StackIterator(){
			if(pos<elements.length)
				n=pos;
			else
				n=elements.length;
		}

		@Override
		public void remove()
		{
			// TODO: Implement this method
		}

		int ipos=1;
		@Override
		public boolean hasNext() {
			return ipos<n;
		}

		@Override
		public Frame next() {
			try {
				return elements[ipos++];
			}catch (Exception e){
				return null;
			}
		}
	}
}
