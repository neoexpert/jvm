package com.cupvm.jvm;

import com.cupvm.jvm.constants.Constant;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Attribute {
	protected Attribute(Attribute a) {
		this.attribute_length = a.attribute_length;
		this.info = a.info;
		this.attribute_name_index = a.attribute_name_index;
	}

	//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}
	public Attribute(ByteBuffer buf){
		attribute_name_index = buf.getChar();
		attribute_length = buf.getInt();
		info = new byte[attribute_length];
		buf.get(info);
	}

	public final int attribute_name_index;
	public final int attribute_length;
	public byte[] info;

	public void write(DataOutput raf) throws IOException {
		raf.writeChar(attribute_name_index);
		raf.writeInt(info.length);
		raf.write(info);
	}


	public final String getName(Constant[] constantPool) {
		return constantPool[attribute_name_index].str;
	}
}
