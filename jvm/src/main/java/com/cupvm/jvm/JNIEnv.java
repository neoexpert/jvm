package com.cupvm.jvm;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoader;

public final class JNIEnv{
    public JNIEnv(ClassLoader classLoader, MyThread thread){
        this.thread=thread;
        this.classLoader=classLoader;
    }

    public final ClassLoader classLoader;
    private final MyThread thread;

    public MyThread getCurrentThread() {
        return thread;
    }

    public AClass getClass(String name) {
        return classLoader.loadClass(name);
    }
}
