package com.cupvm.jvm;

public interface LineNumberEntry {
    int getPC();
    int getLineNumber();
}
