package com.cupvm.jni;
import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm.JVM;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;

import java.util.*;

public class JNI{
	private static Logger logger= ConsoleLogger.get();
	private final HashMap<String, JNIMethod> methods=new HashMap<>();
	public final void addMethod(JNIMethod method){
		methods.put(method.getNativeDesc(),method);
	}

	public final void addAll(LinkedList<JNIMethod> methods){
		for(JNIMethod m:methods){
			addMethod(m);
		}
	}

	public final JNIMethod get(String signature){
		return methods.get(signature);
	}

	public final boolean invoke(String nativeDesc, NativeFrame cF, Number[] lv, JNIEnv t){
		JNIMethod m = methods.get(nativeDesc);
		if(m==null)
			throw new RuntimeException("method "+nativeDesc+" not found");
		return m.invoke(cF,lv,t);
	}

    public void registerNatives(AClass clazz) {
		for(AMethod m:clazz.getNativeMethods()) {
			final JNIMethod handler = get(m.nativeSignature);
			if(handler==null)
				logger.warn("could not bind native method: "+m.nativeSignature);
			//m.nativeHandler=handler;

		}
    }
}
