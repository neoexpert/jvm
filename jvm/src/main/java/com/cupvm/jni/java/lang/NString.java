package com.cupvm.jni.java.lang;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.*;
import java.util.*;
import static com.cupvm.jvm.Globals.iconst_0;
import static com.cupvm.jvm.Globals.iconst_1;

public class NString {
	private static final HashMap<String, Number> strings = new HashMap<>();
	public static LinkedList<JNIMethod> gen(AClass java_lang_String){
		LinkedList<JNIMethod> methods=new LinkedList<>();
		methods.add(new JNIMethod("java/lang/String.init([C)V",1){
		@Override
		public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
			Number saddr = lv[0];
			Instance ci = Heap.get(saddr);
			return false;
		}
		});
		methods.add(new JNIMethod("java/lang/String.intern()Ljava/lang/String;",0){
		@Override
		public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
			Number saddr = lv[0];
			JString ci = (JString) Heap.get(saddr);
			String s = ci.s;
			Number addr = strings.get(s);
			if (addr == null) {
				strings.put(s, saddr);
				cF.push(saddr);
				Heap.makePermanent(saddr);
			} else
				cF.push(addr);
			return false;
		}
		});
		methods.add(new JNIMethod("java/lang/String.charAt(I)C",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString ci = (JString) Heap.get(lv[0]);
				cF.push(Integer.valueOf(ci.s.charAt(lv[1].intValue())));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/String.startsWith(Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString $this = (JString) Heap.get(lv[0]);
				JString str= (JString) Heap.get(lv[1]);
				if($this.s.startsWith(str.s))
					cF.push(iconst_1);
				else
					cF.push(iconst_0);
				return false;
			}
		});
		return methods;
	}





}
