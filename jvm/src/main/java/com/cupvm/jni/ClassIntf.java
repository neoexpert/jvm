package com.cupvm.jvm;

import java.util.List;

public interface ClassIntf{
	boolean isInterface();
	int getID();
	boolean isInitialized();
	String getName();
    List<? extends MethodIntf> getDeclaredMethods();
	MethodIntf getMethodByID(int methodID);

    String getSourceFileName();
	ClassIntf getSuperClass();
	ClassIntf[] getInterfaces();
	List<? extends FieldIntf> getDeclaredFields();
	MethodIntf getDeclaredMethod(String signature);
}
