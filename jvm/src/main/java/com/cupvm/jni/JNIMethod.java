package com.cupvm.jni;

import com.cupvm.jvm.JNIEnv;

public abstract class JNIMethod{
	private final String nativeDesc;
	public final int argsLength;

	public JNIMethod(String nativeDesc, int argsLength) {
		this.nativeDesc=nativeDesc;
		this.argsLength=argsLength;
	}

	public abstract boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t);

	public final String getNativeDesc(){
		return nativeDesc;
	}
	private int id=-1;
	public void setID(int id){
		this.id=id;
	}

    public int getID(){
		return id;
	}
}
