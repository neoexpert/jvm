package com.cupvm.jni;
import com.cupvm.jni.java.lang.*;
import com.cupvm.jvm._class.SystemClassLoader;

public class InitJNI{
	public JNI init(SystemClassLoader classLoader){
		JNI jni=new JNI();	
		jni.addAll(NString.gen(classLoader.loadClass("java/lang/String")));
		return jni;
	}
}
