package com.cupvm.jni;

public interface ThreadInterface{
    NativeFrameStack getStack();

    int getStackSize();

    NativeFrame getPrevFrame();

    void pushFrame(NativeFrame f, Object m);

    String getName();
	void printStackTrace();
}
