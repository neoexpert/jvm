# Metacircular JVM in pure java

This JVM can run itself. It can also run simple Java programs and some advanced ones.

```
mvn clean install
cd jvm_with_jdk/target
java -jar java.jar -jar java.jar -jar java.jar
```
