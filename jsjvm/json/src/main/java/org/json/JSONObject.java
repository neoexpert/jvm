package org.json;
import java.util.*;

public final class JSONObject{
	static{
		System.loadLibrary("json");
	}
	public JSONObject(){
		init();
	}

	public JSONObject(String json){
		initFromString(json);
	}

	private native void init();
	private native void initFromString(String json);

	public native JSONObject put(String key, String value);
	public native JSONObject put(String key, Object value);
	public native JSONObject put(String key, int value);
	public native JSONObject put(String key, long value);
	public native JSONObject put(String key, boolean  value);
	public native String getString(String key);
	public String optString(String key){
		return optString(key, "");	
	}
	public native String optString(String key, String def);
	public native int getInt(String key);
	public native float getFloat(String key);
	public native boolean getBoolean(String key);
	public native Object get(String key);
	public native JSONObject getJSONObject(String key);
	public native JSONArray getJSONArray(String key);
	public native Object remove(String key);
	public native String toString();
	public native String[] getKeys();
	public native boolean has(String key);

	public Set<String> keySet() {
		return new ArraySet<String>(getKeys());
	}
	public Iterator<String> keys(){
               return keySet().iterator();
       }

}
