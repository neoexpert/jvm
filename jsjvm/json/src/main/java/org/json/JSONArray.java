package org.json;

import java.util.Iterator;

public final class JSONArray implements Iterable<Object>{
	static{
		System.loadLibrary("json");
	}
	public JSONArray(){
		init();
	}

	public JSONArray(String json_array){
		initFromString(json_array);
	}

	private native void initFromString(String json_array);
	private native void init();
	public native int length();
	public native boolean isEmpty();

	public JSONArray put(Object value){
		putObject(value);
		return this;
	}
	private native void putObject(Object value);
	public native void put(int value);
	public native void put(long value);
	public native void put(boolean  value);

	public native String getString(int index);
	public native int getInt(int index);
	public native boolean getBoolean(int index);
	public native Object get(int index);
	public native JSONObject getJSONObject(int index);
	public native JSONArray getJSONArray(int index);
	public native String toString();

	public Iterator<Object> iterator(){
		final int length=length();
		return new Iterator(){
			int index=0;
			@Override
			public boolean hasNext(){
				return index<length;
			}

			@Override
			public Object next(){
				return get(index++);
			}
		};
	}
}
