package org.json;
import java.util.*;

class ArraySetIterator<T> implements Iterator<T>{
	private final T[] array;
	private int position=0;
	public ArraySetIterator(T[] array){
		this.array=array;
	}
	public boolean hasNext(){
		return position<array.length;
	}
	public T next(){
		return array[position++];
	}
	public void remove(){
		
	}
}
