package org.json;

import java.util.*;

class ArraySet<T> extends AbstractSet<T>{
	public Iterator<T> iterator(){
		return new ArraySetIterator<T>(array);
	}
	private final T[] array;
	public ArraySet(T[] array){
		this.array=array;
	}
	public void clear(){}
	public int size(){
		return -1;
	}
	public boolean add(T e){
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c){
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> collection) {
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> collection) {
		return false;
	}

	public boolean contains(Object o){
		for(T e:array)
			if(e.equals(o))
				return true;
		return false;
	}

	@Override
	public boolean remove(Object o) {
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> collection) {
		return false;
	}

}
