//import js.*;
import java.io.*;
//import dom.DOM;
public class JVMTest{
	public static boolean sz=true;
	public static byte sb=42;
	public static int si=42;
	public static float sf=42.5f;
	public static char sc=42;
	public static short ss=-42;
	public static long sj=-42L;
	public static double sd=-42.5;

	public boolean z;
	public byte b;
	public int i;
	public float f;
	public char c;
	public short s;
	public long j;
	public double d;

	public JVMTest(){
		z=true;
		b=42;
		i=42;
		f=42.5f;
		c=42;
		s=-42;
		j=-42L;
		d=-42.5;
	}

	public static void testStaticFields(){
		if(!sz)failed(1000);
		sz=false;
		if(sz)failed(1001);

		if(sb!=42)failed(1002);
		sb=-42;
		if(sb!=-42)failed(1003);

		if(si!=42)failed(1004);
		si=-42;
		if(si!=-42)failed(1005);

		if(sf!=42.5f)failed(1006);
		sf=-42.5f;
		if(sf!=-42.5f)failed(1007);

		if(sc!=42)failed(1008);
		sc=43;
		if(sc!=43)failed(1009);


		if(ss!=-42)failed(1010);
		ss=42;
		if(ss!=42)failed(1011);

		if(sj!=-42L)failed(1012);
		sj=42L;
		if(sj!=42L)failed(1013);
		
		if(sd!=-42.5)failed(1014);
		sd=42.5;
		if(sd!=42.5)failed(1015);
	}
	public void testFields(){
		if(!z)failed(2000);
		z=false;
		if(z)failed(2001);

		if(b!=42)failed(2002);
		b=-42;
		if(b!=-42)failed(2003);

		if(i!=42)failed(2004);
		i=-42;
		if(i!=-42)failed(2005);

		if(f!=42.5f)failed(2006);
		f=-42.5f;
		if(f!=-42.5f)failed(2007);

		if(c!=42)failed(2008);
		c=43;
		if(c!=43)failed(2009);


		if(s!=-42)failed(2010);
		s=42;
		if(s!=42)failed(2011);

		if(j!=-42L)failed(2012);
		j=42L;
		if(j!=42L)failed(2013);
		
		if(d!=-42.5)failed(2014);
		d=42.5;
		if(d!=42.5)failed(2015);
	}

	public static native void print(String str);
	public static native void print(int i);
	public static class Animal{
		public int getMaxAge(){
			return 42;
		}
	}
	public static class Cat extends Animal{
		public int getMaxAge(){
			return 15;
		}
	}
	public static class Ape extends Animal{
		public int getMaxAge(){
			return 30;
		}
	}
	public static class Human extends Ape{
		public int getMaxAge(){
			return 100;
		}
	}
	interface MaxAgeGetter{
		int getMaxAge();
	}
	interface Thinkable{
		boolean think();
	}
	public static class Man extends Human implements MaxAgeGetter, Thinkable{
		private int age;
		public void setAge(int age){
			this.age=age;
		}
		public int getAge(){
			return age;
		}
		public boolean think(){
			return true;
		}
	}
	public static void checkClasses(){
		Animal a=new Animal();
		if(a.getMaxAge()!=42)
			failed(291);
		a=new Cat();
		if(a.getMaxAge()!=15)
			failed(92);
		a=new Human();
		if(a.getMaxAge()!=100)
			failed(93);
		Man man=new Man();
		MaxAgeGetter mag=man;
		if(mag.getMaxAge()!=100)
			failed(94);
		Thinkable t=new Man();
		if(!t.think())
			failed(95);
		if(!(a instanceof Animal))
			failed(96);
		if(!(man instanceof Animal))
			failed(97);
		if(!(man instanceof Thinkable))
			failed(98);
	
	}
	public static void main(String[] args){
		/*
		for(int ii=0;ii<256;ii++){
			System.out.println("int _i"+ii+"=_i"+(ii-1)+"+1;");
		}*/
		//bipush:
		{
			int bipush=-4;
			if(-bipush!=4)failed(4);
		}
		//sipush:
		{
			int sipush=-1024;
			if(-sipush!=1024)failed(-1024);
		}
		boolean z=true;
		byte b=1;
		char c=2;
		short s=3;
		int i=4;
		long j=5;
		double d=6.5;
		float f=7.5f;

		if(!z)failed(1);
		if(b!=1)failed(2);
		if(c!=2)failed(3);
		if(s!=3)failed(4);
		if(i!=4)failed(5);
		if(j!=5)failed(16);
		if(d!=6.5)failed(7);
		if(f!=7.5f)failed(98);

		if(b>1)failed(9);
		if(c>2)failed(10);
		if(s>3)failed(11);
		if(i>4)failed(12);
		if(j>5)failed(13);
		if(d>6.5)failed(14);
		if(f>7.5f)failed(15);

		if(b<1)failed(16);
		if(c<2)failed(17);
		if(s<3)failed(18);
		if(i<4)failed(19);
		if(j<5)failed(20);
		if(d<6)failed(21);
		if(f<7)failed(22);

		if(b>=2)failed(23);
		if(c>=3)failed(24);
		if(s>=4)failed(25);
		if(i>=5)failed(26);
		if(j>=6)failed(27);
		if(d>=7)failed(28);
		if(f>=8)failed(29);

		if(b<=0)failed(30);
		if(c<=1)failed(31);
		if(s<=2)failed(32);
		if(i<=3)failed(33);
		if(j<=4)failed(34);
		if(d<=5)failed(35);
		if(f<=6)failed(36);

		int i2=i+42;
		if(i2!=i+42)failed(37);
		int i3=i-42;
		if(i3!=i-42)failed(38);
		int i4=i*42;
		if(i4!=i*42)failed(39);
		int i5=i/42;
		if(i5!=i/42)failed(40);
		int i6=i%42;
		if(i6!=i%42)failed(41);
		int i7=i<<42;
		if(i7!=i<<42)failed(42);
		int i8=i>>42;
		if(i8!=i>>42)failed(43);
		int i9=i>>>42;
		if(i9!=i>>>42)failed(44);
		int i10=i&42;
		if(i10!=(i&42))failed(45);
		int i11=i|42;
		if(i11!=(i|42))failed(46);
		int i12=i^42;
		if(i12!=(i^42))failed(47);
		int i13=-i;
		if(i13!=(-i))failed(48);
		//integer division
		i2=5;
		if((i2/2)!=2)
			failed(49);
		if(testint()!=0)
			failed(535);
		

		float f2=f+42;
		if(f2!=f+42)failed(49);
		float f3=f-42;
		if(f3!=f-42)failed(50);
		float f4=f*42;
		if(f4!=f*42)failed(51);
		float f5=f/42;
		if(f5!=f/42)failed(52);
		float f6=f%42;
		if(f6!=f%42)failed(53);
		float f7=-f;
		if(f7!=-f)failed(54);
		if(testfloat()!=0f)
			failed(455);

		double d2=d+42.0;
		if(d2!=d+42.0)failed(555);
		double d3=d-42;
		if(d3!=d-42)failed(56);
		double d4=d*42;
		if(d4!=d*42)failed(57);
		double d5=d/42;
		if(d5!=d/42)failed(58);
		double d6=d%42;
		if(d6!=d%42)failed(59);
		double d7=-d;
		if(d7!=-d)failed(60);
		if(testdouble()!=0.0)
			failed(60);

		long j2=j+42L;
		if(j2!=j+42L)failed(61);
		long j3=j-42L;
		if(j3!=j-42L)failed(62);
		long j4=j*42L;
		if(j4!=j*42L)failed(63);
		long j5=j/42L;
		if(j5!=j/42L)failed(64);
		long j6=j%42L;
		if(j6!=j%42L)failed(65);
		long j7=j<<42L;
		if(j7!=j<<42L)failed(66);
		long j8=j>>42L;
		if(j8!=j>>42L)failed(67);
		long j9=j>>>42L;
		if(j9!=j>>>42L)failed(68);
		long j10=j&42L;
		if(j10!=(j&42L))failed(69);
		long j11=j|42L;
		if(j11!=(j|42L))failed(70);
		long j12=j^42L;
		if(j12!=(j^42L))failed(71);
		long j13=-j;
		if(j13!=-j)failed(72);
		if(testlong()!=0L)
			failed(735);

		long l=b+c+s+i+j+(long)d+(long)f;
		if(l!=28)failed(473);

		Object o=null;
		if(o!=null)
			failed(74);
		o=new Object();
		if(o==null)
			failed(75);
		boolean[] zarr=new boolean[42];
		zarr[17]=true;
		if(!zarr[17])failed(76);

		byte[] barr=new byte[42];
		barr[17]=43;
		if(barr[17]!=43)failed(79);

		char[] carr=new char[42];
		carr[17]=43;
		if(carr[17]!=43)failed(80);

		short[] sarr=new short[42];
		sarr[17]=43;
		if(sarr[17]!=43)failed(79);

		int[] iarr=new int[42];
		iarr[17]=43;
		if(iarr[17]!=43)failed(80);

		float[] farr=new float[42];
		farr[17]=43.5f;
		if(farr[17]!=43.5f)failed(81);

		double[] darr=new double[42];
		darr[17]=43.5;
		if(darr[17]!=43.5)failed(82);

	
		long[] jarr=new long[42];
		jarr[17]=43L;
		if(jarr[17]!=43L)failed(83);

		Object[] aarr=new Object[42];
		aarr[17]=o;
		if(aarr[17]!=o)failed(84);

		invokestatic();
		invokestatic(z,b,c,j,i,s,d,f,o);
		if(i!=ireturn(z,b,c,j,i,s,d,f,o))
			failed(85);
		if(o!=areturn(z,b,c,j,i,s,d,f,o))
			failed(86);
		if(j!=lreturn(z,b,c,j,i,s,d,f,o))
			failed(87);
		if(d!=dreturn(z,b,c,j,i,s,d,f,o))
			failed(88);
		if(f!=freturn(z,b,c,j,i,s,d,f,o))
			failed(89);
		JVMTest test=new JVMTest();
		if(test.invokevirtual(5,7)!=12)
			failed(90);

		i=4;
		float fi=i;
		if(fi!=4f)failed(100);
		byte bi=(byte)i;
		if(bi!=4)failed(101);
		char ci=(char)i;
		if(ci!=4)failed(102);
		short si=(short)i;
		if(si!=4)failed(103);
		long ji=i;
		if(ji!=4L)failed(104);
		double di=i;
		if(di!=4.0)failed(105);


		c=4;
		fi=c;
		if(fi!=4f)failed(100);
		i=c;
		if(i!=4)failed(101);
		ci=c;
		if(ci!=4)failed(102);
		si=(short)c;
		if(si!=4)failed(103);
		ji=c;
		if(ji!=4L)failed(104);
		di=c;
		if(di!=4.0)failed(105);



		s=4;
		fi=s;
		if(fi!=4f)failed(100);
		i=s;
		if(i!=4)failed(101);
		ci=(char)s;
		if(ci!=4)failed(102);
		si=s;
		if(si!=4)failed(103);
		ji=s;
		if(ji!=4L)failed(104);
		di=s;
		if(di!=4.0)failed(105);

		b=4;
		fi=b;
		if(fi!=4f)failed(100);
		i=b;
		if(i!=4)failed(101);
		ci=(char)b;
		if(ci!=4)failed(102);
		si=b;
		if(si!=4)failed(103);
		ji=b;
		if(ji!=4L)failed(104);
		di=b;
		if(di!=4.0)failed(105);


		f=4f;
		bi=(byte)f;
		if(bi!=4)failed(101);
		i=(int)f;
		if(i!=4)failed(101);
		ci=(char)f;
		if(ci!=4)failed(102);
		si=(short)f;
		if(si!=4)failed(103);
		ji=(long)f;
		if(ji!=4L)failed(104);
		di=(double)f;
		if(di!=4.0)failed(105);


		d=4.0;
		bi=(byte)d;
		if(bi!=4)failed(101);
		i=(int)d;
		if(i!=4)failed(101);
		ci=(char)d;
		if(ci!=4)failed(102);
		si=(short)d;
		if(si!=4)failed(103);
		ji=(long)d;
		if(ji!=4L)failed(104);
		di=d;
		if(di!=4.0)failed(105);



		j=4L;
		bi=(byte)j;
		if(bi!=4)failed(101);
		i=(int)j;
		if(i!=4)failed(101);
		ci=(char)j;
		if(ci!=4)failed(102);
		si=(short)j;
		if(si!=4)failed(103);
		ji=j;
		if(ji!=4L)failed(104);
		di=j;
		if(di!=4.0)failed(105);

		new JVMTest().testFields();
	 	testStaticFields();
		checkClasses();
		widetest();
		passed();
	}

	private static int testint(){
		int l1=1;
		int l2=2;
		int l3=3;
		int l4=4;
		int l5=5;

		int result=(l5+l4-l3)/l3*l2*(-l1);
		if(result!=-4)
		{
			failed(191);
		}
		result*=(-l1);
		result%=l2;
		if(result!=0)
			failed(92);
		return result;
	}
	private static float testfloat(){
		float l1=1f;
		float l2=2f;
		float l3=3f;
		float l4=4f;
		float l5=5f;

		float result=(l5+l4-l3)/l3*l2*(-l1);
		if(result!=-4f)
			failed(93);
		result*=(-l1);
		result%=l2;
		if(result!=0f)
			failed(94);
		return result;
	}

	private static long testlong(){
		long l1=1L;
		long l2=2L;
		long l3=3L;
		long l4=4L;
		long l5=5L;

		long result=(l5+l4-l3)/l3*l2*(-l1);
		if(result!=-4L)
			failed(95);
		result*=(-l1);
		result%=l2;
		if(result!=0L)
			failed(96);
		return result;
	}

	private static double testdouble(){
		double l1=1.0;
		double l2=2.0;
		double l3=3.0;
		double l4=4.0;
		double l5=5.0;
		double result=(l5+l4-l3)/l3*l2*(-l1);
		if(result!=-4.0)
			failed(92);
		result*=(-l1);
		result%=l2;
		if(result!=0.0)
			failed(93);
		return result;

	}
	public static void passed(){}

	public int invokevirtual(int a, int b){
		return a+b;
	}
	public static void invokestatic(){}
	public static void invokestatic(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){}
	public static int ireturn(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){
		return i;
	}
	public static Object areturn(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){
		return o;
	}
	public static long lreturn(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){
		return j;
	}
	public static double dreturn(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){
		return d;
	}
	public static float freturn(boolean z,byte b,char c, long j, int i, short s, double d, float f, Object o){
		return f;
	}

	public static void widetest(){
int _i0=0;
int _i1=_i0+1;
int _i2=_i1+1;
int _i3=_i2+1;
int _i4=_i3+1;
int _i5=_i4+1;
int _i6=_i5+1;
int _i7=_i6+1;
int _i8=_i7+1;
int _i9=_i8+1;
int _i10=_i9+1;
int _i11=_i10+1;
int _i12=_i11+1;
int _i13=_i12+1;
int _i14=_i13+1;
int _i15=_i14+1;
int _i16=_i15+1;
int _i17=_i16+1;
int _i18=_i17+1;
int _i19=_i18+1;
int _i20=_i19+1;
int _i21=_i20+1;
int _i22=_i21+1;
int _i23=_i22+1;
int _i24=_i23+1;
int _i25=_i24+1;
int _i26=_i25+1;
int _i27=_i26+1;
int _i28=_i27+1;
int _i29=_i28+1;
int _i30=_i29+1;
int _i31=_i30+1;
int _i32=_i31+1;
int _i33=_i32+1;
int _i34=_i33+1;
int _i35=_i34+1;
int _i36=_i35+1;
int _i37=_i36+1;
int _i38=_i37+1;
int _i39=_i38+1;
int _i40=_i39+1;
int _i41=_i40+1;
int _i42=_i41+1;
int _i43=_i42+1;
int _i44=_i43+1;
int _i45=_i44+1;
int _i46=_i45+1;
int _i47=_i46+1;
int _i48=_i47+1;
int _i49=_i48+1;
int _i50=_i49+1;
int _i51=_i50+1;
int _i52=_i51+1;
int _i53=_i52+1;
int _i54=_i53+1;
int _i55=_i54+1;
int _i56=_i55+1;
int _i57=_i56+1;
int _i58=_i57+1;
int _i59=_i58+1;
int _i60=_i59+1;
int _i61=_i60+1;
int _i62=_i61+1;
int _i63=_i62+1;
int _i64=_i63+1;
int _i65=_i64+1;
int _i66=_i65+1;
int _i67=_i66+1;
int _i68=_i67+1;
int _i69=_i68+1;
int _i70=_i69+1;
int _i71=_i70+1;
int _i72=_i71+1;
int _i73=_i72+1;
int _i74=_i73+1;
int _i75=_i74+1;
int _i76=_i75+1;
int _i77=_i76+1;
int _i78=_i77+1;
int _i79=_i78+1;
int _i80=_i79+1;
int _i81=_i80+1;
int _i82=_i81+1;
int _i83=_i82+1;
int _i84=_i83+1;
int _i85=_i84+1;
int _i86=_i85+1;
int _i87=_i86+1;
int _i88=_i87+1;
int _i89=_i88+1;
int _i90=_i89+1;
int _i91=_i90+1;
int _i92=_i91+1;
int _i93=_i92+1;
int _i94=_i93+1;
int _i95=_i94+1;
int _i96=_i95+1;
int _i97=_i96+1;
int _i98=_i97+1;
int _i99=_i98+1;
int _i100=_i99+1;
int _i101=_i100+1;
int _i102=_i101+1;
int _i103=_i102+1;
int _i104=_i103+1;
int _i105=_i104+1;
int _i106=_i105+1;
int _i107=_i106+1;
int _i108=_i107+1;
int _i109=_i108+1;
int _i110=_i109+1;
int _i111=_i110+1;
int _i112=_i111+1;
int _i113=_i112+1;
int _i114=_i113+1;
int _i115=_i114+1;
int _i116=_i115+1;
int _i117=_i116+1;
int _i118=_i117+1;
int _i119=_i118+1;
int _i120=_i119+1;
int _i121=_i120+1;
int _i122=_i121+1;
int _i123=_i122+1;
int _i124=_i123+1;
int _i125=_i124+1;
int _i126=_i125+1;
int _i127=_i126+1;
int _i128=_i127+1;
int _i129=_i128+1;
int _i130=_i129+1;
int _i131=_i130+1;
int _i132=_i131+1;
int _i133=_i132+1;
int _i134=_i133+1;
int _i135=_i134+1;
int _i136=_i135+1;
int _i137=_i136+1;
int _i138=_i137+1;
int _i139=_i138+1;
int _i140=_i139+1;
int _i141=_i140+1;
int _i142=_i141+1;
int _i143=_i142+1;
int _i144=_i143+1;
int _i145=_i144+1;
int _i146=_i145+1;
int _i147=_i146+1;
int _i148=_i147+1;
int _i149=_i148+1;
int _i150=_i149+1;
int _i151=_i150+1;
int _i152=_i151+1;
int _i153=_i152+1;
int _i154=_i153+1;
int _i155=_i154+1;
int _i156=_i155+1;
int _i157=_i156+1;
int _i158=_i157+1;
int _i159=_i158+1;
int _i160=_i159+1;
int _i161=_i160+1;
int _i162=_i161+1;
int _i163=_i162+1;
int _i164=_i163+1;
int _i165=_i164+1;
int _i166=_i165+1;
int _i167=_i166+1;
int _i168=_i167+1;
int _i169=_i168+1;
int _i170=_i169+1;
int _i171=_i170+1;
int _i172=_i171+1;
int _i173=_i172+1;
int _i174=_i173+1;
int _i175=_i174+1;
int _i176=_i175+1;
int _i177=_i176+1;
int _i178=_i177+1;
int _i179=_i178+1;
int _i180=_i179+1;
int _i181=_i180+1;
int _i182=_i181+1;
int _i183=_i182+1;
int _i184=_i183+1;
int _i185=_i184+1;
int _i186=_i185+1;
int _i187=_i186+1;
int _i188=_i187+1;
int _i189=_i188+1;
int _i190=_i189+1;
int _i191=_i190+1;
int _i192=_i191+1;
int _i193=_i192+1;
int _i194=_i193+1;
int _i195=_i194+1;
int _i196=_i195+1;
int _i197=_i196+1;
int _i198=_i197+1;
int _i199=_i198+1;
int _i200=_i199+1;
int _i201=_i200+1;
int _i202=_i201+1;
int _i203=_i202+1;
int _i204=_i203+1;
int _i205=_i204+1;
int _i206=_i205+1;
int _i207=_i206+1;
int _i208=_i207+1;
int _i209=_i208+1;
int _i210=_i209+1;
int _i211=_i210+1;
int _i212=_i211+1;
int _i213=_i212+1;
int _i214=_i213+1;
int _i215=_i214+1;
int _i216=_i215+1;
int _i217=_i216+1;
int _i218=_i217+1;
int _i219=_i218+1;
int _i220=_i219+1;
int _i221=_i220+1;
int _i222=_i221+1;
int _i223=_i222+1;
int _i224=_i223+1;
int _i225=_i224+1;
int _i226=_i225+1;
int _i227=_i226+1;
int _i228=_i227+1;
int _i229=_i228+1;
int _i230=_i229+1;
int _i231=_i230+1;
int _i232=_i231+1;
int _i233=_i232+1;
int _i234=_i233+1;
int _i235=_i234+1;
int _i236=_i235+1;
int _i237=_i236+1;
int _i238=_i237+1;
int _i239=_i238+1;
int _i240=_i239+1;
int _i241=_i240+1;
int _i242=_i241+1;
int _i243=_i242+1;
int _i244=_i243+1;
int _i245=_i244+1;
int _i246=_i245+1;
int _i247=_i246+1;
int _i248=_i247+1;
int _i249=_i248+1;
int _i250=_i249+1;
int _i251=_i250+1;
int _i252=_i251+1;
int _i253=_i252+1;
int _i254=_i253+1;
int _i255=_i254+1;
int _i256=_i255+1;
if(_i256!=256)
	failed(256);

		boolean z=true;
		byte b=1;
		char c=2;
		short s=3;
		int i=4;
		long j=5;
		double d=6.0;
		float f=7f;
		if(!z)failed(1);
		if(b!=1)failed(2);
		if(c!=2)failed(3);
		if(s!=3)failed(4);
		if(i!=4)failed(5);
		if(j!=5)failed(6);
		if(d!=6)failed(7);
		if(f!=7)failed(8);

		if(b>1)failed(9);
		if(c>2)failed(10);
		if(s>3)failed(11);
		if(i>4)failed(12);
		if(j>5)failed(13);
		if(d>6)failed(14);
		if(f>7)failed(15);

		if(b<1)failed(16);
		if(c<2)failed(17);
		if(s<3)failed(18);
		if(i<4)failed(19);
		if(j<5)failed(20);
		if(d<6)failed(21);
		if(f<7)failed(22);

		if(b>=2)failed(23);
		if(c>=3)failed(24);
		if(s>=4)failed(25);
		if(i>=5)failed(26);
		if(j>=6)failed(27);
		if(d>=7)failed(28);
		if(f>=8)failed(29);

		if(b<=0)failed(30);
		if(c<=1)failed(31);
		if(s<=2)failed(32);
		if(i<=3)failed(33);
		if(j<=4)failed(34);
		if(d<=5)failed(35);
		if(f<=6)failed(36);

		int i2=i+42;
		if(i2!=i+42)failed(37);
		int i3=i-42;
		if(i3!=i-42)failed(38);
		int i4=i*42;
		if(i4!=i*42)failed(39);
		int i5=i/42;
		if(i5!=i/42)failed(40);
		int i6=i%42;
		if(i6!=i%42)failed(41);
		int i7=i<<42;
		if(i7!=i<<42)failed(42);
		int i8=i>>42;
		if(i8!=i>>42)failed(43);
		int i9=i>>>42;
		if(i9!=i>>>42)failed(44);
		int i10=i&42;
		if(i10!=(i&42))failed(45);
		int i11=i|42;
		if(i11!=(i|42))failed(46);
		int i12=i^42;
		if(i12!=(i^42))failed(47);

		float f2=f+42;
		if(f2!=f+42)failed(48);
		float f3=f-42;
		if(f3!=f-42)failed(49);
		float f4=f*42;
		if(f4!=f*42)failed(50);
		float f5=f/42;
		if(f5!=f/42)failed(51);
		float f6=f%42;
		if(f6!=f%42)failed(52);

		double d2=d+42;
		if(d2!=d+42)failed(53);
		double d3=d-42;
		if(d3!=d-42)failed(54);
		double d4=d*42;
		if(d4!=d*42)failed(545);
		double d5=d/42;
		if(d5!=d/42)failed(56);
		double d6=d%42;
		if(d6!=d%42)failed(57);

		long j2=j+42L;
		if(j2!=j+42L)failed(58);
		long j3=j-42L;
		if(j3!=j-42L)failed(59);
		long j4=j*42L;
		if(j4!=j*42L)failed(60);
		long j5=j/42L;
		if(j5!=j/42L)failed(61);
		long j6=j%42L;
		if(j6!=j%42L)failed(62);
		long j7=j<<42L;
		if(j7!=j<<42L)failed(63);
		long j8=j>>42L;
		if(j8!=j>>42L)failed(64);
		long j9=j>>>42L;
		if(j9!=j>>>42L)failed(65);
		long j10=j&42L;
		if(j10!=(j&42L))failed(66);
		long j11=j|42L;
		if(j11!=(j|42L))failed(67);
		long j12=j^42L;
		if(j12!=(j^42L))failed(68);

		long l=b+c+s+i+j+(long)d+(long)f;
		if(l!=28)failed(69);

		Object o=new Object();
		boolean[] zarr=new boolean[42];
		zarr[17]=true;
		if(!zarr[17])failed(70);


		byte[] barr=new byte[42];
		barr[17]=43;
		if(barr[17]!=43)failed(71);

		char[] carr=new char[42];
		carr[17]=43;
		if(carr[17]!=43)failed(72);

		short[] sarr=new short[42];
		sarr[17]=43;
		if(sarr[17]!=43)failed(73);

		int[] iarr=new int[42];
		iarr[17]=43;
		if(iarr[17]!=43)failed(74);

		float[] farr=new float[42];
		farr[17]=43.5f;
		if(farr[17]!=43.5f)failed(75);

		double[] darr=new double[42];
		darr[17]=43.5;
		if(darr[17]!=43.5)failed(76);

	
		long[] jarr=new long[42];
		jarr[17]=43L;
		if(jarr[17]!=43L)failed(77);

		Object[] aarr=new Object[42];
		aarr[17]=o;
		if(aarr[17]!=o)failed(78);

		invokestatic();
		invokestatic(z,b,c,j,i,s,d,f,o);
		if(i!=ireturn(z,b,c,j,i,s,d,f,o))
			failed(79);
		if(o!=areturn(z,b,c,j,i,s,d,f,o))
			failed(80);
		if(j!=lreturn(z,b,c,j,i,s,d,f,o))
			failed(81);
		if(d!=dreturn(z,b,c,j,i,s,d,f,o))
			failed(82);
		if(f!=freturn(z,b,c,j,i,s,d,f,o))
			failed(83);
		JVMTest test=new JVMTest();
		if(test.invokevirtual(5,7)!=12)
			failed(90);

		boolean ok=false;
		try{
			throw new Throwable();
		}catch(Throwable e){
			ok=true;
		}
		if(!ok)
			failed(4000);
		ok=false;
		try{
			throwsThrowable(1);
		}catch(Throwable e){
			ok=true;
		}
		if(!ok)
			failed(4001);
		ok=false;
		try{
			throwsThrowable(10);
		}catch(Throwable e){
			ok=true;
		}
		if(!ok)
			failed(4002);
		ok=false;
		try{
			throwsIOException(10);
		}catch(IOException  e){
			ok=true;
		}
		catch(Exception e){
			//should not occur
			ok=false;
		}
		if(!ok)
			failed(4003);
		ok=false;
		try{
			o=null;
			o.toString();
		}catch(NullPointerException e){
			ok=true;
		}
		if(!ok)
			failed(4004);
		ok=false;
		try{
			o=null;
			o.toString();
		}catch(Exception e){
			ok=true;
		}
		if(!ok)
			failed(4005);
		ok=false;
		try{
			o=null;
			o.toString();
		}catch(RuntimeException e){
			ok=true;
		}
		if(!ok)
			failed(4006);
		ok=false;
		try{
			o=null;
			o.toString();
		}catch(Throwable e){
			ok=true;
		}
		if(!ok)
			failed(4007);
		ok=false;
		try{
			throw new Throwable();
		}catch(Throwable e){
			ok=false;
		}
		finally{
			ok=true;
		}
		if(!ok)
			failed(4008);
		ok=false;
		try{
			ok=false;
		}catch(Throwable e){
			ok=false;
		}
		finally{
			ok=true;
		}
		if(!ok)
			failed(4008);
		passed();
	}
	public static void throwsIOException(int depth)throws IOException{
		if(depth==0)
			throw new IOException();
		throwsIOException(depth-1);
	}
	public static void throwsThrowable(int depth)throws Throwable{
		if(depth==0)
			throw new Throwable();
		throwsThrowable(depth-1);
	}
	public static void failed(int n){
		print(n);
		//DOM.alert(n);	
		//System.out.println(n);
	}
}
