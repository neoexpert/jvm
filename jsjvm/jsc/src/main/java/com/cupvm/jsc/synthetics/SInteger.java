package com.cupvm.jsc.synthetics;
import com.cupvm.jsc.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._abstract.*;


public class SInteger extends SyntheticClass{
	public SInteger(AClass jli, ClassLoader classLoader){
		super(jli, classLoader);
	}
	SyntheticMethod intValue=new SyntheticMethod(this, true){
		@Override
		public void invokeVirtual(StringBuilder sb, int stackpos){
			sb
				.append("s")
				.append(stackpos)
				.append("=s")
				.append(stackpos)
				.append(".value;\n");
		}
	};
	SyntheticMethod toString=new SyntheticMethod(this,original.getDeclaredMethod("toString()Ljava/lang/String;"), false){
		@Override
		public void invokeVirtual(StringBuilder sb, int stackpos){
			sb
					.append("s")
					.append(stackpos)
					.append("=new JString(s")
					.append(stackpos)
					.append(".value+\"\");\n");
		}
	};
	SyntheticMethod equals=new SyntheticMethod(this,true){
		@Override
		public void invokeVirtual(StringBuilder sb, int stackpos){
			sb
					.append("s")
					.append(stackpos)
					.append("=s")
					.append(stackpos)
					.append(".value;");
		}
	};
	SyntheticMethod valueOf=new SyntheticMethod(this, true){
		@Override
		public void invokeStatic(StringBuilder sb, int stackpos){
			sb
					.append("s")
					.append(stackpos)
					.append("=ivO(t, s")
					.append(stackpos)
					.append(");");
		}
	};

	@Override
	public AMethod getMethod(String signature){
		switch(signature){
			case "intValue()I":
			case "floatValue()F":
				return intValue;
			case "valueOf(I)Ljava/lang/Integer;":
				return valueOf;
			case "toString()Ljava/lang/String;":
				return toString;
			default:
				return super.getMethod(signature);
		}
	}

	@Override
	public AMethod getMethodHead(String signature){
		switch(signature){
			case "intValue()I":
			case "floatValue()F":
				return intValue;
			case "valueOf(I)Ljava/lang/Integer;":
				return valueOf;
			case "toString()Ljava/lang/String;":
				return toString;
			default:
				return super.getMethodHead(signature);
		}
	}

	/*
	@Override
	public AMethod getDeclaredMethod(String signature){
		switch(signature){
			case "toString()Ljava/lang/String;":
				return toString;
			default:
				return super.getDeclaredMethod(signature);
		}
	}*/
}
