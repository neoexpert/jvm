package com.cupvm.jsc.synthetics;

import com.cupvm.jsc.SyntheticClass;
import com.cupvm.jsc.SyntheticMethod;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm._class.ClassLoader;

public class SMath extends SyntheticClass {
    public SMath(AClass jli, ClassLoader classLoader){
        super(jli, classLoader);
    }
    SyntheticMethod sin=new SyntheticMethod(this, true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.sin(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod cos=new SyntheticMethod(this ,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.cos(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod exp=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.exp(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod sqrt=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.sqrt(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod abs=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.abs(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod abs64=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("if(s")
                    .append(stackpos)
                    .append("<0n)s")
                    .append(stackpos)
                    .append("=-s")
                    .append(stackpos)
                    .append(";");
        }
    };
    SyntheticMethod round32=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.round(s")
                    .append(stackpos)
                    .append("|0);");
        }
    };
    SyntheticMethod round64=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=BigInt(Math.round(s")
                    .append(stackpos)
                    .append("));");
        }
    };
    SyntheticMethod log=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.log(s")
                    .append(stackpos)
                    .append(");");
        }
    };
    SyntheticMethod max32=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.max(s")
                    .append(stackpos)
                    .append(",s")
                    .append(stackpos+1)
                    .append(");");
        }
    };
    SyntheticMethod min32=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.min(s")
                    .append(stackpos)
                    .append(",s")
                    .append(stackpos+1)
                    .append(");");
        }
    };
    SyntheticMethod max64=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.max(s")
                    .append(stackpos)
                    .append(",s")
                    .append(stackpos+2)
                    .append(");");
        }
    };
    SyntheticMethod min64=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.min(s")
                    .append(stackpos)
                    .append(",s")
                    .append(stackpos+2)
                    .append(");");
        }
    };
    SyntheticMethod pow=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.pow(s")
                    .append(stackpos)
                    .append(",s")
                    .append(stackpos+2)
                    .append(");");
        }
    };

    SyntheticMethod floorMod=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=(s")
                    .append(stackpos)
                    .append("%s")
                    .append(stackpos+1)
                    .append("+s")
                    .append(stackpos+1)
                    .append(")")
                    .append("%s")
                    .append(stackpos+1)
                    .append(";");
        }
    };
    SyntheticMethod floorDiv=new SyntheticMethod(this,true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append("=Math.floor(s")
                    .append(stackpos)
                    .append("/s")
                    .append(stackpos+1)
                    .append(")|0;");
        }
    };

    @Override
    public AMethod getMethod(String signature){
        switch(signature){
            case "sin(D)D":
                return sin;
            case "cos(D)D":
                return cos;
            case "exp(D)D":
                return exp;
            case "log(D)D":
                return log;
            case "sqrt(D)D":
                return sqrt;
            case "pow(DD)D":
                return pow;
            case "abs(D)D":
            case "abs(F)F":
            case "abs(I)I":
                return abs;
            case "abs(J)J":
                return abs64;
            case "max(FF)F":
            case "max(II)I":
                return max32;
            case "min(FF)F":
            case "min(II)I":
                return min32;
            case "max(DD)D":
            case "max(JJ)J":
                return max64;
            case "min(DD)D":
            case "min(JJ)J":
                return min64;
            case "round(F)I":
                return round32;
            case "round(D)J":
                return round64;
            case "floorMod(II)I":
                return floorMod;
            case "floorDiv(II)I":
                return floorDiv;
            default:
                return super.getMethod(signature);
        }
    }

    @Override
    public AMethod getMethodHead(String signature){
        return getMethod(signature);
    }

    @Override
    public AMethod getDeclaredMethod(String signature){
        switch(signature){
            default:
                return super.getDeclaredMethod(signature);
        }
    }
}
