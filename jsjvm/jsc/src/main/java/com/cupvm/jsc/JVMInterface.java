package com.cupvm.jsc;

import com.cupvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoaderImpl;
import com.cupvm.jvm.classpath.*;
import com.cupvm.jvm.method.*;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.util.*;
import java.util.zip.*;
import com.cupvm.jsc.synthetics.*;

public class JVMInterface {
    private static Logger logger= LoggerImpl.get();
    //enables JDWP debuging mode
    private final SyntheticClassLoader classLoader;
    private final boolean debug;
    public ArrayList<SyntheticClassLoader> classLoaders;

    public JVMInterface(boolean debug){
        this(debug,null);
    }

    public JVMInterface(boolean debug, SyntheticClassLoader parentClassLoader){
        this.classLoader=new SyntheticClassLoader(parentClassLoader, debug);
        classLoaders=new ArrayList<>();
        classLoaders.add(classLoader);
        this.debug=debug;
    }

    private void declareNatives() {
    }

    public static void addSynthetics(SyntheticClassLoader classLoader){
        classLoader.loadClass("java/lang/Integer")
                .createNatives(
                        "valueOf(I)Ljava/lang/Integer;",
                        "intValue()I",
                        "shortValue()S",
                        "byteValue()B",
                        "longValue()J",
                        "floatValue()F",
                        "doubleValue()D",
                        "equals(Ljava/lang/Object;)Z",
                        "hashCode()I",
                        "parseInt(Ljava/lang/String;I)I",
                        "toString(I)Ljava/lang/String;");
        classLoader.loadClass("java/lang/Float")
                .createNatives(
                        "valueOf(F)Ljava/lang/Float;",
                        "intValue()I",
                        "shortValue()S",
                        "byteValue()B",
                        "longValue()J",
                        "floatValue()F",
                        "doubleValue()D",
                        "equals(Ljava/lang/Object;)Z",
                        "hashCode()I",
                        "toString(F)Ljava/lang/String;");
        classLoader.loadClass("java/lang/Short")
                .createNatives(
                        "valueOf(S)Ljava/lang/Short;",
                        "intValue()I",
                        "shortValue()S",
                        "byteValue()B",
                        "longValue()J",
                        "floatValue()F",
                        "doubleValue()D",
                        "equals(Ljava/lang/Object;)Z",
                        "hashCode()I",
                        "toString(S)Ljava/lang/String;");
        classLoader.loadClass("java/lang/Long")
                .createNatives(
                        "valueOf(J)Ljava/lang/Long;",
                        "intValue()I",
                        "shortValue()S",
                        "byteValue()B",
                        "longValue()J",
                        "floatValue()F",
                        "doubleValue()D",
                        "equals(Ljava/lang/Object;)Z",
                        "hashCode()I",
                        "toString(J)Ljava/lang/String;");
        classLoader.loadClass("java/lang/String")
                .createNatives(
                        "contains(Ljava/lang/CharSequence;)Z",
                        "substring(I)Ljava/lang/String;",
                        "length()I",
                        "hashCode()I",
                        "toString()Ljava/lang/String;");
        classLoader.loadClass("java/lang/StringBuilder")
                .createNatives(
                        "<init>()V",
                        "<init>(Ljava/lang/String;)V",
                        "<init>(I)V",
                        "append(Ljava/lang/String;)Ljava/lang/StringBuilder;",
                        "toString()Ljava/lang/String;");
        classLoader.putClass("java/lang/Integer", new SInteger(classLoader.loadClass("java/lang/Integer"),classLoader));
        classLoader.putClass("java/lang/Math", new SMath(classLoader.loadClass("java/lang/Math"),classLoader));
        //classLoader.putClass("java/lang/StringBuilder", new SStringBuilder(classLoader.loadClass("java/lang/StringBuilder"),classLoader));
    }



	public void processStackTrace(JSONArray ja, OutputStream os){
		PrintWriter out=new PrintWriter(os);
		int l=ja.length();
		logger.setColor(Logger.RED);
		logger.log("Client Exception: ");
		for(int i=0;i<l;++i){
			JSONObject se = ja.getJSONObject(i);
			String classname=se.getString("clazz");
            int classloaderID = se.getInt("classloader");

            AClass clazz;
            if(classloaderID==0)
			    clazz = classLoader.loadClass(classname);
            else
                clazz=classLoaders.get(classloaderID).loadClass(classname);
            if(!se.has("method")){
                logger.warn("stack elment skipped: "+se);
                continue;
            }
			String signature=se.getString("method");
			Method m = (Method) clazz.getMethod(signature);
			int pc=se.getInt("pc");
			String lineNumber;
			if(m.isNative)
				lineNumber="(native method)";
			else
				lineNumber = Integer.toString(m.getCodeAttr().getCurrentLine(pc));
			out.print(classname);
			out.print(".");
			out.print(signature);
			out.print(" (");
			String sourceFileName=clazz.getSourceFileName();
			out.print(sourceFileName);
			out.print(": ");
			out.print(lineNumber);
			out.print(")");
			out.println();
			out.flush();
			logger.log("   ",classname, ".", signature, "(", sourceFileName,":", lineNumber,")");
		}
		logger.reset();
	}



    public void addFolderToClassPath(String path){
        classLoader.addToClasspath(path);
    }
    public void addJarToClassPath(String path) throws IOException{
			addJarToClassPath(new File(path));
    }
    public void addJarToClassPath(File jar) throws IOException{
        JarClassPath jcp = new JarClassPath(jar);
        classLoader.addToClasspath(jcp);
    }
    public void addJarToClassPath(File f,String prefix) throws IOException{
        JarClassPath jcp = new JarClassPath(f,prefix);
        classLoader.addToClasspath(jcp);
    }

    public void reset(){
        synchronized(this){
            classLoader.reset();
        }
        /*
        Class.addToClasspath("./webadmin/classes");
        JarClassPath jcp = new JarClassPath("./webadmin/classes/domrenderer.jar");
        Class.addToClasspath(jcp);
        jcp = new JarClassPath("./webadmin/classes/neoui.jar");
        Class.addToClasspath(jcp);
        jcp = new JarClassPath("./webadmin/classes/sadtrenderer.jar");
        Class.addToClasspath(jcp);
	//*/
    }
    public void init(){
        classLoader.loadClass("java/lang/Class");
        addSynthetics(classLoader);
    }

		public File outputDir;
    public SyntheticClassLoader createClassLoader(){
        int id=classLoaders.size();
        SyntheticClassLoader newClassLoader=new SyntheticClassLoader(classLoader, id, debug);
        classLoaders.add(newClassLoader);
        newClassLoader.reset();
				if(outputDir!=null)
					newClassLoader.setOutputPath(outputDir);
        return newClassLoader;
    }
		public void setOutputDir(File out){
			this.outputDir=out;
			classLoader.setOutputPath(out);
			for(SyntheticClassLoader cl:classLoaders)
				cl.setOutputPath(out);
		}
	public  void refreshCompiledJS() throws IOException{
		classLoader.refreshCompiledJS();
		for(SyntheticClassLoader cl:classLoaders)
			cl.refreshCompiledJS();

	}



    public JSONObject getClass(String classname, Integer clID) {
        if(clID==null)
	        return this.classLoader.getClass(classname);
        else
            return classLoaders.get(clID).getClass(classname);
    }


	public JSONArray getClasses(JSONArray classnames, Integer clID) {
		JSONArray classes=new JSONArray();
	    for(Object classname:classnames){
	    	classes.put(getClass((String)classname, clID));
	    }
	    return classes;
    }

    public long getInitialClasses(JSONObject output, long lastModifiedFromBrowser){
	    return classLoader.getInitialClasses(output, lastModifiedFromBrowser);
    }


    public byte[] getMethod(String classname, String signature, Integer clid) throws IOException {
        if(clid==null)
	        return classLoader.getMethod(classname, signature);
        else
            return classLoaders.get(clid).getMethod(classname, signature);
    }



    public long checkLastModifiedJS(long lastModifiedFromBrowser) {
        return classLoader.checkLastModifiedJS(lastModifiedFromBrowser);
    }
    public long getJS(OutputStream os) throws IOException {
	    return classLoader.getJS(os);
    }

    public long getJS(OutputStream os, int clID) throws IOException {
			return classLoaders.get(clID).getJS(os);
    }



    public void setMainClass(final String mainClass){
	    classLoader.setMainClass(mainClass);
    }



    public AClass getLoadedClass(String className) {
	    return classLoader.getLoadedClass(className);
    }

    public List<AClass> getLoadedClasses() {
        return classLoader.getLoadedClasses();
    }

    public AClass getClassByID(int id) {
        return classLoader.getClassByID(id);
    }

    public AClass getClassIntf(String className) {
        return classLoader.loadClass(className);
    }
    public InputStream getResource(String name){
        return classLoader.getResource(name);
    }
    public InputStream getResource(String name, int clID){
        return classLoaders.get(clID).getResource(name);
    }

    public boolean isClassLoaded(String clazz){
	    return classLoader.isClassLoaded(clazz);
    }

    public JSFunction getLoadedMethod(MethodHead mh) {
	    return classLoader.getLoadedMethod(mh);
    }

    public ClassLoaderImpl getClassLoader() {
		    return classLoader;
    }

	public int getLoadedClassID(final String clazz){
	    return classLoader.getLoadedClassID(clazz);
	}
    public void setJarFile(File jarFile) throws IOException{
        ZipFile jar = new ZipFile(jarFile);
        ZipEntry e = jar.getEntry("META-INF/MANIFEST.MF");
        if (e == null) {
            throw new RuntimeException("META-INF/MANIFEST.MF not found");
        }
        InputStream is = jar.getInputStream(e);
        String mainClass = parseManifest(is);
        classLoader.setMainClass(mainClass);
        logger.info("mainClass: ",mainClass);
        addJarToClassPath(jarFile, "");
    }
	private static String parseManifest(InputStream is) throws IOException {
		String line;
		while ((line=readLine(is))!=null){
			if (line.startsWith("Main-Class: ")) {
				//mainClass = mainClass.replaceAll("\\.", "/");
				return line.substring("Main-Class: ".length());
			}
		}
		return null;
	}
	private static String readLine(InputStream is) throws IOException {
		StringBuilder line=new StringBuilder();
		int b=is.read();
		if(b<0)return null;
		while (b>0){
			char c= (char) b;
			if(c=='\r'|c=='\n')
				break;
			if(c=='.')c='/';
			line.append(c);
			b=is.read();
		}
		return line.toString();
	}
}
