package com.cupvm.jsc;
import java.io.*;
import java.util.*;

import com.cupvm.jvm.LineNumberEntry;
import com.cupvm.jvm._abstract.*;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.ClassLoaderImpl;
import com.cupvm.jvm.method.CodeAttribute;
import com.cupvm.jvm.method.MException;
import com.cupvm.jvm.method.Method;
import com.cupvm.jvm.method.MethodHead;
import org.json.JSONArray;
import org.json.JSONObject;


public class SyntheticClassLoader extends ClassLoaderImpl{
	private File outputFile;
	public void setOutputPath(File dir){
		outputFile=new File(dir, id+".js");
		jsDirty=true;
		try{
			refreshCompiledJS();
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	private HashMap<String, SyntheticClass > synthetics=new HashMap<>();

	private long lastModifiedCl=Long.MAX_VALUE;
	private long lastModifiedJS=Long.MAX_VALUE;
	private final Set<String> loadedClasses =new LinkedHashSet<>();
	private JSONArray initialClasses=new JSONArray();
	private final Map<MethodHead, JSFunction> methods=new HashMap<>();
	private final Map<MethodHead, byte[]> method_cache=new HashMap<>();
	private final Map<String, JSONObject> class_cache=new HashMap<>();
	private final String id;
	private final int _id;
	private final boolean debug;
	private String mainClass;
	private boolean jsDirty=true;
	public SyntheticClassLoader(ClassLoader parent, boolean debug){
		super(parent);
		id="rcl";
		_id=0;
		this.debug=debug;
	}
	public SyntheticClassLoader(ClassLoader parent, int id, boolean debug){
		super(parent);
		this.id="cl"+id;
		this._id=id;
		this.debug=debug;
	}
	
    private StringBuilder compiledJS;

	public AClass loadClass(String classname){
		SyntheticClass sclass=synthetics.get(classname);
		if(sclass!=null)
			return sclass;
		AClass clazz=super.loadClass(classname, this);
		return clazz;
	}

	@Override
	public AClass loadClass(String classname, ClassLoader classLoader) {
		SyntheticClass sclass=synthetics.get(classname);
		if(sclass!=null)
			return sclass;
		return super.loadClass(classname, classLoader);
	}

	public SyntheticClass loadSynthetic(String classname){
		return synthetics.get(classname);
	}

	public void putClass(String name, SyntheticClass clazz){
		synthetics.put(name, clazz);
	}

	public String getJSName(){
		return id;
	}

	public int getID(){
		return _id;
	}


	@Override
	public void reset() {
		super.reset();
		try{
			refreshCompiledJS();
		}catch(IOException e){
			throw new RuntimeException(e);
		}
		method_cache.clear();
		//optimizedmethods.clear();
		loadedClasses.clear();
		initialClasses=new JSONArray();
		methods.clear();
		class_cache.clear();
	}

	public void getClassBranch(AClass clazz){
		if(clazz==null) return;
		if(loadedClasses.contains(clazz.getName()))
			return;
		synchronized(loadedClasses) {
			if (loadedClasses.add(clazz.getName())){
				lastModifiedCl = System.currentTimeMillis() / 1000L*1000L;
			}
		}
		AClass sclazz = clazz.getSuperClass();
		getClassBranch(sclazz);
		if(!clazz.getClassLoader().equals(this))
			return;
		synchronized(this){
			String sCLID=null;
			if(sclazz!=null)
				if(!clazz.getClassLoader().equals(sclazz.getClassLoader()))
					sCLID=((SyntheticClassLoader)sclazz.getClassLoader()).getJSName();
			JSONObject jclass=classToJSON(clazz, sCLID);
			logger.info("loading class: ",clazz.getName()," id:", clazz.getID());
			initialClasses.put(jclass);
			String classname=clazz.getName();
			class_cache.put(classname ,jclass);
		}
	}
	public JSFunction buildJSFunction(String className, String signature){
		Class c = (Class) loadClass(className);
		if(c==null){
			throw new RuntimeException("Class not found: "+className);
		}
		getClassBranch(c);
		Method m = (Method) c.getMethod(signature);
		if(m==null)
			throw new RuntimeException("method not found: "+className+ "."+ signature);
		if (m.isNative)
			return null;


		JSFunction result = buildJSFunction((Class) m.getMyClass(), m);
		return result;
	}

	private JSFunction buildJSFunction(Class cC, Method m) {
		if(debug)
			return new ByteCodeToJSDebug(this, cC,m).compile();
		else
			return new ByteCodeToJS(this, cC, m, false).compile();
	}
	private JSONObject classToJSON(AClass clazz, String sCLID){
		JSONObject jclass = new JSONObject();
		jclass.put("name", clazz.getName());
		jclass.put("id", clazz.getID());
		jclass.put("source_file", clazz.getSourceFileName());
		AClass super_class = clazz.getSuperClass();
		if (super_class != null) {
			jclass.put("super", super_class.getName());
			jclass.put("superID", super_class.getID());
			if(sCLID!=null)
				jclass.put("sCLID", sCLID);
		}

		JSONObject jmethods = new JSONObject();
		JSONObject natives = new JSONObject();
		Map<Integer, Method> methods = clazz.getMethods();
		for (Map.Entry<Integer, Method> e : methods.entrySet()) {
			Method m = e.getValue();
			String signature=m.getSignature();
			if (m.isNative){
				jmethods.put(signature, 1);
				StringBuilder nativeType=new StringBuilder();
				nativeType
						.append(clazz.getName().replaceAll("/","_"))
						.append("_")
						.append(m.getName());
				natives.put(signature, nativeType.toString());
			}
			else
				jmethods.put(signature, 0);
		}
		jclass.put("methods", jmethods);
		jclass.put("natives", natives);

		JSONObject jstatics = new JSONObject();
		//Map<String, ValueHolder> staticFields = clazz.getStaticFields();
			/*
				 for (Map.Entry<String, ValueHolder> e : staticFields.entrySet()) {
			//jstatics.put(e.getKey(), e.getValue().value);
			}*/
		jclass.put("statics", jstatics);

		JSONArray jinterfaces = new JSONArray();
		gatherInterfaces(jinterfaces, clazz);
		jclass.put("interfaces", jinterfaces);

		JSONObject jfields=new JSONObject();
		while(clazz!=null){
			final HashMap<String, AField> fields = clazz.getFields();
			for(AField f : fields.values()){
				if(f.isStatic()){
					jstatics.put(f.getName(), escape(jclass, f.getDefaultValue(this)));
					continue;
				}
				if(jfields.has(f.getName()))
					continue;
				jfields.put(f.getName(), escape(jclass, f.getDefaultValue(this)));
			}
			clazz= clazz.getSuperClass();
		}
		jclass.put("fields",jfields);

		return jclass;
	}

	public void gatherInterfaces(JSONArray jinterfaces, AClass clazz){
		AClass[] interfaces = clazz.getInterfaces();
		for (AClass i : interfaces) {
			gatherInterfaces(jinterfaces, i);
			jinterfaces.put(i.getName());
		}
	}

	public synchronized void refreshCompiledJS() throws IOException{
		if(!jsDirty)return;
		jsDirty=false;
		logger.info(id+": compiling...");
		compiledJS=new StringBuilder();
		StringBuilder post=new StringBuilder();
		compiledJS.append("\"use strict\"\n");
		compiledJS.append("const ")
			.append(id)
			.append(" =new ClassLoader(")
                .append(_id)
				.append(");")
				.append("self.")
				.append(id)
				.append("=")
				.append(id)
				.append(";");
		int len = initialClasses.length();
		for(int i=0;i<len;++i){
			JSONObject jclass=initialClasses.getJSONObject(i);
			int id=jclass.getInt("id");
			compiledJS
					.append("const _c")
					.append(id)
					.append("=")
					.append(this.id)
					.append(".rc(")
					.append(jclass.toString())
					.append(");");
			if(jclass.has("superID")){
				int superID=jclass.getInt("superID");
				compiledJS
						.append("_c"+id)
						.append(".setSuperClass(")
						.append("_c")
						.append(superID)
						.append(");");
			}
			if(jclass.getJSONObject("methods").has("<clinit>()V")){
				compiledJS
					.append(this.id)
					.append(".to_clinit.push(")
						.append("_c"+id)
						.append(");");
			}
		}
		Collection<JSFunction> methods = this.methods.values();
		for(JSFunction mh:methods){
			StringBuilder classes=new StringBuilder();
			classes.append("[");
			String prefix="";
			for(Integer classname:mh.classes){
				classes.append(prefix);
				prefix=",";
				classes.append("_c");
				classes.append(classname);
			}
			classes.append("]");
			//mh.jsFunction.buildJS(loadedClasses, this.methods);
			mh.wrap(this,compiledJS, debug);

			//compiledJS
			//      .append(";");
			String f="f"+mh.method.getID();
			String etable=mh.getExceptionTable(this);
			if(etable!=null)
				compiledJS
						.append(f)
						.append(".etable=")
						.append(etable)
						.append(";");
			compiledJS
				.append(f)
				.append(".id=")
				.append(mh.method.getID())
				.append(";");
			post
					.append(id)
					.append(".anma(")
					.append(classes.toString())
					.append(",")
					.append("\"")
					.append(mh.signature)
					.append("\"")
					.append(",")
					.append(f)
					.append(",")
					.append(mh.method.isStatic());

			if(debug) {
				post.append(",");
				post.append(mh.buildLineNumberArray());
			}
			post
					.append(");");
		}

		compiledJS
				//.append("async function initJIT(jvm){")
				.append("self.init_")
				.append(this.id)
				.append("=async function(){")
				.append(post)
				.append("};");
				//.append("}");
        ///*
		if(outputFile!=null){
			PrintWriter out = new PrintWriter(outputFile);
			out.write(compiledJS.toString());
			out.close();
		}
         //*/
	}

	public byte[] getMethod(String classname, String signature) throws IOException {
		MethodHead key=new MethodHead( classname,null, signature,null);
		synchronized (this) {
			byte[] ba=method_cache.get(key);
			if(ba!=null){
				return ba;
			}
			AClass c = loadClass(classname);
			if(c==null){
				logger.error("Class not found: ",classname);
				return null;
			}
			AMethod method=c.getMethod(signature);
			if(method==null){
				logger.error("method not found: ",classname, ".", signature);
				return null;
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos = new DataOutputStream(baos);
			if (method.isNative){
				dos.writeByte(1);
				byte[] nameBytes=method.getMyClass().getName().getBytes();
				dos.write(nameBytes);
				return baos.toByteArray();
			}

			if(method instanceof SyntheticMethod)
				method=((SyntheticMethod)method).getOriginal();
			Method m = (Method) method;
			{
				JSFunction result = buildJSFunction((Class) m.getMyClass(), m);
				logger.info(result.getClass().getSimpleName()," built: ",classname,".",signature," ",result.toString());
				result.buildJS(this);
				result.getExceptionTable(this);
				String js=result.getJS();
				//System.out.println(js);

				String mclassname = m.getMyClass().getName();
				MethodHead rootSignature=new MethodHead(mclassname,null, signature,null);
				JSFunction mh= methods.get(rootSignature);
				if(mh==null) {
					buildMethod(result,m);
					methods.put(rootSignature, result);
				}
				if (!mclassname.equals(classname)) {
					result.classes.add(c.getID());
				}
				jsDirty=true;
				//refreshCompiledJS();


				//isNotRawBytecode:
				dos.writeByte(0);
				dos.writeInt(m.getID());
				byte[] jcode = js.getBytes();
				dos.writeInt(jcode.length);
				dos.write(jcode);

				char[] args=m.args;
				dos.writeInt(args.length);
				for (char arg : args)
					dos.writeByte((byte) arg);
				dos.writeByte(m.isStatic()?1:0);
				//Exception Table/////////
				CodeAttribute codeattr = m.getCodeAttr();
				Class cC = (Class)m.getMyClass();
				if(codeattr!=null) {
					MException[] etable = codeattr.getExceptionTable();
					dos.writeInt(etable.length);
					for (MException e : etable) {
						dos.writeInt(e.start_pc);
						dos.writeInt(e.end_pc);
						dos.writeInt(e.handler_pc);
						if(e.catch_type==0)
							dos.writeInt(0);
						else {
							byte[] eClass = cC.getClassName(e.catch_type).getBytes();
							dos.writeInt(eClass.length);
							dos.write(eClass);
						}

						//dos.writeInt(e.catch_type);
					}
				}
				else
					dos.writeInt(0);
				/////////////////////////
				//LineNumberTable
				if(debug){
					int codeLength=m.code.length;
					List<? extends LineNumberEntry> table = m.getLineNumbers();
					if(table==null)
						dos.writeInt(0);
					else {
						Iterator<? extends LineNumberEntry> it = table.iterator();
						dos.writeInt(codeLength);
						if (it.hasNext()) {
							LineNumberEntry entry = it.next();
							int nextpc = entry.getPC();
							int currentLine = entry.getLineNumber();
							int nextLine = currentLine;
							for (int i = 0; i < codeLength; i++) {
								if (i >= nextpc) {
									currentLine = nextLine;
									if (it.hasNext()) {
										entry = it.next();
										nextpc = entry.getPC();
										nextLine = entry.getLineNumber();
									}
								}
								dos.writeInt(currentLine);
							}
						}
					}
				}
				else
					dos.writeInt(0);
				////////////////

				dos.write(m.getMyClass().getName().getBytes());
				ba=baos.toByteArray();
				method_cache.put(key,ba);
				return ba;
			}
		}

	}
	private void buildMethod(JSFunction jsFunction, Method m){
		lastModifiedJS=System.currentTimeMillis()/1000L*1000L;
	}
	public String getClassID(String className){
		AClass clazz = loadClass(className);
		getClassBranch(clazz);
		return "_c"+clazz.getID();
	}

	public JSONObject getClass(String classname) {
		synchronized (this) {
			JSONObject jclass=class_cache.get(classname);
			if(jclass!=null)
				return jclass;
			AClass clazz = loadClass(classname);
			//jclass=classToJSON(clazz, constantPool);
			getClassBranch(clazz);
			jclass=class_cache.get(classname);
			if(jclass==null)
				logger.error("class not found: ",classname);
			return jclass;
		}
	}

	public long getInitialClasses(JSONObject output, long lastModifiedFromBrowser) {
		try {
			refreshCompiledJS();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//output.put("classes",initialClasses);
		output.put("MainClass",mainClass);
		output.put("debug",debug);
		if(lastModifiedFromBrowser != -1 && lastModifiedCl <= lastModifiedFromBrowser)
			return -1L;

		return lastModifiedCl;
	}
	public void compileRecursively() throws IOException {
		compileRecursively(mainClass, "main([Ljava/lang/String;)V");
	}
	public void compileRecursively(String className, String signature) throws IOException {
		JSFunction jsFunction = buildJSFunction(className, signature);
		if(jsFunction==null) return;
		for(MethodHead mh:jsFunction.methodDependecies){
			if(methods.containsKey(mh))
				continue;
			compileRecursively(mh.classname, mh.signature);
		}
		System.out.println(jsFunction);
	}

	@Override
	public AClass getLoadedClass(String name) {
		if(!class_cache.containsKey(name))
			return null;
		return super.loadClass(name);
	}

	public long getJS(OutputStream os) throws IOException {
		if(jsDirty)
			refreshCompiledJS();
		os.write(compiledJS.toString().getBytes());
		return lastModifiedJS;
	}

	public void setMainClass(String mainClass) {
		this.mainClass=mainClass;
	}

	public boolean isClassLoaded(String clazz) {
		return loadedClasses.contains(clazz);
	}

	public JSFunction getLoadedMethod(MethodHead mh) {
		return methods.get(mh);
	}

	public int getLoadedClassID(String clazz) {
		if(loadedClasses.contains(clazz))
			return loadClass(clazz).getID();
		return -1;
	}

	public long checkLastModifiedJS(long lastModifiedFromBrowser) {
		if(lastModifiedFromBrowser != -1 && lastModifiedJS <= lastModifiedFromBrowser)
			return -1L;
		return lastModifiedJS;
	}
	private static Object escape(JSONObject jclass, Number value){
		if(value instanceof Float){
			if(value.floatValue()==Float.POSITIVE_INFINITY){
				jclass.put("parseStatics", true);
				return "+inf";
			}
			if(value.floatValue()==Float.NEGATIVE_INFINITY){
				jclass.put("parseStatics", true);
				return "-inf";
			}
			if(Float.isNaN(value.floatValue())){
				jclass.put("parseStatics", true);
				return "NaN";
			}
		}
		if(value instanceof Double){
			if(value.doubleValue()==Double.POSITIVE_INFINITY){
				jclass.put("parseStatics", true);
				return "+inf";
			}
			if(value.doubleValue()==Double.NEGATIVE_INFINITY){
				jclass.put("parseStatics", true);
				return "-inf";
			}
			if(Double.isNaN(value.doubleValue())){
				jclass.put("parseStatics", true);
				return "NaN";
			}
		}
		if(value instanceof Long){
			jclass.put("parseStatics", true);
			return value.toString();
		}
		return value;
	}

	@Override
	public boolean equals(Object o) {
	    if(!(o instanceof SyntheticClassLoader))
	    	return false;
	    return id.equals(((SyntheticClassLoader)o).getJSName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(synthetics, lastModifiedCl, lastModifiedJS, loadedClasses, initialClasses, methods, method_cache, class_cache, id, debug, mainClass, jsDirty, compiledJS);
	}

}
