package com.cupvm.jsc;
import com.cupvm.jsc.synthetics.SInteger;
import com.cupvm.jvm._abstract.*;


public abstract class SyntheticMethod extends AMethod{
	private final AMethod original;

	public SyntheticMethod(SyntheticClass clazz, AMethod original, boolean _native){
		super(clazz);
		isNative=_native;
		this.original=original;
	}

	public SyntheticMethod(SyntheticClass clazz, boolean _native) {
		super(clazz);
		isNative=_native;
		original=null;
	}

	public void invokeStatic(StringBuilder sb, int stackpos){}
	public void invokeSpecial(StringBuilder sb, int stackpos){}
	public void invokeVirtual(StringBuilder sb,int stackpos){}

    public AMethod getOriginal() {
		return original;
    }
}
