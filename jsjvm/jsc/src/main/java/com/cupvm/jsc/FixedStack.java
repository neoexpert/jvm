package com.cupvm.jsc;
import java.util.Stack;
import com.cupvm.jsc.jvalue.*;

public class FixedStack{

	private final Stack<JValue> stack=new Stack<>();
    private final int maxSize;

    public FixedStack(int size) {
        super();
        this.maxSize = size;
    }

	public void push(Number _const, Number value) {
		push(_const.toString(),value);
	}

    public void push(String var, Number value) {
        if (stack.size() >= maxSize) {
						throw new IndexOutOfBoundsException("Stack size = " + maxSize);
        }
        stack.push(new JValue(var, value));
    }
    public void push(String var, JValue value) {
        if (stack.size() >= maxSize) {
						throw new IndexOutOfBoundsException("Stack size = " + maxSize);
        }
				if(value==null)
        	stack.push(new JValue(var, null));
				else
        	stack.push(new JValue(var, value.value));
    }
    public void pushUnknown(){
        stack.push(new JValue(null, null));
		}

    public void push(Number value) {
        if (stack.size() >= maxSize) {
						throw new IndexOutOfBoundsException("Stack size = " + maxSize);
        }
        stack.push(new JValue(null, value));
    }

    public void push(JValue value) {
        if (stack.size() >= maxSize) {
						throw new IndexOutOfBoundsException("Stack size = " + maxSize);
        }
				if(value==null)
					value=JValue.NULL;
        stack.push(value);
    }
	public FixedStack clone(){
		FixedStack copy=new FixedStack(maxSize);
		copy.stack.addAll(stack);
		return copy;
	}
	public JValue pop(){
		return stack.pop();
	}
	public String popSource(){
		String source=stack.pop().source;
		if(source!=null)
			return source;
		return "s"+stack.size();

	}
	public String pop2Source() {
		String source=stack.pop().source;
		stack.pop();
		if(source!=null)
			return source;
		return "s"+stack.size();
	}
	public JValue peek(){
		return stack.peek();
	}
	public int size(){
		return stack.size();
	}
	public void setSize(int size){
		stack.setSize(size);
	}

	public void copyHead(JValue[] invokeTemplate) {
		int len=invokeTemplate.length;
		int size=stack.size();
		for(int i=0;i<len;++i){
			invokeTemplate[i]=stack.get(size-len+i);
		}
	}

	public void merge(FixedStack stack) {
		for(int i=0;i<stack.size();++i){
			JValue a=stack.get(i);
			JValue b=get(i);
			if(a==null||b==null||!a.equals(b))
				stack.set(i,null);
		}
	}

	private void set(int i, JValue o) {
		stack.set(i,o);
	}

	private JValue get(int i) {
		return stack.get(i);
	}

}
