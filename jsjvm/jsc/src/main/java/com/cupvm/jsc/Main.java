package com.cupvm.jsc;


import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;

import java.io.*;

public class Main{
	private static final Logger logger= ConsoleLogger.get();
	private static void printUsage() {
		logger.log("Usage: jsc [options] <main-class>");
		logger.log("");
		logger.log("       options:");
		logger.log("           -cp \t add one or multiple jar files to classpath separated by ';'");
	}
	public static void main(String[] args) throws IOException{
		if(args==null){
			printUsage();
			return;
		}
		if(args.length<=0){
			printUsage();
			return;
		}

		JSC jsc=new JSC();
		for(int i=0;i<args.length;++i){
			String arg=args[i];
			switch(arg){
				case "-node":
					jsc.node=true;
					break;
				case "-cp":
					String cp=args[++i];
					final String[] cps = cp.split(";");
					for(String jar:cps)
						try{
							jsc.addJarToClassPath(jar);
						}
						catch(IOException e){
							System.err.print("could not read jar file: ");
							System.err.print(jar);
							System.err.print(" :");
							System.err.println(e.getMessage());
							return;
						}
					break;
				case "-debug":
				case "-v":
					ByteCodeToJS.jsComments =true;
					break;
			}
		}
		jsc.addFolderToClassPath(".");
		String mainClass=args[args.length-1];
		jsc.compile(mainClass);
		File out=new File("./out");
		out.mkdir();
		FileOutputStream fisClasses=new FileOutputStream(new File(out,"./rcl.js"));
		jsc.write(fisClasses);
		writeResource("index.html",out);
		writeResource("style.css",out);
		File jsout=new File(out,"js");
		jsout.mkdir();
		writeResource("js/main.js",out);
		writeResource("js/java.js",out);
		writeResource("js/base.js",out);
		writeResource("js/dom.js",out);
		writeResource("js/svg.js",out);
		writeResource("js/webgl.js",out);
		writeResource("js/json.js",out);
		writeResource("js/filesystem.js",out);
	}

	private static void writeResource(final String path, File outDir) throws IOException{
		final InputStream is = Main.class.getResourceAsStream("/root/"+path);
		if(is==null){
			System.err.println("could not copy resource file: "+path);
			return;
		}
		FileOutputStream fos=new FileOutputStream(new File(outDir,path));
		byte[] buf =new byte[1024];
		int len=is.read(buf);
		while(len>0){
			fos.write(buf,0,len);
			len=is.read(buf);
		}
	}
}
