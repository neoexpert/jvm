package com.cupvm.jsc;

import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.method.Method;

public class ByteCodeToJSDebug extends ByteCodeToJS{
    public ByteCodeToJSDebug(SyntheticClassLoader classLoader, Class cC, Method m) {
        super(classLoader, cC,m, true);
    }

    @Override
    protected void genHead(Method method, boolean isStatic, int argsLength, JSFunction result) {
        StringBuilder head=new StringBuilder();
        head
                .append("let lv=new Array(")
                .append(method.max_locals)
                .append(");");
        if(!isStatic)
            ++argsLength;
        for(int i=0;i<argsLength;++i){
            head
                    .append("lv[")
                    .append(i)
                    .append("]=arguments[")
                    .append(i+1)
                    .append("];");
        }

        head.append("f.lv=lv;");
        result.setHeader(head.toString());
    }

    @Override
    protected void istore(StringBuilder sb, int index, int stackpos, String source) {
			sb
				.append("lv[")
				.append(index)
				.append("]=")	
				.append("s")
				.append(stackpos)
				.append(";");
    }

    @Override
    protected void fstore(StringBuilder sb, int index, int stackpos, String source) {
        istore(sb,index,stackpos,source);
    }

    @Override
    protected void astore(StringBuilder sb, int index, int stackpos, String source) {
        istore(sb,index,stackpos,source);
    }

    @Override
    protected void lstore(StringBuilder sb, int index, int stackpos, String source) {
        //sb.append("f.o();");
        istore(sb,index, stackpos,source);
    }

    @Override
    protected void dstore(StringBuilder sb, int index, int stackpos, String source){
        lstore(sb,index, stackpos,source);
    }

    @Override
    protected String iload(StringBuilder jcode, int index, int stackpos) {
        jcode
					.append("s")
					.append(stackpos)
					.append("=lv[")
					.append(index)
					.append("];");
				return null;
    }

    @Override
    protected String aload(StringBuilder jcode, int index, int stackpos) {
        return iload(jcode, index, stackpos);
    }

    @Override
    protected String fload(StringBuilder jcode, int index, int stackpos) {
        return iload(jcode, index, stackpos);
    }

    @Override
    protected String lload(StringBuilder jcode, int index, int stackpos) {
        return iload(jcode, index, stackpos);
    }

    @Override
    protected String dload(StringBuilder jcode, int index, int stackpos) {
        return lload(jcode,index, stackpos);
    }

    @Override
    protected void iinc(StringBuilder jcode, int index, int increment) {
        jcode
                .append("lv[")
                .append(index)
                .append("]+=")
                .append(increment)
                .append(";");
    }
}
