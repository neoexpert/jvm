package com.cupvm.jsc.synthetics;

import com.cupvm.jsc.SyntheticClass;
import com.cupvm.jsc.SyntheticMethod;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm._class.ClassLoader;

public class SStringBuilder extends SyntheticClass {
    public SStringBuilder(AClass jli, ClassLoader classLoader){
        super(jli, classLoader);
    }
    SyntheticMethod appendString=new SyntheticMethod(this, true){
        @Override
        public void invokeStatic(StringBuilder sb, int stackpos){
            sb
                    .append("s")
                    .append(stackpos)
                    .append(".value+=")
                    .append(stackpos)
                    .append(".str);");
        }
    };

    @Override
    public AMethod getMethod(String signature){
        switch(signature){
            case "append(Ljava/lang/String;)Ljava/lang/StringBuilder;":
                return appendString;
            default:
                return super.getMethod(signature);
        }
    }

    @Override
    public AMethod getMethodHead(String signature){
        return getMethod(signature);
    }

    @Override
    public AMethod getDeclaredMethod(String signature){
        switch(signature){
            default:
                return super.getDeclaredMethod(signature);
        }
    }
}
