package com.cupvm.jsc;


public class BracedJSFunction extends JSFunction{
	public BracedJSFunction(JSFunction base){
		super(base);
		createJumpLabels();
	}


	@Override
	public void buildJS(SyntheticClassLoader ji){
		int length = ocode.length;
		if(length == 1){
			this.js=EMPTY_METHOD;
			return;
		}
		StringBuilder functionBuilder=new StringBuilder();
		StringBuilder bodyBuilder = new StringBuilder();
		buildJSBody(bodyBuilder, ji, debug, 0, length);

		//StringBuilder headerBuilder = new StringBuilder();
		if(ByteCodeToJS.jsComments){
			functionBuilder.append("/*BracedJSFunction*/\n");
			functionBuilder.append("/*")
					.append(method.getMyClass().getName())
					.append(".")
					.append(signature)
					.append("*/");
		}
		if(usesFrame)
			functionBuilder.append("const f=t.peek();");
		if(header!=null)
			functionBuilder.append(header);
		if(debug)
			functionBuilder.append("const depth=t.entries.length;");

		if(!variables.isEmpty()){
			functionBuilder.append("let ");
			String prefix = "";
			for(String variable : variables){
				functionBuilder
						.append(prefix)
						.append(variable);
				prefix=",";
			}
			functionBuilder.append(";");
		}

		//functionBuilder.append(functionBuilder.toString());
		functionBuilder.append(bodyBuilder.toString());

		this.js = functionBuilder.toString();
	}

	protected void buildJSBody(StringBuilder js, SyntheticClassLoader ji, boolean debug, int from, int to){
		loop:
		for(int i = from; i < to; ++i){
			OP op = ocode[i];
			if(op==null)continue;
			if(ByteCodeToJS.jsComments){
				js
					.append("/*")
					.append(op.name)
					.append("*/\n");	
			}

			//System.out.println("check i:"+op.pc);
			Integer j = jumps.get(i);
			if(j != null)
				buildJumpLabel(js, j);
			if(debug)
				js.append("await dstep(t, "+i+");");
			switch(op.op){
				case IFEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("!==0){");
						buildJSBody(js, ji, debug, i+3, op.jumpTarget);
						js.append("}");
						i=op.jumpTarget-1;
					continue loop;
				case IFNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("===0){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);

					js.append("}");
					i=op.jumpTarget-1;continue loop;
				case IFLT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append(">=0){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IFGE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("<0){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IFGT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append("<=0){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IFLE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos)
							.append(">0){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("!==")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("===")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPLT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("<=")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPGE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append(">")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPGT:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append(">=")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ICMPLE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("<")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ACMPEQ:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("!=")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IF_ACMPNE:
					js
							.append("if(")
							.append("s")
							.append(op.stackpos+1)
							.append("==")
							.append("s")
							.append(op.stackpos)
							.append("){");
					buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
					continue loop;
				case IFNULL:
						js
								.append("if(")
								.append("s")
								.append(op.stackpos)
								.append("){");
						buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
						continue loop;
				case IFNONNULL:
					js
							.append("if(!")
							.append("s")
							.append(op.stackpos)
							.append("){");
						buildJSBody(js, ji, debug, i+3, op.jumpTarget);
					js.append("}");
					i=op.jumpTarget-1;
						continue loop;
				case INVOKESTATIC:
					if(op.sm!=null)
						op.sm.invokeStatic(js,op.stackpos);
					else
						genStaticInvocation(op,js,ji);
					continue loop;
				case INVOKESPECIAL:
					if(op.sm!=null)
						op.sm.invokeSpecial(js, op.stackpos);
					else
						genSpecialInvocation(op,js,ji);
					continue loop;
				case INVOKEVIRTUAL:
					if(op.sm!=null)
						op.sm.invokeVirtual(js, op.stackpos);
					else
						genInvocation(op,js,ji);
					continue loop;
			}

			if(op.clazz != null){
				//this op depends on clazz
				if(!variables.contains("clazz")){
					//js.append("var ");
					variables.add("clazz");
				}
				if(ji.isClassLoaded(op.clazz.getName())){
					int id=op.clazz.getID();
					//class should be avaliable under jvm.cs[id]
					js.append("clazz=_c").append
							(id).append(";");
				}
				else
					//load class from server
					js.append("clazz=await ")
							.append(((SyntheticClassLoader)op.clazz.getClassLoader()).getJSName())
							.append(".loadClass(\"").append(op.clazz.getName()).append("\");if(!clazz){f.pc=").append(i).append(";return true;}");
			}
			if(op.js==null)
				throw new RuntimeException("null code");
			js.append(op.js);
			if(op.donpcheck!=null){
				genNPCheck(js, i, op.donpcheck, op);
			}
			if(op.suffix!=null)
				js.append(op.suffix);
			if(ByteCodeToJS.jsComments)
				js.append("\n");
		}
	}




}
