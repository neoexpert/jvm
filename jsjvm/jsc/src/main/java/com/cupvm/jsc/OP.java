package com.cupvm.jsc;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.method.MethodHead;
import com.cupvm.jsc.jvalue.*;
import com.cupvm.jvm._class.Class;

public class OP{
	public SyntheticMethod sm;
	public String name;
	//this op depends on:
	public AClass clazz=null;
	public MethodHead method=null;

	public String donpcheck;
	public JValue[] invokeTemplate;
	public JValue[] lv;
	public FixedStack opstack;

	public OP(int pc, short op ){
		this.pc=pc;
		this.op=op;
	}
	public String js;
	public String suffix;
	public final short op;
	short param1;
	short param2;
	public final int pc;
	public int jumpTarget;
	public int stackpos;
	public void setState(JValue[] lv, FixedStack stack){
		this.lv=lv;
		this.opstack=stack;
	}

	public void mergeInvokeTemplate(JValue[] invokeTemplate){
		if(this.invokeTemplate==null){
			this.invokeTemplate=invokeTemplate;
			return;
		}
		for(int i=0;i<invokeTemplate.length;++i){
			if(invokeTemplate[i]==null||this.invokeTemplate[i]==null||!invokeTemplate[i].equals(this.invokeTemplate[i])){
				this.invokeTemplate[i]=null;
			}
		}
	}

	public void gc() {
		lv=null;
		opstack=null;
	}
}
