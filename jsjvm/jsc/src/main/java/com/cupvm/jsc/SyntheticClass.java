package com.cupvm.jsc;
import com.cupvm.jvm._abstract.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.method.Method;

import java.util.Map;


public class SyntheticClass extends AClass{
	protected final AClass original;
	public SyntheticClass(AClass original, ClassLoader classLoader){
		super(original.getName(), classLoader);
		this.original=original;
	}


	public int getID(){
		return original.getID();
	}

	public AMethod getMethod(String signature){
			return original.getMethod(signature);
	}

  public AMethod getMethodHead(String signature){
				return original.getMethodHead(signature);
	}

	public AMethod getDeclaredMethod(String signature){
			return original.getDeclaredMethod(signature);
	}

	@Override
	public AField getDeclaredField(String name) {
		return original.getDeclaredField(name);
	}

	@Override
	public Map<Integer, Method> getMethods() {
		return original.getMethods();
	}
}
