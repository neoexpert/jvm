package com.cupvm.jsc;

import com.cupvm.LoggerImpl;
import com.cupvm.Logger;
import com.cupvm.jvm.ArrayTypes;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm.field.FieldHead;
import com.cupvm.jvm._class.BootstrapMethod;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.method.*;
import com.cupvm.jvm._class.Class;
import com.cupvm.jsc.jvalue.*;

import java.util.*;

public class ByteCodeToJS extends ByteCodeProcessor {
	private static final Logger logger= LoggerImpl.get();
	//tNE = throwNewException
	//_NP = java/lang/NullPointerException
	//_AE = java/lang/AritheticException

	//prints some comments to the compiled js code
	public static boolean jsComments = false;
	//insert "debugger;" in catch blocks
	public static boolean debugOnException=false;
	//enable jdwp
	public final boolean debug;
	public final String classLoaderID;
	public boolean multiThreaded=true;
	protected SyntheticClassLoader classLoader;
	public ByteCodeToJS(SyntheticClassLoader classLoader, Class cC, Method method, boolean debug){
		super(cC,method);
		this.debug=debug;
		if(debug)
			optimiseStack=false;
		this.classLoader=classLoader;
		String clid=classLoader.getJSName();
		if(clid==null)
			this.classLoaderID="rcl";
		else
			this.classLoaderID=clid;
	}

	public final JSFunction compile(){
		JSFunction result = new JSFunction(method, classLoaderID, debug);
		result.optimizeStack=this.optimiseStack;
		result.classes.add(method.clazz.getID());
		if(debug)result.usesFrame=true;
		short[] code = method.code;
		if(code == null)
			return result;
		Constant[] contantPool=cC.constantPool;
		MethodHead mt=method.mh;
		char[] args=mt.mt.args;
		int argsLength=args.length;
		boolean isStatic=method.isStatic();
		genHead(method, isStatic, argsLength, result);
		if(!isStatic)
			++argsLength;
		switch(code.length){
			case 5:
				if(IS_GETTER(code)){
					result.type = JSFunction.GETTER;
					Constant fc = contantPool[i(code[2], code[3])];
					fc = contantPool[fc.index2];
					Constant fname = contantPool[fc.index1];
					Constant ftype = contantPool[fc.index2];
					result.fieldname = fname.str;
					result.fieldtype = ftype.str;
				}
				break;
			case 6:
				if(IS_SETTER(code)){
					result.type = JSFunction.SETTER;
					Constant fc = contantPool[i(code[3], code[4])];
					fc = contantPool[fc.index2];
					Constant fname = contantPool[(fc.index1)];
					Constant ftype = contantPool[(fc.index2)];
					result.fieldname = fname.str;
					result.fieldtype = ftype.str;
				}
				break;
		}
		OP[] ocode=new OP[code.length];
		result.setCode(ocode);
		JValue[] lv = new JValue[method.getMaxLocals()];
		FixedStack s = new FixedStack(method.max_stack);
		if(!method.isStatic())
			lv[0] = new JValue(1);
		compile(result, ocode, 0, ocode.length, lv, s, new Path());

		if(result.etable!=null)
			for(MException e : result.etable){
				s = new FixedStack(method.max_stack);
				s.push(1);
				compile(result, ocode, e.handler_pc, ocode.length, lv, s, new Path());
			}
		if(result.mayHaveBraces())
			return new BracedJSFunction(result);
		else result.createJumpLabels();
		return result;
	}
	private boolean optimiseStack=true;
	public class Path extends ArrayList<Integer>{
		public Path(){}	
		public Path(Path path){
			super(path);
		}	
	}
	HashSet<Path> processedPaths=new HashSet<>();

	public final void compile(JSFunction result, OP[] ocode, int pc, int length, JValue[] lv, FixedStack s, Path path){
		path.add(pc);
		boolean processed=ocode[pc]!=null;
		if(processed){
			if(processedPaths.contains(path))
				return;
			processedPaths.add(path);
			if(!optimiseStack)
				return;
			lv=mergeLV(lv, ocode[pc].lv);
			s.merge(ocode[pc].opstack);
			logger.info("processing another path");
			if(processedPaths.size()>128){
				result.optimizeStack=false;
				optimiseStack=false;
				return;
			}
			System.out.println(path);
			System.out.println(method.clazz.getName());
			System.out.println(method.signature);
		}
		short[] code = method.code;
		Constant[] contantPool=cC.constantPool;
		int i1, i2, len;
		JValue v1, v2, v3, v4;
		int jumpTarget, jumpOffset;
		String source;
		Constant c0;
		JValue[] invokeTemplate;
		try{
			while(pc < length){
				//if(ocode[pc]!=null)
	//				return;
				boolean npcheck;
				OP op=ocode[pc];
				if(op==null){
					op = new OP(pc, code[pc]);
					ocode[pc]=op;
				}
				op.setState(lv.clone(), s.clone());

				///*
				if(jsComments){
					StringBuilder opNameSB = new StringBuilder();
					ByteCodePrinter.parseOp(pc, code, opNameSB);
					op.name=opNameSB.toString();
					//System.out.println(op.name);
				}
				//*/


				StringBuilder opCode = new StringBuilder();
				StringBuilder codeSuffix=new StringBuilder();
				switch(code[pc]){
					case NOP:
						++pc;
						break;
					case (short) ACONST_NULL:
						opCode
							.append("s")
							.append(s.size())
							.append("=null;");
						s.push("null", (Number)null);
						pc += 1;
						break;
					case (short) ICONST_M1:
						opCode
							.append("s")
							.append(s.size())
							.append("=-1;");
						s.push("-1", -1);
						pc += 1;
						break;
					case (short) ICONST_0:
						opCode
							.append("s")
							.append(s.size())
							.append("=0;");
						s.push("0", 0);
						pc += 1;
						break;
					case (short) ICONST_1:
						opCode
							.append("s")
							.append(s.size())
							.append("=1;");
						s.push("1", 1);
						pc += 1;
						break;
					case (short) ICONST_2:
						opCode
							.append("s")
							.append(s.size())
							.append("=2;");
						s.push("2", 2);
						pc += 1;
						break;
					case (short) ICONST_3:
						opCode
							.append("s")
							.append(s.size())
							.append("=3;");
						s.push("3", 3);
						pc += 1;
						break;
					case (short) ICONST_4:
						opCode
							.append("s")
							.append(s.size())
							.append("=4;");
						s.push("4", 4);
						pc += 1;
						break;
					case (short) ICONST_5:
						opCode
							.append("s")
							.append(s.size())
							.append("=5;");
						s.push("5", 5);
						pc += 1;
						break;
					case (short) LCONST_0:
						opCode
							.append("s")
							.append(s.size())
							.append("=0n;");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) LCONST_1:
						opCode
							.append("s")
							.append(s.size())
							.append("=1n;");
						s.push(1);
						s.push(0);
						pc += 1;
						break;
					case (short) FCONST_0:
						opCode
							.append("s")
							.append(s.size())
							.append("=0.0;");
						s.push("0.0", 0f);
						pc += 1;
						break;
					case (short) FCONST_1:
						opCode
							.append("s")
							.append(s.size())
							.append("=1.0;");
						s.push("1.0", 1f);
						pc += 1;
						break;
					case (short) FCONST_2:
						opCode
							.append("s")
							.append(s.size())
							.append("=2.0;");
						s.push("2.0", 2f);
						pc += 1;
						break;
					case (short) DCONST_0:
						opCode
							.append("s")
							.append(s.size())
							.append("=0.0;");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) DCONST_1:
						opCode
							.append("s")
							.append(s.size())
							.append("=1.0;");
						s.push(1);
						s.push(0);
						pc += 1;
						break;
					case (short) BIPUSH:
						i1=(byte) code[pc + 1];
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append(i1)
							.append(";");

						s.push(i1,i1);
						pc += 2;
						break;
					case (short) SIPUSH:
						i1 = i(code[pc + 1], code[pc + 2]);
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append(i1)
							.append(";");
						s.push(i1,i1);
						pc += 3;
						break;
					case (short) LDC:
						int id = (int) code[pc + 1] & 0xFF;
						c0 = contantPool[(id)];
						switch(c0.tag){
							case Constant.CString:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append("JString.intern(\"")
									.append(escape(c0.str))
									.append("\");");
								s.push(1);
								break;
							case Constant.CFloat:
								opCode
										.append("s")
										.append(s.size())
										.append("=")
										.append(c0.value)
										.append(";");
								s.push(c0.value, c0.value);
								break;
							case Constant.CInteger:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(c0.value)
									.append(";");
								s.push(c0.value, c0.value);
								break;
							case Constant.CClass:
								result.async=true;
								Constant cref = contantPool[c0.index1];
								String cname = cref.str;
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append("await getClassInstance(\"").append(cname).append("\");");

								s.push(1);
								break;
							default:
								throw new RuntimeException("wrong constant type");
						}
						pc += 2;
						break;
					case (short) LDC_W:
						id = ii(code[pc + 1], code[pc + 2]);
						c0 = contantPool[(id)];
						switch(c0.tag){
							case Constant.CString:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append("JString.intern(\"")
									.append(escape(c0.str))
									.append("\");");
								s.push(1);
								break;
							case Constant.CFloat:
								opCode
										.append("s")
										.append(s.size())
										.append("=")
										.append(c0.value)
										.append(";");
								s.push(c0.value, c0.value);
								break;
							case Constant.CInteger:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(c0.value)
									.append(";");
								s.push(c0.value,c0.value);
								break;
							case Constant.CClass:
								result.async=true;
								Constant cref = contantPool[c0.index1];
								String cname = cref.str;
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append("await getClassInstance(\"").append(cname).append("\");");
								s.push(1);
								break;
							default:
								throw new RuntimeException("wrong constant type");
						}
						pc += 3;
						break;
					case (short) LDC2_W:
						id = ii(code[pc + 1], code[pc + 2]);
						c0 = contantPool[(id)];
						switch(c0.tag){
							case Constant.CLong:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(c0.value)
									.append("n;");
								s.push(c0.value+"n",c0.value);
								s.push(0);
								break;
							case Constant.CDouble:
								opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(c0.value)
									.append(";");
								s.push(c0.value,c0.value);
								s.push(0);
								break;
							default:
								throw new RuntimeException("wrong constant type");
						}
						pc += 3;
						break;
					case (short) ILOAD:
						short sindex=code[pc + 1];
						source=iload(opCode, sindex, s.size());
						s.push(source,lv[sindex]);
						pc += 2;
						break;
					case (short) LLOAD:
						sindex=code[pc + 1];
						source=lload(opCode, sindex, s.size());
						s.push(source, lv[sindex]);
						s.push(0);
						pc += 2;
						break;
					case (short) FLOAD:
						sindex=code[pc + 1];
						source=fload(opCode, sindex, s.size());
						s.push(source,lv[sindex]);
						pc += 2;
						break;
					case (short) DLOAD:
						sindex=code[pc + 1];
						source=dload(opCode, sindex, s.size());
						s.push(source, lv[sindex]);
						s.push(0);
						pc += 2;
						break;
					case (short) ALOAD:
						sindex=code[pc + 1];
						source=aload(opCode,sindex,s.size());
						s.push(source, lv[sindex]);
						pc += 2;
						break;
					case (short) ILOAD_0:
						iload(opCode, 0, s.size());
						s.push("v0",lv[0]);
						pc += 1;
						break;
					case (short) ILOAD_1:
						iload(opCode, 1, s.size());
						s.push("v1",lv[1]);
						pc += 1;
						break;
					case (short) ILOAD_2:
						iload(opCode, 2, s.size());
						s.push("v2",lv[2]);
						pc += 1;
						break;
					case (short) ILOAD_3:
						iload(opCode, 3, s.size());
						s.push("v3",lv[3]);
						pc += 1;
						break;
					case (short) LLOAD_0:
						lload(opCode, 0, s.size());
						s.push("v0",lv[0]);
						s.push(0);
						pc += 1;
						break;
					case (short) LLOAD_1:
						lload(opCode, 1, s.size());
						s.push("v1",lv[1]);
						s.push(0);
						pc += 1;
						break;
					case (short) LLOAD_2:
						lload(opCode, 2, s.size());
						s.push("v2",lv[2]);
						s.push(0);
						pc += 1;
						break;
					case (short) LLOAD_3:
						lload(opCode, 3, s.size());
						s.push("v3",lv[3]);
						s.push(0);
						pc += 1;
						break;
					case (short) FLOAD_0:
						fload(opCode, 0, s.size());
						s.push("v0",lv[0]);
						pc += 1;
						break;
					case (short) FLOAD_1:
						fload(opCode, 1, s.size());
						s.push("v1",lv[1]);
						pc += 1;
						break;
					case (short) FLOAD_2:
						fload(opCode, 2, s.size());
						s.push("v2",lv[2]);
						pc += 1;
						break;
					case (short) FLOAD_3:
						fload(opCode, 3, s.size());
						s.push("v3",lv[3]);
						pc += 1;
						break;
					case (short) DLOAD_0:
						dload(opCode, 0, s.size());
						s.push("v0",lv[0]);
						s.push(0);
						pc += 1;
						break;
					case (short) DLOAD_1:
						dload(opCode, 1, s.size());
						s.push("v1",lv[1]);
						s.push(0);
						pc += 1;
						break;
					case (short) DLOAD_2:
						dload(opCode, 2, s.size());
						s.push("v2",lv[2]);
						s.push(0);
						pc += 1;
						break;
					case (short) DLOAD_3:
						dload(opCode, 3, s.size());
						s.push("v3",lv[3]);
						s.push(0);
						pc += 1;
						break;
					case (short) ALOAD_0:
						aload(opCode,0,s.size());
						s.push("v0",lv[0]);
						pc += 1;
						break;
					case (short) ALOAD_1:
						aload(opCode,1, s.size());
						s.push("v1",lv[1]);
						pc += 1;
						break;
					case (short) ALOAD_2:
						aload(opCode,2, s.size());
						s.push("v2",lv[2]);
						pc += 1;
						break;
					case (short) ALOAD_3:
						aload(opCode,3,s.size());
						s.push("v3",lv[3]);
						pc += 1;
						break;
					case (short) IALOAD:
					case (short) FALOAD:
					case (short) AALOAD:
					case (short) BALOAD:
					case (short) CALOAD:
					case (short) SALOAD:
						//index
						String indexSource = s.popSource();
						if(!optimiseStack)
							indexSource="s"+s.size();
						//array
						source = s.popSource();
						if(!optimiseStack)
							source="s"+s.size();
						npcheck = source==null;
						if(!optimiseStack)
							source="s"+s.size();
						op.js=null;
						if(npcheck)
							opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(source).append("[").append(indexSource).append("];");
						else
							opCode
									.append("s")
									.append(s.size())
									.append("=")
									.append(source).append("[").append(indexSource).append("];");

						s.push(0);
						pc += 1;
						break;
					case (short) LALOAD:
					case (short) DALOAD:
						result.addVariable("h1");
						result.addVariable("arr");
						//index
						source=s.popSource();
						if(!optimiseStack)
							source="s"+s.size();
						//array
						npcheck = s.pop().value == null;
						op.js=null;
						if(npcheck)
							opCode
								.append("arr=")
								.append("s")
								.append(s.size())
								.append(";")
								.append("s")
								.append(s.size())
								.append("=arr["+source+"];");
						else
							opCode
									.append("arr=")
									.append("s")
									.append(s.size())
									.append(";")
									.append("s")
									.append(s.size())
									.append("=arr["+source+"];");
						s.push(0);
						s.push(0);

						pc += 1;
						break;
					case (short) ISTORE:
						sindex=code[pc + 1];
						JValue jhelper = lv[sindex] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						istore(opCode, sindex, s.size(), source);
						pc += 2;
						break;
					case (short) LSTORE:
						sindex=code[pc + 1];
						s.pop();
						jhelper=lv[sindex] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						lstore(opCode,sindex, s.size(), source);
						pc += 2;
						break;
					case (short) FSTORE:
						sindex=code[pc + 1];
						jhelper=lv[sindex] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						fstore(opCode,sindex, s.size(), source);
						pc += 2;
						break;
					case (short) DSTORE:
						sindex=code[pc + 1];
						s.pop();
						jhelper=lv[sindex] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						dstore(opCode,sindex, s.size(), source);
						pc += 2;
						break;
					case (short) ASTORE:
						sindex=code[pc + 1];
						jhelper=lv[sindex] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						astore(opCode,sindex, s.size(), source);
						pc += 2;
						break;
					case (short) ISTORE_0:
						jhelper=lv[0] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						istore(opCode, 0, s.size(), source);
						pc += 1;
						break;
					case (short) ISTORE_1:
						jhelper=lv[1] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						istore(opCode, 1, s.size(), source);
						pc += 1;
						break;
					case (short) ISTORE_2:
						jhelper=lv[2] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						istore(opCode, 2, s.size(),source);
						pc += 1;
						break;
					case (short) ISTORE_3:
						jhelper=lv[3] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						istore(opCode, 3, s.size(),source);
						pc += 1;
						break;
					case (short) LSTORE_0:
						s.pop();
						jhelper=lv[0] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						lstore(opCode,0, s.size(),source);
						pc += 1;
						break;
					case (short) LSTORE_1:
						s.pop();
						jhelper=lv[1] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						lstore(opCode,1, s.size(),source);
						pc += 1;
						break;
					case (short) LSTORE_2:
						s.pop();
						jhelper=lv[2] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						lstore(opCode,2, s.size(),source);
						pc += 1;
						break;
					case (short) LSTORE_3:
						s.pop();
						jhelper=lv[3] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						lstore(opCode,3, s.size(),source);

						pc += 1;
						break;
					case (short) FSTORE_0:
						jhelper=lv[0] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						fstore(opCode,0, s.size(),source);
						pc += 1;
						break;
					case (short) FSTORE_1:
						jhelper=lv[1] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						fstore(opCode,1, s.size(),source);
						pc += 1;
						break;
					case (short) FSTORE_2:
						jhelper=lv[2] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						fstore(opCode,2, s.size(),source);
						pc += 1;
						break;
					case (short) FSTORE_3:
						jhelper=lv[3] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						fstore(opCode,3, s.size(),source);
						pc += 1;
						break;
					case (short) DSTORE_0:
						s.pop();
						jhelper=lv[0] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						dstore(opCode,0, s.size(),source);

						pc += 1;
						break;
					case (short) DSTORE_1:
						s.pop();
						jhelper=lv[1] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						dstore(opCode,1, s.size(),source);
						pc += 1;
						break;
					case (short) DSTORE_2:
						s.pop();
						jhelper=lv[2] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						dstore(opCode,2, s.size(),source);
						pc += 1;
						break;
					case (short) DSTORE_3:
						s.pop();
						jhelper=lv[3] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						dstore(opCode,3, s.size(),source);
						pc += 1;
						break;
					case (short) ASTORE_0:
						jhelper=lv[0] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						astore(opCode,0, s.size(),source);
						pc += 1;
						break;
					case (short) ASTORE_1:
						jhelper=lv[1] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						astore(opCode,1, s.size(),source);
						pc += 1;
						break;
					case (short) ASTORE_2:
						jhelper=lv[2] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						astore(opCode,2, s.size(),source);
						pc += 1;
						break;
					case (short) ASTORE_3:
						jhelper=lv[3] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
						astore(opCode,3, s.size(),source);
						pc += 1;
						break;
					case (short) IASTORE:
					case (short) FASTORE:
					case (short) AASTORE:
					case (short) CASTORE:
					case (short) BASTORE:
					case (short) SASTORE:
						result.addVariable("h1");
						result.addVariable("arr");
						//value
						s.pop();
						opCode
							.append("h1=s")
							.append(s.size())
							.append(";");
						//index
						source=s.popSource();
						if(!optimiseStack)
							source="s"+s.size();
						//array
						npcheck = s.pop().value == null;
						op.js=null;
						opCode
							.append("arr=s")
							.append(s.size())
							.append(";");
						if(npcheck)
							op.donpcheck = "arr";
						/*
							 codeSuffix
							 .append("if(!arr){f.pc=")
							 .append(pc)
							 .append(";")
							 .append("await f.tNE(stack,_NP,\"iastore\");")
							 .append("if(stack.entries.length!=depth) throw \"EXCEPTION\";")
							 .append("pc=f.pc;continue;")
							 .append("}");
							 */
						codeSuffix
							.append("arr["+source+"]=h1;");
						pc += 1;
						break;
					case (short) LASTORE:
					case (short) DASTORE:
						result.addVariable("h1");
						result.addVariable("arr");
						//value
						s.pop();
						s.pop();
						op.js=null;
						opCode
							.append("h1=")
							.append("s")
							.append(s.size())
							.append(";");
						//index
						source=s.popSource();
						if(!optimiseStack)
							source="s"+s.size();
						//array
						npcheck = s.pop().value == null;
						opCode
							.append("arr=")
							.append("s")
							.append(s.size())
							.append(";");
						if(npcheck)
							op.donpcheck = "arr";
						op.name="AASTORE";
						/*
							 codeSuffix
							 .append("if(!arr){f.pc=")
							 .append(pc)
							 .append(";")
							 .append("await f.tNE(stack,_NP,\"iastore\");")
							 .append("if(stack.entries.length!=depth) throw \"EXCEPTION\";")
							 .append("pc=f.pc;continue;")
							 .append("}");
							 */
						codeSuffix
							.append("arr["+source+"]=h1;");


						pc += 1;
						break;
					case (short) POP:
						s.pop();
						//jcode
						//		.append("f.o();");
						pc += 1;
						break;
					case (short) POP2:
						s.pop();
						s.pop();
						pc += 1;
						break;
					case (short) DUP:
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size()-1)
							.append(";");
						s.push(s.peek());

						pc += 1;
						break;
					case (short) DUP_X1:
						result.addVariable("h1");
						result.addVariable("h2");
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("s")
							.append(s.size()-2)
							.append("=h1;")
							.append("s")
							.append(s.size()-1)
							.append("=h2;")
							.append("s")
							.append(s.size())
							.append("=h1;");
						v1 = s.pop();
						v2 = s.pop();
						s.push(v1);
						s.push(v2);
						s.push(v1);

						pc += 1;
						break;
					case (short) DUP_X2:
						result.addVariable("h1");
						result.addVariable("h2");
						result.addVariable("h3");
						opCode
							.append("h1=")	
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("h3=")
							.append("s")
							.append(s.size()-3)
							.append(";")
							.append("s")
							.append(s.size()-3)
							.append("=h1;")
							.append("s")
							.append(s.size()-2)
							.append("=h3;")
							.append("s")
							.append(s.size()-1)
							.append("=h2;")
							.append("s")
							.append(s.size())
							.append("=h1;");
						v1 = s.pop();
						v2 = s.pop();
						v3 = s.pop();
						s.push(v1);
						s.push(v3);
						s.push(v2);
						s.push(v1);
						pc += 1;
						break;
					case (short) DUP2:
						result.addVariable("h1");
						result.addVariable("h2");
						opCode
							.append("h2=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h1=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("s")
							.append(s.size()-2)
							.append("=h1;")
							.append("s")
							.append(s.size()-1)
							.append("=h2;")
							.append("s")
							.append(s.size())
							.append("=h1;")
							.append("s")
							.append(s.size()+1)
							.append("=h2;");
						v2 = s.pop();
						v1 = s.pop();
						s.push(v1);
						s.push(v2);
						s.push(v1);
						s.push(v2);
						pc += 1;
						break;
					case (short) DUP2_X1:
						result.addVariable("h1");
						result.addVariable("h2");
						result.addVariable("h3");
						opCode
							.append("h2=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h1=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("h3=")
							.append("s")
							.append(s.size()-3)
							.append(";")
							.append("s")
							.append(s.size()-3)
							.append("=h1;")
							.append("s")
							.append(s.size()-2)
							.append("=h2;")
							.append("s")
							.append(s.size()-1)
							.append("=h3;")
							.append("s")
							.append(s.size())
							.append("=h1;")
							.append("s")
							.append(s.size()+1)
							.append("=h2;");
						v2 = s.pop();
						v1 = s.pop();
						v3 = s.pop();
						s.push(v1);
						s.push(v2);
						s.push(v3);
						s.push(v1);
						s.push(v2);
						pc += 1;
						break;
					case (short) DUP2_X2:
						result.addVariable("h1");
						result.addVariable("h2");
						result.addVariable("h3");
						result.addVariable("h4");
						opCode
							.append("h2=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h1=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("h4=")
							.append("s")
							.append(s.size()-3)
							.append(";")
							.append("h3=")
							.append("s")
							.append(s.size()-4)
							.append(";")
							.append("s")
							.append(s.size()-4)
							.append("=h1;")
							.append("s")
							.append(s.size()-3)
							.append("=h2;")
							.append("s")
							.append(s.size()-2)
							.append("=h3;")
							.append("s")
							.append(s.size()-1)
							.append("=h4;")
							.append("s")
							.append(s.size())
							.append("=h1;")
							.append("s")
							.append(s.size()+1)
							.append("=h2;");
						v2 = s.pop();
						v1 = s.pop();
						v4 = s.pop();
						v3 = s.pop();
						s.push(v1);
						s.push(v2);
						s.push(v3);
						s.push(v4);
						s.push(v1);
						s.push(v2);
						pc += 1;
						break;
					case (short) SWAP:
						result.addVariable("h1");
						result.addVariable("h2");
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size()-2)
							.append(";")
							.append("s")
							.append(s.size()-2)
							.append("=h1;")
							.append("s")
							.append(s.size()-1)
							.append("=h2;");
						v1 = s.pop();
						v2 = s.pop();
						s.push(v1);
						s.push(v2);
						pc += 1;
						break;
					case (short) IADD:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("(s")
							.append(s.size())
							.append("+")
							.append("s")
							.append(s.size()+1)
							.append(")|0;");
						s.push(0);
						pc += 1;
						break;
					case (short) LADD:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=BigInt.asIntN(64,")
							.append("s")
							.append(s.size())
							.append("+")
							.append("s")
							.append(s.size()+2)
							.append(");");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) FADD:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("+")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) DADD:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("+")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) ISUB:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("(s")
							.append(s.size())
							.append("-")
							.append("s")
							.append(s.size()+1)
							.append(")|0;");
						s.push(0);
						pc += 1;
						break;
					case (short) LSUB:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=BigInt.asIntN(64,")
							.append("s")
							.append(s.size())
							.append("-")
							.append("s")
							.append(s.size()+2)
							.append(");");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) FSUB:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("-")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) DSUB:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("-")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IMUL:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("Math.imul(s")
							.append(s.size())
							.append(",")
							.append("s")
							.append(s.size()+1)
							.append(");");
						s.push(0);
						pc += 1;
						break;
					case (short) LMUL:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=BigInt.asIntN(64,")
							.append("s")
							.append(s.size())
							.append("*")
							.append("s")
							.append(s.size()+2)
							.append(");");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) FMUL:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("*")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) DMUL:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("*")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IDIV:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("if(h1===0)throw ae(t, "+pc+","+op.op+");")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=(h2/h1)|0;");
						s.push(0);
						pc += 1;
						break;
					case (short) LDIV:
						result.async=true;
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("if(h1===0)throw ae(t, "+pc+","+op.op+");")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2/h1;");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) FDIV:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2/h1;");
						s.push(0);
						pc += 1;
						break;
					case (short) DDIV:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2/h1;");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IREM:
						result.async=true;
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("if(h1===0)throw ae(t, "+pc+","+op.op+");")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=(h2%h1)|0;");
						s.push(0);
						pc += 1;
						break;
					case (short) LREM:
						result.async=true;
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("if(h1===0)throw ae(t, "+pc+","+op.op+");")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2%h1;");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) FREM:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("%")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) DREM:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("%")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) INEG:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("-s")
							.append(s.size()-1)
							.append(";");
						pc += 1;
						break;
					case (short) LNEG:
						s.pop();
						s.pop();
						s.push(0);
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-2)
							.append("=")
							.append("-s")
							.append(s.size()-2)
							.append(";");
						pc += 1;
						break;
					case (short) FNEG:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("-s")
							.append(s.size()-1)
							.append(";");
						pc += 1;
						break;
					case (short) DNEG:
						s.pop();
						s.pop();
						s.push(0);
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-2)
							.append("=")
							.append("-s")
							.append(s.size()-2)
							.append(";");
						pc += 1;
						break;
					case (short) ISHL:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2<<h1;");
						s.push(0);
						pc += 1;
						break;
					case (short) LSHL:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2<<BigInt(h1);");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) ISHR:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2>>h1;");
						s.push(0);
						pc += 1;
						break;
					case (short) LSHR:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2>>BigInt(h1);");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IUSHR:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							.append("=h2>>>h1;");
						s.push(0);
						pc += 1;
						break;
					case (short) LUSHR:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("s")
							.append(s.size())
							//not posible
							//.append("=h2>>>BigInt(h1);");
							.append("=h2>>BigInt(h1);");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IAND:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("&")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) LAND:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("&")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IOR:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("|")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) LOR:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("|")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IXOR:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("^")
							.append("s")
							.append(s.size()+1)
							.append(";");
						s.push(0);
						pc += 1;
						break;
					case (short) LXOR:
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("^")
							.append("s")
							.append(s.size()+2)
							.append(";");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) IINC:
						sindex=code[pc + 1];
						byte increment=(byte)code[pc + 2];
						iinc(opCode, sindex, increment);
						if(lv[sindex]!=null)
							lv[sindex].source=null;
						pc += 3;
						break;
					case (short) I2L:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=BigInt(s")
							.append(s.size()-1)
							.append(");");
						s.push(0);
						pc += 1;
						break;
					case (short) I2F:
						s.pop();
						s.push(0);
						pc += 1;
						break;
					case (short) I2D:
						s.pop();
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) L2I:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=Number(BigInt.asIntN(64, s")
							.append(s.size())
							.append("));");
						s.push(0);
						pc += 1;
						break;
					case (short) L2F:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=Number(BigInt.asIntN(64, s")
							.append(s.size())
							.append("));");
						s.push(0);
						pc += 1;
						break;
					case (short) L2D:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=Number(BigInt.asIntN(64, s")
							.append(s.size())
							.append("));");
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) F2I:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("s")
							.append(s.size()-1)
							.append("|0;");
						pc += 1;
						break;
					case (short) F2L:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("BigInt(s")
							.append(s.size()-1)
							.append("|0);");
						s.push(0);
						pc += 1;
						break;
					case (short) F2D:
						s.pop();
						s.push(0);
						s.push(0);
						pc += 1;
						break;
					case (short) D2I:
						s.pop();
						s.pop();
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("s")
							.append(s.size())
							.append("|0;");
						s.push(0);
						pc += 1;
						break;
					case (short) D2L:
						s.pop();
						s.pop();
						s.push(0);
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-2)
							.append("=BigInt(s")
							.append(s.size()-2)
							.append("|0);");
						pc += 1;
						break;
					case (short) D2F:
						s.pop();
						s.pop();
						s.push(0);
						pc += 1;
						break;
					case (short) I2B:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("(s")
							.append(s.size()-1)
							.append("<<24)>>24;");
						pc += 1;
						break;
					case (short) I2C:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("(s")
							.append(s.size()-1)
							.append("<<16)>>16;");
						pc += 1;
						break;
					case (short) I2S:
						s.pop();
						s.push(0);
						opCode
							.append("s")
							.append(s.size()-1)
							.append("=")
							.append("(s")
							.append(s.size()-1)
							.append("<<16)>>16;");
						pc += 1;
						break;
					case (short) LCMP:
						result.addVariable("h1");
						result.addVariable("h2");
						s.pop();
						s.pop();
						s.pop();
						s.pop();

						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("if(h2===h1){")
							.append("s")
							.append(s.size())
							.append("=0;}else if(h2>h1){")
							.append("s")
							.append(s.size())
							.append("=1;}else ")
							.append("s")
							.append(s.size())
							.append("=-1;");
						s.push(0);
						pc += 1;
						break;
					case (short) FCMPL:
						result.addVariable("h2");
						result.addVariable("h1");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("if(h2===h1){")
							.append("s")
							.append(s.size())
							.append("=0;}else if(h2>h1){")
							.append("s")
							.append(s.size())
							.append("=1;}else if(h2<h1){")
							.append("s")
							.append(s.size())
							.append("=-1;} else ")
							.append("s")
							.append(s.size())
							.append("=-1;");
						s.push(0);
						pc += 1;
						break;
					case (short) FCMPG:
						result.addVariable("h2");
						result.addVariable("h1");
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+1)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("if(h2===h1){")
							.append("s")
							.append(s.size())
							.append("=0;}else if(h2>h1){")
							.append("s")
							.append(s.size())
							.append("=1;}else if(h2<h1){")
							.append("s")
							.append(s.size())
							.append("=-1;} else ")
							.append("s")
							.append(s.size())
							.append("=1;");
						s.push(0);
						pc += 1;
						break;
					case (short) DCMPL:
						result.addVariable("h2");
						result.addVariable("h1");
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("if(h2===h1){")
							.append("s")
							.append(s.size())
							.append("=0;}else if(h2>h1){")
							.append("s")
							.append(s.size())
							.append("=1;}else if(h2<h1){")
							.append("s")
							.append(s.size())
							.append("=-1;} else ")
							.append("s")
							.append(s.size())
							.append("=-1;");
						s.push(0);
						pc += 1;
						break;
					case (short) DCMPG:
						result.addVariable("h2");
						result.addVariable("h1");
						s.pop();
						s.pop();
						s.pop();
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size()+2)
							.append(";")
							.append("h2=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("if(h2===h1){")
							.append("s")
							.append(s.size())
							.append("=0;}else if(h2>h1){")
							.append("s")
							.append(s.size())
							.append("=1;}else if(h2<h1){")
							.append("s")
							.append(s.size())
							.append("=-1;} else ")
							.append("s")
							.append(s.size())
							.append("=1;");
						s.push(0);
						pc += 1;
						break;
					case (short) IFEQ:
					case (short) IFNE:
					case (short) IFLT:
					case (short) IFGE:
					case (short) IFGT:
					case (short) IFLE:
						s.pop();
						op.stackpos=s.size();
						jumpOffset=i(code[pc + 1], code[pc + 2]);
						if(jumpOffset<=0){
							result.hasBackwardsJumps = true;
							if(multiThreaded){
								result.async=true;
								opCode
									.append("if(--t.s<=0){t.s=t.p;await st();}");
							}
						}
						jumpTarget = pc + jumpOffset;
						op.jumpTarget =jumpTarget;
						result.addJumpRange(pc, jumpOffset);
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
						pc += 3;
						break;
					case (short) IF_ICMPEQ:
					case (short) IF_ICMPNE:
					case (short) IF_ICMPLT:
					case (short) IF_ICMPGE:
					case (short) IF_ICMPGT:
					case (short) IF_ICMPLE:
					case (short) IF_ACMPEQ:
					case (short) IF_ACMPNE:
						s.pop();
						s.pop();
						op.stackpos=s.size();
						jumpOffset=i(code[pc + 1], code[pc + 2]);
						if(jumpOffset<=0){
							result.hasBackwardsJumps = true;
							if(multiThreaded){
								result.async=true;
								opCode
									.append("if(--t.s<=0){t.s=t.p;await st();}");
							}
						}
						jumpTarget = pc + jumpOffset;
						op.jumpTarget =jumpTarget;
						result.addJumpRange(pc, jumpOffset);
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(),new Path(path));
						pc += 3;
						break;
					case (short) GOTO:
						result.hasGOTO = true;
						jumpOffset=i(code[pc + 1], code[pc + 2]);
						if(jumpOffset<=0){
							result.hasBackwardsJumps = true;
							if(multiThreaded){
								result.async=true;
								opCode
									.append("if(--t.s<=0){t.s=t.p;await st();}");
							}
						}
						jumpTarget = pc + jumpOffset;
						result.addJumpRange(pc, jumpOffset);
						opCode
							.append("pc=")
							.append(jumpTarget)
							.append(";")
							.append("continue;");
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
						op.js = opCode.toString();
						pc += 3;
						return;
					case (short) JSR:
					case (short) RET:
						pc += 3;
						throw new RuntimeException("ret");
						//break;
					case (short) TABLESWITCH:
						result.hasSwitches=true;
						int pc2 = pc + (4 - pc % 4);
						int default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
						int def_jump = pc + default_offset;
						s.pop();
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, def_jump, length, lv.clone(), s.clone(), new Path(path));
						result.putJumpLabel(def_jump);
						pc2 += 4;
						int low = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
						pc2 += 4;
						int high = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
						pc2 += 4;

						result.addVariable("jumps");
						opCode
							.append("jumps=[");
						String prefix = "";
						for(i1 = low; i1 <= high; ++i1){
							//pc + (i1 - low) * 4;
							jumpTarget = pc + i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
							compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
							pc2 += 4;
							opCode.append(prefix);
							prefix = ",";
							opCode
								.append(jumpTarget);
							result.putJumpLabel(jumpTarget);
						}
						opCode
							.append("];");
						//pc+=4;
						int n = high - low + 1;

						result.addVariable("i");
						opCode
							.append("i=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode.append("if(i<")
							.append(low)
							.append("||i>")
							.append(high)
							.append("){pc=")
							.append(def_jump)
							.append(";continue;}else {pc=jumps[i- ")
							.append(low).append("];continue;}");
						pc = pc2;
						op.js = opCode.toString();

						return;
					case (short) LOOKUPSWITCH:
						result.hasSwitches=true;
						pc2 = pc + (4 - pc % 4);
						default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
						pc2 += 4;
						i1 = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
						pc2 += 4;
						result.addVariable("i");
						s.pop();
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, pc+default_offset, length, lv.clone(), s.clone(), new Path(path));
						opCode
							.append("i=")
							.append("s")
							.append(s.size())
							.append(";");

						for(i2 = 0; i2 < i1; ++i2){
							int key = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);

							pc2 += 4;
							int value = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
							jumpTarget = pc + value;
							compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
							result.putJumpLabel(jumpTarget);
							opCode.append("if(i===").append(key).append("){pc=").append(jumpTarget).append(";continue;}");
							pc2 += 4;
						}
						jumpTarget = pc + default_offset;
						result.putJumpLabel(jumpTarget);
						opCode.append("pc=").append(jumpTarget).append(";continue;");
						pc = pc2;
						op.js = opCode.toString();
						return;
					case (short) IRETURN:
						pc += 1;
						opCode.append("t.pop();");
						s.pop();
						opCode
							.append("return ")
							.append("s")
							.append(s.size())
							.append(";");
						op.js = opCode.toString();
						return;
					case (short) LRETURN:
						pc += 1;
						opCode.append("t.pop();");
						s.pop();
						s.pop();
						opCode
							.append("return ")
							.append("s")
							.append(s.size())
							.append(";");
						op.js = opCode.toString();
						return;
					case (short) FRETURN:
						pc += 1;
						opCode.append("t.pop();");
						s.pop();
						opCode
							.append("return ")
							.append("s")
							.append(s.size())
							.append(";");
						op.js = opCode.toString();
						return;
					case (short) DRETURN:
						pc += 1;
						opCode.append("t.pop();");
						s.pop();
						s.pop();
						opCode
							.append("return ")
							.append("s")
							.append(s.size())
							.append(";");
						op.js = opCode.toString();
						return;
					case (short) ARETURN:
						pc += 1;
						opCode.append("t.pop();");
						result.returns(s.pop().value);
						opCode
							.append("return ")
							.append("s")
							.append(s.size())
							.append(";");
						op.js = opCode.toString();
						return;
					case (short) RETURN:
						pc += 1;
						opCode
							.append("t.pop();")
							.append("return;");
						op.js = opCode.toString();
						return;
					case (short) GETSTATIC:
						result.async=true;
						c0 = contantPool[(ii(code[pc + 1], code[pc + 2]))];
						FieldHead fh= (FieldHead) c0.data;
						String classname = c0.type;
						if(classname == null)
							throw new RuntimeException("getstatic: " + c0.str);
						op.clazz = c0.clazz;
						opCode
							.append("s")
							.append(s.size())
							.append("=")
							.append("await clazz.gs(\"")
							.append(c0.str)
							.append("\");");
						s.push(0);
						if(fh.wide){
							s.push(0);
						}
						pc += 3;
						break;
					case (short) PUTSTATIC:
						result.async=true;

						c0 = contantPool[(ii(code[pc + 1], code[pc + 2]))];
						fh= (FieldHead) c0.data;
						classname = c0.type;
						if(classname == null)
							throw new RuntimeException("putstatic: " + c0.str);
						op.clazz = c0.clazz;
						if(fh.wide){
							source=s.pop2Source();
						}
						else
							source=s.popSource();
						if(!optimiseStack)
							source="s"+s.size();
						opCode
							.append("clazz.ps(\"")
							.append(c0.str)
							.append("\",")
							.append(source)
							.append(");");
						pc += 3;
						break;
					case (short) GETFIELD:
						c0 = contantPool[(i(code[pc + 1], code[pc + 2]))];
						fh= (FieldHead) c0.data;
						npcheck = s.pop().value == null;

						result.addVariable("c");
						opCode
							.append("c=")
							.append("s")
							.append(s.size())
							.append(";");
						if(npcheck)
							op.donpcheck="c";
						op.name="GETFIELD";
						codeSuffix
							.append("s")
							.append(s.size())
							.append("=c.fields[\"")
							.append(c0.str)
							.append("\"];");
						s.push(0);
						if(fh.wide)
							s.push(0);
						pc += 3;
						break;
					case (short) PUTFIELD:
						c0 = contantPool[(i(code[pc + 1], code[pc + 2]))];
						fh= (FieldHead) c0.data;
						result.addVariable("o1");
						if(fh.wide)
							s.pop();
						npcheck = s.pop().value == null;
						opCode
							.append("o1 = ")
							.append("s")
							.append(s.size())
							.append(";");

						result.addVariable("c");
						s.pop();
						opCode
							.append("c=")
							.append("s")
							.append(s.size())
							.append(";");
						if(npcheck)
							op.donpcheck="c";
						op.name="PUTFIELD";
						codeSuffix
							.append("c.fields[\"")
							.append(c0.str)
							.append("\"]=o1;");

						pc += 3;
						break;
					case (short) INVOKEVIRTUAL:
						result.usesFrame=true;
						result.async=true;
						id = ii(code[pc + 1], code[pc + 2]);
						MethodHead mh = cC.getMethodHead(id);
						AMethod m = cC.getMethod(id);
						if(m!=null) {
							if(!mh.isLoaded) {
								mh.isNative = m.isNative;
								mh.nativeSignature =
									mh.classname.replaceAll("/", "_")
									+ "_"
									+ mh.name;
								mh.clazz=m.clazz;
								mh.isLoaded=true;
							}
							if (m instanceof SyntheticMethod)
								op.sm = (SyntheticMethod) m;
						}
						op.clazz=mh.clazz;

						result.addMethodDependecy(mh);
						len=mh.mt.args.length;
						invokeTemplate=new JValue[len+1];
						s.copyHead(invokeTemplate);
						op.mergeInvokeTemplate(invokeTemplate);

						s.setSize(s.size() - len);
						npcheck = s.pop().value == null;
						op.stackpos=s.size();
						if(npcheck)
							op.donpcheck = "oref";
						for(int i = 0; i < mh.mt.rsize; i++)
							s.push((String)null, (Number)null);
						op.method = mh;

						result.addVariable("m");
						result.addVariable("oref");
						pc += 3;
						break;
					case (short) INVOKESPECIAL:
						result.usesFrame=true;
						result.async=true;
						id = ii(code[pc + 1], code[pc + 2]);
						mh = cC.getMethodHead(id);
						m = cC.getMethod(id);
						if(m!=null) {
							if(!mh.isLoaded) {
								mh.isNative = m.isNative;
								mh.nativeSignature =
									mh.classname.replaceAll("/", "_")
									+ "_"
									+ mh.name;
								mh.isLoaded=true;
								mh.clazz=m.clazz;
							}
							if (m instanceof SyntheticMethod)
								op.sm = (SyntheticMethod) m;
						}
						op.clazz=mh.clazz;
						result.addMethodDependecy(mh);
						len=mh.mt.args.length;
						invokeTemplate=new JValue[len+1];
						s.copyHead(invokeTemplate);
						op.mergeInvokeTemplate(invokeTemplate);

						s.setSize(s.size() - mh.mt.args.length);
						s.pop();
						op.stackpos=s.size();
						classname = mh.classname;
						int a = 0;
						for(int i = 0; i < mh.mt.rsize; i++)
							s.push((String)null, (Number)null);


						result.addVariable("m");
						result.addVariable("oref");
						op.method = mh;


						pc += 3;
						break;
					case (short) INVOKESTATIC:
						result.usesFrame=true;
						result.async=true;
						id = ii(code[pc + 1], code[pc + 2]);
						mh = cC.getMethodHead(id);
						if(mh == null){
							throw new RuntimeException("method not found cC: " + cC.getName() + " id: " + id);
						}

						m = cC.getMethod(id);
						if(m!=null) {
							if(!mh.isLoaded) {
								mh.isNative = m.isNative;
								mh.nativeSignature =
									mh.classname.replaceAll("/", "_")
									+ "_"
									+ mh.name;
								mh.clazz=m.clazz;
								mh.isLoaded=true;
							}
							if (m instanceof SyntheticMethod)
								op.sm = (SyntheticMethod) m;
						}
						op.clazz=mh.clazz;

						result.addMethodDependecy(mh);
						len=mh.mt.args.length;
						invokeTemplate=new JValue[len];
						s.copyHead(invokeTemplate);
						op.mergeInvokeTemplate(invokeTemplate);
						//classname = mh.classname;
						op.method = mh;
						s.setSize(s.size() - mh.mt.args.length);
						op.stackpos=s.size();
						a = 0;
						for(int i = 0; i < mh.mt.rsize; i++)
							s.push((String)null, (Number)null);



						pc += 3;
						break;
					case (short) INVOKEINTERFACE:
						result.usesFrame=true;
						result.async=true;
						mh = cC.getMethodHead(ii(code[pc + 1], code[pc + 2]));
						result.addMethodDependecy(mh);
						short count = code[pc + 3];
						short zero = code[pc + 4];
						len=mh.mt.args.length;
						invokeTemplate=new JValue[len+1];
						s.copyHead(invokeTemplate);
						op.mergeInvokeTemplate(invokeTemplate);
						s.setSize(s.size() - mh.mt.args.length);


						result.addVariable("oref");
						npcheck = s.pop().value == null;
						opCode
							.append("oref=")
							.append("s")
							.append(s.size())
							.append(";");
						op.stackpos=s.size();
						for(int i = 0; i < mh.mt.rsize; i++)
							s.push((String)null, (Number)null);
						if(npcheck)
							op.donpcheck="oref";

						result.addVariable("m");
						codeSuffix
							.append("m = await oref.clazz.getMethodByName(\"")
							.append(mh.signature)
							.append("\");if(!m){f.pc=")
							.append(pc)
							.append(";return true;}");

						pc += 5;

						codeSuffix
							.append("t.push(new Frame(m));f.pc=")
							.append(pc-5)
							.append(";");
						result.beforeInvocation(codeSuffix);
						result.genInvocation("m", codeSuffix, 1+mh.mt.args.length,  mh.mt.rtype, op.stackpos,op.invokeTemplate, true, optimiseStack);
						result.afterInvocation(codeSuffix);
						break;
					case (short) INVOKEDYNAMIC:
						result.usesFrame=true;
						result.async=true;
						Constant dyn = contantPool[(i(code[pc + 1], code[pc + 2]))];
						BootstrapMethod bm = cC.getMethodHandle(dyn.index1);
						Constant[] arguments = bm.arguments;
						result.addVariable("oref");
						result.addVariable("args");
						opCode.append("args=new Array(").append(arguments.length).append(");");
						for(int i=0;i<arguments.length;++i){
							Constant arg=arguments[i];
							switch(arg.tag){
								case Constant.CString:
									Constant utf8=contantPool[arg.index1];
									opCode.append("args[")
										.append(i)
										.append("]={type:")
										.append(arg.tag)
										.append(", value:\"")
										.append(utf8.str)
										.append("\"};");
									break;
								case Constant.CMethodType:
									Constant c3=contantPool[arg.index1];
									opCode.append("args[")
										.append(i)
										.append("]={type:")
										.append(arg.tag)
										.append(", mtype:\"")
										.append(c3.str)
										.append("\"};");
									break;
								case Constant.CMethodHandle:
									Constant _mh=contantPool[arg.index2];
									System.out.println(_mh);
									String cname = contantPool[(contantPool[(_mh.index1)].index1)].str;
									AClass clazz = classLoader.loadClass(cname);
									Constant mnameandtype = contantPool[(_mh.index2)];
									String msignature = contantPool[(mnameandtype.index1)].str + contantPool[(mnameandtype.index2)].str;
									opCode.append("args[")
										.append(i)
										.append("]={type:")
										.append(arg.tag)
										.append(", cname:\"")
										.append(cname)
										.append("\",")
										.append("signature:\"")
										.append(msignature)
										.append("\",")
										.append("cl:\"")
										.append(((SyntheticClassLoader)clazz.getClassLoader()).getJSName())
										.append("\"};");
									break;
								default:
									throw new RuntimeException("unknown invokedynamic argument type");
							}
						}
						result.addVariable("clazz");
						result.addVariable("m");
						result.addVariable("callsite");
						opCode.append("clazz=await ");
						opCode.append("rcl");
						opCode.append(".loadClass(\"")
							.append(bm.mclazz).append("\",false);");
						opCode
							.append("m=await clazz.getMethodByName(\"")
							.append(bm.mname)
							.append(bm.mtype)
							.append("\");");
						Constant nameandtype = contantPool[dyn.index2];
						String mname= contantPool[nameandtype.index1].str;
						String mtype = contantPool[nameandtype.index2].str;
						opCode
							.append("callsite=await invokeDynamic(m, \"")
							.append(mname)
							.append("\", args, \"")
							.append(mname)
							.append(mtype)
							.append("\");");
						switch(bm.referenceKind){
							case REF_invokeStatic:
								opCode
									.append("m=await callsite.fields.target.clazz.method;")
									.append("if(!m.native)")
									.append("t.push(new Frame(m));")
									.append("f.pc=")
									.append(pc)
									.append(";");
								MethodType _mt = new MethodType(mtype);
								len=_mt.args.length;
								invokeTemplate=new JValue[len];
								s.copyHead(invokeTemplate);
								op.mergeInvokeTemplate(invokeTemplate);
								for(int i = 0; i < _mt.args.length; i++)
									s.pop();
								result.genInvocation("m", opCode,  _mt.args.length,_mt.rtype, s.size(), op.invokeTemplate, true,false);
								for(int i = 0; i < _mt.rsize; i++)
									s.push((String)null, (Number)null);
								break;
							default:
								throw new RuntimeException("invokedynamic reference kind not implemented: "+bm.referenceKind);
						}
						pc += 5;
						break;
					case (short) NEW:
					    AClass clazz = (Class) cC.getClass(i(code[pc + 1], code[pc + 2]));
					    if(clazz==null) {
							classname = cC.getClassName(i(code[pc + 1], code[pc + 2]));
							logger.error("class not found: ", classname);
							return;
						}
						//classname = cC.getClassName(i(code[pc + 1], code[pc + 2]));
						result.addClassDependecy(clazz.getName());
						op.clazz = clazz;
						opCode
							.append("s")
							.append(s.size())
							.append("=new ClassInstance(clazz);");
						s.push(1);
						pc += 3;
						break;
					case (short) NEWARRAY:
						result.addVariable("h1");
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("s")
							.append(s.size());
						switch((byte) code[pc + 1]){
							case ArrayTypes.T_BOOLEAN:
								opCode
									.append("=nZa(h1);");
								break;
							case ArrayTypes.T_BYTE:
								opCode
									.append("=nBa(h1);");
								break;
							case ArrayTypes.T_SHORT:
								opCode
									.append("=nSa(h1);");
								break;
							case ArrayTypes.T_CHAR:
								opCode
									.append("=nCa(h1);");
								break;
							case ArrayTypes.T_INT:
								opCode
									.append("=nIa(h1);");
								break;
							case ArrayTypes.T_FLOAT:
								opCode
									.append("=nFa(h1);");
								break;
							case ArrayTypes.T_DOUBLE:
								opCode
									.append("=nDa(h1);");
								break;
							case ArrayTypes.T_LONG:
								opCode
									.append("=nJa(h1);");
								break;
							default:
								throw new RuntimeException("unknown array type");
						}
						s.push(1);
						pc += 2;
						break;
					case (short) ANEWARRAY:
						result.addVariable("h1");
						s.pop();
						opCode
							.append("h1=")
							.append("s")
							.append(s.size())
							.append(";");
						opCode
							.append("s")
							.append(s.size())
							.append("=nLa(h1);");
						s.push(1);
						pc += 3;
						break;
					case (short) ARRAYLENGTH:
						s.pop();
						s.push(0);
						result.addVariable("arr");
						opCode
							.append("arr=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("s")
							.append(s.size()-1)
							.append("=arr.length;");
						pc += 1;
						break;
					case (short) ATHROW:
						result.usesFrame=true;
						result.async=true;
						s.pop();
						result.async=true;
						result.addVariable("e");
						opCode
							.append("f.pc=")
							.append(pc)
							.append(";")
							.append("e=")
							.append("s")
							.append(s.size())
							.append(";")
							.append("if(e===null){e=await __NE(t,_NP);}");
						if(result.hasETable)
							opCode
								.append("if(f.checkETable(e, t)){")
								.append("pc=f.pc;continue;s0=e;}")
								.append("else{")
								.append("t.pop();")
								.append("throw e;")
								.append("}");
						else
							opCode
								.append("t.pop();")
								.append("throw e;");
						pc += 1;
						op.js = opCode.toString();
						return;
					case (short) CHECKCAST:
						result.async=true;
						int index = ii(code[pc + 1], code[pc + 2]);
						c0 = contantPool[index];
						clazz=cC.getClass(index);
						c0 = contantPool[(c0.index1)];
						if(!c0.str.startsWith("[")){
							op.clazz = clazz;
							result.addVariable("o");
							opCode
								.append("o=")
								.append("s")
								.append(s.size()-1)
								.append(";");

							opCode.append("if(o instanceof ClassInstance){if(!o.clazz.isInstance(clazz)){throw await __NE(t,\"java/lang/ClassCastException\",\"\");return false;}}");
						}
						pc += 3;
						break;
					case (short) INSTANCEOF:
						index = ii(code[pc + 1], code[pc + 2]);
						c0 = contantPool[index];
						c0 = contantPool[(c0.index1)];
						result.addVariable("o");
						clazz=cC.getClass(index);
						op.clazz = clazz;
						opCode
							.append("o=")
							.append("s")
							.append(s.size()-1)
							.append(";")
							.append("if(!o){")
							.append("s")
							.append(s.size()-1)
							.append("=0;")
							.append("}else{if(o.clazz.isInstance(clazz))")
							.append("s")
							.append(s.size()-1)
							.append("=1;")
							.append("else ")
							.append("s")
							.append(s.size()-1)
							.append("=0;")
							.append("}");
						pc += 3;
						break;
					case (short) MONITORENTER:
						result.async=true;
						if(multiThreaded)
							opCode
								.append("await monitorEnter(")
								.append("s")
								.append(s.size()-1)
								.append(",t);");
						s.pop();
						pc += 1;
						break;
					case (short) MONITOREXIT:
						if(multiThreaded)
							opCode
								.append("monitorExit(")
								.append("s")
								.append(s.size()-1)
								.append(");");
						s.pop();
						pc += 1;
						break;
					case (short) WIDE:
						pc += 1;
						switch(code[pc]){
							case (short) ILOAD:
								index = ii(code[pc + 1], code[pc + 2]);
								source=iload(opCode, index, s.size());
								s.push(source, lv[index]);
								pc += 3;
								break;
							case (short) LLOAD:
								index = ii(code[pc + 1], code[pc + 2]);
								source=lload(opCode, index, s.size());
								s.push(source,lv[index]);
								s.push(0);
								pc += 3;
								break;
							case (short) FLOAD:
								index = ii(code[pc + 1], code[pc + 2]);
								source=fload(opCode, index, s.size());
								s.push(source,lv[index]);
								pc += 3;
								break;
							case (short) DLOAD:
								index = ii(code[pc + 1], code[pc + 2]);
								source=dload(opCode, index, s.size());
								s.push(source,lv[index]);
								s.push(0);
								pc += 3;
								break;
							case (short) ALOAD:
								index = ii(code[pc + 1], code[pc + 2]);
								source=aload(opCode, index, s.size());
								s.push(source,lv[index]);
								pc += 3;
								break;
							case (short) ISTORE:
								index = ii(code[pc + 1], code[pc + 2]);
						jhelper=lv[index] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
								istore(opCode,index, s.size(),source);
								pc += 3;
								break;
							case (short) LSTORE:
								index = ii(code[pc + 1], code[pc + 2]);
								s.pop();
						jhelper=lv[index] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
								lstore(opCode,index, s.size(),source);
								pc += 3;
								break;
							case (short) FSTORE:
								index = ii(code[pc + 1], code[pc + 2]);
						jhelper=lv[index] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
								fstore(opCode,index, s.size(),source);
								pc += 3;
								break;
							case (short) DSTORE:
								index = ii(code[pc + 1], code[pc + 2]);
								s.pop();
						jhelper=lv[index] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
								dstore(opCode, index, s.size(),source);
								pc += 3;
								break;
							case (short) ASTORE:
								index = ii(code[pc + 1], code[pc + 2]);
						jhelper=lv[index] = s.pop();
						if(jhelper.source==null||!optimiseStack)
							source="s"+s.size();
						else
							source=jhelper.source;
								astore(opCode,index, s.size(),source);
								pc += 3;
								break;
							case (short) IINC:
								index = ii(code[pc + 1], code[pc + 2]);
								int increment16 = ii(code[pc + 3], code[pc + 4]);
								iinc(opCode, index, increment16);
								if(lv[index]!=null)
									lv[index].source=null;
								pc += 5;
								break;

						}
						break;
					case (short) MULTIANEWARRAY:
						i1 = i(code[pc + 1], code[pc + 2]);
						c0 = contantPool[i1];
						c0 = contantPool[c0.index1];
						//dimensions
						i2 = code[pc + 3];
						result.addVariable("sizes");
						result.addVariable("h3");
						opCode
							.append("sizes=[");
						prefix="";
						s.setSize(s.size()-i2);
						for(int i=0;i<i2;++i){
							opCode
								.append(prefix)
								.append("s")
								.append(s.size()+i);
							prefix=",";
						}
						opCode
							.append("];")
							.append("s")
							.append(s.size())
							.append("=multianewarray(")
							.append("'")
							.append(c0.str.charAt(c0.str.length()-1))
							.append("'")
							.append(",sizes,0);");
						s.push(1);
						pc += 4;
						break;
					case (short) IFNULL:
						s.pop();
						op.stackpos=s.size();
						jumpOffset=i(code[pc + 1], code[pc + 2]);
						if(jumpOffset<=0){
							result.hasBackwardsJumps = true;
							if(multiThreaded){
								result.async=true;
								opCode
									.append("if(--t.s<=0){t.s=t.p;await st();}");
							}
						}
						jumpTarget = pc + jumpOffset;
						op.jumpTarget =jumpTarget;
						result.addJumpRange(pc, jumpOffset);
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
						pc += 3;
						break;
					case (short) IFNONNULL:
						s.pop();
						op.stackpos=s.size();
						jumpOffset=i(code[pc + 1], code[pc + 2]);
						if(jumpOffset<=0){
							result.hasBackwardsJumps = true;
							if(multiThreaded){
								result.async=true;
								opCode
									.append("if(--t.s<=0){t.s=t.p;await st();}");
							}
						}
						jumpTarget = pc + jumpOffset;
						op.jumpTarget =jumpTarget;
						result.addJumpRange(pc, jumpOffset);
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
						pc += 3;
						break;
					case (short) GOTO_W:
						result.hasGOTO = true;
						jumpOffset=i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
						jumpTarget = pc+jumpOffset;
						result.addJumpRange(pc, jumpOffset);
						opCode.append("pc=")
							.append(jumpTarget)
							.append(";")
							.append("continue;");
						//s.fillZeros();
						//fillLV(lv);
						compile(result, ocode, jumpTarget, length, lv.clone(), s.clone(), new Path(path));
						pc += 5;
						op.js = opCode.toString();
						return;
					case (short) JSR_W:
						jumpTarget = i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
						pc += 5;
						throw new RuntimeException("JSR");
						//break;
					default:
						pc += 1;
						throw new RuntimeException("unknown opcode");
				}
				if(op.js==null){
					op.js = opCode.toString();
					op.suffix = codeSuffix.toString();
				}
			}
		}
		catch(Exception ex){
			throw new RuntimeException(method.getMyClass().getSourceFileName()+ ":"+method.getCurrentLine(pc),ex);
		}



	}

	private static JValue[] mergeLV(JValue[] lv, JValue[] lv1) {
		for(int i=0;i< lv.length;++i){
			if(lv1[i]==null||lv[i]==null||lv1[i].equals(lv[i]))
				lv[i]=null;
		}
		return lv;
	}


	protected void genHead(Method method,boolean isStatic, int argsLength, JSFunction result) {
		for(int i=argsLength; i<method.max_locals;++i)
			if(isStatic)
				result.addVariable("v"+i);
			else
				result.addVariable("v"+(i+1));
	}

	protected void iinc(StringBuilder jcode, int index, int increment) {
			jcode
					.append("v")
					.append(index)
					.append("+=")
					.append(increment)
					.append(";");
	}

	protected String dload(StringBuilder jcode, int index, int stackpos) {
			jcode
					.append("s")
					.append(stackpos)
					.append("=v")
					.append(index)
					.append(";");
		return "v"+index;
	}

	protected String lload(StringBuilder jcode, int index, int stackpos) {
			jcode
					.append("s")
					.append(stackpos)
					.append("=v")
					.append(index)
					.append(";");
		return "v"+index;
	}

	protected String fload(StringBuilder jcode, int index, int stackpos) {
			jcode
					.append("s")
					.append(stackpos)
					.append("=v")
					.append(index)
					.append(";");
		return "v"+index;
	}

	protected String iload(StringBuilder jcode, int index, int stackpos) {
			jcode
					.append("s")
					.append(stackpos)
					.append("=v")
					.append(index)
					.append(";");
		return "v"+index;
	}
	protected String aload(StringBuilder jcode, int index, int stackpos) {
		jcode
			.append("s")
			.append(stackpos)
			.append('=')
			.append('v')
			.append(index)
			.append(';');
		return "v"+index;
	}

	protected void istore(StringBuilder sb, int index, int stackpos, String source){
		    sb
					.append("v")
					.append(index)
					.append("=s")
					.append(stackpos)
					.append(";");
	}

	protected void fstore(StringBuilder sb, int index, int stackpos, String source) {
			sb
					.append("v")
					.append(index)
					.append("=s")
					.append(stackpos)
					.append(";");
	}
	protected void lstore(StringBuilder sb, int index, int stackpos, String source) {
			sb
					.append("v")
					.append(index)
					.append("=s")
					.append(stackpos)
					.append(";");
	}
	protected void dstore(StringBuilder sb, int index, int stackpos, String source) {
			sb
					.append("v")
					.append(index)
					.append("=s")
					.append(stackpos)
					.append(";");
	}

	protected void astore(StringBuilder sb, int index, int stackpos, String source) {
			sb
					.append("v")
					.append(index)
					.append("=s")
					.append(stackpos)
					.append(";");
	}


	private static String escape(String s){
		StringBuilder sb=new StringBuilder();
		for(char c:s.toCharArray()){
			switch(c){
				case '\"':
					sb.append("\\\"");
					break;
				case '\n':
					sb.append("\\n");
					break;
				case '\r':
					sb.append("\\r");
					break;
				case 0:
					sb.append("\\0");
					break;
				case '\b':
					sb.append("\\b");
					break;
				case '\t':
					sb.append("\\t");
					break;
				default:
					sb.append(c);
			}	
		}
		return sb.toString();
	}
}
