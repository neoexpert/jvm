import com.cupvm.jsc.SyntheticClassLoader;
import js.*;
import js.event.*;
import static js.dom.DOM.*;
import js.dom.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import com.cupvm.jsjvm.*;;

public class DOMMain{
	public static int counter=0;
	
	public static void main(String[] args)throws Throwable{

		long startTime=System.currentTimeMillis();
		//marks current thread as UI-Thread (needed to receive events)
		DOM.registerThread();
		System.loadLibrary("invoke");
		System.out.println("hello world");
		JVMTest.main(null);
		HashMap<String, String> hm=new HashMap<>();
		hm.put("hello","world");
		System.out.println("hello "+hm.get("hello"));
		final DOMElement body=getElementById("body");

		final DOMElement b=createElement("button");
		new Thread(new Runnable(){
			@Override
			public void run(){
				for(int i=0;i<20000;++i){
					///*
					try{
						Thread.sleep(1000);
					}catch(InterruptedException e){
						System.out.println("thread is interrupted");
						return;
					}
					//*/
					b.setContent("hello from thread "+counter++);
				}
			}
		}).start();
		b.setContent("hello world");
		js.Console.log("hello world");
		b.css("background","gray");
		b.css("width","100%");
		b.css("height","100px");
		Object waiter=new Object();
		new Thread(new Runnable(){
			@Override
			public void run(){
				while(true){
					synchronized(waiter){
						try{	
							waiter.wait();
						}catch(InterruptedException e){
							System.out.println("interrupted");
							continue;
						}
					}
					System.out.println("notified");
				}
			}
		}).start();
		EventListener evl=
			new EventListener(){
				public void handle(JSElement e, JSEvent event){
					body.appendChild(b.cloneNode(true));
					synchronized(waiter){
						waiter.notify();
					}
				}

			};
		b.addEventListener("click",evl);
		body.appendChild(b);

		/*
			 try{
			 readFile();
			 }
			 catch(IOException e){
			 e.printStackTrace();
			 }*/

		int duration= (int) (System.currentTimeMillis()-startTime);

		System.out.println("took: "+duration+" ms");
		JSObject localStorage = getWindow().get("localStorage");
		if(localStorage.has("min")){
			int min=localStorage.getInt("min");
			if(duration<min){
				System.out.println("new best time: "+duration);
				localStorage.setInt("min",duration);
			}
		}
		else
			localStorage.setInt("min",duration);

		while(true){
			Runnable ev=DOM.getNextEvent();
			ev.run();
		}
	}
        private static void readFile()throws IOException{
          File f=new File("huhufile.txt");
          
          FileInputStream fis=new FileInputStream(f);
          byte[] buf=new byte[(int)f.length()];
          fis.read(buf);
          
          Console.log(new String(buf));
					fis.close();
        }
        
}
