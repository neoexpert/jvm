import js.*;
import js.event.*;
import static js.dom.DOM.*;
import js.dom.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import com.cupvm.jsjvm.*;

public class Main{
	public static void main(String[] args)throws Throwable{
		if(!DOM.isInitialized()){
			//DOM is not initialized
			JVMServer.startThisJar(args);
			return;
		}
		else DOMMain.main(args);
	}
        
}
