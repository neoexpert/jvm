;;;;lisp is awesome

(require "lib.lisp")
(use-package :mylib)
(println (libHello))
(println '(+ 5 5))
(make-array 5 :element-type float)
(println (+ 1 (+ 1 1 1 1 1 1) 1 (+ 1 1)))
(println "huhu")
(println "last")
(write (/ 8 2))
(defvar in 0)
(defvar x 0)
(println "x:")
(println x)
(defun gethello () "hello from fun")
(println (gethello))

(defun main () 
	(println "before defun")
	(defun foo (x y) (println x) (println y))
	(println "before foo")
	(foo "lala" 1)
	(foo "blub" 3)
	(foo 42 2)
	(foo 43 5);comment

	(loop 
		(println x)
		(incf x)
		(when (> x 10) return)
		)
	;another comment

	(println "hello \"world\"")
	(handler-case (open "main.lis")
		(error (c) (println c)))
	(setq in (open "main.lisp"))
	(close in)
	)


(main)
(main)
(main)
(main)
(main)
(main)
(debugger)
(println (quote (main)))
(read-from-string "(println \"hi\")")
(println "before crash")

(handler-case (read-from-string "(println \"hi)")
	(error (c) (println c)))

(handler-case (/ 3 0)
  (division-by-zero (c)
		    (format t "Caught division by zero: ~a~%" c)))

(read-from-string "(println \"end\")")
(defclass point ()
  (x y z))
(println (type-of point))
(defvar p1 (make-instance point))
(println (type-of p1))

;(defvar day 3)
;(case day
;(1 (println 1))
;(2 (println 2))
;(3 (println 3)))
;(println (cons 1 2))
(defvar out (open "test.out"))
(write "text" out)
(close out)
(defvar out (open "test.out"))
(write (read-char out))
(println "end")
