import lisp.*;
import java.io.*;
public class Main{
	public static void main(String ... args)throws Exception{
		if(args==null||args.length==0){
			shell();
			return;
		}
		FileInputStream fis=new FileInputStream(args[0]);
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		byte[] buf=new byte[1024];
		int len;
		while((len=fis.read(buf))>0){
			baos.write(buf, 0, len);
		}
		String lisp=new String(baos.toByteArray());
		//System.out.println(lisp);
		new LispParser(lisp)
			.setArgs(args)
			.evaluate();
		/*
		   for(String arg:args){
		   new LispParser(arg).evaluate();
		   }*/
	}

	public static void shell()throws Exception{
		Console console=System.console();
		if(console==null){
			StringBuilder lisp=new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String line;
			while ((line = in.readLine()) != null) {
				lisp.append(line);
				lisp.append('\n');
			}
			new LispParser(lisp.toString()).evaluate();
			return;
		}
		LispParser lisp=new LispParser("");
		String input;
		while((input = console.readLine())!=null){
			System.out.println(lisp.evaluate(input));
		}
	}
}
