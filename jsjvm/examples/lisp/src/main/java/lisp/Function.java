package lisp;
import java.util.*;

public class Function extends Symbol{
	public Function(String name){
		super(name);
	}


	@Override
	public Cons evaluate(Cons rest, Context ctx){
		return this;
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(")
			.append(name)
			.append(' ');
			Cons lisp=rest;
			String prefix="";
			while(lisp!=null){
				sb.append(prefix);
				prefix=" ";
				sb.append(lisp.getFirst().toString());
				lisp=lisp.getRest();
			}
			sb.append(')');
		return sb.toString();
	}
}
