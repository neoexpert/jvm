package lisp;
import lisp.primitives.*;
import lisp.functions.*;
import java.io.*;

public class LispParser{
	static{
		//ensure initialisation of Symbol class
		Symbol.class.getName();
	}
	private int pos=0;
	private int line=0;
	private final char[] arr;
	private final Context ctx;

	public LispParser(File f, Context ctx)throws IOException{
		this.ctx=ctx;
		int length=(int)f.length();
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		FileInputStream fis=new FileInputStream(f);
		byte[] buf=new byte[1024];
		int readen;
		while((readen=fis.read(buf))!=-1){
			baos.write(buf, 0, readen);
		}
		this.arr=new String(baos.toByteArray()).toCharArray();
	}

	public LispParser(String lisp){
		this.arr=lisp.toCharArray();
		this.ctx=new Context();
		ctx.putPackage(new DefaultPackage());
		ctx.setPackage("cl");
		IOStream io=new IOStreamImpl(System.in, System.out);	
		ctx.setGlobal("*standard-output*", io);
		ctx.setGlobal("*standard-input*", io);
	}

	public LispParser(String lisp, Context ctx){
		this.arr=lisp.toCharArray();
		this.ctx=ctx;
	}

	public LispParser setArgs(String ... args){
			Cons largs=new Cons();
			ctx.setGlobal("*args*", largs);
			if(args.length==0)
				return this;
			largs.first=new LispString(args[0]);
				
			for(int i=1;i<args.length;++i){
				largs=largs.add(new LispString(args[i]));
			}
			return this;
	}

	private int skipWhiteSpace(int pos){
		while(pos<arr.length){
			char c=arr[pos];
			switch(c){
				case '\n':
					++line;
				case ' ':
				case '\t':
				case '\r':
					++pos;
					continue;
				case ';':
					pos=skipComment(pos);
					continue;
				default:
					return pos;
			}
		}
		return pos;
	}

	private int skipComment(int pos){
		while(pos<arr.length){
			char c=arr[pos];
			if(c=='\n'){
				++line;
				return pos;
			}
			++pos;
		}
		return pos;
	}

	public Cons parse(){
		Cons res=null;
		pos=skipWhiteSpace(pos);
		while(pos<arr.length){
			res=readLisp(arr, pos+1);
			pos=skipWhiteSpace(pos);
		}
		return res;
	}

	public Cons evaluate(){
		Cons res=null;
		pos=skipWhiteSpace(pos);
		while(pos<arr.length){
			Cons cons=readLisp(arr, pos);
			res=cons.evaluate(cons.rest, ctx);
			pos=skipWhiteSpace(pos);
		}
		return res;
	}

	public Cons evaluate(String lisp){
		return new LispParser(lisp, ctx).evaluate();
	}

	public Cons readLisp(char[] arr, int pos){
		while(pos<arr.length){
			char c=arr[pos++];
			switch(c){
				case '(':
					Cons cons=new Cons();
					cons.setFirst(readLisp(arr, pos));
					pos=this.pos;
					if(cons.first==null)
						return Symbol.NIL;
					Cons rest=cons;
					Cons next;
					while((next=readLisp(arr, pos))!=null){
						rest=rest.add(next);
						pos=this.pos;
					}
					
					return cons;
				case ')':
					this.pos=pos;
					return null;
				case '\n':
					++line;
				case ' ':
				case '\t':
				case '\r':
					pos=skipWhiteSpace(pos);
					continue;
				case ';':
					pos=skipComment(pos);
					continue;
				case '\'':
						Quote quote = new Quote();
						quote.add(readLisp(arr, pos));
						return quote;
				case '#':
						return readLiteral(arr, pos);
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '.':
				case '-':
					return readPrimitive(arr, pos-1);
				case '"':
					return readString(arr, pos);
				default:
					return readSymbol(arr, pos-1);
			}
		}
		return null;
	}

	public Primitive readLiteral(char[] arr, int pos){
		boolean _float=false;
		loop:while(pos<arr.length){
			char c=arr[pos++];
			switch(c){
				case 'x':
					return readHex(arr, pos);
				default:
					throw new RuntimeException("illegal literal type: "+c);

			}
		}
		throw new RuntimeException("syntax error");
	}

	public Primitive readHex(char[] arr, int pos){
		boolean _float=false;
		StringBuilder sb=new StringBuilder();
		loop:while(pos<arr.length){
			char c=arr[pos++];
			switch(c){
				case '.':
					_float=true;
				case '-':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
					sb.append(c);
					continue;
				case '\n':
					++line;
					++pos;
				case ')':
					--pos;
				case ' ':
				case '\t':
				case '\r':
					break loop;
				default:
					throw new RuntimeException("illegal hex character: "+c);

			}
		}
		this.pos=pos;
		long l=Long.parseLong(sb.toString(),16);
		return new PLong(l);
	}

	public Primitive readPrimitive(char[] arr, int pos){
		boolean _float=false;
		StringBuilder sb=new StringBuilder();
		loop:while(pos<arr.length){
			char c=arr[pos++];
			switch(c){
				case '.':
					_float=true;
				case '-':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					sb.append(c);
					continue;
				case '\n':
					++line;
					++pos;
				case ')':
					--pos;
				case ' ':
				case '\t':
				case '\r':
					break loop;
						default:
						throw new RuntimeException("illegal number character: "+c);

			}
		}
		this.pos=pos;
		if(_float)
			return new PDouble(Double.valueOf(sb.toString()));
		else
			return new PInteger(Integer.valueOf(sb.toString()));
		
	}

	public LispString readString(char[] arr, int pos){
		StringBuilder unedcaped=new StringBuilder("\"");
		StringBuilder sb=new StringBuilder();
		LispString str;
loop:while(true){
			char c=arr[pos++];
			unedcaped.append(c);
			switch(c){
				case '"':break loop;
				case '\\':
					//escape
					c=arr[pos++];
					unedcaped.append(c);
					sb.append(c);
					continue;
				default:
					sb.append(c);
			}
		}
		str=new LispString(sb.toString(), unedcaped.toString());
		char c=arr[pos++];
		switch(c){
			case '\n':
					++line;
					++pos;
			case ')':
			    --pos;
			case ' ':
			case '\t':
			case '\r':
				this.pos=pos;
				return str;
			default:
				throw new RuntimeException("no white space after string Literal");
		}
	}

	public Symbol readSymbol(char[] arr, int pos){
		StringBuilder sb=new StringBuilder();
		while(true){
			char c=arr[pos++];
			switch(c){
				case '\n':
					++line;
					++pos;
				case ')':
					--pos;
				case ' ':
				case '\t':
				case '\r':
					this.pos=pos;
					return new Symbol(sb.toString());
				default:
					sb.append(c);
			}
		}
	}
}
