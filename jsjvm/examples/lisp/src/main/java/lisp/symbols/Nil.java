package lisp.symbols;
import lisp.*;

public class Nil extends Symbol{
	public Nil(){
		super("nil");
	}

	public Cons evaluate(Context ctx){
		return Symbol.NIL;
	}

	@Override
	public String toLispString(Context ctx){
		return "nil";
	}

	@Override
	public String toString() {
		return "nil";
	}
}
