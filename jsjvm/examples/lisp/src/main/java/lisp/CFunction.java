package lisp;
import java.util.*;

public class CFunction extends Function{
	private static final String[] EMPTY=new String[0];
	private String[] args;
	private Cons body;
	private Cons[] statements;
	private Cons lastStatement;
	public CFunction(String name, Cons args, Cons body){
		super(name);
		this.body=body;
		if(args==Symbol.NIL){
			this.args=EMPTY;
		}
		else{
			ArrayList<String> argsl=new ArrayList<>();
			while(args!=null){
				argsl.add(((Symbol)args.getFirst()).toString());
				if(args.getRest()==null)break;
				args=args.getRest();
			}
			String[] argsarr=new String[argsl.size()];
			for(int i=0;i<argsarr.length;++i)
				argsarr[i]=argsl.get(i);
			this.args=argsarr;
		}


		ArrayList<Cons> statementsl=new ArrayList<>();
		while(body!=null){
			statementsl.add(body.getFirst());
			if(body.getRest()==null)break;
			body=body.getRest();
		}
		int len=statementsl.size();
		if(len==0){
			return;
		}
		Cons[] statementsarr=new Cons[len-1];
		for(int i=0;i<statementsarr.length;++i)
			statementsarr[i]=statementsl.get(i);

		this.statements=statementsarr;
		this.lastStatement=statementsl.get(statementsl.size()-1);
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		if(lastStatement==null)return Symbol.NIL;
		ctx=ctx.push();
		for(String arg:args){
			ctx.setLocal(arg, rest.getFirst().evaluate(this.rest, ctx));	
			rest=rest.getRest();
		}
		Cons[] exps=this.statements;
		for(Cons exp:exps){
			exp.evaluate(exp.rest,   ctx);
		}
		return lastStatement.evaluate(lastStatement.rest, ctx);
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(")
			.append(name)
			.append(' ')
			//.append(_args)
			.append(' ');
			Cons lisp=body;
			String prefix="";
			while(lisp!=null){
				sb.append(prefix);
				prefix=" ";
				sb.append(lisp.getFirst().toString());
				lisp=lisp.getRest();
			}
		sb.append(')');
		return sb.toString();
	}
}
