package lisp;

public class LispString extends Atom {
	private final String value;
	private String quoted;
	public LispString(String value){
		this.value=value;
	}

	public LispString(String value, String quoted){
		this.value=value;
		//this.quoted=quoted;
	}

	@Override
	public String toLispString(Context ctx){
		return value;
	}

	public String toString(){
		if(quoted==null){
			quoted=new StringBuilder()
				.append('\"')
				.append(escape(value))
				.append('\"')
				.toString();
		}
		return quoted;
	}
	public static final LispString TYPE=new LispString("STRING");
	public LispString getType(){
		return TYPE;
	}

	private static String escape(String s){
		StringBuilder sb=new StringBuilder();
		for(char c:s.toCharArray()){
			switch(c){
				case '\"':
					sb.append("\\\"");
					break;
				case '\n':
					sb.append("\\n");
					break;
				case '\r':
					sb.append("\\r");
					break;
				case 0:
					sb.append("\\0");
					break;
				case '\b':
					sb.append("\\b");
					break;
				case '\t':
					sb.append("\\t");
					break;
				default:
					sb.append(c);
			}	
		}
		return sb.toString();
	}

	@Override
	public Cons evaluate(Cons rest, Context context){
		return this;
	}

	public void write(IOStream stream, Context ctx){
		stream.write(value.getBytes());	
	}

	@Override
	public String quote() {
		return toString();
	}
}
