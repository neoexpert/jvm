package lisp.primitives;
import lisp.*;

public class PInteger extends PNumber{
	private final int value;
	public PInteger(int value){
		this.value=value;
		type=TYPE;
	}

	public double doubleValue(){
		return value;
	}

	public int intValue(){
		return value;
	}

	public String toString(){
		return Integer.toString(value);
	}

	@Override
	public Primitive incf(){
		return new PInteger(value+1);
	}

	@Override
	public void write(IOStream stream, Context ctx){
		stream.writeInt(value);
	}

	public static final LispString TYPE=new LispString("INTEGER");
	@Override
	public LispString getType(){
		return TYPE;
	}

    public String toString(int radix) {
		return Integer.toString(value,radix);
    }

	@Override
	public Cons coerce(String type) {
	    switch(type){
			case "long":
			case "int64":
				return new PLong(value);
		}
		return super.coerce(type);
	}
}
