package lisp.primitives;
import lisp.*;

public class PChar extends Primitive{
	private final char value;
	public PChar(char value){
		this.value=value;
	}

	public double doubleValue(){
		return value;
	}
	public int intValue(){
		return value;
	}

	@Override
	public String toString(int radix) {
		return Integer.toString(value, radix);
	}

	public String toString(){
		return Character.toString(value);
	}

	@Override
	public Primitive incf(){
		return new PChar((char)(value+1));
	}

	@Override
	public void write(IOStream stream, Context ctx) {
		stream.writeChar(value);
	}

	public static final LispString TYPE=new LispString("CHAR");
	@Override
	public LispString getType(){
		return TYPE;
	}
}
