package lisp.primitives;
import lisp.*;

public class PDouble extends Primitive{
	private final double value;
	public PDouble(double value){
		this.value=value;
		type=TYPE;
	}

	public double doubleValue(){
		return value;
	}

	public int intValue(){
		return (int)value;
	}

	@Override
	public String toString(int radix) {
		if(radix==10)
			return Double.toString(value);
	    throw new RuntimeException("for double only base 10 is supported");
	}

	public String toString(){
		return Double.toString(value);
	}

	@Override
	public Primitive incf(){
		return new PDouble(value+1.0);
	}

	public static final LispString TYPE=new LispString("DOUBLE");

	@Override
	public void write(IOStream stream, Context ctx) {
	    stream.writeDouble(value);
	}

	@Override
	public LispString getType(){
		return TYPE;
	}
}
