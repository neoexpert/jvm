package lisp.primitives;

import lisp.*;

public class PLong extends PNumber {
    private final long value;

    public PLong(long value) {
        this.value = value;
        type = TYPE;
    }

    public double doubleValue() {
        return value;
    }

    public int intValue() {
        return (int) value;
    }

    public String toString() {
        return Long.toString(value);
    }

    @Override
    public Primitive incf() {
        return new PLong(value + 1L);
    }

    @Override
    public void write(IOStream stream, Context ctx) {
        stream.writeLong(value);
    }

    public static final LispString TYPE = new LispString("LONG_INTEGER");

    @Override
    public LispString getType() {
        return TYPE;
    }

    public String toString(int radix) {
        return Long.toString(value, radix);
    }

    @Override
    public Cons coerce(String type) {
        switch(type){
            case "int32":
                return new PInteger((int) value);
        }
        return super.coerce(type);
    }
}
