package lisp.functions.list;
import lisp.*;

public class First extends Function{
	public First(){
		super("first");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Cons first=rest.getFirst();
		if(first.getClass()==Symbol.class)
			first=first.resolve(ctx);
		return first.getFirst().evaluate(first.getRest(),ctx);
	}
}
