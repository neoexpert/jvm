package lisp.functions;
import lisp.*;

public class If extends Function{
	public If(){
		super("if");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		if(rest.evaluate(this.rest, ctx)==Symbol.T)
			return rest.getRest().evaluate(this.rest, ctx);
		else
			return rest.getRest().getRest().evaluate(this.rest,    ctx);
	}
}
