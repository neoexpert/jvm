package lisp.functions;
import lisp.*;

public class When extends Function{
	public When(){
		super("when");
	}

	@Override
	public Cons evaluate( Cons rest,  Context ctx){
		if(rest.getFirst().evaluate(this.rest,  ctx)==Symbol.T)
			return rest.getRest().getFirst().evaluate(this.rest,  ctx);
		return Symbol.NIL;
	}
}
