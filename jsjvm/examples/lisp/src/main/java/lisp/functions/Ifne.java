package lisp.functions;
import lisp.*;

public class Ifne extends Function{
	public Ifne(){
		super("/=");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		double value=((Primitive)rest.evaluate(this.rest,ctx)).doubleValue();
		Cons lisp=rest;
		while(lisp!=null){
			if(((Primitive)lisp.evaluate(this.rest, ctx)).doubleValue()!=value)
					return Symbol.T;
				lisp=lisp.getRest();
		}
		return Symbol.NIL;
	}
}
