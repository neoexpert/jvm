package lisp.functions;

import lisp.Cons;
import lisp.Context;
import lisp.Function;
import lisp.Symbol;

public class Coerce extends Function {
    public Coerce(){
        super("coerce");
    }

    @Override
    public Cons evaluate(Cons rest, Context ctx) {
        Cons first=rest.getFirst();
        if(first.getClass()==Cons.class)
            first=first.evaluate(first.getRest(),ctx);
        Cons cons=rest.getRest().getFirst();
        Symbol type= (Symbol) cons.evaluate(cons.getRest(),ctx);
        return first.coerce(type.getName());
    }
}
