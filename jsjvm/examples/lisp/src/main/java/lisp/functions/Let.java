package lisp.functions;
import lisp.*;

public class Let extends Function{
	public Let(){
		super("let");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Cons vars=rest;
		System.out.println(vars.getFirst());
		Cons lisp=rest;
		Cons result=NIL;
		while(lisp!=null){
			Cons exp=lisp.getFirst();
			result=exp.evaluate(this.rest,   ctx);
			if(result!=null&&Symbol.RETURN.equals(result))
				return null;
			lisp=lisp.getRest();
		}
		lisp=rest;
		return result;
	}
}
