package lisp.functions;
import lisp.*;

public class Iflt extends Function{
	public Iflt(){
		super("<");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Primitive a=(Primitive)rest.getFirst().evaluate(this.rest,  ctx);
		Primitive b=(Primitive)rest.getRest().getFirst().evaluate( this.rest,   ctx);
		if(a.doubleValue()<b.doubleValue())
					return Symbol.T;
		return Symbol.NIL;
	}
}
