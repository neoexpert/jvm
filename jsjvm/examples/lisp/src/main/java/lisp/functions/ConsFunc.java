package lisp.functions;

import lisp.Cons;
import lisp.Context;
import lisp.Function;
import lisp.Symbol;

public class ConsFunc extends Function{
    public ConsFunc(){
        super("cons");
    }

    @Override
    public Cons evaluate(Cons rest, Context ctx) {
	System.out.println("first: "+rest.getFirst());
	System.out.println("rest: "+rest.getRest().getFirst());
        return rest.getFirst().add(rest.getRest().getFirst());
    }
}
