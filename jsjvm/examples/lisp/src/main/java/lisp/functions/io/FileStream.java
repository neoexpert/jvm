package lisp.functions.io;
import lisp.*;
import java.io.*;

public class FileStream extends IOStream{
	private RandomAccessFile fis;
	public FileStream(String name) throws IOException{
		fis=new RandomAccessFile(name, "rw");
	}
	public void close(){
		try {
			fis.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void write(int b){
		try{
			fis.write(b);	
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public void write(byte[] arr){
		try{
			fis.write(arr);	
		}
		catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public byte readByte() {
		try {
			return fis.readByte();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int readInt32() {
		try {
			return fis.readInt();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeInt(int value) {
		try {
			fis.writeInt(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeDouble(double value) {
		try {
			fis.writeDouble(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public char readChar() {
		try {
			byte b=fis.readByte();
			return (char) b;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeChar(char value) {
		try {
			fis.writeChar(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeLong(long value) {
		try {
			fis.writeLong(value);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
