package lisp.functions.io;
import lisp.*;
import java.io.*;

public class Close extends Function{
	public Close(){
		super("close");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		FileStream fs = (FileStream)ctx.getLocal(((Symbol)rest.getFirst()).getName());
		fs.close();
		return Symbol.NIL;
	}
}
