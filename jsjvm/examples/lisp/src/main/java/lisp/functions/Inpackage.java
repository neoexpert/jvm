package lisp.functions;
import lisp.*;
import lisp.Package;

public class Inpackage extends Function{
	public Inpackage(){
		super("in-package");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		String name=((Symbol)rest.getFirst()).getName();

		ctx.setPackage(name);
		return Symbol.NIL;
	}
}
