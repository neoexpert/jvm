package lisp.functions;
import lisp.*;

public class Defconstant extends Function{
	public Defconstant(){
		super("defconstant");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Symbol var=(Symbol)rest.getFirst();
		ctx.setGlobal(var.getName(), rest.getRest().evaluate(this.rest,  ctx));
		return null;
	}
}
