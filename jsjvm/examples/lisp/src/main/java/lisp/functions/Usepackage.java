package lisp.functions;
import lisp.*;

public class Usepackage extends Function{
	public Usepackage(){
		super("use-package");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		String name=((Symbol)rest.getFirst()).getName();
		ctx.usePackage(name);
			
		return Symbol.NIL;
	}
}
