package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Add extends Function{
	public Add(){
		super("+");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		//rest=this.rest;
		Cons first=rest.getFirst();
		if(first.getClass()==Cons.class)
			first=first.evaluate(first.getRest(),ctx);
		double result=((Primitive)first).doubleValue();
		Cons lisp=rest.getRest();
		while(lisp!=null){
			first=lisp.getFirst();
			if(first.getClass()==Cons.class)
				first=first.evaluate(lisp.getRest(),ctx);
			
			result += ((Primitive)first).doubleValue();
			lisp=lisp.getRest();
		}
		return new PDouble(result);
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(+");
		Cons exp=rest;
		while(exp!=null){
			sb.append(' ')
				.append(exp.getFirst());
			exp=exp.getRest();
		}
		sb.append(')');
		return sb.toString();
	}
}
