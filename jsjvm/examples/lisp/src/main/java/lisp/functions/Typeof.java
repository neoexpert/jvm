package lisp.functions;
import lisp.*;

public class Typeof extends Function{
	public Typeof(){
		super("type-of");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Cons value=rest.getFirst();
		if(value.getClass()==Symbol.class){
			value=ctx.getLocal(((Symbol)value).getName());
		}
		else
			value=value.evaluate(value.getRest(), ctx);
		return value.getType();
	}
}
