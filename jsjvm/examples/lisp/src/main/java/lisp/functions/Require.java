package lisp.functions;
import lisp.*;
import java.io.*;

public class Require extends Function{
	public Require(){
		super("require");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		String file=((LispString)rest.getFirst()).toLispString(ctx);
		try{
			ctx.require(file);
		}
		catch(IOException e){
			throw new RuntimeException();
		}
		return Symbol.NIL;
	}
}
