package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Sub extends Function{
	public Sub(){
		super("-");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		double result=((Primitive)rest.getFirst().evaluate( this.rest,  ctx)).doubleValue();
		Cons lisp=rest.getRest();
		while(lisp!=null){
			result -= ((Primitive)lisp.getFirst().evaluate(this.rest,   ctx)).doubleValue();
			lisp=lisp.getRest();
		}
		return new PDouble(result);
	}

}
