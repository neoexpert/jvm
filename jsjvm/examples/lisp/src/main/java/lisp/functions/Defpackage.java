package lisp.functions;
import lisp.*;
import lisp.Package;

public class Defpackage extends Function{
	public Defpackage(){
		super("defpackage");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		String name=((Symbol)rest.getFirst()).getName();
		Package pkg=new Package(name);
		ctx.putPackage(pkg);
		rest=rest.getRest();
		Cons use=rest.getFirst();
		if(use!=null){
			System.out.println(use.getFirst());
			System.out.println(use.getRest().getFirst());
			pkg.use(ctx.getPackage(((Symbol)use.getRest().getFirst()).getName().substring(1)));
			
		}

		rest=rest.getRest();
		Cons exports=rest.getFirst();
		if(exports!=null){
			System.out.println(exports.getFirst());
			System.out.println(exports.getRest().getFirst());
			pkg.export(((Symbol)exports.getRest().getFirst()).getName().substring(1));
			
		}

		return pkg;
	}
}
