package lisp.functions;
import lisp.*;

public class Eval extends Function{
	public Eval(){
		super("eval");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		return rest.evaluate(rest.getRest(), ctx);
	}
}
