package lisp.functions;
import lisp.*;

public class Ifge extends Function{
	public Ifge(){
		super(">=");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Primitive a=(Primitive)rest.evaluate( this.rest,  ctx);
		Primitive b=(Primitive)rest.getRest().evaluate(this.rest,    ctx);
		if(a.doubleValue()>=b.doubleValue())
					return Symbol.T;
		return Symbol.NIL;
	}
}
