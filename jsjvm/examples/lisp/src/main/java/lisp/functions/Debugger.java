package lisp.functions;

import lisp.Cons;
import lisp.Context;
import lisp.Function;
import lisp.Symbol;

public class Debugger extends Function{
    public Debugger(){
        super("debugger");
    }

    @Override
    public Cons evaluate(Cons rest,   Context ctx) {
        return Symbol.NIL;
    }
}
