package lisp.functions.io;
import lisp.*;
import lisp.primitives.PInteger;
import lisp.primitives.PNumber;

public class WriteToString extends Function{
	public WriteToString(){
		super("write-to-string");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		Cons value=rest.getFirst().evaluate(this.rest,   ctx);
		rest=rest.getRest();
		String str;
		if(rest!=null){
			Cons param = rest.getFirst();
			switch(((Symbol)param).getName()){
				case ":base":
					PInteger base = (PInteger) rest.getRest().getFirst();
					str=((Primitive)value).toString(base.intValue());
					break;
				default:
					throw new RuntimeException("unknown parameter");
			}
		}
		else 
			str=value.toLispString(ctx);
		return new LispString(str);
	}
}
