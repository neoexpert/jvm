package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Max extends Function{
	public Max(){
		super("max");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		double max=Double.NEGATIVE_INFINITY;
		Cons lisp=rest;
		while(lisp!=null){
			PDouble n=(PDouble)lisp.evaluate(this.rest,  ctx);
			if(n.doubleValue()>max){
				max=n.doubleValue();
			}
			lisp=lisp.getRest();
		}
		return new PDouble(max);
	}
}
