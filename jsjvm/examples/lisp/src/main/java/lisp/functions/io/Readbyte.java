package lisp.functions.io;
import lisp.*;

public class Readbyte extends Function {
    public Readbyte(){
        super("read-byte");
    }

    @Override
    public Cons evaluate(Cons rest, Context ctx){
        IOStream io;
        if(rest==null)
            io=(IOStream)ctx.getLocal("*standard-input*");
        else {
            Cons stream=rest.getFirst();
            if(stream.getClass()== Symbol.class){
                io=(IOStream) stream.resolve(ctx);
            }
            else
                io= (IOStream) (stream.evaluate(null, ctx)).resolve(ctx);
        }
        return io.readPByte();
    }
}
