package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Mul extends Function{
	public Mul(){
		super("*");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		double result=((PDouble)rest.getFirst().evaluate(this.rest,   ctx)).doubleValue();
		Cons lisp=rest.getRest();
		while(lisp!=null){
			result *= ((PDouble)lisp.getFirst().evaluate(this.rest,    ctx)).doubleValue();
			lisp=lisp.getRest();
		}
		return new PDouble(result);
	}
}
