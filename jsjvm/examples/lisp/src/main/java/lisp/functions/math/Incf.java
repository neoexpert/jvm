package lisp.functions.math;
import lisp.*;
import lisp.primitives.*;

public class Incf extends Function{
	public Incf(){
		super("incf");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Symbol s=(Symbol)rest.getFirst();
		Primitive var=((Primitive)s.resolve(ctx)).incf();
		ctx.setValue(s.getName(), var);
		return var;
	}
}
