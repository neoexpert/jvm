package lisp.functions;
import lisp.*;

public class Ifgt extends Function{
	public Ifgt(){
		super(">");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		Primitive a;
		Primitive b;
		Cons cons=rest.getFirst();
		if(cons.getClass()==Symbol.class)
			a=(Primitive)ctx.getLocal(((Symbol)cons).getName());
		else 
			a=(Primitive)cons.evaluate(cons.getRest(), ctx);
		cons=rest.getRest().getFirst();
		if(cons.getClass()==Symbol.class)
			b=(Primitive)ctx.getLocal(((Symbol)cons).getName());
		else 
			b=(Primitive)cons.evaluate(cons.getRest(), ctx);

		if(a.doubleValue()>b.doubleValue())
					return Symbol.T;
		return Symbol.NIL;
	}
}
