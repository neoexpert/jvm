package lisp.functions;
import lisp.*;

public class Return extends Function{
	public Return(){
		super("return");
	}

	@Override
	public Cons evaluate(Cons rest,  Context ctx){
		return this;
	}
}
