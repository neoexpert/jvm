package lisp.functions;
import lisp.*;

public class Loop extends Function{
	public Loop(){
		super("loop");
	}

	@Override
	public Cons evaluate(Cons rest,   Context ctx){
		Cons lisp=rest;
		while(true){
			while(lisp!=null){
				Cons exp=lisp.getFirst();
				Cons result=exp.evaluate(this.rest,  ctx);
				if(result!=null&&Symbol.RETURN.equals(result))
					return null;
				lisp=lisp.getRest();
			}
			lisp=rest;
		}
	}
}
