package lisp.functions;
import lisp.*;

public class Println extends Function{
	public Println(){
		super("println");
	}

	@Override
	public Cons evaluate(Cons rest, Context ctx){
		Cons value=rest.getFirst();
		if(value.getClass()==Symbol.class){
			value=value.resolve(ctx);
		}
		else
			value=value.evaluate(value.getRest(),   ctx);
		rest=rest.getRest();
		IOStream io;
		if(rest==null)
			io=(IOStream)ctx.getLocal("*standard-output*");
		else
		 	io=(IOStream)rest.getRest().evaluate(null, ctx);
		value.write(io, ctx);
		io.write('\n');
		return null;
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder();
		sb.append("(println");
			Cons lisp=rest;
			while(lisp!=null){
				sb.append(' ');
				sb.append(lisp.getFirst());
				lisp=lisp.getRest();
			}
		sb.append(')');
		return sb.toString();
	}
}
