package lisp;
import java.util.*;
import java.io.*;

public class Context{
	private final Context parent;
	private Package _package;
	private HashMap<String, Cons> globals;
	private HashMap<String, Cons> locals=new HashMap<>();
	private HashMap<String, Package> packages;
	private HashSet<String> required=new HashSet<>();;

	public Context(){
		this.parent=null;
		globals=new HashMap<>();
		packages=new HashMap<>();
	}

	public void require(String filename)throws IOException{
		if(required.contains(filename))return;
		Package _package=this._package;
		new LispParser(new File(filename), this).evaluate();
		required.add(filename);
		this._package=_package;
	}

	public Symbol intern(Symbol symbol){
		return _package.intern(symbol);
	}

	public Symbol getSymbol(String name){
		return _package.getSymbol(name);
	}

	public void put(Symbol symbol){
		_package.put(symbol);
	}

	public void putPackage(Package symbol){
		packages.put(symbol.name, symbol);
	}

	public void setPackage(String name){
		this._package=packages.get(name);
	}

	public Package getPackage(String name){
		return packages.get(name);
	}
	public void usePackage(String name){
		Package pkg= packages.get(name);
		_package.use(pkg);
	}

	public Context(Context parent){
		this.parent=parent;
		this._package=parent._package;
		globals=parent.globals;
		packages=parent.packages;
		required=parent.required;
	}

	public Context push(){
		return new Context(this);
	}

	public Context pop(){
		return parent;
	}

	public void setLocal(String name, Cons value){
		locals.put(name, value);
	}
	public void setGlobal(String name, Cons value){
		globals.put(name, value);
	}
	public void unsetLocal(String name){
		locals.remove(name);
	}
	public Cons getLocal(String name){
		Cons value=locals.get(name);
		if(value!=null)
			return value;
		value=globals.get(name);
		if(value!=null)
			return value;
		if(parent!=null)
			return parent.getLocal(name);
		else
			return Symbol.NIL;
	}
	public void setValue(String name, Cons value){
		if(locals.containsKey(name))
			locals.put(name, value);
		if(globals.containsKey(name))
			globals.put(name, value);
	}
}
