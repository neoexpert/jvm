package lisp;
import java.util.*;
import lisp.symbols.*;
import lisp.classes.*;
import lisp.functions.*;
import lisp.functions.math.*;
import lisp.functions.list.*;
import lisp.functions.list.List;
import lisp.functions.io.*;

public class DefaultPackage extends Package{
	public DefaultPackage(){
		super("cl");
		//basics:
		export(new First());
		export(new Rest());
		export(new ConsFunc());
		export(new Quote());
		//export(new EQ());
		//export(new Cond());
		//export(new Lambda());
		export(new Coerce());
		
		export(Symbol.NIL);
		export(Symbol.T);
		export(Symbol.ERROR);
		
		//lists
		export(new List());

		//packages
		export(new Defpackage());
		export(new Inpackage());
		export(new Usepackage());

		export(new Require());

		export(new Defun());
		export(new Funcall());
		export(new Eval());

		export(new Defvar());
		export(new Defconstant());
		export(new Setq());
		export(new Incf());
		export(new Setf());
		export(new Defclass());
		export(new Makeinstance());
		export(new HandlerCase());
		export(new ReadFromString());
		export(new Load());
		export(new Debugger());
		export(new Makearray());
		export(new Typeof());
		export(new If());
		export(new When());
		export(new Loop());
		export(new Let());
		export(new Return());
		export(new Ifeq());
		export(new Ifne());
		export(new Iflt());
		export(new Ifgt());
		export(new Ifle());
		export(new Ifge());

		//math
		export(new Add());
		export(new Sub());
		export(new Mul());
		export(new Div());
		export(new Min());
		export(new Max());

		//io
		export(new Open());
		export(new Close());
		export(new Readbyte());
		export(new Readchar());
		export(new Readint32());
		export(new Write());
		export(new WriteToString());
		export(new Println());
	}
}
