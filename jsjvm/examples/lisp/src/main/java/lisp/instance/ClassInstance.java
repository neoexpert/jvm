package lisp.instance;
import lisp.*;
import lisp.classes.*;
import java.util.*;

public class ClassInstance extends Instance{
	private LClass clazz;
	private LispString type;
	public ClassInstance(LClass clazz){
		this.clazz=clazz;
		type=new LispString(clazz.getName());
	}

	public void setf(Cons name, Cons value){
	}

	public LispString getType(){
		return type;
	}

	public Cons evaluate(Context ctx){
		return this;
	}
}
