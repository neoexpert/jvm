package lisp.instance;
import lisp.*;
import lisp.primitives.*;
import java.util.*;

public class ObjectArrayInstance extends Instance{
	private final LispString type;
	private final Cons[] arr;
	public ObjectArrayInstance(int size){
		type=new LispString("ARRAY");
		arr=new Cons[size];
	}

	public void setf(Cons name, Cons value){
		arr[((PInteger)name).intValue()]=value;	
	}

	public LispString getType(){
		return type;
	}

	public Cons evaluate(Context ctx){
		return this;
	}
}
