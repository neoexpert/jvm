package lisp.instance;
import lisp.*;
import java.util.*;

public abstract class Instance extends Cons{
	public abstract void setf(Cons name, Cons value);
}
