package lisp;
import java.util.*;
import lisp.symbols.*;
import lisp.classes.*;
import lisp.functions.*;
import lisp.functions.math.*;
import lisp.functions.list.*;
import lisp.functions.io.*;

public class Package extends Symbol {
	private final HashMap<String, Symbol> symbols=new HashMap<>();
	private final LinkedList<String> exports=new LinkedList<>();
	

	public Package(String name){
		super(name);
	}

	public Symbol intern(Symbol symbol){
	  String name=symbol.getName();
		Symbol __symbol=symbols.get(name);
		if(__symbol==null){
			symbols.put(name, symbol);
			return symbol;
		}
		return (Symbol)__symbol.convert(symbol);
	}

	public void export(String name){
		exports.add(name);
	}
	public void export(Symbol symbol){
		put(symbol);
		exports.add(symbol.getName());
	}

	public void use(Package pkg){
		for(String s:pkg.exports){
			symbols.put(s, pkg.symbols.get(s));
		}
	}

	public void put(Symbol symbol){
		symbols.put(symbol.name, symbol);
	}

	public Symbol getSymbol(String name){
		return symbols.get(name);
	}

	public static final LispString TYPE=new LispString("PACKAGE");
	@Override
	public LispString getType(){
		return TYPE;
	}

	public String quote() {
		return name;
	}

	public String getName() {
		return name;
	}
}
