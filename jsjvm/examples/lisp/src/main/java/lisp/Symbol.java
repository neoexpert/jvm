package lisp;
import java.util.*;
import lisp.symbols.*;
import lisp.classes.*;
import lisp.functions.*;
import lisp.functions.math.*;
import lisp.functions.list.*;
import lisp.functions.io.*;

public class Symbol extends Atom {
	public static final Nil NIL=new Nil();
	public static final T T=new T();
	public static final Error ERROR=new Error();
	public static final Function RETURN=new Return();
	//private static final HashMap<String, Symbol> symbols=new HashMap<>();
	/*
	static{
		put(NIL);
		put(T);
		put(ERROR);
		put(new Defvar());
		put(new Setq());
		put(new Incf());
		put(new Setf());
		put(new Defclass());
		put(new Makeinstance());
		put(new HandlerCase());
		put(new ReadFromString());
		put(new Debugger());
		put(new Defun());
		put(new Makearray());
		put(new Typeof());
		put(new Quote());
		put(new If());
		put(new When());
		put(new First());
		put(new Rest());
		put(new Loop());
		put(new Let());
		put(new Return());
		put(new Ifeq());
		put(new Ifne());
		put(new Iflt());
		put(new Ifgt());
		put(new Ifle());
		put(new Ifge());

		//math
		put(new Add());
		put(new Sub());
		put(new Mul());
		put(new Div());
		put(new Min());
		put(new Max());

		//io
		put(new Open());
		put(new Close());
		put(new Write());
		put(new Println());
	}

	public static Cons intern(Symbol symbol){
	  String name=symbol.getName();
		Symbol __symbol=symbols.get(name);
		if(__symbol==null){
			symbols.put(name, symbol);
			return symbol;
		}
		return __symbol.convert(symbol);
	}

	public static void put(Symbol symbol){
		symbols.put(symbol.name, symbol);
	}
	*/

	protected String name;

	public Symbol(String name){
		this.name=name.intern();
	}

	public Cons resolve(Context ctx){
		return ctx.getLocal(name);
	}

	public int hashCode(){
		return name.hashCode();
	}
	public boolean equals(Object o){
		if(o instanceof Symbol)
			return name.equals(((Symbol)o).name);
		return false;
	}

	@Override
	public String toLispString(Context ctx){
		Cons value=ctx.getLocal(name);
		if(value==this)
			return "nil";
		return value.toLispString(ctx);
	}
	@Override
	public String toString(){
		return name;
		/*
		StringBuilder sb=new StringBuilder();
		sb
			.append("(")
			.append(name)
			.append(')');
		return sb.toString();
		*/
	}


	public Cons evaluate(Cons rest, Context ctx){
		/*
		Cons local = ctx.getLocal(name);
		if(local!=null)
			return local;
			*/
		Symbol symbol=ctx.getSymbol(name);
		if(symbol==null)
			throw new RuntimeException("unbound symbol: "+name);
		if(symbol==this)
			return symbol;
		return symbol.evaluate(rest,  ctx);
	}

	public static final LispString TYPE=new LispString("SYMBOL");
	@Override
	public LispString getType(){
		return TYPE;
	}


	public String quote() {
		return name;
	}
	public String getName() {
		return name;
	}
}
