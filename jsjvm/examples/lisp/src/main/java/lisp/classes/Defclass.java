package lisp.classes;
import lisp.*;

public class Defclass extends Function{
	public Defclass(){
		super("defclass");
	}

	public Cons evaluate(Cons rest, Context ctx){
		Symbol name=(Symbol)rest.getFirst();
		System.out.println("defclass: ");
		System.out.println(name);
		rest=rest.getRest();
		rest=rest.getRest();
		rest=rest.getFirst();
		Cons fields=rest;
		ctx.put(new LClass(name.getName(), fields));
		return Symbol.NIL;
	}
}
