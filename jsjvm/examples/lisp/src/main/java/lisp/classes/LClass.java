package lisp.classes;
import lisp.*;
import java.util.*;

public class LClass extends Symbol{
	private final String[] fields;
	public LClass(String name, Cons args){
		super(name);
		ArrayList<String> argsl=new ArrayList<>();
		argsl.add(((Symbol)args.getFirst()).getName());
		args=args.getRest();
		while(args!=null){
			argsl.add(((Symbol)args.getFirst()).getName());
			if(args.getRest()==null)break;
			args=args.getRest();
		}
		String[] argsarr=new String[argsl.size()];
		for(int i=0;i<argsarr.length;++i)
			argsarr[i]=argsl.get(i);
		this.fields=argsarr;
		for(String f:fields){
			System.out.println(f);
		}
	}

	public static final LispString TYPE=new LispString("CLASS");
	public LispString getType(){
		return TYPE;
	}

	public Cons evaluate(Context ctx){
		return this;
	}
}
