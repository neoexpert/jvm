package lisp.classes;
import lisp.*;
import lisp.instance.*;

public class Setf extends Function{
	public Setf(){
		super("setf");
	}

	public Cons evaluate(Cons rest, Context ctx){
		Instance i=(Instance)rest;
		i.setf(rest.getRest().getFirst(), rest.getRest().getRest().getFirst());
		return Symbol.NIL;
	}
}
