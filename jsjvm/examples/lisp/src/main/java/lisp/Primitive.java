package lisp;

public abstract class Primitive extends Atom {
	public abstract Primitive incf();
	public abstract double doubleValue();
	public abstract int intValue();
	public Primitive resolve(Context ctx){
		return this;
	}

	@Override
	public String toLispString(Context ctx){
		return toString();
	}
	
	public String quote() {
		return toString();
	}

    public abstract String toString(int radix);
}
