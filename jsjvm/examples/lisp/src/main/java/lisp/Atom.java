package lisp;

public class Atom extends Cons
{
	@Override
	public Cons evaluate(Cons rest, Context context){
		return this;
	}

	@Override
	public Cons resolve(Context ctx){
		return this;
	}
}
