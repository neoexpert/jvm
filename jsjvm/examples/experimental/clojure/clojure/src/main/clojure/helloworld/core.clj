(ns helloworld.core
  (:gen-class
    :name helloworld.core
    :methods [#^{:static true} [hello [] void]])
  )

(defn -hello
  "I can say 'Hello World'."
  []
  (println "Hello, World!"))
