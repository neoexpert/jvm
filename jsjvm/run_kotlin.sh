#build example project:
mvn -f kotlin-example/pom.xml clean install -q &&
#copy the js jvm
cp -r webroot/js/* jvmserver/webroot/js/jvm/ &&
#
cd jvmserver &&
#build jsjvm, for this you need to compile the whole project from root first
mvn clean install -q &&
#run the server
java -jar target/jvm-jar-with-dependencies.jar -v -jar ../kotlin-example/target/hello-world-1.0-SNAPSHOT.jar
