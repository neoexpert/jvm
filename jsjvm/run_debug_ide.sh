#!/bin/bash
echo param1
echo $1
echo param2
echo $2
#
cd jvmserver &&
#run the server
echo "██████████████████████████████"
echo "█                            █"
echo "█  jdwp debugger port: 5005  █"
echo "█                            █"
echo "██████████████████████████████"
pwd
java -jar target/jvm-jar-with-dependencies.jar -v -cp "$1/target/classes" "$2" &

echo "Waiting for port to 8080 to open..."

while ! nc -z localhost 8080; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

xdg-open http://localhost:8080/
echo "Waiting for port to 5005 to open..."

while ! nc -z localhost 5005; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

echo "debugger can connect now"
