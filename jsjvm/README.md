# JavaScript JVM implementation

This JVM runs in a webbrowser.

You can pass arguments to the main method over the query string.

the equivalent of

java \<main-class\> arg1 arg2

would be then:

http://localhost:8080/?arg1,arg2
