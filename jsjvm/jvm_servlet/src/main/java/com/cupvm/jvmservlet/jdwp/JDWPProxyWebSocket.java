package com.cupvm.jvmservlet.jdwp;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.debugger.*;
import com.cupvm.jsc.JVMInterface;
import com.cupvm.jvm.ClassIntf;
import com.cupvm.jvm.FieldIntf;
import com.cupvm.jvm.LineNumberEntry;
import com.cupvm.jvm.MethodIntf;
import com.cupvm.jvm._abstract.AClass;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.List;

@WebSocket(maxIdleTime=Integer.MAX_VALUE)
public class JDWPProxyWebSocket implements
        JDWPProxy.Proxy,
        Constants,
        CommandSets,
        VMCommandSet,
        ReferenceTypeCommandSet ,
        MethodCommandSet ,
        ThreadGroupReferenceCommandSet,
        ObjectReferenceCommandSet,
        ErroCodes{
    private static final Logger logger= ConsoleLogger.get();
    private final JVMInterface ji;
    private Session session;
    private JDWPProxy jdwp;

    public JDWPProxyWebSocket(JVMInterface ji){
        this.ji=ji;
    }


    @OnWebSocketMessage
    public void onMessage(InputStream is) {
            try {
                synchronized (waiter) {
                    ReplyPacket result = new ReplyPacket(new DataInputStream(is));
                    if (!result.isReply()) {
                        jdwp.sendEvent(result);
                        return;
                    }
                    this.result=result;
                    waiter.notify();
                }

            } catch(IOException e){
                e.printStackTrace();
                session.close();
            }
    }

    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
        logger.info("WebClient disconnected, closing jdwp...");
        if(jdwp==null)return;
            try {
                jdwp.close();
                jdwp=null;
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        t.printStackTrace();
        logger.info("WebClient error, closing jdwp...");
        if(jdwp==null)return;
            try {
                jdwp.close();
                jdwp=null;
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @OnWebSocketConnect
    public void onConnect(Session session){
        if(jdwp!=null){
            logger.warn("another JDWP Connection");
            session.close();
            return;
        }
        logger.info("WebClient connected to JDWP");
        this.session=session;
        this.jdwp = new JDWPProxy(this);
            jdwp.setOnDisconnectListener(new Runnable() {
                @Override
                public void run() {
                    session.close();
                }
            });
            jdwp.startasync();
    }

    private volatile ReplyPacket result;
    private final Object waiter=new Object();

    @Override
    public ReplyPacket processCommand(CommandPacket cmd) throws IOException {
        logger.info("processing command: ",cmd.command_set, " ",cmd.command);
        ReplyPacket reply=processCommandLocally(cmd);
        if(reply!=null)
            return reply;
        try {
            session.getRemote().sendBytes(ByteBuffer.wrap(cmd.toArray()));
        } catch (IOException e) {
            e.printStackTrace();
            session.close();
            return null;
        }
        try{
            while (true) {
                synchronized (waiter) {
                    result = null;
                    waiter.wait();
                    if (result == null)
                        throw new RuntimeException("timeout");
                    if (!result.isReply()) {
                        continue;
                    }
                }
                return result;
            }
        }catch(InterruptedException e){
            e.printStackTrace();
                throw new RuntimeException(e);
            }
    }

    private ReplyPacket processCommandLocally(CommandPacket cmd) throws IOException {
        switch(cmd.command_set){
            case VM_COMMAND_SET:
                return processVMCommand(cmd);
            case REFERENCETYPE:
                return processReferenceTypeCommand(cmd);
            case METHOD_COMMAND_SET:
                return processMethodCommand(cmd);
            case OBJECT_REFERENCE:
                return processObjectReferenceCommand(cmd);
            case THREADGROUPREFERENCE:
                return processThreadGroupReference(cmd);
        }
        return null;
    }

    private ReplyPacket processVMCommand(CommandPacket cmd) throws IOException {
        switch (cmd.command){
            case VERSION:
                logger.info("JDWP Commandcmd: VERSION");
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos=new DataOutputStream(baos);
                String desc="0.0";
                JDWP.writeString(dos,desc);
                //major
                dos.writeInt(0);
                //minor
                dos.writeInt(0);
                //vm version
                String vm_version="0.0";
                JDWP.writeString(dos, vm_version);

                String vm_name="neoVM";
                JDWP.writeString(dos, vm_name);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case CLASSES_BY_SIGNATURE:
                DataInputStream dis = cmd.getDataInputStream();
                String signature=JDWP.readString(dis);
                logger.info("cmd: CLASSES_BY_SIGNATURE: ",signature);
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                String className=signature.substring(1,signature.length()-1);
                logger.info("retrieving class: ",className);

                AClass clazz=ji.getLoadedClass(className);
                if(clazz==null)
                    dos.writeInt(0);
                else{
                    dos.writeInt(1);
                    if(clazz.isInterface())
                        dos.writeByte(INTERFACE);
                    else
                        dos.writeByte(CLASS);
                    //referenceTypeID typeID
                    //string signature
                    dos.writeInt(clazz.getID());
                    //status
                    //if(clazz.isInitialized())
                    dos.writeInt(VERIFIED | PREPARED | INITIALIZED);
                    //else
                    //dos.writeInt(PREPARED);
                }
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case IDSIZES:
                logger.info("cmd: id_sizes");
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                //fieldIDSize
                dos.writeInt(4);
                //methodIDSize
                dos.writeInt(4);
                //objectIDSize
                dos.writeInt(4);
                //frameIDSize
                dos.writeInt(4);
                //referenceTypeIDSize
                dos.writeInt(4);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case TOPLEVELTHREADGRPOUPS:
                logger.info("cmd: TOPLEVELTHREADGRPOUPS");
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                //count
                dos.writeInt(1);
                //threadGroupID: group:
                dos.writeInt(-1);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case CAPABILITIES:
                logger.info("cmd: CAPABILITIES");
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                dos.writeBoolean(false);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case CLASSPATHS:
                logger.info("cmd: CLASSPATHS");
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                String baseDir="./";
                byte[] arr=baseDir.getBytes();
                dos.writeInt(arr.length);
                dos.write(arr);
                dos.writeInt(0);
                dos.writeInt(0);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
        }
        return null;
    }

    private ReplyPacket processReferenceTypeCommand(CommandPacket cmd) throws IOException {
        switch (cmd.command){
            case SIGNATURE:
                DataInputStream dis = cmd.getDataInputStream();
                int refTypeId = dis.readInt();
                logger.info("cmd: SIGNATURE FOR: "+refTypeId);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                switch (refTypeId){
                    case -1:
                        JDWP.writeSignature(dos,"java/lang/ThreadGroup");
                        break;
                    default:
                        AClass clazz = ji.getClassByID(refTypeId);
                        String signature=clazz.getName();
                        logger.info("signature :", signature);
                        JDWP.writeSignature(dos,signature);
                        break;
                }
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case FIELDS:
                logger.info("cmd: FIELDS");
                dis = cmd.getDataInputStream();
                refTypeId = dis.readInt();
                AClass clazz = ji.getClassByID(refTypeId);
                List<? extends FieldIntf> fields = clazz.getDeclaredFields();
                baos = new ByteArrayOutputStream();
                dos = new DataOutputStream(baos);
                int size=fields.size();
                dos.writeInt(size);
                for(int i=0;i<size;++i){
                    FieldIntf f = fields.get(i);
                    dos.writeInt(f.getID());
                    JDWP.writeString(dos, f.getName());
                    JDWP.writeString(dos, f.getSignature());
                    dos.writeInt(f.getAccessFlags());
                }
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case METHODS:
                logger.info("cmd: METHODS");
                dis = cmd.getDataInputStream();
                refTypeId = dis.readInt();
                clazz = ji.getClassByID(refTypeId);
                if(clazz==null) {
                    logger.error("no class found for ID: ", refTypeId);
                    return new ReplyPacket(cmd.id,(short) ErroCodes.INVALID_OBJECT);
                }
                List<? extends MethodIntf> methods = clazz.getDeclaredMethods();
                baos = new ByteArrayOutputStream();
                dos = new DataOutputStream(baos);
                size=methods.size();
                dos.writeInt(size);
                for(int i=0;i<size;++i){
                    MethodIntf m = methods.get(i);
                    dos.writeInt(m.getID());
                    JDWP.writeString(dos, m.getName());
                    JDWP.writeString(dos, m.getSignature());
                    dos.writeInt(m.getAccessFlags());
                }
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case SOURCE_FILE:
                logger.info("cmd: SOURCE_FILE");
                dis = cmd.getDataInputStream();
                refTypeId=dis.readInt();
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                clazz=ji.getClassByID(refTypeId);
                JDWP.writeString(dos, clazz.getSourceFileName());
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case STATUS:
                logger.info("cmd: STATUS");
                dis = cmd.getDataInputStream();
                refTypeId=dis.readInt();
                logger.info("refTypeID: "+refTypeId);
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                clazz=ji.getClassByID(refTypeId);
                //if(clazz.isInitialized())
                dos.writeInt(VERIFIED | PREPARED | INITIALIZED);
                //else
                //	dos.writeInt(PREPARED);
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case INTERFACES:
                logger.info("cmd: INTERFACES");
                dis = cmd.getDataInputStream();
                refTypeId=dis.readInt();
                baos=new ByteArrayOutputStream();
                dos=new DataOutputStream(baos);
                clazz=ji.getClassByID(refTypeId);
                AClass[] intfs = clazz.getInterfaces();
                dos.writeInt(intfs.length);
                for(AClass intf:intfs)
                    dos.writeInt(intf.getID());
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());

        }
        return null;
    }
    private ReplyPacket processMethodCommand(CommandPacket cmd) throws IOException {
        switch (cmd.command){
            case LINE_TABLE:
                logger.info("cmd: LINE_TABLE");
                DataInputStream dis = cmd.getDataInputStream();
                int classID = dis.readInt();
                AClass clazz = ji.getClassByID(classID);
                int methodID = dis.readInt();
                MethodIntf method = clazz.getMethodByID(methodID);
                List<? extends LineNumberEntry> lineNumbers = method.getLineNumbers();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                dos.writeLong(0L);
                dos.writeLong(method.getCodeLength());
                dos.writeInt(lineNumbers.size());
                for(LineNumberEntry line:lineNumbers){
                    dos.writeLong(line.getPC());
                    dos.writeInt(line.getLineNumber());
                }
                return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
            case VARIABLE_TABLE:
                logger.info("cmd: VARIABLE_TABLE");
                dis = cmd.getDataInputStream();
                classID = dis.readInt();
                clazz = ji.getClassByID(classID);
                methodID = dis.readInt();
                method = clazz.getMethodByID(methodID);
                baos = new ByteArrayOutputStream();
                dos = new DataOutputStream(baos);
                return new ReplyPacket(cmd.id,(short) ABSENT_INFORMATION,baos.toByteArray());
            default:
                throw new RuntimeException("Unknown Method Command: "+cmd.command);
        }
    }
    private ReplyPacket processObjectReferenceCommand(CommandPacket cmd) throws IOException {
        switch (cmd.command) {
            case REFERENCE_TYPE:
                DataInputStream dis = cmd.getDataInputStream();
                int objectID = dis.readInt();
                logger.info("cmd: REFERENCE_TYPE: " + objectID);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                switch (objectID) {
                    case -1:
                        dos.writeByte(CLASS);
                        AClass threadGroupClass = ji.getLoadedClass("java/lang/ThreadGroup");
                        dos.writeInt(threadGroupClass.getID());
                        break;
                    default:
                        return null;
                }
                return new ReplyPacket(cmd.id, (short) 0, baos.toByteArray());
            case ISCOLLECTED:
                logger.info("cmd: ISCOLLECTED");
                dis = cmd.getDataInputStream();
                objectID = dis.readInt();
                baos = new ByteArrayOutputStream();
                dos = new DataOutputStream(baos);
                switch (objectID) {
                    case -1:
                        dos.writeBoolean(false);
                        break;
                    default:
                        dos.writeBoolean(false);
                        break;
                }
                return new ReplyPacket(cmd.id, (short) 0, baos.toByteArray());
        }
        return null;
    }
	private ReplyPacket processThreadGroupReference(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case THREAD_GROUP_NAME:
				DataInputStream dis = cmd.getDataInputStream();
				int threadGroupID=dis.readInt();
				logger.info("THREADGROUP_NAME for: "+threadGroupID);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				JDWP.writeString(dos, "BrowserThreadGroup");
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
		}
		return null;
	}
}
