package com.cupvm.jvmservlet.jdwp;

import com.cupvm.jsc.JVMInterface;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;


public class JDWPWebsocketCreator implements WebSocketCreator{
	private JVMInterface ji;
	public JDWPWebsocketCreator(){
	}

	public void setJVMInterface(JVMInterface ji){
		this.ji=ji;
	}

	@Override
	public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse res) {
		if(ji==null){
			throw new RuntimeException("JVMInterface is not set");
		}
		return new JDWPProxyWebSocket(ji);
	}
}
