package com.cupvm.jvmservlet;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

@WebSocket
public class FileSystemWebsocket {
	private RandomAccessFile file;
	private final File root;
	public FileSystemWebsocket(){
		File root=new File("webfs/").getAbsoluteFile();
		if(!root.exists()){
			this.root = null;
			return;
		}
		if(!root.isDirectory()){
			this.root=null;
			return;
		}
		this.root=root;
	}

	private static byte IO_ERROR=-1;
	private static byte FILE_NOT_FOUND=-2;
	@OnWebSocketMessage
	public void onMessage(Session session, String message) {
		try{
			final File f = new File(root, message);
			if(!f.getAbsolutePath().startsWith(root.getAbsolutePath())){
				try{
					session.getRemote().sendBytes(ByteBuffer.wrap(new byte[]{IO_ERROR}));
				}catch(IOException e){
					e.printStackTrace();
				}
				session.close();
			}
			file=new RandomAccessFile(f, "rw");
			try{
				session.getRemote().sendBytes(ByteBuffer.wrap(new byte[]{0}));
			}catch(IOException e){
				e.printStackTrace();
			}
		}catch(FileNotFoundException e){
			try{
				session.getRemote().sendBytes(ByteBuffer.wrap(new byte[]{FILE_NOT_FOUND}));
			}catch(IOException ex){
				ex.printStackTrace();
			}
			session.close();
			e.printStackTrace();
			file=null;
		}
	}

	@OnWebSocketMessage
	public void onMessage(Session session, byte[] msg, int offset, int length) {
		switch(msg[0]){
			case 2:
				try{
					file.write(msg,1,msg.length-1);
				}catch(IOException e){
					e.printStackTrace();
					session.close();
				}
				break;
			case 3:
				ByteBuffer bb = ByteBuffer.wrap(msg);
				bb.get();
				int len = bb.getInt();
				byte[] buf=new byte[len+1];
				try {
					int readen = file.read(buf,1,len);
					buf[0]=0;
					session.getRemote().sendBytes(ByteBuffer.wrap(buf,0,readen));
				} catch (IOException e) {
					try {
						session.getRemote().sendBytes(ByteBuffer.wrap(new byte[]{IO_ERROR}));
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					session.close();
					if(file!=null) {
						try {
							file.close();
						} catch (IOException ioException) {
							ioException.printStackTrace();
						}
					}
					e.printStackTrace();
				}
				break;
		}
	}

	@OnWebSocketClose
	public void onClose(Session session, int statusCode, String reason) {
		System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
		try{
			if(file!=null)
				file.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	@OnWebSocketError
	public void onError(Throwable t) {
		t.printStackTrace();
	}

	@OnWebSocketConnect
	public void onConnect(Session session) {
	}
}
