package com.cupvm.jvmservlet;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;

import javax.servlet.http.HttpSession;

public class FileSystemWebsocketCreator implements WebSocketCreator{
	@Override
	public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse res) {
		HttpSession httpsession = req.getSession();
		/*
		Object logged_in = httpsession.getAttribute("logged_in");
		if(logged_in==null)
			return null;
		 */
		return new FileSystemWebsocket();
	}
}
