package com.cupvm.jvmservlet;

import com.cupvm.jsc.JVMInterface;
import com.cupvm.*;
import com.cupvm.jsc.SyntheticClassLoader;
import com.cupvm.jvm._class.ClassLoader;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class JVMServlet extends HttpServlet{
  private static Logger logger= LoggerImpl.get();
	private final JVMInterface ji;
	public JVMServlet(boolean debug, SyntheticClassLoader classLoader){
	    ji=new JVMInterface(debug, classLoader);
	}

	public void addFolderToClassPath(String path){
		ji.addFolderToClassPath(path);
	}

	public void addJarToClassPath(File jar, String prefix)throws IOException{
		ji.addJarToClassPath(jar, prefix);
	}
	public void setMainClass(String mainClass){
		ji.setMainClass(mainClass);
	}

	public void reset(){
		ji.reset();
	}

	public void init(){
		ji.init();
	}

	public void setJarFile(String jarFile) throws IOException{
		ji.setJarFile(new File(jarFile));
	}
	public void setJarFile(File jarFile) throws IOException{
		ji.setJarFile(jarFile);
	}

	private static String parseManifest(InputStream is) throws IOException {
		String line;
		while ((line=readLine(is))!=null){
			if (line.startsWith("Main-Class: ")) {
				//mainClass = mainClass.replaceAll("\\.", "/");
				return line.substring("Main-Class: ".length());
			}
		}
		return null;
	}
	private static String readLine(InputStream is) throws IOException {
		StringBuilder line=new StringBuilder();
		int b=is.read();
		if(b<0)return null;
		while (b>0){
			char c= (char) b;
			if(c=='\r'|c=='\n')
				break;
			if(c=='.')c='/';
			line.append(c);
			b=is.read();
		}
		return line.toString();
	}

	private Runnable onreset;
	public void setOnResetListener(Runnable onreset){
		this.onreset=onreset;
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String cmd=req.getParameter("cmd");
		OutputStream os = res.getOutputStream();
		String cl= req.getParameter("cl");
		switch (cmd) {
			case "GETCLASS":
				res.setContentType("application/json");
				String classname = req.getParameter("classname");
				JSONObject jclass;
				if(cl==null)
					jclass=ji.getClass(classname, null);
				else
					jclass=ji.getClass(classname, Integer.parseInt(cl));

				if(jclass==null){
				    return;
				}
				os.write(jclass.toString().getBytes());
				return;
			case "GETCLASSES":
				res.setContentType("application/json");
				String[] classnames = req.getParameterValues("classnames[]");
				JSONArray jarray = ji.getClasses(new JSONArray(classnames),null);
				os.write(jarray.toString().getBytes());
				return;
			case "GETINITIALCLASSES":
				long lastModifiedFromBrowser=req.getDateHeader("If-Modified-Since");
				//final String line = br.readLine();
				//JSONObject jo=new JSONObject(line);
				//constatntPool = "true".equals(req.getParameter("constant_pool"));
				//JSONArray initialClasses = jo.getJSONArray("classnames[]");
				JSONObject result=new JSONObject();
				long last_modified = ji.getInitialClasses(result, lastModifiedFromBrowser);
				if (last_modified<0) {
					//setting 304 and returning with empty body
					res.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
					logger.info("classes from cache");
					return;
				}
				logger.info("classes fresh");
				res.addDateHeader("Last-Modified",last_modified);
				res.addHeader("Cache-Control","No-Cache");
				res.setContentType("application/json");
				os.write(result.toString().getBytes());
				return;
			case "GETJS":
				lastModifiedFromBrowser=req.getDateHeader("If-Modified-Since");
				last_modified=ji.checkLastModifiedJS(lastModifiedFromBrowser);
				if (last_modified<0) {
					//setting 304 and returning with empty body
					res.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
					logger.info("JS from cache");
					return;
				}
				logger.info("JS fresh ", "lastModifiedFromBrowser: ", lastModifiedFromBrowser, " lastModified: ", last_modified);
				res.addDateHeader("Last-Modified",last_modified);
				res.addHeader("Cache-Control","No-Cache");
				res.setContentType("application/javascript");
				if(cl==null)
					ji.getJS(os);
				else
					ji.getJS(os,Integer.parseInt(cl));
				return;
			case "PRECOMPILE":
				ji.refreshCompiledJS();
				return;
			case "GETMETHOD":
				res.setContentType("application/octet-stream");
				classname = req.getParameter("classname");
				String methodname = req.getParameter("methodname");
				String raw = req.getParameter("raw");
				try {
					byte[] method;
					if(cl==null)
						method=ji.getMethod(classname, methodname,null);
					else
						method=ji.getMethod(classname, methodname,Integer.parseInt(cl));
					if(method!=null)
						os.write(method);
				}catch (RuntimeException e){
					String message="could not load method: "+classname+"."+methodname;
					throw new RuntimeException(message,e);
				}
				return;
			case "STACKTRACE":
				JSONArray ja=new JSONArray(req.getParameter("stacktrace"));
				ji.processStackTrace(ja,os);
				os.close();
				return;
			case "PRIVILEGED":
				String pcmd=req.getParameter("pcmd");
				switch(cmd){
					case "INSTALL_JAR":
						String jar=req.getParameter("jar");
						break;
				}
				return;

		}
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req,resp);
	}

	public JVMInterface getJVMInterface() {
		return ji;
    }
}
