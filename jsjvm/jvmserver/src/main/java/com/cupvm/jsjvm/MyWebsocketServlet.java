package com.cupvm.jsjvm;

import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class MyWebsocketServlet extends WebSocketServlet{
	private final WebSocketCreator wsCreator;

	public MyWebsocketServlet(WebSocketCreator wsCreator){
		this.wsCreator=wsCreator;
	}
	@Override
	public void configure(final WebSocketServletFactory webSocketServletFactory){
		webSocketServletFactory.setCreator(wsCreator);
	}
}
