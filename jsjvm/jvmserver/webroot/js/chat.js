const REGISTER=1;
const LOGIN=2;
const LOGIN_FAILED=3;
const LOGIN_SUCCESS=4;
const RETURN=5;


class Chat{
	constructor(){
		this.request_id=1;
		this.requests={};
		this.callbacks=[];
	}

	connect(onconnected){ 
		this.socket = new WebSocket("ws://" + location.host + "/jvm");
		this.socket.binaryType = 'arraybuffer';
		var that=this;
		this.socket.onmessage = function(e){
			var server_message = e.data;
			that.callbacks.shift()(server_message);
		}
		this.socket.onopen = function(){
			if(onconnected)
				onconnected();
			const obj={cmd:"lala"};
			that.socket.send(JSON.stringify(obj));

		}
		this.socket.onclose = function(){
			setTimeout(function(){ 
				chat.connect();
			}, 5000);

    		};
	}
	send(msg, callback){
		this.callbacks.push(callback);
		this.socket.send(msg);
	}

}
