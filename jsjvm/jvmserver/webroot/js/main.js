const chat=new Chat();
function GETBIN(url){
	return new Promise(function(resolve, reject){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.responseType = "arraybuffer";
		xhr.onload = function() {
			if (xhr.status === 200) {
				resolve(xhr.response);
			}
			else {
				reject(xhr.status);
			}
		};
		xhr.onerror=reject;
		xhr.send();
	});
}
function onOpen(){
		/*
	let cmd={cmd:"GETSOURCE",classname:"Main"};
	chat.send(JSON.stringify(cmd),function(msg){
		$("#code").val(msg);

	});*/
	start();
	$("#compile_button").click(function(){
		let cmd={cmd:"COMPILE",classname:"Main",
		code:$("#code").val()};
		chat.send(JSON.stringify(cmd),function(msg){
			if(msg!="")
				alert(msg);
			location.reload();
		});
	});
}
function start(){
	const jvm_interface={};
	jvm_interface.GETCLASS= async function(classname,constant_pool){
		let cmd={cmd:"GETCLASS",classname:classname,constant_pool:constant_pool};
			return new Promise(function(resolve, reject){
					chat.send(JSON.stringify(cmd),function(msg){
							resolve(JSON.parse(msg));
					});
			});
	};
	jvm_interface.GETCLASSES=async function(classnames){
		let cmd={cmd:"GETCLASSES",classnames:classnames};
			return new Promise(function(resolve, reject){
					chat.send(JSON.stringify(cmd),function(msg){
							resolve(JSON.parse(msg));
					});
			});
		
	};
	jvm_interface.GETINITIALCLASSES=async function(classnames,constant_pool){
		let cmd={cmd:"GETINITIALCLASSES",classnames:classnames,constant_pool:constant_pool};
			return new Promise(function(resolve,reject){
					chat.send(JSON.stringify(cmd),function(msg){
							resolve(JSON.parse(msg));
					});
			});
		
	};
	jvm_interface.GETMETHOD=async function(classname,signature, raw){
		let cmd={cmd:"GETMETHOD",classname:classname,methodname:signature, raw:raw};
		return new Promise(function(resolve, reject){
				chat.send(JSON.stringify(cmd),function(msg){
						resolve(msg);
				});
		});
		
	};
	jvm_interface.loadLib=async function(lib){
			return new Promise(function(resolve, reject){
					var script = document.createElement('script');
					script.onload = resolve;
					script.src = "js/jvm/"+lib+".js";
					document.head.appendChild(script);
			});
	}
		jvm_interface.printStackTrace=function(message, stacktrace){
				if(!stacktrace){
						console.error(message);
						return;
				}
				const l=stacktrace.length;
				const st=new Array(l);
				for(let i=0;i<l;i++){
						const se=stacktrace[i];
						const method=se.method;
						st[i]={clazz:method.clazz.name,method:method.signature,pc:se.pc};
				}
				let cmd={cmd:"GETSTACKTRACE",stacktrace:st};
				chat.send(JSON.stringify(cmd),function(msg){
						if(message)
								console.error(message +"\n"+msg);
						else
								console.error(msg);
				});
		}
		const jvm=createJVM(jvm_interface);
	//initBASE(jvm);
	startMain(jvm);
}
chat.connect(onOpen);
