const _ARR=91;
const _J=74;
const _D=68;
const _L=76;
const _I=73;
const _F=70;
const _Z=90;
const _B=66;
const _C=67;
const _S=83;

//primitivesCache
const lowP = -128;
const highP = 127;

const _NP="java/lang/NullPointerException";
const _AE="java/lang/ArithmeticException";
class Instance{
	constructor(clazz) {
		this.clazz=clazz;
	}
}
class ClassInstance extends Instance{
	constructor(clazz) {
		super(clazz);
		this.fields={};
		Object.assign(this.fields, clazz.instanceTemplate);
		/*
		const defaultFields=clazz.fields;
		const fieldNames=clazz.fieldNames;
		const l=fieldNames.length;
		for(let i=0;i<l;++i){
			const fieldname=fieldNames[i];
			this.fields[fieldname]=defaultFields[fieldname];
		}*/
	}
	clone(){
		const ci=new ClassInstance();
		const keys=Object.keys(this.fields);
		const l=keys.length;
		for(let i=0;i<l;++i){
			const key=keys[i];
			const f=this.fields[key];
			if(f)if(f.isCloning===true)continue;
			if(f instanceof Instance){
				f.isCloning=true;
				ci.fields[key]=f.clone();
			}
			else    ci.fields[key]=f;
		}
		return ci;
	}
}

function monitorEnter(i, thread){
	if(i.owner){
		if(i.owner===thread) return;
		if(!i.monitorQueue)
			i.monitorQueue=[];
		return new Promise(function(resolve, reject){
			thread.state=TS_MONITOR;
			thread.monitorExit=resolve;
			i.monitorQueue.push(thread);
			_yield();
		});
	}
	i.owner=thread;
}

function monitorExit(i){
	if(i.monitorQueue){
		if(i.monitorQueue.length>0){
			const thread=i.monitorQueue.shift();
			i.owner=thread;
			thread.state=TS_RUNNING;
			sthreads.push(thread.monitorExit);
			return;
		}
	}
	i.owner=undefined;
}


const TS_ZOMBIE=0;
const TS_RUNNING=1;
const TS_SLEEPING=2;
const TS_MONITOR=3;
const TS_WAIT=4;
class Thread extends ClassInstance{
	constructor(){
		super(java_lang_Thread);
		this.entries=[];
		this.setPriority(5);
		this.s=this.p;
		this.suspended=0;
	}
	setPriority(priority){
		/*
		if(priority>10)
			priority=10;
*/
		if(priority<=0)
			priority=1;
		this.p=priority*8;
	}
	suspend(){}
	resume(){}
	isEmpty(){
		return this.entries.length<=0;	
	}
	peek(){
		return this.entries[this.entries.length-1];
	}
	push(frame){
		this.entries.push(frame);	
	}
	pop(){
		return this.entries.pop();	
	}
	clone(){
		const fs=new Thread();
		const l=this.entries.length;
		for(let i=0;i<l;++i){
			fs.push(this.entries[i].clone());
		}
		return fs;
		
	}
}


class ArrayClass extends AClass{
	constructor(){
		super("ArrayClass");
		this.interfaces=[];
	}
	async getMethodByName(name){
		switch(name){
			case "clone()Ljava/lang/Object;":
				return function(stack, $this){
					stack.pop();
					return $this.slice();
				};
		}
	}
}
const arrayClass=new ArrayClass();
//new boolean array
function nZa(length){
	const arr=new Int8Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new byte array
function nBa(length){
	const arr=new Int8Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new short array
function nSa(length){
	const arr=new Int16Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new char array
function nCa(length){
	const arr=new Uint16Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new int array
function nIa(length){
	const arr=new Int32Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new float array
function nFa(length){
	const arr=new Float32Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new double array
function nDa(length){
	const arr=new Float64Array(length);
	arr.clazz=arrayClass;
	return arr;
}
//new long array
function nJa(length){
	const arr=new Array(length);
	arr.fill(0n);
	arr.clazz=arrayClass;
	return arr;
}
//new object array
function nLa(length){
	const arr=new Array(length);
	arr.fill(null);
	arr.clazz=arrayClass;
	return arr;
}

multianewarray=function(type, sizes, dim){
	const size=sizes[dim];
	let array;
	if(dim>=sizes.length-1){
		switch(type){
			case 'L':
			case ';':
				array=nLa(size);
				break;
			case 'Z':
				array=nZa(size);
				break;
			case 'C':
				array=nCa(size);
				break;
			case 'F':
				array=nFa(size);
				break;
			case 'D':
				array=nDa(size);
				break;
			case 'B':
				array=nBa(size);
				break;
			case 'S':
				array=nSa(size);
				break;
			case 'I':
				array=nIa(size);
				break;
			case 'J':
				array=nJa(size);
				break;
			default: throw "unknown arraytype: "+type;
		}
	}
	else{
		array=new Array(size);
		for(let i=0;i<size;++i)
			array[i] = multianewarray(type, sizes, dim + 1);
	}
	array.clazz=arrayClass;
	return array;
}
class JString extends ClassInstance{
	constructor(str) {
		super(java_lang_String);
		this.str=str;
	}
}
self.JString=JString;

JString.strings={};
JString.strings["toString"]=undefined;
JString.strings["valueOf"]=undefined;
JString.intern=function(str){
	if(!JString.strings[str])
		JString.strings[str]=new JString(str);
	return JString.strings[str];
}


class JClass extends ClassInstance{
	constructor(clazz, primitive) {
		super(rcl.classes["java/lang/Class"]);
		this.fields.name=new JString(clazz.name);
		this.the_clazz=clazz;
		this.primitive = (primitive===true);
	}
}
class JConstructor extends ClassInstance{
	constructor(clazz, method) {
		super(rcl.classes["java/lang/reflect/Constructor"]);
		this.the_method=method;
		this.the_clazz=clazz;
	}
}
class JInteger extends ClassInstance{
	constructor(value) {
		super(java_lang_Integer);
		this.value=value;
	}
}
class JFloat extends ClassInstance{
	constructor(value) {
		super(java_lang_Float);
		this.value=value;
	}
}
class JDouble extends ClassInstance{
	constructor(value) {
		super(rcl.classes["java/lang/Double"]);
		this.value=value;
	}
}
class JLong extends ClassInstance{
	constructor(value) {
		super(java_lang_Long);
		this.value=value;
	}
}
class JShort extends ClassInstance{
	constructor(value) {
		super(rcl.classes["java/lang/Short"]);
		this.value=value;
	}
}
class JCharacter extends ClassInstance{
	constructor(value) {
		super(rcl.classes["java/lang/Character"]);
		this.value=value;
	}
}
async function getClassInstance(cname){
	const clazz=await rcl.loadClass(cname, true);
	return clazz.getInstance();
}

self.initBASE=
async function(cl){
let java_lang_Object = await cl.loadClass("java/lang/Object",true);
java_lang_Object.bindNatives();
let java_lang_Class = await cl.loadClass("java/lang/Class",true);
java_lang_Class.bindNatives();
let java_lang_reflect_Constructor = await cl.loadClass("java/lang/reflect/Constructor",true);
java_lang_reflect_Constructor.bindNatives();
let java_lang_ThreadLocal = await cl.loadClass("java/lang/ThreadLocal",true);
java_lang_ThreadLocal.bindNatives();
self.java_lang_String = await cl.loadClass("java/lang/String",true);
java_lang_String.bindNatives();
self.java_lang_Thread = await cl.loadClass("java/lang/Thread",true);
java_lang_Thread.bindNatives();
self.java_lang_Integer = await cl.loadClass("java/lang/Integer",true);
java_lang_Integer.bindNatives();
jvm.to_clinit.add(java_lang_Integer);
let java_lang_Boolean = await cl.loadClass("java/lang/Boolean", false);
if(!java_lang_Boolean.initialized)
	await java_lang_Boolean.clinit();
//jvm.to_clinit.add(java_lang_Boolean);
self.java_lang_Float = await cl.loadClass("java/lang/Float",true);
java_lang_Float.bindNatives();
let java_lang_Double = await cl.loadClass("java/lang/Double",true);
java_lang_Double.bindNatives();
self.java_lang_Long = await cl.loadClass("java/lang/Long",true);
java_lang_Long.bindNatives();
self.java_lang_Short = await cl.loadClass("java/lang/Short",true);
java_lang_Short.bindNatives();
let java_lang_Character = await cl.loadClass("java/lang/Character",true);
java_lang_Character.bindNatives();
let java_lang_StringBuilder = await cl.loadClass("java/lang/StringBuilder",true);
java_lang_StringBuilder.bindNatives();
let java_lang_Math = await cl.loadClass("java/lang/Math",true);
jvm.to_clinit.add(java_lang_Math);
let java_lang_System = await cl.loadClass("java/lang/System",true);
java_lang_System.bindNatives();
jvm.to_clinit.add(java_lang_System);
let java_lang_Throwable = await cl.loadClass("java/lang/Throwable",true);
java_lang_Throwable.bindNatives();
let java_lang_Exception = await cl.loadClass("java/lang/Exception",true);
self.java_lang_InterruptedException= await cl.loadClass("java/lang/InterruptedException",true);

let java_lang_RuntimeException = await cl.loadClass("java/lang/RuntimeException",true);
self.java_lang_NullPointerException = await cl.loadClass(_NP,true);
self.java_lang_Error = await cl.loadClass("java/lang/Error",true);
self.java_lang_UnsatisfiedLinkError = await cl.loadClass("java/lang/UnsatisfiedLinkError",true);
self.java_lang_AE = await cl.loadClass(_AE,true);
let java_lang_ClassCastException = await cl.loadClass("java/lang/ClassCastException",true);
self.java_io_IOException = await cl.loadClass("java/io/IOException",true);
let js_Console = await cl.loadClass("js/Console",true);
js_Console.bindNatives();

let reentrantLockClass = await cl.loadClass("java/util/concurrent/locks/ReentrantLock",true);
reentrantLockClass.bindNatives();
jvm.inits.push(async function(cl){

cl.
dn(java_lang_Throwable, "printStackTrace()V", function(stack, $this){
       printStackTrace($this);
});

cl.
dn(java_lang_StringBuilder,"<init>()V", function(stack, $this){
	$this.value="";
});
cl.
dn(java_lang_StringBuilder,"<init>(Ljava/lang/String;)V",function(stack, $this, str){
	$this.value=str.str;
});
cl.
dn(java_lang_StringBuilder,"append(C)Ljava/lang/StringBuilder;", function(stack, $this, ch){
	$this.value+=String.fromCharCode(ch);
	return $this;
});
cl.
dn(java_lang_String,"init([B)[C", function(stack, $this, arr){
	const str= new TextDecoder("utf-8").decode(arr);
	$this.str=str;
	return 0;
});
cl.
dn(java_lang_String,"toString()Ljava/lang/String;", function(stack, $this){
	return $this;
});
cl.
dn(java_lang_String,"intern()Ljava/lang/String;", function(stack, $this){
	return JString.intern($this.str);
});


cl.
dn(java_lang_String,"charAt(I)C", function(stack, $this, index){
	const c=$this.str.charCodeAt(index);
	return c;
});
cl.
dn(java_lang_String,"indexOf(I)I", function(stack, $this, ch){
	const c=String.fromCharCode(ch);
	return $this.str.indexOf(c);
});
cl.
dn(java_lang_String,"toCharArray()[C", function(stack, $this){
	const charArray=Array.from($this.str);
	const len=charArray.length;
	const arr = nSa(len);
	for(let i=0;i<len;++i)
		arr[i]=charArray[i].charCodeAt(0);
	return arr;
});
cl.
dn(java_lang_String,"startsWith(Ljava/lang/String;)Z",function(stack, $this, prefix){
	if($this.str.startsWith(prefix.str))
		return 1;
	else
		return 0;
});
cl.
dn(java_lang_String,"compareTo(Ljava/lang/String;)I",function(stack, $this, str){
	return $this.str.localeCompare(str.str);
});
cl.
dn(java_lang_String,"replace(CC)Ljava/lang/String;",[_L,_L],function(stack, $this, character, replacement){
	const str=$this.str.replace(RegExp(String.fromCharCode(character), "g"), String.fromCharCode(replacement));
	return new JString(str);
});
cl.
dn(java_lang_String,"replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", function(stack, $this, regex, replacement){
	var re = new RegExp(regex.str, 'g');
	const str=$this.str.replace(re, replacement.str);
	return new JString(str);
});
cl.
dn(java_lang_String,"matches(Ljava/lang/String;)Z", function(stack, $this, regex){
	return RegExp(regex.str).test($this.str)?1:0;
});
cl.
dn(java_lang_String,"equals(Ljava/lang/Object;)Z", function(stack, $this, obj){
	if(!obj) return 0;
	if($this.str==obj.str)
		return 1;
	else
		return 0;
});
cl.
dn(java_lang_String,"valueOf(Ljava/lang/Object;)Ljava/lang/String;", function(stack, obj){
	if(obj instanceof JString)
		return obj;
	else throw "check me";
});
cl.
dn(java_lang_String,"valueOf(I)Ljava/lang/String;", function(stack, i){
	return new JString(i+"");
});
const length=(highP - lowP) + 1;
self.intcache = new Array(length);
self.longcache = new Array(length);
self.shortcache = new Array(length);
let j = lowP;
for(let k = 0; k < length; ++k){
	intcache[k] = new JInteger(j);
	longcache[k] = new JLong(BigInt(j));
	shortcache[k] = new JShort(j);
	++j;
}

});
}
function java_lang_System_print0(stack, text){
	if(text)
		text=text.str;
	else text="null";
	if(self.out)
		out(text);
	else
		console.log(text);
}

function java_lang_System_arraycopy(stack, src, srcPos, dest, destPos, count){
		if(src==dest)
			src=src.slice(0);
		for(let i=0;i<count;++i)
				dest[destPos+i]=src[srcPos+i];
}

function java_lang_System_gc(stack){
		//we use the garbage collector of the Browser
}
function java_lang_System_currentTimeMillis(stack){
	return BigInt(Date.now());
}
function java_lang_System_nanoTime(stack){
	return BigInt(Date.now()*1000);
}
async function java_lang_System_loadLibrary(stack, name){
	const lib=name.str;
	await jvm.loadLibrary(lib);
}
var last_hash=1;
function java_lang_Object_hashCode(stack, $this){
	if($this.hash===undefined)
		$this.hash=last_hash++;
	return $this.hash;
}
function java_lang_Object_wait(thread, $this){
	if($this.owner!=thread)
		throw "illegal monitor";
	if(thread.interrupted){
		thread.interrupted=false;
		const ie=java_lang_InterruptedException;
		throw new ClassInstance(ie);
	}
	monitorExit($this);
	if(!$this.waitingQueue)
		$this.waitingQueue=[];
	return new Promise(function(resolve, reject){
			thread.notify=function(){
					if($this.owner){
							//if(i.owner===thread) return;
							if(!$this.monitorQueue)
									$this.monitorQueue=[];
							thread.state=TS_MONITOR;
							thread.monitorExit=resolve;
							$this.monitorQueue.push(thread);
							return;
					}
					$this.owner=thread;
					thread.state=TS_RUNNING;
					sthreads.push(resolve);
			};
		thread.interrupt=function(){
			const ie=java_lang_InterruptedException;
			reject(new ClassInstance(ie));
		};
		thread.state=TS_WAIT;
		$this.waitingQueue.push(thread);
	});
}

function java_lang_Object_notify(stack, $this){
	if($this.owner!=stack)
		throw "illegal monitor";
	if($this.waitingQueue){
		const thread=$this.waitingQueue.shift();
		thread.notify();
	}
}

function java_lang_Object_notifyAll(stack, $this){
	if($this.waitingQueue)
		sthreads.concat($this.waitingQueue);
}
function java_lang_Object_getClass(stack, $this){
	return $this.clazz.getInstance();
}
function java_lang_Object_getSuperClass(stack, $this){
	return $this.clazz.super_class.getInstance();
}
async function java_lang_Class_forName(stack,name){
	let classname=name.str;
	switch(classname){
		case "I":
		case "F":
		case "J":
		case "D":
		case "B":
		case "C":
		case "S":
		case "Z":
		case "V":
			return new JClass(classname, true);
		default:
			const clazz=await rcl.loadClass(classname.replace(/\./g,'/'), false);
			return clazz.getInstance();
	}
}

async function java_lang_Class_getConstructor(stack, $this, argTypes){
	const clazz=$this.the_clazz;
	const l=argTypes.length;
	const ructor=await clazz.getMethodByName("<init>()V");
	return new JConstructor(clazz, ructor);
}

async function java_lang_Thread_interrupt(stack, $this){
	if($this.thread.interrupt){
		sthreads.push($this.thread.interrupt);
	}
	else
		$this.thread.interrupted=true;

}

function java_lang_Thread_sleep(stack, duration){
	return new Promise(async function(resolve, reject){
		stack.interrupt=function(){
			stack.interrupt=undefined;
			if($this.thread.timeout)
				clearTimeOut($this.thread.timeout);
			reject(new ClassInstance(java_lang_InterruptedException));
		};
		stack.state=TS_SLEEPING;
		stack.timeout=setTimeout(function(){
			stack.interrupt=undefined;
			stack.timeout=undefined;
			sthreads.push(resolve);
			stack.state=TS_RUNNING;
			_yield();
		}, Number(duration));
		_yield();
	});
}

function java_lang_Thread_currentThread(stack){
	return stack.instance;
}
function java_lang_Thread_init(stack, $this){
	const thread=new Thread();
	$this.thread=thread;
	thread.instance=$this;
}
async function java_lang_Thread_start(stack, $this){
	const m=await java_lang_Thread.getMethodByName("run()V");
	startThread(m, $this, $this);
}
async function java_lang_Thread_yield(stack){
	await st();
}
function java_lang_Thread_join(stack, $this){
	if($this.thread.state===TS_ZOMBIE)
		return;
	return new Promise(function(resolve, reject){
		const thread=$this.thread;
		if(!thread.joined)
			thread.joined=[];
		thread.joined.push(resolve);
		stack.state=TS_WAIT;
		_yield();
	});
}
function java_lang_Thread_setName(stack, $this){
}
function java_lang_Thread_setPriority(stack, $this, newPriority){
	$this.thread.setPriority(newPrioroty);
}
function java_lang_ThreadLocal_get0(stack, $this){
	if(stack.locals)
		return stack.locals[$this.threadLocalID];
	return null;
}
var threadLocalIDCounter=0;
function java_lang_ThreadLocal_set(stack, $this, v){
	if(!stack.locals){
		stack.locals={};
	}
	const id=++threadLocalIDCounter;
	stack.locals[id]=v;
	$this.threadLocalID=id;
}
function java_lang_ThreadLocal_remove(stack, $this){
	if(stack.locals)
		delete stack.locals[$this.threadLocalID];
}

function js_Console_log(stack, text){
	if(text)
		console.log(text.str);
	else
		console.log("null");
}
function js_Console_error(stack, text){
	if(text)
		console.error(text.str);
	else
		console.error("null");
}
function js_Console_warn(stack, text){
	if(text)
		console.warn(text.str);
	else
		console.warn("null");
}
function js_Console_debugger(stack){
	debugger;
}
function java_util_concurrent_locks_ReentrantLock_init(stack){
}
function java_util_concurrent_locks_ReentrantLock_lock(stack){
}
function java_util_concurrent_locks_ReentrantLock_unlock(stack){
}

function java_lang_reflect_Array_newInstance(stack, componentType, length){
	if(componentType.primitive){
		switch(componentType.the_clazz){
			case 'Z':
			case 'B':
				return nBa(length);
			case 'C':
				return nCa(length);
			case 'S':
				return nSa(length);
			case 'I':
				return nIa(length);
			case 'F':
				return nFa(length);
			case 'D':
				return nDa(length);
			case 'J':
				return nLa(length);

		}
	}
	return new Array(length);
}

async function java_lang_reflect_Constructor_newInstance(stack, $this, args){
	const ructor=$this.the_method;
	const f=new Frame(ructor);
	f.init();
	const ci=new ClassInstance($this.the_clazz);
	stack.push(f);
	await ructor(stack, ci);
	return ci;
}
function java_util_EnumMap_getKeyUniverse(stack, clazz){
		return clazz.the_clazz.statics.ENUM$VALUES;
}
function java_lang_Float_valueOf(stack, f){
	return new JFloat(f);
}
function java_lang_Character_valueOf(stack, ch){
	return new JCharacter(ch);
}
function java_lang_Character_toString(stack, ch){
	return new JString(String.fromCharCode(ch));
}
function java_lang_Double_valueOf(stack, d){
	return new JDouble(d);
}
function java_lang_Integer_valueOf(stack, i){
	if (i >= lowP && i <= highP){
		return intcache[i + (-lowP)];
	}
	return new JInteger(i);
}
self.ivO=java_lang_Integer_valueOf;
function java_lang_Long_valueOf(stack, i){
	if (i >= lowP && i <= highP){
		return longcache[Number(i) + (-lowP)];
	}
	return new JLong(i);
}
function java_lang_Long_toString(stack, j){
	return new JString(j+"");
}
function java_lang_Integer_floatValue(stack, $this){
	return $this.value;
}
function java_lang_Integer_toString(stack, i){
	return new JString(i+"");
}
function java_lang_Integer_intValue(stack, $this){
	return $this.value;
}
function java_lang_String_substring(stack, $this, index){
	const str=$this.str.substring(index);
	return new JString(str);
}
function java_lang_String_contains(stack, $this, string){
	return $this.str.includes(string.str)?1:0;
}
function java_lang_String_length(stack, $this){
	return $this.str.length;
}
function java_lang_Integer_longValue(stack, $this){
	return BigInt($this.value);
}
function java_lang_StringBuilder_append(stack, $this, str){
	if(str)
		$this.value+=str.str;
	else
		$this.value+="null";
	return $this;
}
function java_lang_StringBuilder_toString(stack, $this){
	return new JString($this.value);
}
String.prototype.hashCode = function() {
	  var hash = 0, i, chr;
	  if (this.length === 0) return hash;
	  for (i = 0; i < this.length; ++i) {
		      chr   = this.charCodeAt(i);
		      hash  = ((hash << 5) - hash) + chr;
		      hash |= 0; // Convert to 32bit integer
		    }
	  return hash;
};
function java_lang_String_hashCode(stack, $this){
	const string=$this;
	if(!string.hash)
		string.hash=string.str.hashCode();
	return string.hash;
}
function java_lang_Float_toString(stack, value){
	return new JString(value+"");
}
function java_lang_Float_floatValue(stack, $this){
	return $this.value;
}
function java_lang_Integer_hashCode(stack, $this){
	return $this.value;
}
function java_lang_Integer_equals(stack, $this, obj){
	return $this.value===obj.value?1:0;
}
function java_lang_Integer_toHexString(stack, i){
	return new JString(i.toString(16));
}
function java_lang_Integer_doubleValue(stack, $this){
	return $this.value;
}
function java_lang_Integer_parseInt(stack, str, radix){
	return parseInt(str.str);
}
function java_lang_Long_hashCode(stack, $this){
	return Number($this.value)|0;
}
function java_lang_Long_equals(stack, $this, obj){
	return $this.value===obj.value?1:0;
}
function java_lang_Long_doubleValue(stack, $this){
	return $this.value;
}
function java_lang_Long_intValue(stack, $this){
	return $this.value;
}
function java_lang_Long_floatValue(stack, $this){
	return $this.value;
}
function java_lang_Long_toString(stack, j){
	return new JString(j+"");
}
function java_lang_Long_longValue(stack,$this){
	return $this.value;
}
function java_lang_Short_valueOf(stack, i){
	if (i >= lowP && i <= highP){
		return shortcache[i + (-lowP)];
	}
	return new JShort(i);
}
function java_lang_Short_hashCode(stack, $this){
	return $this.value;
}
function java_lang_Short_equals(stack, $this, obj){
	return $this.value===obj.value?1:0;
}
function java_lang_Short_longValue(stack, $this){
	return BigInt($this.value);
}
function java_lang_Short_intValue(stack, $this){
	return $this.value;
}

function java_lang_Character_charValue(stack, $this){
	return $this.value;
}
function java_lang_Float_intValue(stack, $this){
	return $this.value|0;
}
function java_lang_Float(stack, str){
	return parseFloat(str.str);
}
function java_lang_Float_longValue(stack, $this){
	return BigInt($this.value|0);
}
function java_lang_Double_toString(stack, v){
	return new JString(v+"");
}
function java_lang_Double_doubleValue(stack, $this){
	return $this.value;
}
function java_lang_Double_intValue(stack, $this){
	return $this.value|0;
}
function java_lang_Double_longValue(stack, $this){
	return BigInt($this.value|0);
}
function java_lang_Double_valueOf(stack, d){
	return new JDouble(d);
}

function java_lang_String_toUpperCase(stack, $this){
	const str=$this.str.toUpperCase();
	return new JString(str);
}
function java_lang_String_toLowerCase(stack, $this){
	const str=$this.str.toLowerCase();
	return new JString(str);
}
function java_lang_String_lastIndexOf(stack, $this, str, fromIndex){
	return $this.str.lastIndexOf(str.str, fromIndex);
}
