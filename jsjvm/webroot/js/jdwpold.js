const VM_COMMAND=1;
	const CLASSES_BY_SIGNATURE=2;

const GET_LOADED_CLASS=1;
const GET_LOADED_CLASSES=2;
const GET_CLASS_BYID=3;
const SUSPEND=4;
const RESUME=5;
const ADD_EVENT_LISTENER=6;
const REMOVE_EVENT_LISTENER=7;
const RESPONSE=8;
const EVENT=9;
const GET_THREAD_FRAMES=10;
const DISPOSE=11;

//EventKinds:
const SINGLE_STEP=1;
const BREAKPOINT=2;
const CLASS_PREPARE=8;

function isPrimitive(test) {
    return (test !== Object(test));
}

let refCounter=1;
function parseJValues(lv) {
		if(!lv)
				return undefined;
		const l=lv.length;
		let rlv=new Array(l);
		for(let i=0;i<l;i++){
				const value=lv[i];
				if(!value){
						rlv[i]=0;
						continue;
				}
				if(isPrimitive(value))
						rlv[i]=value;
				else{
						if(!value.ref){
								value.ref=refCounter++;
						}
						rlv[i]=value.ref;
				}
		}
		return rlv;
}

class EventHandler{
	constructor(jdwp, suspendPolicy, requestID){
		this.jdwp=jdwp;
		this.requestID=requestID;
		this.suspendPolicy=suspendPolicy;
	}
	remove(){}
	handle(){}
	sendEvent(ev){
			ev.cmd=EVENT;
			ev.requestID=this.requestID;
			ev.eventKind=this.eventKind;
			this.jdwp.socket.send(JSON.stringify(ev));
			this.jdwp.suspended=this.suspendPolicy>0;
	}
}

class SingleStepHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods, currentStack){
		super(jdwp, suspendPolicy, requestID);	
		this.eventKind=SINGLE_STEP;
		if(jdwp.onSingleStep)
			throw "another singleStep event";
		const l=mods.length;
		let loc;
		let depth;
		let size;
		let count=1;
		for(let i=0;i<l;++i){
			const mod=mods[i];
			switch(mod.kind){
				case 1://threadID
					count=mod.count;
					break;
				case 6://classFilter
					break;
				case 10:
					depth=mod.depth;
					size=mod.size;
					break;
				default: debugger;
			}
		}
		const $this = this;
		let currentStackDepth=currentStack.entries.length;
		const cF=currentStack.peek();
		let currentPC=cF.pc;
		const lineNumbers=cF.method.lineNumbers;
		const len=lineNumbers.length;
		const currentLine=lineNumbers[currentPC];
		let nextPC=currentPC;
		for(let i=currentPC+1;i<len;++i){
			if(lineNumbers[i]!=currentLine){
				nextPC=i;
				break;
			}
		}
		switch(depth){
			case 0:
				debugger;
				//step INTO
				jdwp.onSingleStep=function(stack, pc){
					const f=stack.peek();
					const loc={};
					loc.clazz=f.method.clazz.name;
					loc.method=f.method.signature;
					loc.pc=pc;
					$this.handle(loc);
				}
				break;
			case 1:
				//step OVER
				jdwp.onSingleStep=function(stack, pc){
					if(stack.entries.length>currentStackDepth) return;
					const f=stack.peek();
					if(pc>nextPC){
						const loc={};
						loc.clazz=f.method.clazz.name;
						loc.method=f.method.signature;
						loc.pc=pc;
						$this.handle(loc);
					}
				}
				break;
			case 2:
				debugger;
				//step OUT
				jdwp.onSingleStep=function(stack, pc){
					debugger;
					const loc={};
					loc.clazz=frame.method.clazz.name;
					loc.method=frame.method.signature;
					loc.pc=pc;
					const e={cmd:EVENT, eventKind:1, loc:loc, requestID:requestID};
					$this.socket.send(JSON.stringify(e));
				}
				break;
		}
	}
	remove(){
		this.jdwp.onSingleStep=undefined;
	}
	handle(loc){
		this.sendEvent({loc:loc});	
	}
}
class BreakPointHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods){
		super(jdwp, suspendPolicy, requestID);	
			this.eventKind=BREAKPOINT;
			const l=mods.length;
			let loc;
			for(let i=0;i<l;++i){
					const mod=mods[i];
					switch(mod.kind){
							case 3://threadID
									break;
							case 7:
									loc=mod.loc;
									break;
							default: debugger;
					}
			}
			debugger;
			const method=self["f"+loc.method];
			let breakpoints=method.breakpoints;
			if(!breakpoints){
					breakpoints={};
					method.breakpoints=breakpoints;
			}
			breakpoints[loc.pc]={requestID: requestID, loc:loc};
			this.method=method;
			this.loc=loc;
	}
	remove(){
			delete this.method.breakpoints[this.loc.pc];
	}
	handle(){
			this.sendEvent({loc:this.loc});
	}
}

class ClassPrepareHandler extends EventHandler{
	constructor(jdwp, suspendPolicy, requestID, mods){
		super(jdwp, suspendPolicy, requestID);	
			this.eventKind=CLASS_PREPARE;
			let classPattern=undefined;
			let classExclude=undefined;
			let l=mods.length;
			for(let i=0;i<l;++i){
					const mod=mods[i];
					switch(mod.kind){
							case 5:
									classPattern=mod.classPattern;
									break;
							case 6:
									classExclude=mod.classExclude;
									break;
							default:debugger;

					}
			}
			this.classPattern=classPattern;
			this.classExclude=classExclude;
			//this.class_prepare_events.push({pattern:classPattern, exclude:classExclude, requestID:requestID});
	}
	remove(){
			let evs=jdwp.classPrepareEvents;
			let l=evs.length;
			for(let i=0;i<l;++i){
				if(evs[i].requestID==this.requestID){
					evs.splice(i, 1);
					return;
				}
			}
			console.error("could not remove ClassPrepare event Handler");
			debugger;
	}
	handle(className){
			if(this.classPattern==undefined){
				this.sendEvent({name:className});
				return;
			}
			if(this.classExclude==className)
					return;
			if(this.classPatern==className){
				this.sendEvent({name:className});
				return;
			}
	}
}

class JDWP{
		constructor(){
				this.eventHandlers={};
				this.classPrepareEvents=[];
		}
		async waitForConnection(){
				const $this=this;
				return new Promise(function(resolve, reject){
						if($this.connected){
								resolve();
								return;
						}
						$this.onOpen=function(){
							resolve();
						}
				});
		}
		connect(){
				var loc = window.location, new_uri;
				if (loc.protocol === "https:") {
						new_uri = "wss:";
				} else {
						new_uri = "ws:";
				}
				new_uri += "//"+loc.host+"/jdwp";
				let socket;
				try{
						socket=new WebSocket(new_uri);
						this.socket=socket;
				}
				catch(error){
						console.error(error);
						return;
				}
				const $this=this;
				$this.socket.onmessage=async function(ev){
						const cmd=JSON.parse(ev.data);
						console.log(ev.data);
						$this.connected=true;
						switch(cmd.cmd){
								case GET_LOADED_CLASS:
										$this.getLoadedClass(cmd.name);
										break;
								case GET_CLASS_BYID:
										$this.getClassByID(cmd.id);
										break;
								case GET_LOADED_CLASSES:
										$this.getLoadedClasses();
										break;
								case SUSPEND:
										$this.suspended=true;
										break;
								case RESUME:
										$this.suspended=false;
										if($this.resume)
												$this.resume();
										break;
								case DISPOSE:
										$this.suspended=false;
										if($this.resume)
												$this.resume();
										$this.socket.close();
										createJDWPConnection();
										break;
								case ADD_EVENT_LISTENER:
										$this.addEventListener(cmd.suspendPolicy, cmd.eventKind, cmd.requestID, cmd.mods);
										break;
								case REMOVE_EVENT_LISTENER:
										$this.removeEventListener(cmd.eventKind, cmd.requestID);
										break;
								case GET_THREAD_FRAMES:
										$this.getThreadFrames();
										break;
						}
						if($this.onOpen)
								$this.onOpen();
				}
				$this.socket.onopen=function(ev){

				}
				$this.socket.onclose=function(ev){
						$this.connected=false;
				}
				$this.socket.onerror=function(error){
						$this.connected=false;
				}
		}
		getClassByID(id){
				debugger;
				const clazz=jvm.cs[id];
				let toSend;
				if(clazz)
					toSend={name:name, isInterface: 0, isInitialized:true, id: clazz.id};
				else
					toSend={};
				this.socket.send(JSON.stringify({cmd:RESPONSE, response:toSend}));
		}
		getLoadedClass(name){
				debugger;
				const clazz=jvm.classes[name];
				let toSend;
				if(clazz)
					toSend={name:name, isInterface: 0, isInitialized:1, id: clazz.id};
				else
					toSend={};
				this.socket.send(JSON.stringify({cmd:RESPONSE, response:toSend}));
		}
		getLoadedClasses(){
			let toSend=[];
			const classes=jvm.classes;
			const names=Object.keys(classes);
			const l=names.length;
			for(let i=0;i<l;++i){
				const name=names[i];
				const clazz=classes[name];
				toSend.push({name:name, isInterface: 0, isInitialized:true, id: clazz.id});

			}
			this.socket.send(JSON.stringify({cmd:RESPONSE, response:toSend}));
		}

		removeEventListener(eventKind, requestID){
				const handler=this.eventHandlers[requestID];
				if(!handler){
						console.error("eventHandler not found. requestID: "+requestID);
						debugger;
						return;
				}
				handler.remove();
				delete this.eventHandlers[requestID];
		}

		addEventListener(suspendPolicy, eventKind, requestID, mods){
				let handler;
				let msize;
				switch(eventKind){
						case SINGLE_STEP:
								handler=new SingleStepHandler(this, suspendPolicy, requestID, mods, jvm.threads.currentThread);
								break;
						case BREAKPOINT:
								handler=new BreakPointHandler(this, suspendPolicy, requestID, mods);
								break;
						case CLASS_PREPARE:
								handler=new ClassPrepareHandler(this, suspendPolicy, requestID, mods);
								/*
								if(handler.classPattern){
										if(jvm.classes[handler.classPattern]){
												handler.handle(handler.classPattern);
												return;
										}
								}*/
								this.classPrepareEvents.push(handler);
								if(jvm.events.onClassPrepared)break;
								const $this=this;
								const classPrepareEvents=this.classPrepareEvents;
								jvm.events.onClassPrepared=function(name){
										const l=classPrepareEvents.length;
										for(let i=0;i<l;++i){
											const ev=classPrepareEvents[i];
											ev.handle(name);
										}
								};
								break;
				}
				this.eventHandlers[requestID]=handler;
		}
		async breakpoint(brk){
			const $this=this;
			return new Promise(function(resolve, reject){
				$this.resume=function(){
					resolve();
				}
				const handler=$this.eventHandlers[brk.requestID];
				if(!handler){
					console.error("eventHandler not found. requestID: "+requestID);
					return;
				}
				handler.handle();
			});	
		}
		singleStep(requestID){
		}
		async wait(){
			const $this=this;
			return new Promise(function(resolve, reject){
				$this.resume=function(){
					$this.resume=undefined;
					resolve();
				}
			});	
		}
		getThreadFrames(){
			refCounter=1;
			const jframes=jvm.threads.currentThread.entries;
			const l=jframes.length;
			let frames=new Array(l);
				for(let i=0;i<l;++i){
						let jframe=jframes[i];	
						let frame={};
						frame.clazz=jframe.method.clazz.name;
						frame.method=jframe.method.signature;
						frame.pc=jframe.pc;
						frame.lv=parseJValues(jframe.lv);
						frame.stack=parseJValues(jframe.stack);
						frames[i]=frame;
				}
			this.socket.send(JSON.stringify({cmd:RESPONSE, frames:frames}));
		}

}
var jdwp;
function createJDWPConnection(){
	jdwp=new JDWP();
	jdwp.connect();
}
createJDWPConnection();
async function dstep(stack, pc, lv){
		if(jdwp.suspended){
				await jdwp.wait();
		}
		if(jdwp.onSingleStep){
				stack.peek().pc=pc;
				jdwp.onSingleStep(stack, pc);
		}
		const method=stack.peek().method;
		if(method.breakpoints){
				if(method.breakpoints[pc]){
						stack.peek().pc=pc;
						await jdwp.breakpoint(method.breakpoints[pc]);
				}
		}
}
