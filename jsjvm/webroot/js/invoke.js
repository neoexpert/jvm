"use strict";
self.
init_invoke=async function(cl){
jvm.inits.push(async function(cl){
	const lookupClass=await cl.loadClass("java/lang/invoke/MethodHandles$Lookup",false);
	lookupClass.bindNatives();
	const lambdaFactory=await cl.loadClass("java/lang/invoke/LambdaMetafactory",false);
	const methodHandleClass=await cl.loadClass("java/lang/invoke/MethodHandle",false);
	const callsiteClass=await cl.loadClass("java/lang/invoke/CallSite",false);
	cl.
		dn(lambdaFactory,"metafactory(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;",  async function(stack, caller, invokedName, invokedType, samMethodType, implMethod, instantiatedMethodType){
			const callsite=new ClassInstance(callsiteClass);
			let m;
			let theMethod=implMethod.clazz.method;
			if(theMethod.isStatic){
				m=
					function(stack){
						var s_args = [].slice.call(arguments);
						s_args.shift();
						m=
							function(stack, $this){
								var args = [].slice.call(arguments);
								args.shift();
								args.shift();
								let _args=s_args.concat(args);
								_args.unshift(stack);
								return theMethod.apply(null, _args);
							};
						m=processMethod(theMethod.clazz, m);
						let mhc=new MethodHandleClass(m);
						mhc.setSignature(invokedName.str+samMethodType.asString);
						stack.pop();
						return new ClassInstance(mhc);	
					};
				m=processMethod(null,m);
			}
			else{
				m=
					function(stack, obj){
						var s_args = [].slice.call(arguments);
						s_args.shift();
						m=
							function(stack, $this){
								var args = [].slice.call(arguments);
								args.shift();
								args.shift();
								let _args=s_args.concat(args);
								_args.unshift(stack);
								return theMethod.apply(null, _args);
							};
						m=processMethod(theMethod.clazz,m);
						let mhc=new MethodHandleClass(m);
						mhc.setSignature(invokedName.str+samMethodType.asString);
						stack.pop();
						return new ClassInstance(mhc);	
					};
				m=processMethod(null,m);
			}
			const mh=new MethodHandleClass(m);
			callsite.fields.target=new ClassInstance(mh);
			return callsite;
		});
	/*
	jvm.
		dn(lookupClass,"findVirtual(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;", [_L, _L, _L], async function(stack, $this, clazz, methodname, type){
			let signature=methodname.str+"(";
			const ptypes=type.fields.ptypes;
			const l=ptypes.length;
			for(let i=0;i<l;++i){
				const ptype=ptypes[i];
				if(ptype.primitive)
					signature+=ptype.the_clazz;
				else
					signature+="L"+ptype.the_clazz.name+";";
			}
			signature+=")";
			const rtype=type.fields.rtype;
			if(rtype.primitive)
				signature+=rtype.the_clazz;
			else
				signature+="L"+rtype.the_clazz.name+";";
			const m=await clazz.the_clazz.getMethodByName(signature);

			return new ClassInstance(new MethodHandleClass(m));
		});
		*/
	cl.
		dn(lookupClass,"findStatic(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;", async function(stack, $this, clazz, methodname, type){
			let signature=methodname.str+"(";
			const ptypes=type.fields.ptypes;
			const l=ptypes.length;
			for(let i=0;i<l;++i){
				const ptype=ptypes[i];
				if(ptype.primitive)
					signature+=ptype.the_clazz;
				else
					signature+="L"+ptype.the_clazz.name+";";
			}
			signature+=")";
			const rtype=type.fields.rtype;
			if(rtype.primitive)
				signature+=rtype.the_clazz;
			else
				signature+="L"+rtype.the_clazz.name+";";
			const m=await clazz.the_clazz.getMethodByName(signature);

			return new ClassInstance(new MethodHandleClass(m));
		});

});
}
async function java_lang_invoke_MethodHandles$Lookup_findVirtual(stack, $this, clazz, methodname, type){
	let signature=methodname.str+"(";
	const ptypes=type.fields.ptypes;
	const l=ptypes.length;
	for(let i=0;i<l;++i){
		const ptype=ptypes[i];
		if(ptype.primitive)
			signature+=ptype.the_clazz;
		else
			signature+="L"+ptype.the_clazz.name+";";
	}
	signature+=")";
	const rtype=type.fields.rtype;
	if(rtype.primitive)
		signature+=rtype.the_clazz;
	else
		signature+="L"+rtype.the_clazz.name+";";
	const m=await clazz.the_clazz.getMethodByName(signature);

	return new ClassInstance(new MethodHandleClass(m));
}
