async function init_bytebuffer(jvm){
self.java_nio_ByteBuffer=await jvm.getClass("java/nio/ByteBuffer",true);
self.java_nio_NativeByteBuffer=await jvm.getClass("java/nio/NativeByteBuffer",true);
self.java_nio_ByteOrder=await jvm.getClass("java/nio/ByteOrder",true);
jvm.inits.push(function(jvm){
jvm.
dn(java_nio_NativeByteBuffer,"init([B)V",[_ARR],function(stack, $this, arr){
	//heap.get(lv[0]).value="";
	const dv=new DataView(new Int8Array(arr.arr).buffer);
	const fields=$this.fields;
	$this.dv=dv;
	fields.position=0;
	fields.capacity=dv.byteLength;
	fields.limit=dv.byteLength;
	$this.littleEndian=false;
});
jvm.
dn(java_nio_NativeByteBuffer,"getInt()I",[],function(stack,lv,pF){
	//heap.get(lv[0]).value="";
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=4;
	return (me.dv.getInt32(pos,me.littleEndian))
});
jvm.
dn(java_nio_NativeByteBuffer,"getLong()J",[],function(stack,lv,pF){
	const me=lv[0];
	const f=stack.peek();
	let l0=me.dv.getInt32(me.fields.position+4,me.littleEndian);
	let l1=me.dv.getInt32(me.fields.position,me.littleEndian);
	me.fields.position+=8;
	return l0;
});
jvm.
dn(java_nio_NativeByteBuffer,"getFloat()F",[],function(stack,lv,pF){
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=4;
	return me.dv.getFloat32(pos, me.littleEndian);
});
jvm.
dn(java_nio_NativeByteBuffer,"getDouble()D",[],function(stack,lv,pF){
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=8;
	return me.dv.getFloat64(pos, me.littleEndian);
});
jvm.
dn(java_nio_NativeByteBuffer,"get()B",[],function(stack,lv,pF){
	//heap.get(lv[0]).value="";
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=1;
	return me.dv.getInt8(pos);
});
jvm.
dn(java_nio_NativeByteBuffer,"get([B)Ljava/nio/ByteBuffer;",[_ARR],function(stack,lv,pF){
	const arr=lv[1].arr;
	const length=lv[1].length();
	const me=lv[0];
	const dv=me.dv;
	const position=me.fields.position;
	for(let i=0;i<length;++i){
		arr[i]=dv.getInt8(position+i);
	}
	me.fields.position+=length;
	return (me);
});
jvm.
dn(java_nio_NativeByteBuffer,"getChar()C",[],function(stack,lv,pF){
	//heap.get(lv[0]).value="";
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=2;
	return (me.dv.getUint16(pos, me.littleEndian));
});
jvm.
dn(java_nio_NativeByteBuffer,"getShort()S",[],function(stack,lv,pF){
	//heap.get(lv[0]).value="";
	const me=lv[0];
	let pos=me.fields.position;
	me.fields.position+=2;
	return (me.dv.getInt16(pos, me.littleEndian))
});
jvm.
dn(java_nio_NativeByteBuffer,"order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;",[_L],function(stack,lv,pF){
	const me=lv[0];
	me.littleEndian=lv[1].fields.order.str=="LITTLE_ENDIAN";
	return me;
});
jvm.
dn(java_nio_NativeByteBuffer,"capacity()I",[],function(stack,lv,pF){
	const me=lv[0];
	return (me.fields.capacity)
});
jvm.
dn(java_nio_NativeByteBuffer,"limit()I",[],function(stack,lv,pF){
	const me=lv[0];
	return (me.fields.limit)
});
jvm.
dn(java_nio_NativeByteBuffer,"position()I",[],function(stack,lv,pF){
	const me=lv[0];
	return (me.fields.position)
});
jvm.
dn(java_nio_NativeByteBuffer,"limit(I)Ljava/nio/ByteBuffer;",[_I],function(stack,lv,pF){
	const me=lv[0];
	me.fields.limit=lv[1];
	return (me)
});
jvm.
dn(java_nio_NativeByteBuffer,"hasRemaining()Z",[],function(stack,lv,pF){
	const me=lv[0];
	const fields=me.fields;
	if(fields.limit - fields.position>0)
		return (1)
	else
		return (0);
});
});
}
