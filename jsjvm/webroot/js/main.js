const jvm_interface={};
jvm_interface.GETCLASS=async function(classname, classloader){
	let url=path+"?cmd=GETCLASS&classname="+classname;
	if(classloader!==undefined)
		url+="&cl="+classloader;
	return await GETJSON(url);
};
async function startAPP(initialClass){
	jvm_interface.GETCLASSES=function(classnames, constant_pool){
		return new Promise(function(resolve,reject){
			var parameters = {
				"classnames[]": classnames,
				"constant_pool":constant_pool
			};
			$.post(path+"?cmd=GETCLASSES",parameters,function(response){
				resolve(JSON.parse(response));
			});
		});
	};
	jvm_interface.GETINITIALCLASSES=async function(initial_classes, constant_pool, callback){
		var parameters = {
			  "classnames[]": initial_classes,
			  "constant_pool":constant_pool
		};
		return await GETJSON(path+"?cmd=GETINITIALCLASSES",parameters);
	};
	jvm_interface.GETMETHOD=async function(classname, methodname, classloader, callback){
		let url=path+"?cmd=GETMETHOD&classname="+classname+"&methodname="+methodname;
		if(classloader!==undefined)
			url+="&cl="+classloader;
		return await GETBIN(url);
	};

	jvm_interface.onError=function(stacktrace){
		if(confirm("the application has crashed, would you like to reload page? stacktrace: \n"+stacktrace))
			location.reload();

	};
	jvm_interface.getStackTrace=async function(message, stacktrace){
		const l=stacktrace.length;
		const st=new Array(l);
		for(let i=0;i<l;i++){
			const se=stacktrace[i];
			const method=se.method;
			const clazz=method.clazz;
			st[i]={classloader: clazz.classLoader.id, clazz:clazz.name, method:method.signature,pc:se.pc};
		}
		return new Promise(function(resolve, reject){
			$.post(path+"?cmd=STACKTRACE",{stacktrace:JSON.stringify(st)},function(response){
				const msg=message+"\n"+response;
				resolve(msg);
			});
		});
	};
	if(!jvm_interface.loadLib)
		jvm_interface.loadLib=async function(lib, callback){
			return new Promise(function(resolve, reject){
				var script = document.createElement('script');
				script.onload = resolve;
				script.src = libPath+lib+".js";
				document.head.appendChild(script);
			});
		};
	const jvm=createJVM(jvm_interface);
	//jvm.initial_classes.push("main/neodomui/DOMContext");
	if(initialClass)
		jvm.initial_classes.push(initialClass);
	startMain(jvm);
}

function GETJSON(url, data){
	return new Promise(function(resolve, reject){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		//xhr.responseType = "arraybuffer";
		xhr.onload = function() {
			if (xhr.status === 200) {
				resolve(JSON.parse(xhr.responseText));
			}
			else {
				reject();
			}
		};
		if(data)
			xhr.send(JSON.stringify(data));
		else
			xhr.send("");
	});
}
function GETBIN(url){
	return new Promise(function(resolve, reject){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.responseType = "arraybuffer";
		xhr.onload = function() {
			if (xhr.status === 200) {
				resolve(xhr.response);
			}
			else {
				reject(xhr.status);
			}
		};
		xhr.onerror=reject;
		xhr.send();
	});
}
function POSTJSON(url, data, callback){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	//xhr.responseType = "arraybuffer";
	xhr.onload = function() {
		if (xhr.status === 200) {
			callback(JSON.parse(xhr.responseText));
		}
		else {
			printStackTrace();
		}
	};
	xhr.send(JSON.stringify(data));
}

