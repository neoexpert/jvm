"use strict";
const _ARR=91;
const _J=74;
const _D=68;
const _L=76;
const _I=73;
const _F=70;
const _Z=90;
const _B=66;
const _C=67;
const _S=83;
//let heap;

class JVM{
	constructor(jvm_interface){
		this.classes={};
		this.classes_to_load=0;
		this.bex=new ByteCodeExecutor();
		//heap=new Heap(this.bex);
		this.waiting_threads=[];
		this.jvm_interface=jvm_interface;
		this.initial_classes=[];
		this.inits=[];
	}

	//addNativeMethod
	anm(classname, signature, args, handler){
		const clazz=Class.getClass(classname);
		if(!clazz){
			//debugger;
			var that=this;
			++this.classes_to_load;
			jvm.getClassFromServer(classname, function(c){
				that.anm(classname,signature,args,handler);
			});
		}
		else{
			clazz.methods[signature]=
				new NativeMethod(clazz, signature, args, args.length, handler);
			this.classes_to_load--;
			if(this.classes_to_load==0)
				if(this.oninit)
					this.oninit();
		}
	}


	setOnInitCallback(callback){
		this.oninit=callback;
	}

	getMethodFromServer(classname, signature, callback){
		//const fullname=classname+"."+signature;
		const clazz=jvm.classes[classname];
		let m;
		if(clazz)
			m=clazz.methods[signature];

		if(m && m!=="onserver")
			return m;
		const c=jvm.getClassFromServer(classname, true,function(c){
			jvm.getMethodFromServer(classname, signature, callback);
		});
		if(c)
			this.jvm_interface.
			GETMETHOD(classname, signature,true,  function(bytebuffer){
				callback(c, signature, bytebuffer);
			});
	}

	getClassFromServer(classname, callback){
		const c=jvm.classes[classname];
		if(c) return c;
		var that=this;
		this.jvm_interface.
		GETCLASS(classname, true, function(json){
			let c=jvm.classes[classname];
			if(c){
				if(callback)callback(c);
				return;
			}
			that.readClass(json,callback);
			//if(callback)callback(c);
		});
	}
	getClassesFromServer(classnames, callback){
		var that=this;
		this.jvm_interface.
		GETCLASSES(classnames,true,  function(arr){
			const l=arr.length;
			let initialized=l;
			for(let i=0;i<l;++i){
				that.readClass(arr[i],function(){
					initialized--;
					if(initialized==0)
						if(callback)callback();
				});
			}
			if(initialized==0&callback)callback();
		});
	}
	readClass(json,callback){
		const c = new Class(json);
		Object.seal(c)
		//c.name=classname;
		this.classes[c.name]=c;
		const cmethods=json.methods;
		const length=cmethods.length;
		for(let i=0; i< length; ++i){
			const signature=cmethods[i];
			c.methods[signature]="onserver";
		}
		c.clinit(callback);
	}
	init(callback){
		const that=this;
		that.jvm_interface.
			GETINITIALCLASSES(this.initial_classes,true,function(arr){
				const l=arr.length;
				let initialized=l;
				for(let i=0;i<l;++i){
					that.readClass(arr[i],function(){
						initialized--;
						if(initialized==0)
							if(callback)callback();
					});
				}
				const inits=that.inits;
				const li=inits.length;
				for(let i=0;i<li;++i)
					inits[i](that);
				if(initialized==0&&callback)callback();
			});
	}
	printStats(){
		const classes=this.classes;
		const methods=[];
		const keys = Object.keys(classes)
		const l=keys.length;
		for(let i=0;i<l;++i){
			const classname=keys[i];
			const clazz=classes[classname];
			const clmethods=clazz.methods;
			const methodnames = Object.keys(clmethods)
			const ml=methodnames.length;
			for(let j=0;j<ml;j++){
				const mname=methodnames[j];
				const method=clmethods[mname];
				if(method==="onserver")continue;
				if(method.invoked===undefined)
					debugger;
				methods.push({invoked:method.invoked, name:classname+"."+mname});
			}

		}
		methods.sort(function(a,b){return b.invoked-a.invoked});
		console.dir(methods);
	}
}


/*
class Heap{
	constructor(bex){
		this.heap={};
		this.bex=bex;
		this.addr=1;
		this.permanent=new Set();
	}
	add(i){
		this.heap[this.addr]=i;
		return this.addr++;
	}
	get(addr){
		return this.heap[addr];
	}
	makePermanent(addr){
		this.permanent.add(addr);
	}
	clearPermanent(addr){
		this.permanent.delete(addr);
	}
	gc(){
		const roots=new Set();
		const frames=this.bex.stack.entries;
		const depth=frames.length;
		for(let i=0;i<depth;++i)
		{
			const frame=frames[i];
			const lv=frame.lv;
			const length=lv.length;
			for(let j=0;j<length;j++)
			{
				roots.add(lv[j]);	
			}
			
		}
		const refs=new Set();
		for (let addr of roots){
			refs.add(addr);
		}
		for (let addr of this.permanent){
			refs.add(addr);
		}
		for (let addr of refs){
			if(!addr)
				continue;
			const inst=heap.get(addr);
			if(inst)
			inst.gatherRefs(refs);
		}

		const newheap={};
		for (let addr of refs){
			newheap[addr]=this.heap[addr];
		}
		//let oldheap=this.heap;
		this.heap=newheap;
		//delete oldheap;
	}
}
*/

class NativeMethod{
	constructor(clazz, signature, args, args_length, handler, etable){
		this.clazz=clazz;
		this.signature=signature;
		this.args=args;
		this.args_length=args_length;
		this.isNative=true;
		this.invoke=handler;
		this.etable=etable;
		this.invoked=0;
	}
}

class Method{
	constructor(clazz, signature, code, dv,  args, max_locals, max_stack, etable, code_type){
		this.clazz=clazz;
		this.signature=signature;
		this.code=code;
		this.dv=dv;
		this.args=args;
		this.args_length=args.byteLength;
		this.max_locals=max_locals;
		this.max_stack=max_stack;
		this.etable=etable;
		this.isNative=false;
		this.code_type=code_type;
		this.invoked=0;
	}
}

class Frame{
	constructor(method) {
		this.code=method.code;
		this.dv=method.dv;
		this.method=method;
		this.pc=0;
		this.stackpos=0;
	}

	clear(){
		this.stackpos=0;
	}
	push(v){
		this.stack[this.stackpos++]=v;
	}
	pushl(v){
		this.stack[this.stackpos]=v;
		this.stackpos+=2;
	}
	pop(){
		return this.stack[--this.stackpos];
	}
	peek(){
		return this.stack[this.stackpos-1];
	}

	newRefFrame(m){
		const f = new Frame(m);
		const p = m.args_length + 1;
		this.stackpos-=p;
		f.lv=this.stack.slice(this.stackpos,this.stackpos+p);
		f.stack=new Array(m.max_stack);
		return f;
	}

	newFrame(m){
		const f = new Frame(m);
		const p = m.args_length;
		this.stackpos-=p;
		f.lv=this.stack.slice(this.stackpos,this.stackpos+p);
		f.stack=new Array(m.max_stack);
		return f;
	}
	throw(stack, oref, ci){
		const pc=this.pc;
		const etable=this.method.etable;
		const l=etable.length;
		for(let i=0;i<l;++i){
			const e=etable[i];
			//alert(pc + " ["+e.start_pc+", "+e.end_pc+"]");
			if(pc>=e.start_pc && pc<=e.end_pc){
				const catch_type=e.catch_type;
				if(catch_type!==0){
					const cC=this.method.clazz;
					let c=cC.constants[catch_type-1];
					c=cC.constants[c.index1-1];
					const clazz=Class.getClass(c.str);
					if(!clazz)
						throw "class was not loaded";
					if(!ci.clazz.isInstance(clazz))
						continue;
				}
				this.pc=e.handler_pc;
				this.clear();
				this.push(ci);
				return;
			}
		}
		stack.pop();
		if(stack.isEmpty())
		{
			printStackTrace(ci);		
			return;
		}
		stack.peek().throw(stack, oref, ci);
	}

	tNE(stack, name, message){
		let msg=name;
		if(message)
			msg+=": "+message;
		const cl=Class.getClass(name);
		const ci=new ClassInstance(cl);
		ci.fields.detailMessage=new JString(msg);
		fillInStackTrace(stack, ci);
		//this.throw(stack, heap.add(ci), ci);
		this.throw(stack, ci, ci);
	}

	init(){
		this.stack=new Array(this.method.max_stack);
		this.lv=new Array(this.method.max_locals);
		return this;
	}
}

class Class{
	constructor(json){
		this.name=json.name;
		this.source_file=json.source_file;
		this.constants=json.cpool;
		this.super_class_name=json["super"];
		this.ref_names=json.ref_names;
		this.this_class=json.this_class;
		this.statics=json.statics;
		this.interfaces=json.interfaces;
		this.resolvedFields={};
		this.methods={};
		this.resolvedMethods=[];
	}
	ldc(index, cF){
		let c=this.constants[index-1];	
		switch (c.tag) {
			case 3:
				cF.push(c.value);
				return;
			case 4:
				cF.push(c.value);
				return;
			case 5:
				cF.push(c.value);
				cF.push(0);
				return;
			case 6:
				cF.push(c.value);
				cF.push(0);
				return;
			case 7:
				const cref = this.constants[c.index1];
				const cname = cref.str;
				if (cname.charAt(0) == '[') {
					oref = Class.getArrayClass(cname).getMyInstanceRef();
					cF.push(oref);
					return;
				}
				oref = getClass(cref.str, false).getMyInstanceRef();
				cF.push(oref);
				return;
			case 8:
				let oref=c.value;
				if(!oref) {
					//c = this.constants[c.index1-1];
					oref = JString.intern(c.str);
					c.value=oref;
				}
				cF.push(oref);
				return;
		}
	}
	getMethodFromConstantPool(index){
		if(this.resolvedMethods[index])
			return this.resolvedMethods[index];
		let c=this.constants[index-1];
		let c1=this.constants[c.index1-1];
		let c2=this.constants[c.index2-1];
		c1=this.constants[c1.index1-1];
		let c3=this.constants[c2.index1-1];
		let c4=this.constants[c2.index2-1];
		const clazz=Class.getClass(c1.str);
		if(!clazz){
			return false;
		}
		let m;
		
		if(!m)
			m=jvm.getMethodFromServer(c1.str,c3.str+c4.str, readMethodAndResume);
		if(m){
			this.resolvedMethods[index]=m;
			return m;
		}
		return false;
	}

	getMethodBySignature(signature){
		return jvm.getMethodFromServer(this.name, signature, readMethodAndResume);
	}

	getMethodSignature(index){
		let c=this.constants[index-1];
		//let c1=this.constants[c.index1-1];
		let c2=this.constants[c.index2-1];
		//c1=this.constants[c1.index1-1];
		let c3=this.constants[c2.index1-1];
		let c4=this.constants[c2.index2-1];
		return c3.str+c4.str;
	}

	getMethodByName(name){
		const method=this.methods[name];
		if(method=="onserver")
			return jvm.getMethodFromServer(this.name, name, readMethodAndResume);
		if(method)
			return method;

		return this.getSuperClass().getMethodByName(name);
	}

	getSuperClass(){
		return jvm.classes[this.super_class_name];
	}

	getClass(index){
		let c=this.constants[index-1];
		let c1=this.constants[c.index1-1];
		return Class.getClass(c1.str);
	}
	isInstance(cl){
		if(this.name===cl.name)return true;
		const interfaces=this.interfaces;
		for(let i=0;i<interfaces.length;++i)
		{
			if(cl.name===interfaces[i])
				return true;
		}
		const sc=this.getSuperClass();
		if(sc)
			return sc.isInstance(cl);
		return false;
	}
	clinit(callback){
		if(this.methods["<clinit>()V"])
		{
			jvm.getMethodFromServer(this.name,"<clinit>()V",function(cl,signature,bytebuffer){
				const bex=new ByteCodeExecutor(function(){
					callback();
				});
				const method=readMethod(cl, signature, bytebuffer);
				const lv=[];
				const pF=new Frame(method);
				pF.init();
				bex.stack.push(pF);
				jvm.waiting_threads.push(jvm.bex);
				jvm.bex=bex;
				jvm.bex.run();
			});
		}
		else if(callback) callback();
	}
}

Class.getClass=function(classname){
	return jvm.getClassFromServer(classname, readClassAndResume);
}

class Instance{
	constructor(clazz) {
		this.clazz=clazz;
	}
	monitorenter(thread){
	
	}
	monitorexit(thread){
	
	}
}
class ClassInstance extends Instance{
	constructor(clazz) {
		super(clazz);
		this.fields={};
	}
	gatherRefs(refs){
		const ref_names=this.clazz.ref_names;
		const l=ref_names.length;
		for(let i=0;i<l;++i){
			const ref_name=ref_names[i];
			const ref=this.fields[ref_name];
			if(ref){
				//alert(ref.clazz.name);
				refs.add(ref);
				const ci=heap.get(ref)
				if(ci)
					ci.gatherRefs(refs);
			}
		}
	}
}
class ArrayInstance extends Instance{
	constructor(clazz,length, type){
		super(clazz);
		this.type=type;
		this._length=length;
		this.arr=new Array(length).fill(0);
	}
	length(){
		return this._length;
	}
	get(i){
		return this.arr[i];
	}
	set(i, v){
		this.arr[i]=v;
	}
	gatherRefs(refs){
		if(this.type!=_L)
			return;
		const length=this.arr.length;
		for(let i=0;i<length;++i)
			refs.add(this.arr[i]);
	}
}
ArrayInstance.multianewarray=function(type, sizes, dim){
	const size=sizes[dim];
	let array;
	if(dim>=sizes.length-1){
		array=new ArrayInstance("", size, type)
	}
	else{
		array=new ArrayInstance("", size, _L)
		for(let i=0;i<size;++i)
			array.set(i, ArrayInstance.multianewarray(type, sizes, dim + 1));
	}
	return array;
}


function fillInStackTrace(stack, ci){
	const entries=stack.entries;
	const l=entries.length;
	const stacktrace=new Array(l);
	for(let i=0;i<l;++i){
		const entry=entries[i];	
		const se={};
		se.methodname=entry.method.clazz.name+"."+entry.method.signature;
		se.methodname+=" ("+entry.method.clazz.source_file+")";
		se.pc=entry.pc;
		stacktrace[i]=se;
	}
	ci.fields["stacktraceAsString"]=stacktrace;
}

function readClassAndResume(){
	jvm.bex.run();
}

function readMethod(cl, signature,  bytebuffer){
	let bytearray=new Uint8Array(bytebuffer);
	const dv=new DataView(bytebuffer);
	if(dv.byteLength<=4)
	{
		let err="method "+cl.name+"."+signature+" not found";
		throw err;
	}
	let pos=0;
	const type=dv.getInt8(pos);
	//is optimized:
	if(type==0){
		//parsejscode
		++pos;
		const code_length = dv.getInt32(pos,false);
		pos+=4;
		const jsarr=bytebuffer.slice(pos, pos+code_length);
		var js = new TextDecoder("utf-8").decode(jsarr);
		//console.log(js);
		//alert(js);

		const handler=new Function("stack","lv","pF",js);

		pos+=code_length;

		const args_length = dv.getInt32(pos,false);
		pos+=4;
		const args=bytebuffer.slice(pos, pos+args_length);
		pos+=args_length;

		//read exception table
		const exception_table_length = dv.getInt32(pos,false);
		const etable=new Array(exception_table_length);
		pos+=4;
		for(let i=0;i<exception_table_length;++i){
			const e={};
			e.start_pc = dv.getInt32(pos,false);
			pos+=4;	
			e.end_pc = dv.getInt32(pos,false);
			pos+=4;	
			e.handler_pc = dv.getInt32(pos,false);
			pos+=4;	
			e.catch_type = dv.getInt32(pos,false);
			pos+=4;	
			etable[i]=e;
		}
		//////////////////
		
		const classnamearr=bytebuffer.slice(pos,bytebuffer.byteLength);
		var classname = new TextDecoder("utf-8").decode(classnamearr);
		const clazz=Class.getClass(classname);
		return new NativeMethod(clazz, signature, args, args.byteLength, handler, etable);
	}
	++pos;
	const code_length = dv.getInt32(pos,false);
	pos+=4;
	const code=bytebuffer.slice(pos,code_length+pos);


	pos+=code_length;
	
	const args_length = dv.getInt32(pos,false);
	pos+=4;
	const args=bytebuffer.slice(pos, pos+args_length);
	pos+=args_length;
	
	const max_locals = dv.getInt32(pos,false);
	pos+=4;
	const max_stack= dv.getInt32(pos,false);
	pos+=4;
	
	//read exception table
	const exception_table_length = dv.getInt32(pos,false);
	const etable=new Array(exception_table_length);
	pos+=4;
	for(let i=0;i<exception_table_length;++i){
		const e={};
		e.start_pc = dv.getInt32(pos,false);
		pos+=4;	
		e.end_pc = dv.getInt32(pos,false);
		pos+=4;	
		e.handler_pc = dv.getInt32(pos,false);
		pos+=4;	
		e.catch_type = dv.getInt32(pos,false);
		pos+=4;	
		etable[i]=e;
	}

	const bcode=new Uint8Array(code);
	const codedv=new DataView(code);
	const arr=bytebuffer.slice(pos,bytebuffer.byteLength);
	var classname = new TextDecoder("utf-8").decode(arr);
	const clazz=Class.getClass(classname);
	return new Method(clazz, signature, bcode, codedv,  args, max_locals, max_stack, etable, type);
}

function readMethodAndResume(cl, signature, bytebuffer){
	const method=readMethod(cl, signature, bytebuffer);
	Object.seal(method);

	//2 means it is native
	if(method.code_type===2){
		cl.methods[signature]=method.clazz.methods[signature];
	}
	else{
		method.clazz.methods[signature]=method;
		cl.methods[signature]=method;
	}

	jvm.bex.run();
}

function pushMethod(cl, signature,  bytebuffer){
	const method=readMethod(cl, signature, bytebuffer);
	Object.seal(method);
	cl.methods[signature]=method;
	jvm.bex.stack.push(new Frame(method).init());
	jvm.bex.run();
}

function printStackTraceNative(){
	const stack=jvm.bex.stack.entries;
	for(let i=0;i<stack.length;++i){
		console.log(stack[i].method.signature);
	}
}
function printStackTrace(ci){
	const stacktrace=ci.fields["stacktrace"];
	const l=stacktrace.length;
	let result="Exception: ";
	const message=ci.fields["detailMessage"];
	if(message)
		result+=message.str;
	result+="\n";
	for(let i=0;i<l;++i){
		const se=stacktrace[i];
		result+=se.methodname+" (pc:"+se.pc+")\n";
	}
	//throw result;
	console.error(result);
}

function getStackTrace(){
	let trace="";
	const stack=jvm.bex.stack.entries;
	for(let i=0;i<stack.length;++i){
		const frame=stack[i];
		trace+=frame.method.clazz.name+".";
		trace+=frame.method.signature;
		trace+=" (pc:"+frame.pc+")";
		trace+="\n";
	}
	return trace;
}

function GET(url, callback, onerror){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	//xhr.responseType = "arraybuffer";
	xhr.onload = function() {
		if (xhr.status === 200) {
			callback(xhr.response);
		}
		else {
			printStackTraceNative();
		}
	};
	xhr.onerror= onerror;
	xhr.send();
}

var jvm;
function createJVM(jvm_interface){
	jvm=new JVM(jvm_interface);
	Object.seal(jvm);
	return jvm;
}
function startMain(jvm){
	jvm.init(function(){
		jvm.getMethodFromServer("Main","main([Ljava/lang/String;)V",pushMethod);
	});
}
class FrameStack{
	constructor(){
		this.entries=[];
	}
	isEmpty(){
		return this.entries.length<=0;	
	}
	peek(){
		return this.entries[this.entries.length-1];
	}
	push(frame){
		this.entries.push(frame);	
	}
	pop(frame){
		return this.entries.pop();	
	}
}
	
class ByteCodeExecutor{
	constructor(onstop){
		this.stack=new FrameStack();
		this.onstop=onstop;
	}

	onhalt(){
		if(jvm.waiting_threads.length===0)return 0;
		jvm.bex=jvm.waiting_threads.pop();
		if(this.onstop)
			this.onstop();
		else
			jvm.bex.run();
	}
//The Holy Grail
	run(){
	//const thread={};
	let stack=this.stack;
	if (stack.isEmpty())
	{
		//thread.destroy();
		if(this.onhalt)
			this.onhalt();
		return 1;
	}
	//local variables are better for performance
	//current Frame
	let cF = stack.peek();
	let nf;
	let cC=cF.method.clazz;;
	//program counter
	let pc = cF.pc;
	//ClassInstance 
	let c;
	let i1, i2, i3, i4, oref;
	let l1, l2;
	let f1, f2;
	let d1, d2;
	//AClass cl;
	let cl;
	//AMethod m;
	let m;
	//ArrayInstance array;
	let array;
	//Object obj;
	let obj;
	//Constant fc;
	let fc;
	//Constant fname;
	let fname;
	//Constant ftype;
	let ftype;

	let code = cF.code;
	let dv = cF.dv;
	let lv = cF.lv;
	let clazz;

	mainLoop:
	do {
		//System.out.println(stack.get(0).stackpos);
		/*
			   StringBuilder sb = new StringBuilder();
			   sb.append(s);
			   sb.append(": ");
			   CodeAttribute.parseOp(pc, code, sb);
			   System.out.println(sb);
		//*/

		switch (code[pc]) {
			case 0x00:
				//nop
				pc += 1;
				continue;
			case 0x01:
				//aconst_null
				cF.push(0);
				pc += 1;
				continue;
			case 0x02:
				//iconst_m1
				cF.push(-1);
				pc += 1;
				continue;
			case  0x03:
				//iconst_0
				cF.push(0);
				pc += 1;
				continue;
			case  0x04:
				//iconst_1
				cF.push(1);
				pc += 1;
				continue;
			case  0x05:
				//iconst_2
				cF.push(2);
				pc += 1;
				continue;
			case  0x06:
				//iconst_3
				cF.push(3);
				pc += 1;
				continue;
			case  0x07:
				//iconst_4
				cF.push(4);
				pc += 1;
				continue;
			case  0x08:
				//iconst_5
				cF.push(5);
				pc += 1;
				continue;
			case  0x09:
				//lconst_0
				cF.push(0);
				cF.push(0);
				pc += 1;
				continue;
			case  0x0A:
				//lconst_1
				cF.push(1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x0B:
				//fconst_0
				cF.push(0.0);
				pc += 1;
				continue;
			case  0x0C:
				//fconst_1
				cF.push(1.0);
				pc += 1;
				continue;
			case  0x0D:
				//fconst_2
				cF.push(2.0);
				pc += 1;
				continue;
			case  0x0E:
				//dconst_0
				cF.push(0.0);
				cF.push(0);
				pc += 1;
				continue;
			case  0x0F:
				//dconst_1
				cF.push(1.0);
				cF.push(0);
				pc += 1;
				continue;
			case  0x10:
				//bipush
				cF.push(dv.getInt8(pc+1));
				pc += 2;
				continue;
			case  0x11:
				//sipush
				cF.push(dv.getInt16(pc+1,false));
				pc += 3;
				continue;
			case  0x12:
				//ldc
				cC.ldc(code[pc + 1] & 0xFF, cF);
				pc += 2;
				continue;
			case  0x13:
				//ldc_w
				cC.ldc(dv.getInt16(pc+1,false), cF);
				pc += 3;
				continue;
			case  0x14:
				//ldc2_w
				cC.ldc(dv.getInt16(pc+1,false), cF);
				pc += 3;
				continue;
			case  0x15:
				//iload
				cF.push(lv[ code[pc + 1]]);
				pc += 2;
				continue;
			case  0x16:
				//lload
				cF.push(lv[ code[pc + 1]]);
				cF.push(lv[ code[pc + 1] + 1]);
				pc += 2;
				continue;
			case  0x17:
				//fload
				cF.push(lv[ code[pc + 1]]);
				pc += 2;
				continue;
			case  0x18:
				//dload
				cF.push(lv[code[pc + 1]]);
				cF.push(lv[code[pc + 1] + 1]);
				pc += 2;
				continue;
			case  0x19:
				//aload
				cF.push(lv[ code[pc + 1]]);
				pc += 2;
				continue;
			case  0x1A:
				//iload_0
				cF.push(lv[0]);
				pc += 1;
				continue;
			case  0x1B:
				//iload_1
				cF.push(lv[1]);
				pc += 1;
				continue;
			case  0x1C:
				//iload_2
				cF.push(lv[2]);
				pc += 1;
				continue;
			case  0x1D:
				//iload_3";
				cF.push(lv[3]);
				pc += 1;
				continue;
			case  0x1E:
				//lload_0;
				cF.push(lv[0]);
				cF.push(lv[1]);
				pc += 1;
				continue;
			case  0x1F:
				//lload_1
				cF.push(lv[1]);
				cF.push(lv[2]);
				pc += 1;
				continue;
			case  0x20:
				//lload_2
				cF.push(lv[2]);
				cF.push(lv[3]);
				pc += 1;
				continue;
			case  0x21:
				//lload_3
				cF.push(lv[3]);
				cF.push(lv[4]);
				pc += 1;
				continue;
			case  0x22:
				//fload_0
				cF.push(lv[0]);
				pc += 1;
				continue;
			case  0x23:
				//fload_1
				cF.push(lv[1]);
				pc += 1;
				continue;
			case  0x24:
				//fload_2
				cF.push(lv[2]);
				pc += 1;
				continue;
			case  0x25:
				//fload_3;
				cF.push(lv[3]);
				pc += 1;
				continue;
			case  0x26:
				//dload_0;
				cF.push(lv[0]);
				cF.push(lv[1]);
				pc += 1;
				continue;
			case  0x27:
				//dload_1
				cF.push(lv[1]);
				cF.push(lv[2]);
				pc += 1;
				continue;
			case  0x28:
				//dload_2
				cF.push(lv[2]);
				cF.push(lv[3]);
				pc += 1;
				continue;
			case  0x29:
				//dload_3
				cF.push(lv[3]);
				cF.push(lv[4]);
				pc += 1;
				continue;
			case  0x2A:
				//aload_0
				cF.push(lv[0]);
				pc += 1;
				continue;
			case  0x2B:
				//aload_1
				cF.push(lv[1]);
				pc += 1;
				continue;
			case  0x2C:
				//aload_2
				cF.push(lv[2]);
				pc += 1;
				continue;
			case  0x2D:
				//aload_3
				cF.push(lv[3]);
				pc += 1;
				continue;
			case  0x2E:
				//iaload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x2F:
				//laload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				cF.push(0);
				pc += 1;
				continue;
			case  0x30:
				//faload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x31:
				//daload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				cF.push(0);
				pc += 1;
				continue;
			case  0x32:
				//aaload
				i1 =  cF.pop();
				array = (cF.pop());
				if(!array){
					cF.tNE(stack,_NP, "aaload");
					pc=cF.pc;
					continue;
				}
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x33:
				//baload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x34:
				//caload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x35:
				//saload
				i1 =  cF.pop();
				array = (cF.pop());
				cF.push(array.get(i1));
				pc += 1;
				continue;
			case  0x36:
				//istore
				lv[code[pc + 1]] = cF.pop();
				pc += 2;
				continue;
			case  0x37:
				//lstore
				lv[code[pc + 1] + 1] = cF.pop();
				lv[code[pc + 1]] = cF.pop();
				pc += 2;
				continue;
			case  0x38:
				//fstore
				lv[code[pc + 1]] = cF.pop();
				pc += 2;
				continue;
			case  0x39:
				//dstore
				lv[code[pc + 1]+1] = cF.pop();
				lv[code[pc + 1]] = cF.pop();
				pc += 2;
				continue;
			case  0x3A:
				//astore
				lv[code[pc + 1]] = cF.pop();
				pc += 2;
				continue;
			case  0x3B:
				//istore_0
				lv[0] = cF.pop();
				pc += 1;
				continue;
			case  0x3C:
				//istore_1
				lv[1] = cF.pop();
				pc += 1;
				continue;
			case  0x3D:
				//istore_2
				lv[2] = cF.pop();
				pc += 1;
				continue;
			case  0x3E:
				//istore_3
				lv[3] = cF.pop();
				pc += 1;
				continue;
			case  0x3F:
				//lstore_0
				lv[1] = cF.pop();
				lv[0] = cF.pop();
				pc += 1;
				continue;
			case  0x40:
				//lstore_1
				lv[2] = cF.pop();
				lv[1] = cF.pop();
				pc += 1;
				continue;
			case  0x41:
				//store_2
				lv[3] = cF.pop();
				lv[2] = cF.pop();
				pc += 1;
				continue;
			case  0x42:
				//lstore_3
				lv[4] = cF.pop();
				lv[3] = cF.pop();
				pc += 1;
				continue;
			case  0x43:
				//fstore_0
				lv[0] = cF.pop();
				pc += 1;
				continue;
			case  0x44:
				//fstore_1
				lv[1] = cF.pop();
				pc += 1;
				continue;
			case  0x45:
				//fstore_2
				lv[2] = cF.pop();
				pc += 1;
				continue;
			case  0x46:
				//fstore_3"
				lv[3] = cF.pop();
				pc += 1;
				continue;
			case  0x47:
				//dstore_0
				lv[1] = cF.pop();
				lv[0] = cF.pop();
				pc += 1;
				continue;
			case  0x48:
				//dstore_1
				lv[2] = cF.pop();
				lv[1] = cF.pop();
				pc += 1;
				continue;
			case  0x49:
				//dstore_2
				lv[3] = cF.pop();
				lv[2] = cF.pop();
				pc += 1;
				continue;
			case  0x4A:
				//dstore_3
				lv[4] = cF.pop();
				lv[3] = cF.pop();
				pc += 1;
				continue;
			case  0x4B:
				//astore_0
				lv[0] = cF.pop();
				pc += 1;
				continue;
			case  0x4C:
				//astore_1
				lv[1] = cF.pop();
				pc += 1;
				continue;
			case  0x4D:
				//astore_2
				lv[2] = cF.pop();
				pc += 1;
				continue;
			case  0x4E:
				//astore_3
				lv[3] = cF.pop();
				pc += 1;
				continue;
			case  0x4F:
				//iastore
				i1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, i1);
				pc += 1;
				continue;
			case  0x50:
				//lastore
				cF.pop();
				l1 = cF.pop();
				i1 =  cF.pop();
				array = (cF.pop());
				array.set(i1, l1);
				pc += 1;
				continue;
			case  0x51:
				//fastore
				f1 = cF.pop();
				i1 =  cF.pop();
				array = (cF.pop());
				array.set(i1, f1);
				pc += 1;
				continue;
			case  0x52:
				//dastore
				cF.pop();
				d1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, d1);
				pc += 1;
				continue;
			case  0x53:
				//aastore
				i1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, i1);
				pc += 1;
				continue;
			case  0x54:
				//bastore
				i1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, i1);
				pc += 1;
				continue;
			case  0x55:
				//castore
				i1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, i1);
				pc += 1;
				continue;
			case  0x56:
				//sastore
				i1 =  cF.pop();
				i2 =  cF.pop();
				array = (cF.pop());
				array.set(i2, i1);
				pc += 1;
				continue;
			case  0x57:
				//pop
				cF.pop();
				pc += 1;
				continue;
			case  0x58:
				//pop2
				cF.pop();
				cF.pop();
				pc += 1;
				continue;
			case  0x59:
				//dup
				cF.push(cF.peek());
				pc += 1;
				continue;
			case  0x5A:
				//dup_x1
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i1);
				cF.push(i2);
				cF.push(i1);
				pc += 1;
				continue;
			case  0x5B:
				//dup_x2
				i1 =  cF.pop();
				i2 =  cF.pop();
				i3 =  cF.pop();
				cF.push(i1);
				cF.push(i3);
				cF.push(i2);
				cF.push(i1);
				pc += 1;
				continue;
			case  0x5C:
				//dup2
				i2 =  cF.pop();
				i1 =  cF.pop();
				cF.push(i1);
				cF.push(i2);
				cF.push(i1);
				cF.push(i2);
				pc += 1;
				continue;
			case  0x5D:
				//dup2_x1
				i2 =  cF.pop();
				i1 =  cF.pop();
				i3 =  cF.pop();
				cF.push(i1);
				cF.push(i2);
				cF.push(i3);
				cF.push(i1);
				cF.push(i2);
				pc += 1;
				throw new RuntimeException("check that and contniue");
				//continue;
			case  0x5E:
				//dup2_x2
				i2 =  cF.pop();
				i1 =  cF.pop();
				i4 =  cF.pop();
				i3 =  cF.pop();
				cF.push(i1);
				cF.push(i2);
				cF.push(i3);
				cF.push(i4);
				cF.push(i1);
				cF.push(i2);
				pc += 1;
				throw new RuntimeException("check that and continue");
				//continue;
			case  0x5F:
				//swap
				cF.push(cF.get(0));
				pc += 1;
				continue;
			case  0x60:
				//iadd
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i1 + i2);
				pc += 1;
				continue;
			case  0x61:
				//ladd
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 + l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x62:
				//fadd
				f1 = cF.pop();
				f2 = cF.pop();
				cF.push(f1 + f2);
				pc += 1;
				continue;
			case  0x63:
				//dadd
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				d1 = d2 + d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x64:
				//isub
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 - i1);
				pc += 1;
				continue;
			case  0x65:
				//lsub
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 - l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x66:
				//fsub
				f1 = cF.pop();
				f2 = cF.pop();
				cF.push(f2 - f1);
				pc += 1;
				continue;
			case  0x67:
				//dsub
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				d1 = d2 - d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x68:
				//imul
				i1 =  cF.pop();
				i2 =  cF.pop();
				//cF.push(i2 * i1);
				cF.push(Math.imul(i2, i1));
				pc += 1;
				continue;
			case  0x69:
				//lmul
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 * l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x6A:
				//fmul
				f1 = cF.pop();
				f2 = cF.pop();
				cF.push(f2 * f1);
				pc += 1;
				continue;
			case  0x6B:
				//dmul
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				d1 = d2 * d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x6C:
				//idiv
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 / i1 | 0);
				pc += 1;
				continue;
			case  0x6D:
				//ldiv
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 / l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x6E:
				//fdiv
				f1 = cF.pop();
				f2 = cF.pop();
				cF.push(f2 / f1);
				pc += 1;
				continue;
			case  0x6F:
				//ddiv
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				d1 = d2 / d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x70:
				//irem
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 % i1);
				pc += 1;
				continue;
			case  0x71:
				//lrem
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 % l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x72:
				//frem
				f1 =  cF.pop();
				f2 =  cF.pop();
				cF.push(f2 % f1);
				pc += 1;
				continue;
			case  0x73:
				//drem
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				d1 = d2 % d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x74:
				//ineg
				cF.push(- cF.pop());
				pc += 1;
				continue;
			case  0x75:
				//lneg
				cF.pop();
				l1 = cF.pop();
				l1 = -l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x76:
				//fneg
				cF.push(-cF.pop());
				pc += 1;
				continue;
			case  0x77:
				//dneg
				cF.pop();
				d1 = cF.pop();
				d1 = -d1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x78:
				//ishl
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 << i1);
				pc += 1;
				continue;
			case  0x79:
				//lshl
				i1 =  cF.pop();
				cF.pop();
				l1 =  cF.pop();
				l1 = l1 << i1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x7A:
				//ishr
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 >> i1);
				pc += 1;
				continue;
			case  0x7B:
				//lshr
				i1 =  cF.pop();
				cF.pop();
				l1 =  cF.pop();
				l1 = l1 >> i1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x7C:
				//iushr
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 >>> i1);
				pc += 1;
				continue;
			case  0x7D:
				//lushr
				i1 =  cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 >>> i1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x7E:
				//iand
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 & i1);
				pc += 1;
				continue;
			case  0x7F:
				//land
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 & l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x80:
				//ior
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 | i1);
				pc += 1;
				continue;
			case  0x81:
				//lor
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 | l1;
				cF.push(l1);
				cF.push(l1);
				pc += 1;
				continue;
			case  0x82:
				//ixor
				i1 =  cF.pop();
				i2 =  cF.pop();
				cF.push(i2 ^ i1);
				pc += 1;
				continue;
			case  0x83:
				//lxor
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				l1 = l2 ^ l1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x84:
				//iinc
				i1 = lv[ code[pc + 1]];
				lv[ code[pc + 1]] = i1 + code[pc + 2];
				pc += 3;
				continue;
			case  0x85:
				//i2l
				i1 = cF.pop();
				l1=i1;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x86:
				//i2f
				cF.push(cF.pop()|0);
				pc += 1;
				continue;
			case  0x87:
				//i2d
				i1 =  cF.pop();
				cF.push(i1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x88:
				//l2i
				cF.pop();
				l1 = cF.pop();
				cF.push( l1);
				pc += 1;
				continue;
			case  0x89:
				//l2f
				l1 = _2i2l( cF.pop(),  cF.pop());
				cF.push(l1);
				pc += 1;
				continue;
			case  0x8A:
				//l2d
				cF.pop();
				l1 = cF.pop();
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x8B:
				//f2i
				cF.push(cF.pop()|0);
				pc += 1;
				continue;
			case  0x8C:
				//f2l
				f1 = cF.pop();
				l1 =  f1|0;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x8D:
				//f2d
				f1 = cF.pop();
				d1 = f1;
				cF.push(d1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x8E:
				//d2i
				cF.pop();
				d1 = cF.pop();
				cF.push(d1|0);
				pc += 1;
				continue;
			case  0x8F:
				//d2l
				cF.pop();
				d1 = cF.pop();
				l1 =  d1|0;
				cF.push(l1);
				cF.push(0);
				pc += 1;
				continue;
			case  0x90:
				//d2f
				l1 = _2i2l( cF.pop(),  cF.pop());
				d1 = Double.longBitsToDouble(l1);
				cF.push(d1);
				pc += 1;
				continue;
			case  0x91:
				//i2b
				cF.push(cF.pop());
				pc += 1;
				continue;
			case  0x92:
				//i2c
				cF.push(cF.pop());
				pc += 1;
				continue;
			case  0x93:
				//i2s
				cF.push(cF.pop());
				pc += 1;
				continue;
			case  0x94:
				//lcmp
				cF.pop();
				l1 = cF.pop();
				cF.pop();
				l2 = cF.pop();
				if (l2 == l1) {
					cF.push(0);
				} else if (l2 > l1) {
					cF.push(1);
				} else
					cF.push(-1);
				pc += 1;
				continue;
			case  0x95:
				//fcmpl
				f1 = cF.pop();
				f2 = cF.pop();
				if (f2 == f1) {
					cF.push(0);
				} else if (f2 > f1) {
					cF.push(1);
				} else if (f2 < f1)
					cF.push(-1);
				else
					cF.push(-1);
				pc += 1;
				continue;
			case  0x96:
				//fcmpg
				f1 = cF.pop();
				f2 = cF.pop();
				if (f2 == f1) {
					cF.push(0);
				} else if (f2 > f1) {
					cF.push(1);
				} else if (f2 < f1)
					cF.push(-1);
				else
					cF.push(1);
				pc += 1;
				continue;
			case  0x97:
				//dcmpl
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				if (d2 == d1) {
					cF.push(0);
				} else if (d2 > d1) {
					cF.push(1);
				} else if (d2 < d1)
					cF.push(-1);
				else
					cF.push(-1);
				pc += 1;
				continue;
			case  0x98:
				//dcmpg
				cF.pop();
				d1 = cF.pop();
				cF.pop();
				d2 = cF.pop();
				if (d2 == d1) {
					cF.push(0);
				} else if (d2 > d1) {
					cF.push(1);
				} else if (d2 < d1)
					cF.push(-1);
				else
					cF.push(1);
				pc += 1;
				continue;
			case  0x99:
				//ifeq
				if(cF.pop() == 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9A:
				//ifne
				if ( cF.pop() != 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9B:
				//iflt
				if ( cF.pop() < 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9C:
				//ifge
				if ( cF.pop() >= 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9D:
				//ifgt
				if ( cF.pop() > 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9E:
				//ifle
				if ( cF.pop() <= 0)
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0x9F:
				//if_icmpeq
				if ( cF.pop() ==  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA0:
				//if_icmpne
				if ( cF.pop() !=  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA1:
				//if_icmplt
				if ( cF.pop() >  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA2:
				//if_icmpge
				if ( cF.pop() <=  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA3:
				//if_icmpgt
				if ( cF.pop() <  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA4:
				//if_icmple
				if ( cF.pop() >=  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA5:
				//if_acmpeq
				if ( cF.pop() ==  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA6:
				//if_acmpeq
				if ( cF.pop() !=  cF.pop())
					pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xA7:
				//goto
				pc += dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				continue;
			case  0xA8:
				//jsr
				cF.push(pc + 3);
				pc += _i(code[pc + 1], code[pc + 2]);
				continue;
			case  0xA9:
				//ret
				pc =  lv[code[pc + 1]];
				continue;
			case  0xAA:
				//tableswitch
				{
					i1 =  cF.pop();
					let pc2 = pc + (4 - pc % 4);
					let default_offset = dv.getInt32(pc2,false);
					pc2 += 4;
					let low = dv.getInt32(pc2,false);
					pc2 += 4;
					let high = dv.getInt32(pc2,false);
					pc2 += 4;
					if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
						pc += default_offset;
						// branch to default
					} else {
						pc2 += (i1 - low) * 4;
						pc += dv.getInt32(pc2,false);
					}
				}
				continue;
			case  0xAB:
				//lookupswitch
				{
					i1 =  cF.pop();
					let pc2 = pc + (4 - pc % 4);
					let default_offset = dv.getInt32(pc2,false);// _4b2i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					i2 = dv.getInt32(pc2,false);// _4b2i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					for (i3 = 0; i3 < i2; ++i3) {
						let key = dv.getInt32(pc2,false);// _4b2i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);

						if (key == i1) {
							pc2 += 4;
							let value =dv.getInt32(pc2,false);// _4b2i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
							pc += value;
							continue mainLoop;
						}
						pc2 += 8;
					}
					pc += default_offset;
				}
				continue;
			case  0xAC:
				//ireturn
				i1 =  cF.pop();
				pc += 1;
				this.last_frame = stack.pop();
				if (stack.isEmpty()) {
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				cC = cF.method.clazz;
				cF.push(i1);
				code = cF.code;
				dv=cF.dv;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case  0xAD:
				//lreturn
				i2 =  cF.pop();
				i1 =  cF.pop();
				pc += 1;
				this.last_frame = stack.pop();
				if (stack.isEmpty()) {
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				//cC = cF.getMyClass();
				cC = cF.method.clazz;
				cF.push(i1);
				cF.push(i2);
				code = cF.code;
				dv=cF.dv;
				// this.code=c.getMethod(11).attrs[0].code;
				//System.out.println("lreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				continue;
			case  0xAE:
				//freturn
				obj = cF.pop();
				pc += 1;
				this.last_frame = stack.pop();
				if (stack.isEmpty()) {
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				//cC = cF.getMyClass();
				cC = cF.method.clazz;
				cF.push(obj);
				code = cF.code;
				dv=cF.dv;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case  0xAF:
				//dreturn
				i2 =  cF.pop();
				i1 =  cF.pop();
				pc += 1;
				this.last_frame = stack.pop();
				if (stack.isEmpty()) {
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				//cC = cF.getMyClass();
				cC = cF.method.clazz;
				//System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
				cF.push(i1);
				cF.push(i2);
				code = cF.code;
				dv=cF.dv;
				// this.code=c.getMethod(11).attrs[0].code;
				continue;
			case  0xB0:
				//areturn
				i1 =  cF.pop();
				pc += 1;
				this.last_frame = stack.pop();
				if (stack.isEmpty()) {
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				//cC = cF.getMyClass();
				cC = cF.method.clazz;
				cF.push(i1);
				code = cF.code;
				dv=cF.dv;
				continue;
			case  0xB1:
				//_return
				pc += 1;
				this.last_frame = stack.pop();
				if(stack.isEmpty()){
					this.last_frame.pc=pc;
					//thread.destroy();
					if(this.onhalt)
						this.onhalt();
					return 1;
				}
				cF = stack.peek();
				pc = cF.pc;
				lv = cF.lv;
				//cC = cF.getMyClass();
				cC = cF.method.clazz;
				code = cF.code;
				dv=cF.dv;
				continue;
			case  0xB2:
				//getstatic
				fc = cC.constants[dv.getInt16(pc+1,false)-1];
				var cclazz = cC.constants[fc.index1-1];
				cclazz = cC.constants[cclazz.index1-1];
				clazz=Class.getClass(cclazz.str);
				if(!clazz){
					cF.pc=pc;
					cF.lv=lv;
					return;
				}
				fc = cC.constants[fc.index2-1];
				fname = cC.constants[fc.index1-1];
				ftype = cC.constants[fc.index2-1];
				obj=clazz.statics[fname.str];
				cF.push(obj);
				switch (ftype.str) {
					case "J":
					case "D":
						cF.push(0);
						break;
				}
				pc += 3;
				continue;
			case  0xB3:
				//putstatic
				obj = cF.pop();
				fc = cC.constants[dv.getInt16(pc+1,false)-1];
				cclazz = cC.constants[fc.index1-1];
				cclazz = cC.constants[cclazz.index1-1];
				clazz=Class.getClass(cclazz.str);
				if(!clazz){
					cF.pc=pc;
					cF.lv=lv;
					return;
				}
				fc = cC.constants[fc.index2-1];
				fname = cC.constants[fc.index1-1];
				ftype = cC.constants[fc.index2-1];
				switch (ftype.str) {
					case "J":
					case "D":
						obj=cF.pop();
						break;
				}
				clazz.statics[fname.str]=obj;
				pc += 3;
				continue;
			case  0xB4:
				//getfield
				i1=dv.getInt16(pc+1,false);
				//const index=ii(code[pc + 1], code[pc + 2]);
				if(cC.resolvedFields[i1]){
					const ct=cC.resolvedFields[i1]
					fname=ct.name;
					ftype=ct.type;
				}
				else{
					fc = cC.constants[i1-1];
					fc = cC.constants[fc.index2-1];
					fname = cC.constants[fc.index1-1].str;
					ftype = cC.constants[fc.index2-1].str;
					cC.resolvedFields[i1]={name:fname,type:ftype};
				}
				oref = cF.pop();
				c = (oref);
				if (c == null){
					//throw "NullPointerException(\"field \"" + fname +" \" reference: \"" + oref;
					cF.tNE(stack,_NP, "getfield: "+fname);
					cF=stack.peek();
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					pc=cF.pc;
					continue;
				}
				cF.push(c.fields[fname]);
				switch (ftype) {
					case "J":
					case "D":
						cF.push(0);
						break;
				}
				pc += 3;
				continue;
			case  0xB5:
				//putfield
				obj = cF.pop();
				i1=dv.getInt16(pc+1,false);
				//const index=ii(code[pc + 1], code[pc + 2]);
				if(cC.resolvedFields[i1]){
					const ct=cC.resolvedFields[i1]
					fname=ct.name;
					ftype=ct.type;
				}
				else{
					fc = cC.constants[i1-1];
					fc = cC.constants[fc.index2-1];
					fname = cC.constants[fc.index1-1].str;
					ftype = cC.constants[fc.index2-1].str;
					cC.resolvedFields[i1]={name:fname,type:ftype};
				}
				switch (ftype) {
					case "J":
					case "D":
						obj = cF.pop();
						break;
				}
				oref = cF.pop();
				c = (oref);
				if (c == null)
				{
					cF.tNE(stack,_NP, "putfield: "+fname);
					cF=stack.peek();
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					pc=cF.pc;
				}
				c.fields[fname]= obj;
				pc += 3;
				continue;
			case  0xB6:
				//invokevirtual
				// int objref=cF.pop();
				m = cC.getMethodFromConstantPool(dv.getInt16(pc+1,false));
				if(!m){
					cF.lv=lv;
					cF.pc=pc;
					return;
				}
				oref=cF.stack[cF.stackpos-m.args_length-1];
				if(oref===0||oref==undefined){
					cF.pc=pc;
					cF.tNE(stack,_NP, "invokevirtual: " + m.signature);
					cF=stack.peek();
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					pc=cF.pc;
					//throw "NullPointerException: \n"+getStackTrace();
					continue;
				}
				if(oref._native)
					cl=oref.clazz;
				else
					cl = ((oref)).clazz;
				let mname = m.signature;
				m = cl.getMethodByName(mname);
				if(!m) {
					cF.lv=lv;
					cF.pc=pc;
					return;
				}

				nf = cF.newRefFrame(m);
				//oref =  nf.lv[0];
				pc += 3;
				cF.pc = pc;
				++m.invoked;
				if (m.isNative) {
					//cF = stack.peek();
					stack.push(nf);
					pc = cF.pc;
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					if(m.invoke(stack, nf.lv, cF))
						return;
					cF=stack.peek();
					code=cF.code;
					pc=cF.pc;
					dv=cF.dv;
					lv=cF.lv;
					cC=cF.method.clazz;
					continue;
				}

				if (m == null)
					throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
				nf.method=m;

				cF = nf;
				pc = cF.pc;
				lv = cF.lv;

				cC = m.clazz;
				stack.push(cF);
				code = cF.code;
				dv=cF.dv;
				continue;
			case  0xB7:
				//invokespecial
				m = cC.getMethodFromConstantPool(dv.getInt16(pc+1,false));
				if(!m) {
					cF.lv=lv;
					cF.pc=pc;
					return;
				}
				pc += 3;
				cF.pc = pc;
				//System.out.println("invokespecial: " + m.getMyClass().getName() + "." + m.getSignature());

				nf = cF.newRefFrame(m);
				//heap.clearPermanent(nf.lv[0]);
				++m.invoked;
				if (m.isNative) {
					//cF = stack.peek();
					stack.push(nf);
					pc = cF.pc;
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					if(m.invoke(stack, nf.lv, cF))
						return;
					cF=stack.peek();
					code=cF.code;
					pc=cF.pc;
					dv=cF.dv;
					lv=cF.lv;
					cC=cF.method.clazz;
					continue;
				}
				cF = nf;
				pc = cF.pc;
				lv = cF.lv;
				//cC = m.getMyClass();
				cC=m.clazz;
				stack.push(cF);
				code = cF.code;
				dv=cF.dv;
				continue;
			case  0xB8:
				//invokestatic
				m = cC.getMethodFromConstantPool(dv.getInt16(pc+1,false));
				if(!m) {
					
					cF.lv=lv;
					cF.pc=pc;
					return;
				}
				pc += 3;
				cF.pc = pc;


				nf = cF.newFrame(m);
				++m.invoked;
				if (m.isNative) {
					//cF = stack.peek();
					stack.push(nf);
					pc = cF.pc;
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					if(m.invoke(stack, nf.lv, cF))
						return;
					cF=stack.peek();
					code=cF.code;
					pc=cF.pc;
					dv=cF.dv;
					lv=cF.lv;
					cC=cF.method.clazz;
					continue;
				}
				cF = nf;
				pc = cF.pc;
				code = cF.code;
				dv=cF.dv;
				lv = cF.lv;
				cC=m.clazz;
				stack.push(cF);
				continue;
			case  0xB9:
				//invokeinterface
				const signature = cC.getMethodSignature(dv.getInt16(pc+1,false));
				//if(!m) return;

				let count = code[pc + 3];
				let zero = code[pc + 4];
				oref=cF.stack[cF.stackpos-count];
				if(oref===0||oref===undefined)
				{
					cF.tNE(stack,_NP, "invokeinterface: "+signature);
					cF=stack.peek();
					code = cF.code;
					dv=cF.dv;
					lv = cF.lv;
					pc=cF.pc;
					continue;
				}
				let ci = (oref);
				let mc = ci.clazz;
				m = mc.getMethodBySignature(signature);

				if(!m){
					cF.pc=pc;
					return;
				}

				pc += 5;
				cF.pc = pc;

				let mlv = new Array(count);
				for (let ix = 0; ix < count; ++ix) {
					//nf.getLv().put(p - ix - 1, cF.pop());
					mlv[count - ix - 1] = cF.pop();
				}


				nf = new Frame(m);


				nf.init();

				nf.lv=mlv;
				++m.invoked;
				if (m.isNative) {
					stack.push(nf);
					m.invoke(stack, mlv, cF);
					cF=stack.peek();
					code=cF.code;
					pc=cF.pc;
					dv=cF.dv;
					lv=cF.lv;
					cC=cF.method.clazz;
					continue;
				}

				if (m._abstract) {
					m = ((oref)).getMyClass().getMethod(m.getSignature());
				}
				cF = nf;
				pc = cF.pc;
				lv = cF.lv;

				cC = m.clazz;
				stack.push(cF);
				code = cF.code;
				dv=cF.dv;
				continue;
			case  0xBA:
				//invokedynamic
				throw new RuntimeException("invokedynamic not implemented");
			case  0xBB:
				//new
				clazz=cC.getClass(dv.getInt16(pc+1,false));
				if(!clazz){ 
					cF.lv=lv;
					cF.pc=pc;
					return;
				}
				c = new ClassInstance(clazz);
				//heap.makePermanent(a);
				cF.push(c);
				pc += 3;
				continue;
			case  0xBC:
				//newarray
				i1 =  cF.pop();
				array = new ArrayInstance("", i1, code[pc + 1]);
				cF.push((array));
				pc += 2;
				continue;
			case  0xBD:
				//anewarray
				i1 =  cF.pop();
				array = new ArrayInstance("", i1, _L);
				cF.push((array));
				pc += 3;
				continue;
			case  0xBE:
				//arraylength
				array = (cF.pop());
				cF.push(array.length());
				pc += 1;
				continue;
			case  0xBF:
				//athrow
				oref =  cF.pop();
				cF.pc=pc;
				cF.throw(stack, oref, (oref));
				cF=stack.peek();
				code = cF.code;
				dv=cF.dv;
				lv = cF.lv;
				//cF.push(oref);
				pc=cF.pc;
				//pc += 1;

				continue;
			case  0xC0:
				//checkcast
				fc = cC.constants[dv.getInt16(pc+1,false)-1];
				fc = cC.constants[fc.index1-1];
				if (!fc.str.startsWith("[")) {
					let cl2 = Class.getClass(fc.str);
					if(!cl2){
						cF.pc=pc;
						return;
					}
					i1 =  cF.peek();
					if(i1){
						if(i1._native)
							obj=i1;
						else
							obj = (i1);

						if (obj instanceof ClassInstance) {
							cl = obj.clazz;
							if (!cl.isInstance(cl2)) {
								//ClassCastException
								throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
							}
						}
					}
				}
				pc += 3;
				continue;
			case  0xC1:
				//instanceof
				i1 = dv.getInt16(pc+1,false);
				let cl2 = cC.getClass(i1);
				if(!cl2){
					cF.pc=pc;
					return;
				}
				i1 =  cF.pop();
				if (i1 == 0) {
					cF.push(0);
					pc += 3;
					continue;
				}
				obj = (i1);
				cl = obj.clazz;
				if (cl.isInstance(cl2))
					cF.push(1);
				else
					cF.push(0);
				pc += 3;
				continue;
			case  0xC2:
				//monitorenter
				oref =  cF.pop();
				let i=(oref);
				/*
				if(i.monitorenter(thread))
				{
					thread._wait();
					return 1;
				}
				*/
				pc += 1;
				continue;
			case  0xC3:
				//monitorexit
				oref =  cF.pop();
				let _i=(oref);
				/*
				let next=_i.monitorexit(thread);
				if(next!=null)
					JVM.notify(next);
				*/
				pc += 1;
				continue;

			case  0xC4:
				//wide
				//alert("wide");
				pc += 1;
				switch (code[pc]) {
					case  0x15:
						//iload
						cF.push(lv[dv.getInt16(pc+1,false)]);
						pc += 3;
						continue;
					case  0x16:
						//lload
						i1 = dv.getInt16(pc+1,false);
						cF.push(lv[i1]);
						cF.push(lv[i1 + 1]);
						pc += 3;
						continue;
					case  0x17:
						//fload
						cF.push(lv[dv.getInt16(pc+1,false)]);
						pc += 3;
						continue;
					case  0x18:
						//dload
						i1 = dv.getInt16(pc+1,false);
						cF.push(lv[i1]);
						cF.push(lv[i1 + 1]);
						pc += 3;
						continue;
					case  0x19:
						//aload
						cF.push(lv[dv.getInt16(pc+1,false)]);
						pc += 3;
						continue;
					case  0x36:
						//istore
						lv[dv.getInt16(pc+1,false)] = cF.pop();
						pc += 3;
						continue;
					case  0x37:
						//lstore
						i1 = dv.getInt16(pc+1,false);
						lv[i1+1] = cF.pop();
						lv[i1] = cF.pop();
						pc += 3;
						continue;
					case  0x38:
						//fstore
						lv[dv.getInt16(pc+1,false)] = cF.pop();
						pc += 3;
						continue;
					case  0x39:
						//dstore
						i1 = dv.getInt16(pc+1,false);
						lv[i1+1] = cF.pop();
						lv[i1] = cF.pop();
						pc += 3;
						continue;
					case  0x3A:
						//astore
						oref =  cF.pop();
						lv[dv.getInt16(pc+1,false)] = oref;
						//cF.lvpt[ii(code[pc + 1], code[pc + 2])] = oref;
						pc += 3;
						continue;
					case  0x84:
						//iinc
						let index=dv.getInt16(pc+1,false);
						i1 =  lv[index];
						lv[index] = i1 + dv.getInt16(pc+3,false);// ii(code[pc + 3], code[pc + 4]);
						pc += 5;
						continue;

				}
				throw new RuntimeException("wide not implememted");
				//continue;
			case  0xC5:
				//multianewarray
				i1 = dv.getInt16(pc+1,false); //_2b2i(code[pc + 1], code[pc + 2]);
				//dimensions
				i2 = code[pc + 3];
				const sizes=new Array(i2);
				for(i3=0;i3<i2;++i3){
					sizes[i2-1-i3]=cF.pop();
				}
				array = ArrayInstance.multianewarray(i1, sizes, 0);
				cF.push(array);
				pc += 4;
				//throw new RuntimeException("not implememted");
				continue;
			case  0xC6:
				//ifnull
				oref=cF.pop();
				if ( oref == 0 || oref==undefined)
					pc += dv.getInt16(pc+1,false);// _2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xC7:
				//ifnonnull
				oref=cF.pop();
				if (oref != 0&& oref!==undefined)
					pc += dv.getInt16(pc+1,false);// _2b2i(code[pc + 1], code[pc + 2]);
				else
					pc += 3;
				continue;
			case  0xC8:
				//goto
				pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
				continue;
			case  0xC9:
				//jsr_w
				cF.push(pc + 5);
				pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
				continue;
			default:
				pc += 1;
				throw ("wrong opcode at pc: "+pc+" op:" + code[pc]);
				// cs+= byteToHex(code[i])+"\n";
		}
	} while (true);
		cF.pc = pc;
		onhalt();
		return 0;
	}
}
