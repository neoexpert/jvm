"use strict";
class CustomElement extends HTMLElement {
	constructor() {
		super();
	}
};
class JSArray{
	constructor(arr){
		this.arr=arr;
	}
	length(){
		return this.arr.length;
	}
	get(index){
		return new JSObject(this.arr[index],self.js_JSObject);
	}
}
class JSObject{
	constructor(obj,clazz,type){
		this.obj=obj;
		this.clazz=clazz;
		if(type===undefined)
			type=0;
		this.fields={type:type};
	}
	static async create(obj, classname, type){
		let clazz;
		if(classname)
			clazz=await Class.getClass(classname);
		else{
			if(obj instanceof Element){
				clazz=js_dom_DOMElement;
				type=2;
			}
			else
			{
				clazz=js_JSObject;
			}
		}
		if(type===undefined)
			type=0;
		return new JSObject(obj,clazz, type);
	}
	//this.gatherRefs=function(refs){};
}
class DOMCollection{
	constructor(collection){
		this.collection=collection;
		this.clazz=self.js_dom_DOMCollection;
	}
	//this.gatherRefs=function(refs){};
}
self.
init_dom=async function(cl){
jvm.inits.push(async function(cl){
	self.js_JSObject= 		await cl.loadClass("js/JSObject",true);
	self.js_JSObject.bindNatives();
	self.js_JSEvent= 		await cl.loadClass("js/JSEvent",true);
	self.js_JSEvent.bindNatives();
	self.js_JSStyle= 		await cl.loadClass("js/JSStyle",true);
	self.js_JSStyle.bindNatives();
	self.js_JSException= 		await cl.loadClass("js/JSException",true);
	self.js_dom_ImageData= 		await cl.loadClass("js/dom/ImageData",true);
	self.js_dom_ImageData.bindNatives();
	self.js_net_AJAX= 		await cl.loadClass("js/net/AJAX",true);
	self.js_net_AJAX.bindNatives();
	self.js_dom_DOM=		await cl.loadClass("js/dom/DOM",true);
	self.js_dom_DOM.bindNatives();
	self.js_dom_DOMElement=		await cl.loadClass("js/dom/DOMElement",true);
	self.js_dom_DOMElement.bindNatives();
	self.js_dom_Canvas2D=		await cl.loadClass("js/dom/Canvas2D",true);
	self.js_dom_Canvas2D.bindNatives();
	self.js_dom_Canvas2D$Context2D=	await cl.loadClass("js/dom/Canvas2D$Context2D",true);
	self.js_dom_Canvas2D$Context2D.bindNatives();
	self.js_dom_ShadowRoot=		await cl.loadClass("js/dom/ShadowRoot",true);
	self.js_dom_ShadowRoot.bindNatives();
	self.js_dom_DOMCollection=	await cl.loadClass("js/dom/DOMCollection",true);
	self.js_dom_DOMCollection.bindNatives();
	self.js_dom_JSElement=		await cl.loadClass("js/dom/JSElement",true);
	self.js_dom_JSElement.bindNatives();
const bclazz=
await
cl.loadClass("java/lang/Boolean");
cl.
dn(js_JSObject, "getBoolean(Ljava/lang/String;)Ljava/lang/Boolean;" , function(stack, $this, name){
	const obj=$this.obj;
	if(obj){
		const attr=obj[name.str];
		if(attr===undefined)
			return 0;
		else
		{
			return attr?bclazz.statics["TRUE"]:bclazz.statics["FALSE"];
		}
	}
	else throw "obj is undefined";
});
});
}

function js_dom_DOM_requestAnimationFrame(stack, runnable){
	window.requestAnimationFrame(async function(){
		await stack.dispatchEvent(function(){return runnable;});
	});
}

function js_dom_DOM_replaceState(stack, state){
	history.replaceState(null, "", state.str)

}

function js_dom_DOM_setQuery(stack, query){
	let newURL=window.location.pathname;
	newURL+="?/"+query.str;
	history.replaceState(null, "", newURL);
}

function js_dom_DOM_defineCustomElement(stack, name){
	if(self.customElements)
		customElements.define(name.str, CustomElement);
}

async function js_dom_DOM_getWindow(stack){
	return new JSObject(window,js_dom_JSElement);
}

function js_dom_DOM_getDocument(stack){
	return new JSObject(document,js_dom_JSElement);
}

async function js_dom_DOM_getElementById(stack, id){
	const obj=document.getElementById(id.str);
	if(obj===null)
		return 0;
	else{
		const element_addr=await JSObject.create(obj);
		return element_addr;
	}
}

function js_dom_DOM_getElementsByClassName(stack, classname){
	const arr=document.getElementsByClassName(classname.str);
	if(arr===null)
		return 0;
	else{
		return new DOMCollection(Array.from(arr));
	}
}
async function js_dom_ShadowRoot_getElementById(stack, $this, id){
	const obj=$this.obj.getElementById(id.str);
	if(obj===null)
		return 0;
	else{
		const element_addr=await JSObject.create(obj);
		return element_addr;
	}
}

function js_dom_ShadowRoot_getElementsByClassName(stack, $this, classname){
	const arr=$this.obj.querySelectorAll("."+classname.str);
	if(arr===null)
		return 0;
	else{
		return new DOMCollection(Array.from(arr));
	}
}
async function js_dom_DOM_createImage(stack){
	return new JSObject.create(new Image(),js_dom_Image);
}

function js_dom_DOM_registerThread(stack){
	if(stack.uiThread===true)return;
	stack.uiThread=true;
	stack.eventqueue=[];
	stack.setPriority(128);
	stack.dispatchEvent=async function(ev){
		if(stack.eventcallback){
			let evc=stack.eventcallback;
			stack.eventcallback=undefined;
			stack.state=TS_RUNNING;
			evc(ev());
		}
		else
			return new Promise(function(resolve, reject){
				stack.eventqueue.push(function(){
					resolve();
					return ev();
				});
			});
	}
}

async function js_dom_DOM_unregisterThread(stack, thread){
	const t=thread.thread;
	await t.dispatchEvent(function(){
		t.uiThread=false;
		t.eventqueue=undefined;
		t.dispatchEvent=undefined;
		return null;
	});
}

function js_dom_DOM_createElement(stack, name){
	switch(name.str){
		case "canvas":
			return new JSObject(document.createElement(name.str), js_dom_Canvas2D);
		default:
			return new JSObject(document.createElement(name.str), js_dom_DOMElement);
	}
}
function js_dom_DOM_htmlToElement(stack, html){
	var template = document.createElement('template');
	let _html = html.str.trim();
	template.innerHTML = _html;
	return new JSObject(template.content.firstChild.nextElementSibling, js_dom_DOMElement);
}
function js_dom_DOM_createElementNS(stack, namespace, name){
	return new JSObject(document.createElementNS(namespace.str, name.str),js_dom_DOMElement);
}

async function js_dom_DOM_getNextEvent(stack){
	/*
	while(stack.eventqueue.length==0 && threads.length>0)
		await st();
		*/
	///*
	stack.s=stack.p;
	if(stack.eventqueue.length==0 && sthreads.length>0)
		_yield();
		//*/
	if(stack.eventqueue.length>0)
	{
		console.log("queue:"+stack.eventqueue.length);
		return stack.eventqueue.shift()();
	}
	return new Promise(function(resolve, reject){
		if(stack.eventqueue.length>0)
		{
			console.log("queue:"+stack.eventqueue.length);
			resolve(stack.eventqueue.shift()());
			return;
		}
		stack.state=TS_WAIT;
		stack.eventcallback=resolve;
	});
}

function js_dom_DOM_getWidth(stack){
	return document.documentElement.clientWidth;
}
function js_dom_DOM_getHeight(stack){
	return document.documentElement.clientHeight;
}
function js_dom_DOM_confirm(stack, message){
	return confirm(message.str)?1:0;
}
function js_dom_DOM_prompt(stack, title, message){
	const result=prompt(title.str, message.str);
	if(result==null)
		return 0;
	else 
		return JString.intern(result);
}
function js_dom_DOM_alert(stack, message){
	if(message)
		alert(message.str);
	else
		alert("null");
}

function js_JSObject_toString(stack, $this, key){
	const obj=$this.obj.toString();
}
function js_JSElement(stack, $this, key){
	const obj=$this.obj.toString();
}

async function js_JSObject_get(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		if(attr===undefined)
			return 0;
		else
		{
			return await JSObject.create(attr);
		}
}
function js_JSObject_setInt(stack, $this, name, value){
	const obj=$this.obj;
	obj[name.str]=value;
}
function js_JSObject_setFloat(stack, $this, name, value){
	const obj=$this.obj;
	obj[name.str]=value;
}
function js_JSObject_setBoolean(stack, $this, name, value){
	const obj=$this.obj;
	obj[name.str]=value==1;
}
function js_JSObject_setString(stack, $this, name, value){
	const obj=$this.obj;
	obj[name.str]=value.str;
}
function js_JSObject_getInteger(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		if(attr===undefined)
			return 0;
		else
		{
			return new JInteger(attr|0);
		}
}
function js_JSObject_has(stack, $this, key){
	const obj=$this.obj;
	const attr=obj[key.str];
	return attr===undefined?0:1;
}
function js_JSObject_getInt(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		if(attr===undefined)
			throw "no such key";
		else
		{
			return attr|0;
		}
}
function js_JSObject_getString(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		if(attr===undefined)
			return 0;
		else
		{
			return new JString(attr);
		}
}
function js_JSObject_getArray(stack, $this, name){
	const obj=$this.obj;
		const arr=obj[name.str];
		if(arr===undefined)
			return 0;
		else{
			const len=arr.length;
			const jarr=new Array(len);
			for(let i=0;i<len;++i)
				jarr[i]=new JSObject(arr[i],self.js_JSObject);
			return jarr;
		}
}

function js_dom_JSElement_addEventListener0(stack, $this, name, listener){
	const obj=$this.obj;
	const ev=name.str;
	const stopPropagation=listener.fields.stopPropagation;
	listener.event_listener=async function(e){
		await stack.dispatchEvent(function(){
			listener.fields.event=new JSObject(e, js_JSEvent);
			return listener;
		});
	};
	obj.addEventListener(ev, listener.event_listener);
}

function js_dom_JSElement_addEventListenerPreventDefault0(stack, $this, name, listener){
       const obj=$this.obj;
       const ev=name.str;
       listener.event_listener=function(e){
               stack.dispatchEvent(function(){
                       listener.fields.event=new JSObject(e,js_JSEvent);
                       return listener;
               });
               e.stopPropagation();
               e.preventDefault();
       };
       obj.addEventListener(ev, listener.event_listener);
}


function js_dom_JSElement_removeEventListener0(stack, $this, name, listener){
	const obj=$this.obj;
	obj.removeEventListener(name.str, listener.event_listener);
}

async function js_dom_DOMElement_click(stack, $this){
	const obj=$this.obj;
	obj.click();
}
function js_dom_DOMElement_getAnchor(stack, $this){
	const obj=$this.obj;
	return obj._anchor;
}
function js_dom_DOMElement_setAnchor(stack, $this, anchor){
	const obj=$this.obj;
	obj._anchor=anchor;
}
function js_dom_DOMElement_attachShadow(stack, $this){
	const obj=$this.obj;
	return new JSObject(obj.attachShadow({mode: 'open'}), js_dom_ShadowRoot);
}
async function js_dom_DOMElement_cloneNode(stack, $this, deep){
	const obj=$this.obj;
	const el=await JSObject.create(obj.cloneNode(deep==1));
	return el;
}
async function js_dom_JSElement_getChildren(stack, $this){
	const obj=$this.obj;
	return new DOMCollection(Array.from(obj.childNodes));
}
async function js_dom_JSElement_setContent(stack, $this, content){
	if(!content)
	{
		throw await __NE(stack, _NP, "content is null");
	}
	const obj=$this.obj;
		obj.innerHTML=content.str;
		return new Promise(function(resolve, reject){
			setTimeout(function(){resolve();}, 0);
			
		});
}
async function js_dom_JSElement_empty(stack, $this){
	const obj=$this.obj;
		const el=obj;
		while (el.firstChild) el.removeChild(el.firstChild);

}
async function js_dom_DOMElement_addClass(stack, $this, classname){
		try{
			$this.obj.classList.add(classname.str);
		}
		catch(e){
			throw __NE(stack,"js/JSException",e.message);
		}
}
async function js_dom_DOMElement_hasClass(stack, $this, classname){
		try{
			const z=$this.obj.classList.contains(classname.str);
			return z?1:0;
		}
		catch(e){
			throw __NE(stack,"js/JSException",e.message);
		}
}

function js_dom_DOMElement_removeClass(stack, $this, classname){
	const obj=$this.obj;
		obj.classList.remove(classname.str);
}
function js_dom_DOMElement_setAttribute(stack, $this, name, value){
	const obj=$this.obj;
	obj.setAttribute(name.str, value.str);
}
function js_dom_DOMElement_setAttributeNS(stack, $this,namespace,  name, value){
	const obj=$this.obj;
	obj.setAttributeNS(namespace.str, name.str, value.str);
}
function js_dom_DOMElement_setIntAttribute(stack, $this, name, value){
	const obj=$this.obj;
		obj.setAttribute(name.str, value);
}
function js_dom_DOMElement_setFloatAttribute(stack, $this, name, value){
	const obj=$this.obj;
	obj.setAttribute(name.str, value);
}
function js_dom_DOMElement_getAttribute(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		if(typeof attr=="string")
			return JString.intern(attr);
		else
		{
			return 0;
		}
}
function js_dom_DOMElement_getStyle(stack, $this){
	const obj=$this.obj;
	return new JSObject(obj.style, js_JSStyle, 5);
}

function js_dom_DOMElement_getIntAttribute(stack, $this, name){
	const obj=$this.obj;
		const attr=obj[name.str];
		return attr|0;
}
function js_dom_DOMElement_offsetLeft(stack, $this){
	const obj=$this.obj;
		return $(obj).offset().left|0;
}
function js_dom_DOMElement_offsetTop(stack, $this){
	const obj=$this.obj;
		return $(obj).offset().top|0;
}
function js_dom_DOMElement_offsetLeftRelativeToWindow(stack, $this){
	const obj=$this.obj;
		return obj.getBoundingClientRect().left|0;
}
function js_dom_DOMElement_offsetTopRelativeToWindow(stack, $this){
	const obj=$this.obj;
		return obj.getBoundingClientRect().top|0;
}
function js_dom_DOMElement_outerWidth(stack, $this){
	const obj=$this.obj;
		return $(obj).outerWidth();
}
function js_dom_DOMElement_outerHeight(stack, $this){
	const obj=$this.obj;
		return $(obj).outerHeight();
}
function js_dom_Canvas2D_init(stack, $this){
	const obj=document.createElement("canvas");
	$this.obj=obj;
	$this.fields.context2D=new JSObject(obj.getContext("2d"), js_dom_Canvas2D$Context2D);
}
function js_dom_Canvas2D$Context2D_setFillStyle(stack, $this, fillStyle){
	$this.obj.fillStyle=fillStyle.str;
}
function js_dom_Canvas2D$Context2D_setFont(stack, $this, font){
	$this.obj.font=font.str;
}
function js_dom_Canvas2D$Context2D_setFont(stack, $this, font){
	$this.obj.font=font.str;
}
function js_dom_Canvas2D$Context2D_setTextAlign(stack, $this, align){
	$this.obj.textAlign=align.str;
}
function js_dom_Canvas2D$Context2D_fillText(stack, $this, text, x, y){
	$this.obj.fillText(text.str,x,y);
}
function js_dom_Canvas2D$Context2D_fillRect(stack, $this, x, y, w, h){
	$this.obj.fillRect(x,y,w,h);
}
function js_dom_Canvas2D$Context2D_createImageData(stack, $this, width, height){
	const obj=$this.obj;
		return new JSObject(obj.createImageData(width,height), js_dom_ImageData);
}
function js_dom_Canvas2D$Context2D_getImageData(stack, $this, x, y, width, height){
	const obj=$this.obj;
	return new JSObject(obj.getImageData(x,y,width,height), js_dom_ImageData);
}

function js_dom_Canvas2D$Context2D_putImageData(stack, $this, x, y, imageData){
	const obj=$this.obj;
		obj.putImageData(imageData.obj,x,y);
}

function js_dom_ImageData_setIntArray(stack, $this, int32Array){
	const obj=$this.obj;
		obj.data.set(new Uint8ClampedArray(int32Array.buffer));
}

function js_dom_JSElement_appendChild(stack, $this, child){
	const obj=$this.obj;
	const childobj=child.obj;
	try{
			obj.appendChild(childobj);
	}
	catch(e){
		throw __NE(stack,"js/JSException",e.message);

	}
}
function js_dom_JSElement_prependChild(stack, $this, child){
	const obj=$this.obj;
	const childobj=child.obj;
	try{
			obj.insertBefore(childobj, obj.firstChild);
	}
	catch(e){
		throw __NE(stack,"js/JSException",e.message);

	}
}
function js_dom_JSElement_insertBefore(stack, $this, child, before){
	const obj=$this.obj;
	const childobj=child.obj;
	const beforeobj=before.obj;
	try{
			obj.insertBefore(childobj,beforeobj);
	}
	catch(e){
		throw __NE(stack,"js/JSException",e.message);
	}
}
function js_dom_JSElement_removeChild(stack, $this, child){
	const obj=$this.obj;
	const childobj=child.obj;
		let r;
		try{
			r=obj.removeChild(childobj);
		}
		catch(e){
			throw __NE(stack,"js/JSException",e.message);
		}

		if(r)return 1;
		else return 0;
}
function js_dom_DOMElement_remove(stack, $this){
	const obj=$this.obj;
	try{
		obj.remove();
	}
	catch(e){
		throw __NE(stack,"js/JSException",e.message);
	}
}
function js_dom_DOMElement_setID(stack, $this, id){
	const obj=$this.obj;
	obj.id=id.str;
}
function js_dom_DOMElement_css(stack, $this, name, value){
	const obj=$this.obj;
		obj.style[name.str]=value.str;
}
function js_dom_DOMElement_cssRGBA(stack, $this, name, r, g, b, a){
	const obj=$this.obj;
		obj.style[name.str]="rgba("+r+","+g+","+b+","+a+")";
}
function js_dom_DOMElement_draggable(stack, $this, start, drag, stop, helper){
	const obj=$this.obj;
		var options={};
		options.start=async function(event, ui) {
			//$(this).css("background","red");
			$(ui.helper).width($(this).width());
			if(!start)
				return;
			await stack.dispatchEvent(function(){
				start.fields.canceled=false;
				return start;
			});
			return !start.fields.canceled;
		};
		if(drag)
			options.drag=async function() {
				await stack.dispatchEvent(function(){return drag;});
			};
		if(stop)
			options.stop=async function() {
				await stack.dispatchEvent(function(){return stop;});
			};
		if(helper)
			options.helper=helper.str;
		$(obj).draggable(options);
}
function js_dom_DOMElement_droppable(stack, $this, dropevent){
	const obj=$this.obj;
		var options={};
		if(dropevent)
			options.drop=async function(event, ui) {
				const d=ui.helper;
				const doff=d.offset();
				const poff=$(this).offset();
				const left=doff.left-poff.left;
				const top=doff.top-poff.top;
				await stack.dispatchEvent(function(){
					dropevent.fields.x=left;
					dropevent.fields.y=top;
					return dropevent;
				});
			};
		$(obj).droppable(options);
}


function js_dom_DOMCollection_size(stack, $this){
	return $this.collection.length;
}
function js_dom_DOMCollection_addClass(stack, $this, _classname){
	const c=$this.collection;
		const classname=_classname.str;
		for (let el of c) 
			el.classList.add(classname);
}
function js_dom_DOMCollection_removeClass(stack, $this, _classname){
	const c=$this.collection;
		const classname=_classname.str;
		for (let el of c)
			el.classList.remove(classname);
}
function js_JSStyle_setpx(stack, $this, name, value){
	const obj=$this.obj;
		obj[name.str]=value+"px";
}
function js_JSEvent_preventDefault(stack, $this){
	const obj=$this.obj;
	obj.preventDefault();
	obj.stopPropagation();
}

function js_net_AJAX_getAsync(stack, url, ajaxResult){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url.str);
		//xhr.responseType = "arraybuffer";
	xhr.onload = async function() {
		if (xhr.status === 200) {
			await stack.dispatchEvent(function(){
				ajaxResult.fields.text=new JString(xhr.response);
				return ajaxResult;
			});
		}
		else {
			throw xhr.status;
		}
	};
	xhr.onerror= async function(e){
		await stack.dispatchEvent(function(){
			ajaxResult.fields.exception = __NE(stack,"java/io/IOException", e.message);

			return ajaxResult;
		});
	};
	xhr.send();

}
async function js_net_AJAX_get(stack, url){
	return new Promise(function(resolve, reject){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url.str, true);
		//xhr.responseType = "arraybuffer";
		xhr.onload = function() {
			if (xhr.status === 200) {
				resolve(new JString(xhr.response));
			}
			else {
				throw xhr.status;
			}
		};
		xhr.onerror= async function(e){
			debugger;
			reject(__NE(stack,"java/io/IOException"));
		};
		xhr.send();
	});
}
async function js_net_AJAX_getByteArray(stack, url){
	return new Promise(function(resolve, reject){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url.str, true);
		xhr.responseType = "arraybuffer";
		xhr.onload = function() {
			if (xhr.status === 200) {
				resolve(new Int8Array(xhr.response));
			}
			else {
				throw xhr.status;
			}
		};
		xhr.onerror= async function(e){
			debugger;
			reject(__NE(stack,"java/io/IOException"));
		};
		xhr.send();
	});
}
function js_net_AJAX_post(stack, url, params){
	return new Promise(function(resolve, reject){
			try{
					$.post(url.str, params.obj,function(result){
							resolve(new JString(result));
					});
			}
			catch(e){
					reject(__NE(stack,"java/io/IOException",e.message));
			}
	});
}
