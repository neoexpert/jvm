function initCodeEditor(jvm){
jvm.initial_classes.push("CodeEditor");
jvm.inits.push(function(jvm){
jvm.
anm("CodeEditor", "init0(Ljava/lang/Object;)V" , [_L], function(stack,lv,pF){
	const $this=(lv[0]);
	const ne=(lv[1]);
	const container=ne.element;
	const textarea=document.createElement("textarea");
	container.appendChild(textarea);
	$this.editor=CodeMirror.fromTextArea(textarea, {
		keyMap: "vim",
		lineNumbers: true,
		mode: "text/x-java",
		matchBrackets: true,
		theme:"dracula",
		showCursorWhenSelecting: true,
		inputStyle: "contenteditable",
		extraKeys: {
			"Ctrl-S": function(instance) { saveSource(instance.getValue()); }
		}
	});
	stack.pop();
});
jvm.
anm("CodeEditor", "setValue(Ljava/lang/String;)V" , [_L], function(stack,lv,pF){
	const $this=(lv[0]);
	$this.editor.setValue(lv[1].str);
	stack.pop();
});
jvm.
anm("CodeEditor", "getValue()Ljava/lang/String;" , [], function(stack,lv,pF){
	const $this=(lv[0]);
	stack.pop();
	stack.peek().push(new JString($this.editor.getValue()));
});
});
}
