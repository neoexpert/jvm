"use strict";

//suspended threads
const sthreads=[];

var sync_t_counter=4;
async function sync_t(){
	return new Promise(function(resolve, reject){
		setTimeout(function(){
			sync_t_counter=4;
			resolve();
		},0);
	});
}
//scheduler
async function st(){
	///*
	if(--sync_t_counter<=0)
		await sync_t();
	if(sthreads.length==0)
		return;
	return new Promise(function(resolve, reject){
		sthreads.push(resolve);
		sthreads.shift()();
	});
	//*/
	/*
	return new Promise(function(resolve, reject){
		setTimeout(function(){
			if(sthreads.length==0){
				resolve();
				return;
			}
			sthreads.push(resolve);
			sthreads.shift()();
		}, 0);

	});
	//*/
}

function _yield(){
	if(sthreads.length==0)
		return;
	sthreads.shift()();
}

class ClassLoader{
	constructor(id){
		this.classes={};
		this.cs=[];
		this.to_clinit=[];
		this.id=id;
	}
	async loadClass(classname, dont_init){
		const c=this.classes[classname];
		if(c!==undefined) return c;
		let json=await jvm_interface.
		GETCLASS(classname, this.clID);
		return await this.readClass(json, dont_init);
	}
	rc(json){
		let id=json.id;
		let c = new Class(json, this);
		this.classes[c.name]=c;
		this.cs[id]=c;
		const cmethods=json.methods;
		c.methods=json.methods;
		return c;
	}
	async readClass(json, dont_init){
		let id=json.id;
		if(this.cs[id])
			return this.cs[id];
		let c = new Class(json, this);
		this.classes[c.name]=c;
		this.cs[id]=c;
		jvm.cs[id]=c;
		self["_c"+id]=c;
		const cmethods=json.methods;
		c.methods=json.methods;
		if(!dont_init)
			await c.clinit();
		if(json["super"]){
			let superClass;
			if(json["sCLID"])
				c.setSuperClass(await self[json["sCLID"]].loadClass(json["super"]));
			else
				c.setSuperClass(await this.loadClass(json["super"]));
		}
		if(jvm.events.onClassPrepared)
			jvm.events.onClassPrepared(c.name);
		c.bindNatives();
		return c;
	}
	//declare native
	dn(clazz, signature, handler){
		const method=processMethod(clazz, handler, signature);
		clazz.methods[signature]=method;
		method.native=true;
	}
	async getMethod(classname, signature){
		let cl=await this.loadClass(classname);
		let m=cl.methods[signature];
		if(m!==undefined&&m!==0)
			return m;
		let bytebuffer=await jvm_interface.GETMETHOD(classname,signature, this.clID);
		m=await readMethod(cl, signature,  bytebuffer);
		cl.methods[signature]=m;
		return m;
	}

	anmc(clazz, signature, handler, isStatic, lineNumbers, isNative){
		const method=processMethod(clazz, handler, signature);
		clazz.methods[signature]=method;
		method.isStatic=isStatic;
		method.native=isNative;
		method.lineNumbers=lineNumbers;
	}


	anma(classes, signature, handler, isStatic, lineNumbers){
		const l=classes.length;
		for(let i=0;i<l;++i){
			this.anmc(classes[i],signature,handler, isStatic, lineNumbers);
		}
	}
}

class JVM{
	constructor(jvm_interface){
		this.events={};
		this.threads={};
		this.suspendedThreads=[];
		this.classes_to_load=0;
		this.to_clinit=new Set();
		this.jvm_interface=jvm_interface;
		this.initial_classes=[];
		this.inits=[];
		this.booted=false;
		this.libs={};
		//volatile halt
		this.vh=false;
		this.classloaders=[];
		this.cs=[];
	}
	afterboot(){
		this.booted=true;
	}


	setOnInitCallback(callback){
		this.oninit=callback;
	}

	saveState(){
		//return this.bex.stack.clone();
	}

	restoreState(stack){
		//this.bex.stack=stack;
	}


	async getClassFromServerByID(classname, callback){
		const c=jvm.classes[classname];
		if(c) return c;
		var that=this;
		let json=this.jvm_interface.
		GETCLASS(classname);
		return readClass(json);
	}

	async getClassesFromServer(classnames){
		let arr=await this.jvm_interface.
		GETCLASSES(classnames);
		const l=arr.length;
		for(let i=0;i<l;++i)
			await this.readClass(arr[i]);
	}

	async init(){
		if(self.init_rcl){
			await init_rcl();
			init_rcl=undefined;
		}
		const initial_classes=await this.jvm_interface.
			GETINITIALCLASSES(false);
		if(!self.mainClass)
			self.mainClass=initial_classes.MainClass;
		if(initial_classes.debug)
			await this.jvm_interface.loadLib("jdwp");
		if(self.initBASE){
			await initBASE(rcl);
			initBASE=undefined;
		}
		const inits=this.inits;
		const li=inits.length;
		for(let i=0;i<li;++i)
			await inits[i](rcl);
		this.inits=[];

		for (let classname of this.initial_classes)
			await rcl.loadClass(classname,false);

		for (let clazz of rcl.to_clinit){
			await clazz.clinit();
		}
		
		jvm.classloaders.push(rcl);

		await jvm.afterboot();
	}

	async loadLibrary(lib){
		if(this.libs[lib]){
			return false;
		}
		this.libs[lib]=true;

		await this.jvm_interface.loadLib(lib);
		const init=window["init_"+lib];
		if(!init)
			throw "could not load library: "+lib;
		await init(rcl);
		/*
		let l=this.initial_classes.length;
		for(let i=0;i<l;++i){
			const clazz=this.initial_classes[i];
			if(this.classes[clazz])
			continue;
			await this.getClass(clazz);
		}*/
		const inits=this.inits;
		const li=inits.length;
		for(let i=0;i<li;++i)
			await inits[i](rcl);
		this.inits=[];
	}
}
function getVolatile(name){
	jvm.vh=true;
	setTimeout(function(){
		//jvm.bex.run();
	},0);

	console.log("getVolatile: "+name);
}
function putVolatile(name){
	console.log("putVolatile: "+name);
}


function processMethod(clazz, handler, signature, lineNumbers){
	handler.clazz=clazz;
	handler.signature=signature;
	handler.lineNumbers=lineNumbers;
	if(clazz)
		handler.cl=clazz.classLoader;
	return handler;
}



class Frame{
	constructor(method) {
		this.method=method;
		//this.stackpos=0;
		this.pc=0;
		//this.pop=this.o;
		//this.push=this.p;
	}
	clear(){
		this.stackpos=0;
	}
	//push
	p(v){
		this.stack[this.stackpos++]=v;
		/*
		if(this.stackpos>this.method.max_stack)
			debugger;
			*/
	}
	pushl(v){
		this.stack[this.stackpos]=v;
		this.stackpos+=2;
	}
	//pop
	o(){
		return this.stack[--this.stackpos];
	}
	peek(){
		return this.stack[this.stackpos-1];
	}

	newRefFrame(m){
		const f = new Frame(m);
		return f;
	}

	newFrame(m){
		const f = new Frame(m);
		return f;
	}

	checkETable(ci, stack){
		if(!(ci instanceof ClassInstance)){
			throw _NE(stack, java_lang_Error, ci.message);
		}
		const pc=this.pc;
		const etable=this.method.etable;
		if(etable){
			const l=etable.length;
			for(let i=0;i<l;++i){
				const e=etable[i];
				if(pc>=e[0] && pc<=e[1]){
					const catch_type=e[3];
					if(catch_type!==0){
						if(!ci.clazz.isInstance(catch_type))
							continue;
					}
					this.pc=e[2];
					this.clear();
					return true;
				}
			}
		}
		return false;
	}

	async throw(stack, ci){
		if(ci===null){
			const clazz= await rcl.loadClass(_NP);
			ci=new ClassInstance(clazz);
		}
		if(!this.checkETable(ci))return;
		stack.pop();
		if(stack.isEmpty())
		{
			printStackTrace(ci,true);
			return;
		}
		let f=stack.peek();
		await f.throw(stack, ci);
	}

	//throw new exception

	init(){
		//this.stack=new Array(this.method.max_stack);
		//this.lv=new Array(this.method.max_locals);
	}
}

async function invokeDynamic(bm, mname, args){
	const f=new Frame(bm);
	f.init();
	const stack=new Thread();
	stack.push(f);
	const l=args.length;
	let _args=[stack, 0, new JString(mname), 0];
	for(let i=0;i<l;++i){
		const arg=args[i];
		switch(arg.type){
			case 8:
				_args.push(new JString(arg.value));
				break
			case 15:
				//MethodHandle
				//const mh=new ClassInstance(await jvm.loadClass("java/lang/invoke/MethodHandle",true));
				const classLoader=arg.cl;
				const clazz=await self[classLoader].loadClass(arg.cname, true);
				const m=await clazz.getMethodByName(arg.signature);
				_args.push(new ClassInstance(new MethodHandleClass(m)));
				break;
			case 16:
				//MethodType
				const mt=new ClassInstance(await rcl.loadClass("java/lang/invoke/MethodType",true));
				mt.asString=arg.mtype;
				_args.push(mt);
				break;
		}
	}

	const callsite=await bm.apply(null, _args);
	return callsite;
	/*
		let clazz=await Class.getClass(cname);
		let targetmethod=clazz.methods[target];
		if(targetmethod==="onserver")
			targetmethod=await rcl.getMethod(cname, target);
		let ci=this.pop();
		let newci=new ClassInstance(new DClass(targetmethod,ci.clazz));
		newci.fields=ci.fields;
		this.push(newci);	
		return true;
		*/
}

//Dynamic Class
class DClass{
	constructor(method, pclazz){
		this.method=method;
		this.clazz=pclazz;
		this.fields={};
		this.fieldNames=[];
	}
	getMethodByName(){
		return this.method;
	}
	isInstance(clazz){
		return this.clazz.isInstance(clazz);
	}

}
class AClass{
	constructor(name){
		this.name=name;
	}
	isInstance(cl){
		if(this.name===cl.name)return true;
		const interfaces=this.interfaces;
		for(let i=0;i<interfaces.length;++i)
		{
			if(cl.name===interfaces[i])
				return true;
		}
		const sc=this.getSuperClass();
		if(sc)
			return sc.isInstance(cl);
		return false;
	}
	getSuperClass(){
		return this.super_class;
	}
}
function parseValue(value){
				switch(value){
						case "NaN":
								return NaN;
								break;
						case "+inf":
								return Infinity;
						case "-inf":
								return -Infinity;
						default:
								return BigInt(value);
				}
}

class Class extends AClass{
	constructor(json, classLoader){
		super(json.name);
		this.classLoader=classLoader;
		this.subclasses=[];
		this.initialized=false;
		this.id=json.id;
		this.source_file=json.source_file;
		this.constants=json.cpool;
		this.ref_names=json.ref_names;
		this.this_class=json.this_class;
		this.statics=json.statics;
		this.natives=json.natives;
		if(json.parseStatics){
			const keys=Object.keys(this.statics);
			const length=keys.length;
			for(let i=0;i<length;++i){
				const key=keys[i];
				const value=this.statics[key];
				if((typeof value)==="string")
					this.statics[key]=parseValue(value);

			}
		}
		this.interfaces=json.interfaces;
		this.fields=json.fields;
		Object.freeze(this.fields);
		this.fieldNames=Object.keys(this.fields);
		this.resolvedFields={};
		this.methods={};
		this.resolvedMethods=[];

		this.instanceTemplate={};
		const defaultFields=this.fields;
		const fieldNames=this.fieldNames;
		const l=fieldNames.length;
		for(let i=0;i<l;++i){
			const fieldname=fieldNames[i];
			const value=defaultFields[fieldname];
			if((typeof value)==="string")
				this.instanceTemplate[fieldname]=parseValue(value);
			else
				this.instanceTemplate[fieldname]=value;
		}
	}
	setSuperClass(clazz){
		this.super_class=clazz;
		clazz.subclasses.push(this);
	}
	getInstance(){
		if(!this.instance)
			this.instance=new JClass(this);
		return this.instance;
	}

	async gs(name){
		if(!this.initialized)
			await this.clinit();
		return this.statics[name];
	}

	async ps(name, value){
		if(!this.initialized)
			await this.clinit();
		this.statics[name]=value;
	}

	bindNatives(){
		const names=Object.keys(this.natives);
		const l=names.length;
		for(let i=0;i<l;++i){
			const mname=names[i];
			const nativeType=this.natives[mname];
			const handler=self[nativeType];
			if((typeof handler)!=="function"){
				//console.warn("could not bind nativeMethod: " + nativeType);
				continue;
			}
			handler.native=true;
			this.methods[mname]=processMethod(this, handler, mname);
		}

	}

	async getMethodByName(name){
		let method=this.methods[name];
		if(method===0)
			return this.classLoader.getMethod(this.name, name);
		if(method!==undefined)
			return method;
		const sclazz=this.getSuperClass();
		if(!sclazz)
			return null;
		method=await sclazz.getMethodByName(name);
		if(method) {
			this.methods[name]=method;
			return method;
		}
		const l=this.interfaces.length;
		for(let i=0;i<l;++i){
			let intf=this.interfaces[i];
			intf=await this.classLoader.loadClass(intf, true);
			method=await intf.getMethodByName(name);
			if(method) {
				this.methods[name]=method;
				return method;
			}

		}
	}


	async clinit(){
		this.initialized=true;
		if(!("<clinit>()V" in this.methods))
			return false;
		//if(!jvm.booted){
			//jvm.toclinit.push(this);
			//return false;
		//}
		const method= await this.classLoader.getMethod(this.name,"<clinit>()V");
		const stack=new Thread();
		const pF=new Frame(method);
		pF.init();
		stack.push(pF);
		try{
			await method(stack);
		}
		catch(error){
			if(error instanceof ClassInstance)
				printStackTrace(error, true);
			else 
				console.error(error);
		}
	}
}
class MethodHandleClass extends Class{
	constructor(method, signature){
		super({fields:{}});
		this.method=method;
		this.methods[signature]=method;
		this.interfaces=[];
	}
	setSignature(signature){
		this.methods[signature]=this.method;
	}
	getTheMethod(){
		return this.method;
	}
}

/*
function c(id){
	return jvm.getClass(id);
}
*/


function java_lang_Throwable_fillInStackTrace(stack, $this){
	const entries=stack.entries;
	const l=entries.length;
	const stacktrace=new Array(l);
	for(let i=0;i<l;++i){
		const entry=entries[i];	
		const se={};
		se.method=entry.method;
		se.pc=entry.pc;
		stacktrace[i]=se;
	}
	$this.fields["stacktrace"]=stacktrace;
}


const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;

/*
function int8ToArg(i){
		switch(i){
				case 91:
				//_ARR
						return 'l';
				case 74: return 'j';
				case 68: return 'd';
				case 76: return 'l';
				case 73: return 'i';
				case 70: return 'f';
				case 90: return 'i';
				case 66: return 'i';
				case 67: return 'i';
				case 83: return 'i';
				default: throw "unknown arg number: " +i;
	}
}
*/
async function readMethod(cl, signature,  bytebuffer){
	let bytearray=new Uint8Array(bytebuffer);
	const dv=new DataView(bytebuffer);
	if(dv.byteLength<=4)
	{
		let err="method "+cl.name+"."+signature+" not found\n";
		throw err;
	}
	let pos=0;
	const type=dv.getInt8(pos);
	++pos;
	//is optimized:
	switch(type){
		case 0:
			//parsejscode
			const id=dv.getInt32(pos,false);
			pos+=4;
			const code_length = dv.getInt32(pos,false);
			pos+=4;
			const jsarr=bytebuffer.slice(pos, pos+code_length);
			var js = new TextDecoder("utf-8").decode(jsarr);
			//console.log(js);
			//alert(js);


			pos+=code_length;

			const args_length = dv.getInt32(pos,false);
			pos+=4;
			const args=bytebuffer.slice(pos, pos+args_length);
			pos+=args_length;
			const isStatic=dv.getInt8(pos++)==1;

			let handler
			try{
				let _args=["t"];
				if(!isStatic)
					_args.push("v0");
				let args8=new Int8Array(args);
				for(let i=0;i<args_length;++i)
					if(isStatic)
						_args.push('v'+i);
				else
					_args.push('v'+(i+1));
				_args.push(js);
				handler=AsyncFunction.apply(null, _args);
				//handler=new AsyncFunction("stack","lv","pF",js);

			}
			catch(error){
				debugger;
				console.error(js);
				throw error;
			}


			//read exception table
			const exception_table_length = dv.getInt32(pos,false);
			const etable=new Array(exception_table_length);
			pos+=4;
			for(let i=0;i<exception_table_length;++i){
				const e=[];
				//start_pc
				e[0] = dv.getInt32(pos,false);
				pos+=4;	
				//end_pc
				e[1] = dv.getInt32(pos,false);
				pos+=4;	
				//handler_pc
				e[2] = dv.getInt32(pos,false);
				pos+=4;	

				//catch_type
				const strlen = dv.getInt32(pos,false);
				pos+=4;	
				if(strlen==0)
					e[3]=0;
				else{
					const strbytes=bytebuffer.slice(pos,pos+strlen);
					const className = new TextDecoder("utf-8").decode(strbytes);
					pos+=strlen;	
					//e[3]=await rcl.loadClass(className);
					e[3]=className;
				}
				etable[i]=e;
			}
			handler.etable=etable;

			let lineNumbers=undefined;
			//LineNumberTaleLength
			const lntl = dv.getInt32(pos,false);
			pos+=4;	
			if(lntl>0){
				lineNumbers=new Array(lntl);
				for(let i=0;i<lntl;++i){
					lineNumbers[i]=dv.getInt32(pos,false);
					pos+=4;
				}

			}
			/////////////

			var classnamearr=bytebuffer.slice(pos,bytebuffer.byteLength);
			var classname = new TextDecoder("utf-8").decode(classnamearr);
			const clazz=await cl.classLoader.loadClass(classname);

			const nmethod=processMethod(clazz, handler, signature, lineNumbers);
			nmethod.isStatic=isStatic;
			self["f"+id]=nmethod;
			nmethod.id=id;
			return nmethod;
		case 1:
			var classnamearr=bytebuffer.slice(pos,bytebuffer.byteLength);
			var classname = new TextDecoder("utf-8").decode(classnamearr);
			if(cl.name==classname){
				let err="method "+classname+"."+signature+" not found\n";
				return null;
			}
			let acl=await rcl.loadClass(classname);
			const method=acl.methods[signature];
			cl.methods[signature]=method;
			return method;
	}
	throw "wrong codetype";
}



async function printStackTrace(ci, _throw){
	const stacktrace=ci.fields["stacktrace"];
	let message=ci.clazz.name + ": ";
	message += ci.fields["detailMessage"].str;
		if(!stacktrace){
				console.error(message);
				return;
		}
	if(jvm.jvm_interface.getStackTrace){
               const msg=await jvm.jvm_interface.getStackTrace(message, stacktrace)
               console.error(msg);
		return;
	}
	const l=stacktrace.length;
	let result="";
		result+=message;
	result+="\n";
	for(let i=0;i<l;++i){
		const se=stacktrace[i];
		const method=se.method;
		result+=method.clazz.name+"."+method.signature;
		result+=" ("+method.clazz.source_file+")";
		result+=" (pc:"+se.pc+")\n";
	}
	//throw result;
	if(_throw){
		if(jvm.jvm_interface.onError)
			jvm.jvm_interface.onError(result);
		throw result;
	}
	else
		console.error(result);
}

var jvm;
self.
createJVM=function(jvm_interface){
	jvm=new JVM(jvm_interface);
	Object.seal(jvm);
	return jvm;
}

self.
startMain=async function(jvm, args){
	await jvm.init();
	const m=await rcl.getMethod(mainClass,"main([Ljava/lang/String;)V");
	//const f=new Frame(m);
	//f.init();
	if(!args){
		const query=window.location.search.substring(1);
		if(query!="")
			args=query.split(",");
		else args=[];
	}
	const array=new Array(args.length);
	for(let i=0;i<args.length;++i){
		array[i]=new JString(args[i]);
	}

	const thread=new ClassInstance(java_lang_Thread);
	thread.thread=new Thread();
	thread.thread.instance=thread;
	thread.fields.name=new JString("main");
	await startThread(m, thread, array);
}

var threadCounter=0;

async function startThread(method, thread, arg0){
	var threadID=++threadCounter;
	if(!thread.fields.name)
		thread.fields.name=new JString("Thread "+threadID);
	const stack=thread.thread;
	const pF=new Frame(method);
	pF.init();
	stack.push(pF);
	stack.ID=threadID;
	jvm.threads[threadID]=stack;
	stack.state=TS_RUNNING;
	try{
		await method(stack, arg0);
	}catch(error){
		if(error instanceof ClassInstance)
			printStackTrace(error, true);
		else{
			console.error(error);
		}
	}
	stack.state=TS_ZOMBIE;
	const joined=stack.joined;
	if(joined){
		const l=joined.length;
		for(let i=0;i<l;++i)
			sthreads.push(joined[i]);
	}
	delete jvm.threads[threadID];
	if(sthreads.length>0)
		sthreads.shift()();

}

//processExceptionTable
async function processETable(method){
		const etable=method.etable;
		if(!etable) return;
		const l=etable.length;
		for(let i=0;i<l;++i){
				let e=etable[i];
				if(e[3]!=0){
						e[3]=await rcl.loadClass(e[3]);
				}
		}
}

//throw ArithmeticException
function ae(stack, pc, msg){
	const e = _NE(stack, java_lang_AE, msg);
	const f=stack.peek();
	f.pc=pc;
	if(f.checkETable(e)) {
		return null;
	} else {
		stack.pop();
		return e;
	}
	
}

//throw NullPointerException
function np(stack, pc, msg){
	const e = _NE(stack, java_lang_NullPointerException, msg);
	const f=stack.peek();
	f.pc=pc;
	if(f.checkETable(e)) {
		return null;
	} else {
		stack.pop();
		return e;
	}
	
}

function _e(err){
	if(err instanceof ClassInstance) return err;
	else{
			debugger;
			const _error=new ClassInstance(java_lang_Error);
			_error.fields.detailMessage=new JString(err.messsage);
			return _error;
	}
}

//throw UnsatisfiedLinkError
function ule(stack, pc, msg){
	const e = _NE(stack, java_lang_UnsatisfiedLinkError, msg);
	const f=stack.peek();
	f.pc=pc;
	if (f.checkETable(e)) {
		return null;
	} else {
		stack.pop();
		return e;
	}
}

//new Exception from classname
async function __NE(stack, name, message){
	const clazz= await rcl.loadClass(name);
	return _NE(stack, clazz, message);
}

//new Exception
function _NE(stack, clazz, message){
	let msg=name;
	if(message)
		msg+=": "+message;
	const ci=new ClassInstance(clazz);
	ci.fields.detailMessage=new JString(msg);
	java_lang_Throwable_fillInStackTrace(stack, ci);
	return ci;
}
//new Error
function _NError(message){
	const ci=new ClassInstance(java_lang_Error);
	ci.fields.detailMessage=new JString(message);
	return ci;
}

