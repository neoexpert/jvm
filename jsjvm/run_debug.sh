#build example project:
mvn -f examples/jvmtest/pom.xml clean install -q &&
#copy the js jvm
cp -r webroot/js/* jvmserver/webroot/js/jvm/ &&
#
cd jvmserver &&
#build jsjvm, for this you need to compile the whole project from root first
mvn clean install -q &&
#run the server
echo "██████████████████████████████"
echo "█                            █"
echo "█  jdwp debugger port: 5005  █"
echo "█                            █"
echo "██████████████████████████████"
java -jar target/jvm-jar-with-dependencies.jar -v -debug -jar ../examples/jvmtest/target/jsjvm_example-jar-with-dependencies.jar
