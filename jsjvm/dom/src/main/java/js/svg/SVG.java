package js.svg;


public class SVG {
    static{
        System.loadLibrary("svg");
    }
    public static native Group createGroup();
    public static native Path createPath();
    public static native Line createLine();
	public static native PolyLine createPolyLine();
    public static native Rect createRect();
    public static native Image createImage();
    public static native Circle createCircle();
    public static native Text createText();
}
