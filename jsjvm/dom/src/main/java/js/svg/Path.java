package js.svg;

import js.JSObject;

public class Path extends SVGElement {
    static{
        System.loadLibrary("svg");
    }
    private Path(){}
    public native JSObject getSegmentList();
}
