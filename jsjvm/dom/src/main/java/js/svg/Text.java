package js.svg;

public class Text extends SVGElement{
	static{
		System.loadLibrary("svg");
	}

	private Text(){}
	public void setText(String text){
		setContent(text);
	}

	public void setTextSize(int size){
		setIntAttribute("font-size",size);
	}
}
