package js;

/**
 * The ByteCodeToJS Compiler will try to inline methods marked with this annotaion with native JavasScript method.
 */
public @interface NativeField{
}
