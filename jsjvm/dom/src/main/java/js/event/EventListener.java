package js.event;

import js.JSEvent;
import js.dom.JSElement;


public abstract class EventListener implements Runnable{
	private boolean stopPropagation;
	protected JSElement element;
	protected JSEvent event;
	public EventListener(){
		stopPropagation=true;
	} 
	public EventListener(boolean stopPropagation){
		this.stopPropagation=stopPropagation;
	} 
	public void setDOMElement(JSElement element){
		this.element=element;
	}
	@Override
	public void run(){
		handle(element,event);
	}
	public abstract void handle(JSElement e, JSEvent event);
}
