package js;

public class JSObject{
    public static final int JSELEMENT = 1;
    public static final int DOMELEMENT=2;
    public static final int SHADOWROOT = 3;
    public static final int SVGELEMENT=4;
    public static final int JSEVENT = 5;
    public static final int JSSTYLE = 6;
    public static final int CANVAS = 7;
    public static final int CONTEXT2D = 8;
    public static final int IMAGE = 9;
    private JSObject(){
        type=0;
    }

    protected JSObject(int type){
        this.type=type;
    }
    public final int type;
    public final native boolean has(String key);
    public final native void set(String key, JSObject value);
    public final native void setString(String key, String value);
    public final native void setInt(String key, int value);
    public final native void setBoolean(String key, boolean value);
    public final native void setFloat(String key, float value);
    public final native JSObject get(String key);
    public final native String getString(String key);
    public final native Integer getInteger(String key);
    public final native int getInt(String key);
    public final native Boolean getBoolean(String key);
    public final native Float getFloat(String key);
    public final native JSObject[] getArray(String key);

    @Override
    public native String toString();

    public final String getClassName(){
			JSObject constructor=get("constructor");
			if(constructor==null) return null;
			return constructor.getString("name");
	}
}
