package js.dom;

public class ImageData extends JSElement {
	private ImageData(){}
	public native int getHeight();
	public native int getWidth();
	public native void setByteArray(byte[] arr);
	public native void setIntArray(int[] arr);
	public native void set(int index, int b);
	public native int get(int index);
}
