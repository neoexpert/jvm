package js.dom;
import js.*;

public class Canvas2D extends DOMElement {
	//will be set natively
	private Context2D context2D;
	public native void init();

	public static class Context2D extends JSObject{
		private Context2D(){
			super(CONTEXT2D);
		}
		public native ImageData createImageData(int width, int height);
		public native ImageData getImageData(int x, int y, int width, int height);
		public native void putImageData(int x, int y, ImageData imageData);
		public native void setFillStyle(String fillStyle);
		public native void setStrokeStyle(String strokeStyle);
		public native void setFont(String font);
		public native void setTextAlign(String textAlign);
		public native void fillText(String text, int x, int y);
		public native void fillRect(int x, int y, int width, int height);
		public native void rect(int x, int y, int width, int height);
		public native void fill();//	Fills the current drawing (path)
		public native void stroke();//	Actually draws the path you have defined
		public native void beginPath();//	Begins a path, or resets the current path
		public native void moveTo(int x, int y);//	Moves the path to the specified point in the canvas, without creating a line
		public native void closePath();//	Creates a path from the current point back to the starting point
		public native void lineTo(int x, int y);//	Adds a new point and creates a line to that point from the last specified point in the canvas
		public native void clip();//	Clips a region of any shape and size from the original canvas
		public native void quadraticCurveTo(int x1, int y1, int x2, int y2);//	Creates a quadratic Bézier curve
		public native void bezierCurveTo(int cp1x, int cp1y,int cp2x,int cp2y,int x,int y);//	Creates a cubic Bézier curve
		public native void arc(int x,int y,int r,float sAngle,float eAngle,boolean counterclockwise);//	Creates an arc/curve (used to create circles, or parts of circles)
		public native void arcTo(int x1,int y1,int x2,int y2,int r);//	Creates an arc/curve between two tangents
		public native boolean isPointInPath(int x, int y);
	}
	public Canvas2D(){
		super(CANVAS);
		init();
	}
	public Context2D getContext2D(){
		return context2D;
	}
	public int getWidth(){
		return getInt("width");
	}
	public int getHeight(){
		return getInt("height");
	}
}
