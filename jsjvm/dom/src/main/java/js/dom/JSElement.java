package js.dom;

import js.event.EventListener;
import js.JSObject;

public class JSElement extends JSObject {

	protected JSElement(){
		super(JSELEMENT);
	}

	public JSElement(int type){
		super(type);
	}

	public final void addEventListener(String event, EventListener listener){
		listener.setDOMElement(this);
		addEventListener0(event, listener);
	}
	public final void addEventListenerPreventDefault(String event, EventListener listener){
		listener.setDOMElement(this);
		addEventListenerPreventDefault0(event, listener);
	}
	private native void addEventListener0(String event, EventListener listener);
	private native void addEventListenerPreventDefault0(String event, EventListener listener);

	public final void removeEventListener(String event, EventListener listener){
		removeEventListener0(event, listener);
	}
	private native void removeEventListener0(String event, EventListener listener);
	public final native DOMCollection getChildren();
	public final native void appendChild(DOMElement child);
	public final native void prependChild(DOMElement child);
	public final native void empty();
	public final native void insertBefore(final DOMElement child, final JSElement before);
	public final native boolean removeChild(DOMElement element);
	public final native void setContent(String content);
	@Override
	public native String toString();
}
