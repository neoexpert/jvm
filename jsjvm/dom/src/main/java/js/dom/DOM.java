package js.dom;

public class DOM{
	private static final boolean initialized;
	static{
		boolean _initialized;
		try{
			System.loadLibrary("dom");
			_initialized=true;
		}
		catch (UnsatisfiedLinkError e){
			_initialized=false;
		}
		initialized=_initialized;
	}
	public static boolean isInitialized(){
		return initialized;
	}
	private DOM(){}
	public static native <T extends DOMElement> T getElementById(String id);

	public static native <T extends DOMElement> T createElement(String name);
	public static native <T extends DOMElement> T htmlToElement(String html);
	public static native DOMElement createElementNS(String namespace, String name);
	public static native ImageData createImageData(int width, int height);

	public static native DOMElement createElement0(String name, Object anc);
	public static native DOMElement createElementNS0(String namespace, String name, Object anc);
	public static native ImageData createImage0(Object anc);


	public static native void defineCustomElement(String name);

	//marks current thread as UI-Thread (creates an eventqueue)
	public static native void registerThread();
	//getNextEvent() returns immediately null
	public static native void unregisterThread(Thread uiThread);

	public static native Runnable getNextEvent();
	public static native void requestAnimationFrame(Runnable listener);
	public static native String prompt(String message, String def);
	public static native void alert(String message);
	public static native void alert(int i);
	public static native void alert(long l);
	public static native boolean confirm(String message);
	public static native int getWidth();
	public static native int getHeight();
	public static native JSElement getWindow();
	public static native JSElement getDocument();
	public static native DOMCollection getElementsByClassName(String popup);
	public static native void replaceState(String url);
	public static native void setQuery(String query);
}
