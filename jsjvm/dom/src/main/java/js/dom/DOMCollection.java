package js.dom;

import js.event.OnDragEvent;
import js.event.OnDropEvent;

import java.util.Iterator;

public class DOMCollection implements Iterable<DOMElement>{
	public final native void setContent(String content);
	public final native void empty();
	public final native void css(String name, String value);
	public final void draggable(OnDragEvent onDragStart, OnDragEvent onDrag, OnDragEvent onDragEnd){
		draggable(onDragStart,onDrag,onDragEnd,null);
	}
	public final native void draggable(OnDragEvent onDragStart, OnDragEvent onDrag,OnDragEvent onDragEnd, String helper);
	public final void draggable(OnDragEvent onDrag){
		draggable(null,onDrag,null,null);
	}
	public final native void setAttribute(String key, String value);
	public final native void setAttribute(String key, int value);
	public final native String getAttribute(String key);
	public final native void addClass(String classname);
	public final native void removeClass(String hidden);
	public final native void click();
	public final native void droppable(OnDropEvent onDropEvent);
	public final native void remove();
	public final native boolean isEmpty();

	public native int size();

	private class Itr implements Iterator<DOMElement>{
		public Itr(){
			init();
		}
		private native void init();
		@Override
		public native boolean hasNext();
		@Override
		public native DOMElement next();
	}

	@Override
	public Iterator<DOMElement> iterator(){
		return new Itr();
	}
}
