package js;

public class Console{
	public native static void log(String msg);
	public native static void warn(String msg);
	public native static void error(String msg);
	public static void log(Object o){
		if(o==null)
			log("null");
		else
			log(o.toString());
	}
	public static void warn(Object o){
		if(o==null)
			warn("null");
		else
			warn(o.toString());
	}

	public static void log(boolean b){
		log(b?"true":"false");
	}

	public static void error(Throwable o){
		if(o==null)
			error("null");
		else
			error(o.toString());
	}
	public native static void debugger();
}
