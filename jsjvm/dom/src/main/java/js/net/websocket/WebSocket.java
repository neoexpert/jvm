package js.net.websocket;


import java.io.IOException;

public class WebSocket{
	static {
		System.loadLibrary("websocket");
	}
	private final String url;
	private final Runnable onOpen;
	private final OnMessage onMessage;
	private final Runnable onClose;


	public WebSocket(String url, Runnable onOpen, OnMessage onMessage, Runnable onClose){
		this.url=url;
		this.onOpen=onOpen;
		this.onMessage=onMessage;
		this.onClose=onClose;
	}
	public final native void connect() throws IOException;
	public final native void send(String message) throws IOException;
	public final native void close() throws IOException;
}
