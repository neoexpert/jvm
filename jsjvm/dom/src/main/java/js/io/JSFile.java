package js.io;

import js.*;
import java.io.*;

public class JSFile extends File{
	private final JSObject jsfile;
	private final String name;
	private final long length;
	public JSFile(JSObject jsfile){
		super(jsfile.getString("name"));
		this.jsfile=jsfile;
		this.name=jsfile.getString("name");
		this.length=jsfile.getInteger("size");
	}
	public String getName(){
		return name;
	}
	public long length(){
		return length;
	}
}

