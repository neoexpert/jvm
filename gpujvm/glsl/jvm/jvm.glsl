#version 430


//in vec2 position;

//flat out int cmd;
//flat out int v1;

layout (local_size_x = 16, local_size_y = 16) in;

int i(int i1,int i2, int i3, int i4){
    return 42;
}

int ii(int i1,int i2){
    return ((((i1) & 0xff) << 8) | ((i2) & 0xff));
}

layout(std430, binding = 2) buffer ColorSSBO {
    int[] code;
};

layout(std430, binding = 3) buffer Mem {
    int[] mem;
};
/**
struct Method{
	int args_length;
	int max_locals;
	int max_stack;
	int[] code;
}
struct Frame{
	int prev_frame_offset;
    int method_info_offset;
	int pc;
	int stackpos;
	int[] lv;
	int[] stack;
}
**/
void main(void){
    //vec2 newpos=position;
    //gl_Position = vec4(newpos,0.0,1.0);
    //frame offet
    int fo=mem[0];
    //method_info_offset
    int mio=mem[fo+1];
    int args_length=code[mio];
    int max_lv=code[mio+1];
    int max_stack=code[mio+2];

    int prevFo=mem[fo];
    int pc=mem[fo+2];
    int stackpos=mem[fo+3];
    //lv offset
    int lvo=fo+4;
    //op stack offset
    int so=lvo+max_stack;
    int i1,i2,i3,i4,oref,aref,o1,o2,o3,f1,f2,l1,l2,d1,d2;
    //v1=pc;
    mem[1023]=pc;
    bool run=true;
    //cmd=1;
    mem[1022]=1;
    int maxsteps=1024;
    while(--maxsteps>0){
        //run=false;
        switch (code[pc]){
            case 0x00:
            //nop
            pc += 1;
            continue;
            case 0x01:
            //aconst_null
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x02:
            //iconst_m1
            mem[stackpos++]=(-1);
            pc += 1;
            continue;
            case 0x03:
            //iconst_0
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x04:
            //iconst_1
            mem[stackpos++]=(1);
            pc += 1;
            continue;
            case 0x05:
            //iconst_2
            mem[stackpos++]=(2);
            pc += 1;
            continue;
            case 0x06:
            //iconst_3
            mem[stackpos++]=(3);
            pc += 1;
            continue;
            case 0x07:
            //iconst_4
            mem[stackpos++]=(4);
            pc += 1;
            continue;
            case 0x08:
            //iconst_5
            mem[stackpos++]=(5);
            pc += 1;
            continue;
            case 0x09:
            //lconst_0
            mem[stackpos++]=0;
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x0A:
            //lconst_1
            mem[stackpos++]=(1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x0B:
            //fconst_0
            mem[stackpos++]=floatBitsToInt(0.0f);
            pc += 1;
            continue;
            case 0x0C:
            //fconst_1
            mem[stackpos++]=floatBitsToInt(1.0f);
            pc += 1;
            continue;
            case 0x0D:
            //fconst_2
            //mem[stackpos++]=(2f);
            pc += 1;
            continue;
            case 0x0E:
            //dconst_0
            //mem[stackpos++]=(0.0);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x0F:
            //dconst_1
            //mem[stackpos++]=(1.0);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x10:
            //bipush
            mem[stackpos++]=code[pc + 1];
            pc += 2;
            continue;
            case 0x11:
            //sipush
            mem[stackpos++]=code[pc + 1];
            pc += 3;
            continue;
            case 0x12:
            //ldc
            //cC.ldc((int) code[pc + 1], cF);
            pc += 2;
            continue;
            case 0x13:
            //ldc_w
            //cC.ldc(ii(code[pc + 1], code[pc + 2]), cF);
            pc += 3;
            continue;
            case 0x14:
            //ldc2_w
            //cC.ldc2(ii(code[pc + 1], code[pc + 2]), cF);
            pc += 3;
            continue;
            case 0x15:
            //iload
            mem[stackpos++]=(mem[lvo+code[pc + 1]]);
            pc += 2;
            continue;
            case 0x16:
            //lload
            mem[stackpos++]=(mem[lvo+ code[pc + 1]]);
            mem[stackpos++]=(mem[lvo+ code[pc + 1] + 1]);
            pc += 2;
            continue;
            case 0x17:
            //fload
            mem[stackpos++]=(mem[lvo+code[pc + 1]]);
            pc += 2;
            continue;
            case 0x18:
            //dload
            mem[stackpos++]=(mem[lvo+ code[pc + 1]]);
            mem[stackpos++]=(mem[lvo+ code[pc + 1] + 1]);
            pc += 2;
            continue;
            case 0x19:
            //aload
            mem[stackpos++]=(mem[lvo+ code[pc + 1]]);
            pc += 2;
            continue;
            case 0x1A:
            //iload_0
            mem[stackpos++]=(mem[lvo]);
            pc += 1;
            continue;
            case 0x1B:
            //iload_1
            mem[stackpos++]=(mem[lvo+1]);
            pc += 1;
            continue;
            case 0x1C:
            //iload_2
            mem[stackpos++]=(mem[lvo+2]);
            pc += 1;
            continue;
            case 0x1D:
            //iload_3";
            mem[stackpos++]=(mem[lvo+3]);
            pc += 1;
            continue;
            case 0x1E:
            //lload_0;
            mem[stackpos++]=(mem[lvo+0]);
            mem[stackpos++]=(mem[lvo+1]);
            pc += 1;
            continue;
            case 0x1F:
            //lload_1
            mem[stackpos++]=(mem[lvo+1]);
            mem[stackpos++]=(mem[lvo+2]);
            pc += 1;
            continue;
            case 0x20:
            //lload_2
            mem[stackpos++]=(mem[lvo+2]);
            mem[stackpos++]=(mem[lvo+3]);
            pc += 1;
            continue;
            case 0x21:
            //lload_3
            mem[stackpos++]=(mem[lvo+3]);
            mem[stackpos++]=(mem[lvo+4]);
            pc += 1;
            continue;
            case 0x22:
            //fload_0
            mem[stackpos++]=(mem[lvo]);
            pc += 1;
            continue;
            case 0x23:
            //fload_1
            mem[stackpos++]=(mem[lvo+1]);
            pc += 1;
            continue;
            case 0x24:
            //fload_2
            mem[stackpos++]=(mem[lvo+2]);
            pc += 1;
            continue;
            case 0x25:
            //fload_3;
            mem[stackpos++]=(mem[lvo+3]);
            pc += 1;
            continue;
            case 0x26:
            //dload_0;
            mem[stackpos++]=(mem[lvo+0]);
            mem[stackpos++]=(mem[lvo+1]);
            pc += 1;
            continue;
            case 0x27:
            //dload_1
            mem[stackpos++]=(mem[lvo+1]);
            mem[stackpos++]=(mem[lvo+2]);
            pc += 1;
            continue;
            case 0x28:
            //dload_2
            mem[stackpos++]=(mem[lvo+2]);
            mem[stackpos++]=(mem[lvo+3]);
            pc += 1;
            continue;
            case 0x29:
            //dload_3
            mem[stackpos++]=(mem[lvo+3]);
            mem[stackpos++]=(mem[lvo+4]);
            pc += 1;
            continue;
            case 0x2A:
            //aload_0
            mem[stackpos++]=(mem[lvo+0]);
            pc += 1;
            continue;
            case 0x2B:
            //aload_1
            mem[stackpos++]=(mem[lvo+1]);
            pc += 1;
            continue;
            case 0x2C:
            //aload_2
            mem[stackpos++]=(mem[lvo+2]);
            pc += 1;
            continue;
            case 0x2D:
            //aload_3
            mem[stackpos++]=(mem[lvo+3]);
            pc += 1;
            continue;
            case 0x2E:
            //iaload
            /*
            i1 = (int) mem[--stackpos];
            aref = (int) stack[--stackpos]();
            array = (ArrayInstance) Heap.get(aref);
            mem[stackpos++]=(((int[]) (array.a))[i1]);
pc += 1;
*/
            continue;
            case 0x2F:
            //laload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
l1 = ((long[]) array.a)[i1];
mem[stackpos++]=(l1);
mem[stackpos++]=(null);
pc += 1;
*/
            continue;
            case 0x30:
            //faload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
mem[stackpos++]=(((float[]) array.a)[i1]);
pc += 1;
*/
            continue;
            case 0x31:
            //daload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
d1 = ((double[]) array.a)[i1];
mem[stackpos++]=(d1);
mem[stackpos++]=(null);
pc += 1;
*/
            continue;
            case 0x32:
            //aaload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
mem[stackpos++]=(((int[]) array.a)[i1]);
pc += 1;
*/
            continue;
            case 0x33:
            //baload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
mem[stackpos++]=((int) ((byte[]) array.a)[i1]);
pc += 1;
*/
            continue;
            case 0x34:
            //caload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
mem[stackpos++]=((int) ((char[]) array.a)[i1]);
pc += 1;
*/
            continue;
            case 0x35:
            //saload
            /*
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
mem[stackpos++]=((int) ((short[]) array.a)[i1]);
*/
            pc += 1;
            continue;
            case 0x36:
            //istore
            mem[lvo+code[pc + 1]] = mem[--stackpos];
            pc += 2;
            continue;
            case 0x37:
            //lstore
            //mem[lvo+[(int) code[pc + 1] + 1] = mem[--stackpos];
            //mem[lvo+[(int) code[pc + 1]] = mem[--stackpos];
            pc += 2;
            continue;
            case 0x38:
            //fstore
            //mem[lvo+[(int) code[pc + 1]] = mem[--stackpos];
            pc += 2;
            continue;
            case 0x39:
            //dstore
            //mem[lvo+[(int) code[pc + 1] + 1] = mem[--stackpos];
            //mem[lvo+[(int) code[pc + 1]] = mem[--stackpos];
            pc += 2;
            continue;
            case 0x3A:
            //astore
            //oref = (int) mem[--stackpos];
            //mem[lvo+[code[pc + 1]] = oref;
            //cF.mem[lvo+pt[code[pc + 1]] = oref;
            pc += 2;
            continue;
            case 0x3B:
            //istore_0
            mem[lvo+0] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x3C:
            //istore_1
            mem[lvo+1] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x3D:
            //istore_2
            mem[lvo+2] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x3E:
            //istore_3
            mem[lvo+3] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x3F:
            //lstore_0
            mem[lvo+1] = mem[--stackpos];
            mem[lvo+0] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x40:
            //lstore_1
            mem[lvo+2] = mem[--stackpos];
            mem[lvo+1] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x41:
            //store_2
            mem[lvo+3] = mem[--stackpos];
            mem[lvo+2] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x42:
            //lstore_3
            mem[lvo+4] = mem[--stackpos];
            mem[lvo+3] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x43:
            //fstore_0
            mem[lvo+0] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x44:
            //fstore_1
            mem[lvo+1] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x45:
            //fstore_2
            mem[lvo+2] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x46:
            //fstore_3"
            mem[lvo+3] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x47:
            //dstore_0
            mem[lvo+1] = mem[--stackpos];
            mem[lvo+0] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x48:
            //dstore_1
            mem[lvo+2] = mem[--stackpos];
            mem[lvo+1] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x49:
            //dstore_2
            mem[lvo+3] = mem[--stackpos];
            mem[lvo+2] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x4A:
            //dstore_3
            mem[lvo+4] = mem[--stackpos];
            mem[lvo+3] = mem[--stackpos];
            pc += 1;
            continue;
            case 0x4B:
            //astore_0
            mem[lvo+0] = mem[--stackpos];
            //cF.lvpt[0]= (int) lv[0];
            pc += 1;
            continue;
            case 0x4C:
            //astore_1
            mem[lvo+1] = mem[--stackpos];
            //cF.lvpt[1]= (int) lv[1];
            pc += 1;
            continue;
            case 0x4D:
            //astore_2
            mem[lvo+2] = mem[--stackpos];
            //cF.lvpt[2] = (int) lv[2];
            pc += 1;
            continue;
            case 0x4E:
            //astore_3
            mem[lvo+3] = mem[--stackpos];
            //cF.lvpt[3]= (int) lv[3];
            pc += 1;
            continue;
            case 0x4F:
            //iastore
            i1 =  mem[--stackpos];
            i2 =  mem[--stackpos];
            aref =  mem[--stackpos];
            //array = (ArrayInstance) Heap.get(aref);
            //((int[]) array.a)[i2] = i1;
            pc += 1;
            continue;
            case 0x50:
            //lastore
            /*
stack[--stackpos];
l1 = (long) mem[--stackpos];
i3 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((long[]) array.a)[i3] = l1;
*/
            pc += 1;
            continue;
            case 0x51:
            //fastore
            /*
f1 = (float) mem[--stackpos];
i1 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((float[]) array.a)[i1] = f1;
pc += 1;
*/
            continue;
            case 0x52:
            //dastore
            /*
mem[--stackpos];
d1 = (double) mem[--stackpos];
i3 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((double[]) array.a)[i3] = d1;
pc += 1;
*/
            continue;
            case 0x53:
            //aastore
            /*
i1 = (int) mem[--stackpos];
i2 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((int[]) array.a)[i2] = i1;
pc += 1;
*/
            continue;
            case 0x54:
            //bastore
            /*
i1 = (int) mem[--stackpos];
i2 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((byte[]) array.a)[i2] = (byte) i1;
pc += 1;
*/
            //throw new RuntimeException("implement me!");
            continue;
            case 0x55:
            //castore
            /*
i1 = (int) mem[--stackpos];
i2 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((char[]) array.a)[i2] = (char) i1;
pc += 1;
*/
            continue;
            case 0x56:
            //sastore
            /*
i1 = (int) mem[--stackpos];
i2 = (int) mem[--stackpos];
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
((short[]) array.a)[i2] = (short) i1;
pc += 1;
*/
            continue;
            case 0x57:
            //pop
            mem[--stackpos];
            pc += 1;
            continue;
            case 0x58:
            //pop2
            mem[--stackpos];
            mem[--stackpos];
            pc += 1;
            continue;
            case 0x59:
            //dup
            mem[stackpos++]=mem[stackpos];
            pc += 1;
            continue;
            case 0x5A:
            //dup_x1
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i1);
            mem[stackpos++]=(i2);
            mem[stackpos++]=(i1);
            pc += 1;
            continue;
            case 0x5B:
            //dup_x2
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            i3 = mem[--stackpos];
            mem[stackpos++]=(i1);
            mem[stackpos++]=(i3);
            mem[stackpos++]=(i2);
            mem[stackpos++]=(i1);
            pc += 1;
            continue;
            case 0x5C:
            //dup2
            o2 = mem[--stackpos];
            o1 = mem[--stackpos];
            mem[stackpos++]=(o1);
            mem[stackpos++]=(o2);
            mem[stackpos++]=(o1);
            mem[stackpos++]=(o2);
            pc += 1;
            continue;
            case 0x5D:
            //dup2_x1
            o2 = mem[--stackpos];
            o1 = mem[--stackpos];
            o3 = mem[--stackpos];
            mem[stackpos++]=(o1);
            mem[stackpos++]=(o2);
            mem[stackpos++]=(o3);
            mem[stackpos++]=(o1);
            mem[stackpos++]=(o2);
            pc += 1;
            continue;
            case 0x5E:
            //dup2_x2
            i2 = mem[--stackpos];
            i1 = mem[--stackpos];
            i4 = mem[--stackpos];
            i3 = mem[--stackpos];
            mem[stackpos++]=(i1);
            mem[stackpos++]=(i2);
            mem[stackpos++]=(i3);
            mem[stackpos++]=(i4);
            mem[stackpos++]=(i1);
            mem[stackpos++]=(i2);
            pc += 1;
            continue;
            case 0x5F:
            //swap
            mem[stackpos++]=(mem[stackpos]);
            pc += 1;
            continue;
            case 0x60:
            //iadd
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i1 + i2);
            pc += 1;
            continue;
            case 0x61:
            //ladd
            /*
mem[--stackpos];
l1 = (long) mem[--stackpos];
mem[--stackpos];
l2 = (long) mem[--stackpos];
l1 = l2 + l1;
mem[stackpos++]=(l1);
mem[stackpos++]=(null);
pc += 1;
*/
            continue;
            case 0x62:
            //fadd
            /*
f1 = (float) mem[--stackpos];
f2 = (float) mem[--stackpos];
mem[stackpos++]=(f1 + f2);
pc += 1;
*/
            continue;
            case 0x63:
            //dadd
            /*
mem[--stackpos];
d1 = (double) mem[--stackpos];
mem[--stackpos];
d2 = (double) mem[--stackpos];
d1 = d2 + d1;
mem[stackpos++]=(d1);
mem[stackpos++]=(null);
*/
            pc += 1;
            continue;
            case 0x64:
            //isub
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 - i1);
            pc += 1;
            continue;
            case 0x65:
            //lsub
            /*
mem[--stackpos];
l1 = (long) mem[--stackpos];
mem[--stackpos];
l2 = (long) mem[--stackpos];
l1 = l2 - l1;
mem[stackpos++]=(l1);
mem[stackpos++]=(null);
pc += 1;
*/
            continue;
            case 0x66:
            //fsub
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            mem[stackpos++]=(f2 - f1);
            pc += 1;
            continue;
            case 0x67:
            //dsub
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            d1 = d2 - d1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x68:
            //imul
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 * i1);
            pc += 1;
            continue;
            case 0x69:
            //lmul
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l1 * l2;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x6A:
            //fmul
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            mem[stackpos++]=(f2 * f1);
            pc += 1;
            continue;
            case 0x6B:
            //dmul
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            d1 = d2 * d1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x6C:
            //idiv
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 / i1);
            pc += 1;
            continue;
            case 0x6D:
            //ldiv
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 / l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x6E:
            //fdiv
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            mem[stackpos++]=(f2 / f1);
            pc += 1;
            continue;
            case 0x6F:
            //ddiv
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            d1 = d2 / d1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x70:
            //irem
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 % i1);
            pc += 1;
            continue;
            case 0x71:
            //lrem
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 % l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x72:
            //frem
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            mem[stackpos++]=(f2 % f1);
            pc += 1;
            continue;
            case 0x73:
            //drem
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            d1 = d2 % d1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x74:
            //ineg
            mem[stackpos++]=(-mem[--stackpos]);
            pc += 1;
            continue;
            case 0x75:
            //lneg
            mem[--stackpos];
            l1 = mem[--stackpos];
            l1 = -l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x76:
            //fneg
            mem[stackpos++]=(-mem[--stackpos]);
            pc += 1;
            continue;
            case 0x77:
            //dneg
            mem[--stackpos];
            d1 = mem[--stackpos];
            d1 = -d1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x78:
            //ishl
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 << i1);
            pc += 1;
            continue;
            case 0x79:
            //lshl
            i1 = mem[--stackpos];
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[stackpos++]=(l1<<i1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x7A:
            //ishr
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 >> i1);
            pc += 1;
            continue;
            case 0x7B:
            //lshr
            i1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 >> i1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x7C:
            //iushr
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            //mem[stackpos++]=(i2 >>> i1);
            pc += 1;
            continue;
            case 0x7D:
            //lushr
            i1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            //l1 = l2 >>> i1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x7E:
            //iand
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 & i1);
            pc += 1;
            continue;
            case 0x7F:
            //land
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 & l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x80:
            //ior
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 | i1);
            pc += 1;
            continue;
            case 0x81:
            //lor
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 | l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x82:
            //ixor
            i1 = mem[--stackpos];
            i2 = mem[--stackpos];
            mem[stackpos++]=(i2 ^ i1);
            pc += 1;
            continue;
            case 0x83:
            //lxor
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            l1 = l2 ^ l1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x84:
            //iinc
            i1 = mem[lvo+code[pc + 1]];
            mem[lvo+code[pc + 1]] = i1 + code[pc + 2];
            pc += 3;
            continue;
            case 0x85:
            //i2l
            i1= mem[--stackpos];
            l1=i1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=(0);
            pc += 1;
            continue;
            case 0x86:
            //i2f
            mem[stackpos++]=(mem[--stackpos]);
            pc += 1;
            continue;
            case 0x87:
            //i2d
            i1 = mem[--stackpos];
            mem[stackpos++]=(i1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x88:
            //l2i
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[stackpos++]=(l1);
            pc += 1;
            continue;
            case 0x89:
            //l2f
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[stackpos++]=(l1);
            pc += 1;
            continue;
            case 0x8A:
            //l2d
            mem[--stackpos];
            l1 = mem[--stackpos];
            d1 = l1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x8B:
            //f2i
            mem[stackpos++]=mem[--stackpos];
            pc += 1;
            continue;
            case 0x8C:
            //f2l
            f1 = mem[--stackpos];
            l1 = f1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x8D:
            //f2d
            f1 = mem[--stackpos];
            d1 = f1;
            mem[stackpos++]=(d1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x8E:
            //d2i
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[stackpos++]=(d1);
            pc += 1;
            continue;
            case 0x8F:
            //d2l
            mem[--stackpos];
            d1 = mem[--stackpos];
            l1 = d1;
            mem[stackpos++]=(l1);
            mem[stackpos++]=0;
            pc += 1;
            continue;
            case 0x90:
            //d2f
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[stackpos++]=(d1);
            pc += 1;
            continue;
            case 0x91:
            //i2b
            mem[stackpos++]=(mem[--stackpos]);
            pc += 1;
            continue;
            case 0x92:
            //i2c
            mem[stackpos++]=(mem[--stackpos]);
            pc += 1;
            continue;
            case 0x93:
            //i2s
            mem[stackpos++]=(mem[--stackpos]);
            pc += 1;
            continue;
            case 0x94:
            //lcmp
            mem[--stackpos];
            l1 = mem[--stackpos];
            mem[--stackpos];
            l2 = mem[--stackpos];
            if (l2 == l1) {
                mem[stackpos++]=(0);
            } else if (l2 > l1) {
                mem[stackpos++]=(1);
            } else
            mem[stackpos++]=(-1);
            pc += 1;
            continue;
            case 0x95:
            //fcmpl
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            if (f2 == f1) {
                mem[stackpos++]=(0);
            } else if (f2 > f1) {
                mem[stackpos++]=(1);
            } else if (f2 < f1)
            mem[stackpos++]=(-1);
            else
            mem[stackpos++]=(-1);
            pc += 1;
            continue;
            case 0x96:
            //fcmpg
            f1 = mem[--stackpos];
            f2 = mem[--stackpos];
            if (f2 == f1) {
                mem[stackpos++]=(0);
            } else if (f2 > f1) {
                mem[stackpos++]=(1);
            } else if (f2 < f1)
            mem[stackpos++]=(-1);
            else
            mem[stackpos++]=(1);
            pc += 1;
            continue;
            case 0x97:
            //dcmpl
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            if (d2 == d1) {
                mem[stackpos++]=(0);
            } else if (d2 > d1) {
                mem[stackpos++]=(1);
            } else if (d2 < d1)
            mem[stackpos++]=(-1);
            else
            mem[stackpos++]=(-1);
            pc += 1;
            continue;
            case 0x98:
            //dcmpg
            mem[--stackpos];
            d1 = mem[--stackpos];
            mem[--stackpos];
            d2 = mem[--stackpos];
            if (d2 == d1) {
                mem[stackpos++]=(0);
            } else if (d2 > d1) {
                mem[stackpos++]=(1);
            } else if (d2 < d1)
            mem[stackpos++]=(-1);
            else
            mem[stackpos++]=(1);
            pc += 1;
            continue;
            case 0x99:
            //ifeq
            if (mem[--stackpos] == 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9A:
            //ifne
            if (mem[--stackpos] != 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9B:
            //iflt
            if (mem[--stackpos] < 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9C:
            //ifge
            if (mem[--stackpos] >= 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9D:
            //ifgt
            if (mem[--stackpos] > 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9E:
            //ifle
            if (mem[--stackpos] <= 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0x9F:
            //if_icmpeq
            if (mem[--stackpos] == mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA0:
            //if_icmpne
            if (mem[--stackpos] != mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA1:
            //if_icmplt
            if (mem[--stackpos] >mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA2:
            //if_icmpge
            if (mem[--stackpos] <= mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA3:
            //if_icmpgt
            if (mem[--stackpos] < mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA4:
            //if_icmple
            if (mem[--stackpos] >= mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA5:
            //if_acmpeq
            if (mem[--stackpos] == mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA6:
            //if_acmpne
            if (mem[--stackpos] != mem[--stackpos])
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xA7:
            //goto
            pc += code[pc + 1];
            continue;
            case 0xA8:
            //jsr
            mem[stackpos++]=(pc + 3);
            continue;
            case 0xA9:
            //ret
            pc =mem[lvo+code[pc + 1]];
            continue;
            case 0xAA:
            //tableswitch
            i1 = mem[--stackpos];
            int pc2 = pc + (4 - pc % 4);
            int dfp=pc2;

            pc2 += 4;
            //i(code[pc2], code[++pc2], code[++pc2], code[++pc2]);
            //pc2 += 4;
            int low = ((code[pc2]) << 24) | ((code[++pc2]) << 16) |((code[++pc2]) << 8) | (code[++pc2]);
            //pc2 += 4;
            int high = ((code[++pc2]) << 24) | ((code[++pc2]) << 16) |((code[++pc2]) << 8) | (code[++pc2]);
            //pc2 += 4;
            ++pc2;
            if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
                pc += ((code[dfp]) << 24) | ((code[++dfp]) << 16) |((code[++dfp]) << 8) | (code[++dfp]);
                // branch to default
            } else {
                pc2 += (i1 - low) * 4;
                pc += ((code[pc2]) << 24) | ((code[++pc2]) << 16) |((code[++pc2]) << 8) | (code[++pc2]);
            }
            continue;
            case 0xAB:
            //lookupswitch
            i1 = mem[--stackpos];
            pc2 = pc + (4 - pc % 4);
            int default_offset = i(code[pc2], code[++pc2], code[++pc2], code[++pc2]);
            //pc2 += 4;
            i2 = i(code[++pc2], code[++pc2], code[++pc2], code[++pc2]);
            //pc2 += 4;
            for (i3 = 0; i3 < i2; ++i3) {
                int key = i(code[++pc2], code[++pc2], code[++pc2], code[++pc2]);

                if (key == i1) {
                    //pc2 += 4;
                    int value = i(code[++pc2], code[++pc2], code[++pc2], code[++pc2]);
                    pc += value;
                    continue;
                }
                else
                pc2 += 4;
            }
            pc += default_offset;
            continue;
            case 0xAC:
            //ireturn
            i1 = mem[--stackpos];
            pc += 1;
            /*
last_frame = mem.pop();
if (mem.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = mem.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
//System.out.println("ireturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
mem[stackpos++]=(i1);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xAD:
            //lreturn
            /*
mem[--stackpos];
l1 = (long) mem[--stackpos];
pc += 1;
last_frame = mem.pop();
if (stack.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = stack.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
mem[stackpos++]=(l1);
mem[stackpos++]=(null);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xAE:
            //freturn
            /*
o1 = mem[--stackpos];
pc += 1;
last_frame = mem.pop();
if (stack.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = stack.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
mem[stackpos++]=(o1);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xAF:
            //dreturn
            /*
mem[--stackpos];
d1= (double) mem[--stackpos];
pc += 1;
last_frame = stack.pop();
if (stack.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = stack.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
//System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
mem[stackpos++]=(d1);
mem[stackpos++]=(null);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xB0:
            //areturn
            /*
i1 = (int) mem[--stackpos];
pc += 1;
last_frame = stack.pop();
if (stack.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = stack.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
//System.out.println("areturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
mem[stackpos++]=(i1);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xB1:
            //_return
            //v1=fo;
            fo=mem[fo];
            pc=mem[fo+2];
            //cmd=0xB1;
            mem[1022]=0xB1;
            mem[0]=fo;
            if(fo==0){
                //cmd=0;
                mem[1022]=0;
                maxsteps=-1;
                return;
            }
            //v1=0;
            //color++;
            //pc += 1;
            /*
last_frame = stack.pop();
if (stack.isEmpty()) {
    last_frame.pc=pc;
    //thread.destroy();
    return 1;
}
cF = stack.peek();
pc = cF.pc;
lv = cF.lv;
cC = cF.getMyClass();
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
//System.out.println("return: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
*/
            continue;
            case 0xB2:
            //getstatic
            mem[stackpos++]=mem[512];
            /*
fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
//if(JVM.verbose)
o1=Class.getClass((String)fc.value).getStatic(fc.str);
if(o1==null)
mem[stackpos++]=(0);
else
mem[stackpos++]=(o1);
if(fc.index1==-2) {
    mem[stackpos++]=(0);
}
*/
            pc += 3;
            continue;
            case 0xB3:
            //putstatic
            o1 = mem[--stackpos];
            mem[512]=o1;
            /*
fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
if(fc.index1==-2) {
    o1 = mem[--stackpos];
}
// int addrr=(code[i+1] << 8) | code[i+2];
Class.getClass((String)fc.value).putStatic(fc.str, o1);
*/
            pc += 3;
            continue;
            case 0xB4:
            //getfield
            /*
oref = (int) mem[--stackpos];
o1 = Heap.get(oref);
fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
//fc = cC.getConstant(fc.index2);
//fname = cC.getConstant(fc.index1);
//ftype = cC.getConstant(fc.index2);
if (o1 != null)
o1 = ((ClassInstance) o1).getField(fc.str);
else o1 = 0;
mem[stackpos++]=(o1);
if(fc.index1==-2){
    mem[stackpos++]=(null);
}
pc += 3;
*/
            continue;
            case 0xB5:
            //putfield
            o1 = mem[--stackpos];
            /*
fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
if(fc.index1==-2) {
    o1 = mem[--stackpos];
}
Object o = mem[--stackpos];
oref = (int) o;
c = (ClassInstance) Heap.get(oref);
if (c == null) {
    JVM.log("field " + fc.str + " reference: " + oref);
thread.printStackTrace();

throwNewException("java/lang/NullPointerException",pc,"putfield: "+fc.str);
cF=stack.peek();
pc=cF.pc;
code = cF.code;
lv = cF.lv;
continue;
}

c.putField(fc.str, o1);
pc += 3;
*/
            continue;
            case 0xB6:
            /*
//invokevirtual
// int objref=(int)mem[--stackpos];

m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
pc += 3;
cF.pc = pc;
//System.out.println("invokevirtual: " + m.getMyClass().getName() + "." + m.getSignature());
//Frame nf = cF.newRefFrame(m);
Frame nf = cF.newRefFrame((Method) m);
oref = (int) nf.lv[0];
//if (m.isAbstract())
if(oref==0){
    throwNewException("java/lang/NullPointerException",pc, "invokevirtual: "+m.signature);
cF=mem.peek();
pc=cF.pc;
code = cF.code;
lv = cF.lv;
continue;
}

cl = ((Instance) Heap.get(oref)).getMyClass();
String mname = m.signature;
m = cl.getMethod(mname);
if (m.isNative()) {
    if(jni.invoke(m.getNativeDesc(), cF, nf.lv,thread))
    return 1;
    cF = stack.peek();
    if(cF.code_type!= Method.BYTE_CODE)
    return cF.code_type;
    pc = cF.pc;
    code = cF.code;
    lv = cF.lv;

    continue;
}
if (m == null)
throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
nf.initLocals((Method) m);
nf.setMethod((Method) m);

cF = nf;
pc = cF.pc;
lv = cF.lv;

cC = m.getMyClass();
stack.push(cF);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
*/
            continue;
            case 0xB7:
            //invokespecial

            /*
m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
pc += 3;
cF.pc = pc;

nf = cF.newRefFrame((Method) m);
Heap.objectCreated((int) nf.lv[0]);
if (m.isNative()) {
    if(jni.invoke(m.getNativeDesc(), cF, lv,thread))
    return 1;
    cF = stack.peek();
    if(cF.code_type!= Method.BYTE_CODE)
    return cF.code_type;
    pc = cF.pc;
    code = cF.code;
    lv = cF.lv;
    continue;
}
cF = nf;
pc = cF.pc;
lv = cF.lv;
cC = m.getMyClass();
stack.push(cF);
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;

continue;
*/
            case 0xB8:
            //invokestatic
            //cmd=3;
            mem[1022]=3;
            //v1=ii(code[pc + 1], code[pc + 2]);
            mio=code[pc + 1];
            int newfo=fo+max_lv+max_stack+4;
            pc += 3;
            //cF.pc=pc
            mem[newfo+1]=mio;
            mem[fo+2]=pc;
            args_length=code[mio];
            for(int i=0;i<args_length;++i){
                mem[newfo+4+i]=mem[--stackpos];
            }
            mem[fo+3]=stackpos;
            max_lv=code[mio+1];
            if(max_lv==-1){
                //native method
                //cmd=4;
                mem[1022]=4;
                //v1=newfo;
                mem[1023]=newfo;
                run=false;
                maxsteps=-1;
                break;
            }
            mem[newfo]=fo;
            fo=newfo;
            max_stack=code[mio+2];
            pc=mio+3;
            mem[newfo+2]=pc;
            mem[0]=newfo;
            stackpos=fo+4+max_lv;
            //code[0]=pc;
            /*
m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
cF.pc = pc;

nf = cF.newFrame((Method) m);
if (m.isNative()) {
    if(jni.invoke(m.getNativeDesc(), cF, nf.lv,thread))
    return 1;
    cF = stack.peek();
    if(cF.code_type!= Method.BYTE_CODE)
    return cF.code_type;
    pc = cF.pc;
    code = cF.code;
    lv = cF.lv;
    continue;
}
cF = nf;
stack.push(cF);
cC = m.getMyClass();
if(cF.code_type!= Method.BYTE_CODE)
return cF.code_type;
code = cF.code;
pc = cF.pc;
lv = cF.lv;
*/
            continue;
            case 0xB9:
            //invokeinterface
            /*
            m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
            //System.out.println("invokeinterface: " + m.getMyClass().getName() + "." + m.getSignature());

            short count = code[pc + 3];
            short zero = code[pc + 4];
            pc += 5;
            cF.pc = pc;
            //if(count>1) {
            nf = new Frame((Method) m);

            Object[] mlv = new Object[count];
            for (int ix = 0; ix < count; ix++) {
                //nf.getLv().put(p - ix - 1, mem[--stackpos]);
                mlv[count - ix - 1] = mem[--stackpos];
            }

            //}
            oref = (int) mlv[0];

            ClassInstance ci = (ClassInstance) Heap.get(oref);
            if(ci==null) {
                throwNewException("java/lang/NullPointerException",pc, "invokeinterface: "+m.signature);
            cF=stack.peek();
            pc=cF.pc;
            code = cF.code;
            lv = cF.lv;
            continue;
            }
            AClass mc = ci.getMyClass();
            m = mc.getMethod(m.signature);


            if (m.isNative()) {
                //m.invoke(cF.stack, this);
                throw new RuntimeException("check.that");
            //continue;
            }
            nf.lv = new Object[((Method)m).getMaxLocals()];
            nf.lvpt = new int[((Method)m).getMaxLocals()];

            char[] args=m.args;
            for (int j = 0; j < mlv.length; ++j) {
                if(j>0)
                if(args[j-1]=='L')
            nf.lvpt[j]= (int) mlv[j];
            nf.lv[j] = mlv[j];
            }

            nf.setMethod((Method) m);
            cF = nf;
            pc = cF.pc;
            lv = cF.lv;

            cC = m.getMyClass();
            stack.push(cF);
            if(cF.code_type!= Method.BYTE_CODE)
            return cF.code_type;
            code = cF.code;
            */
            continue;
            case 0xBA:
            /*
//invokedynamic (very complex instruction)
i1=ii(code[pc + 1], code[pc + 2]);
throw new RuntimeException("invokedynamic not implemented");
*/
            continue;
            case 0xBB:
            //new

            /*
c = new ClassInstance(cC.getClass(i(code[pc + 1], code[pc + 2])));
// cC.getClass(i(code[pc + 1], code[pc + 2]));
int a = Heap.add(c);
Heap.newObject(a);
mem[stackpos++]=(a);
*/
            pc += 3;
            continue;
            case 0xBC:
            //newarray
            /*
i1 = (int) mem[--stackpos];
array = new ArrayInstance(i1, (byte)code[pc + 1]);
aref = Heap.add(array);
mem[stackpos++]=(aref);
*/
            pc += 2;
            continue;
            case 0xBD:
            //anewarray
            /*
i1 = (int) mem[--stackpos];
array = new ArrayInstance(i1, ArrayInstance.T_OBJECT);
aref = Heap.add(array);
mem[stackpos++]=(aref);
*/
            pc += 3;
            continue;
            case 0xBE:
            //arraylength
            /*
aref = (int) mem[--stackpos];
array = (ArrayInstance) Heap.get(aref);
if(array==null)
thread.printStackTrace();
mem[stackpos++]=(array.getLength());
*/
            pc += 1;
            continue;
            case 0xBF:
            //athrow
            /*
oref = (int) mem[--stackpos];
throwException(pc,oref);
cF=stack.peek();
if(cF.code_type!= Method.OPTIMIZED_BYTE_CODE)
return cF.code_type;
code = cF.code;
lv = cF.lv;

*/
            continue;
            case 0xC0:
            //checkcast
            /*
fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
fc = cC.getConstant(fc.index1);
if (!fc.str.startsWith("[")) {
AClass cl2 = Class.getClass(fc.str);
i1 = (int) cF.peek();
o1 = Heap.get(i1);

if (o1 instanceof ClassInstance) {
cl = ((ClassInstance) o1).getMyClass();
if (!cl.isInstance(cl2)) {
    //ClassCastException
    throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
}
}
}
*/
            pc += 3;
            continue;
            case 0xC1:
            //instanceof
            /*
i1 = (int) mem[--stackpos];
if (i1 == 0) {
    mem[stackpos++]=(0);
    pc += 3;
    continue;
}
o1 = Heap.get(i1);
i1 = ii(code[pc + 1], code[pc + 2]);
AClass cl2 = cC.getClass(i1);
cl = ((ClassInstance) o1).getMyClass();
if (cl.isInstance(cl2))
stack[stackpos++]=(1);
else
mem[stackpos++]=(0);
*/
            pc += 3;
            continue;
            case 0xC2:
            //monitorenter
            /*
oref = (int) mem[--stackpos];
Instance i=(Instance)Heap.get(oref);
if(i.monitorenter(thread))
{
    thread._wait();
    return 1;
}
*/
            pc += 1;
            continue;
            case 0xC3:
            //monitorexit
            /*
oref = (int) mem[--stackpos];
Instance _i=(Instance)Heap.get(oref);
MyThread next=_i.monitorexit(thread);
if(next!=null)
JVM.notify(next);
*/
            pc += 1;
            continue;

            case 0xC4:
            //wide
            pc += 1;
            switch (code[pc]) {
                case 0x15:
                //iload
                mem[stackpos++]=(mem[lvo+ii(code[pc + 1], code[pc + 2])]);
                pc += 3;
                continue;
                case 0x16:
                //lload
                i1 = ii(code[pc + 1], code[pc + 2]);
                mem[stackpos++]=(mem[lvo+i1 + 1]);
                mem[stackpos++]=(mem[lvo+i1]);
                pc += 3;
                continue;
                case 0x17:
                //fload
                mem[stackpos++]=(mem[lvo+ii(code[pc + 1], code[pc + 2])]);
                pc += 3;
                continue;
                case 0x18:
                //dload
                i1 = ii(code[pc + 1], code[pc + 2]);
                mem[stackpos++]=(mem[lvo+i1 + 1]);
                mem[stackpos++]=(mem[lvo+i1]);
                pc += 3;
                continue;
                case 0x19:
                //aload
                mem[stackpos++]=(mem[lvo+ii(code[pc + 1], code[pc + 2])]);
                pc += 3;
                continue;
                case 0x36:
                //istore
                mem[lvo+ii(code[pc + 1], code[pc + 2])] = mem[--stackpos];
                pc += 3;
                continue;
                case 0x37:
                //lstore
                i1 = ii(code[pc + 1], code[pc + 2]);
                mem[lvo+i1] = mem[--stackpos];
                mem[lvo+i1 + 1] = mem[--stackpos];
                pc += 3;
                continue;
                case 0x38:
                //fstore
                mem[lvo+ii(code[pc + 1], code[pc + 2])] = mem[--stackpos];
                pc += 3;
                continue;
                case 0x39:
                //dstore
                i1 = ii(code[pc + 1], code[pc + 2]);
                mem[lvo+i1] = mem[--stackpos];
                mem[lvo+i1 + 1] = mem[--stackpos];
                pc += 3;
                continue;
                case 0x3A:
                //astore
                oref = mem[--stackpos];
                mem[lvo+ii(code[pc + 1], code[pc + 2])] = oref;
                //cF.lvpt[ii(code[pc + 1], code[pc + 2])] = oref;
                pc += 3;
                continue;
                case 0x84:
                //iinc
                i1 = mem[lvo+code[pc + 1]];
                mem[lvo+code[pc + 1]] = i1 + code[pc + 2];
                pc += 5;
                continue;

            }
            continue;
            case 0xC5:
            //multianewarray
            /*
 i1 = i(code[pc + 1], code[pc + 2]);
 //dimensions
 i2 = code[pc + 3];
 int[] sizes=new int[i2];
 for(i3=0;i3<i2;i3++) {
     sizes[i3]= (int) mem[--stackpos];
 }
 ArrayInstance ai=new ArrayInstance(i2,0,sizes,ArrayInstance.T_OBJECT);
 aref=Heap.add(ai);
 mem[stackpos++]=(aref);
 */
            pc += 4;
            continue;
            case 0xC6:
            //ifnull
            if (mem[--stackpos] == 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xC7:
            //ifnonnull
            if (mem[--stackpos] != 0)
            pc += code[pc + 1];
            else
            pc += 3;
            continue;
            case 0xC8:
            //goto
            //pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
            continue;
            case 0xC9:
            //jsr_w
            mem[stackpos++]=(pc + 5);
            //pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
            continue;
            default:
            pc += 1;
            //throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
            // cs+= byteToHex(code[i])+"\n";

        }
    }
    mem[fo+2]=pc;
    mem[fo+3]=stackpos;
}
