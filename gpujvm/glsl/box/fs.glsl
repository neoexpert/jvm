#version 150 core

in vec4 vertexColor;
out vec4 fragColor;
uniform mat4 view;
uniform sampler2D font_texture;
in vec2 pass_TextureCoord;

void main() {
    //fragColor = vec4(vertexColor, 1.0);
	//fragColor = texture(font_texture, pass_TextureCoord);
	vec4 textureColor = texture(font_texture, pass_TextureCoord );
    	fragColor = vertexColor * textureColor;
}
