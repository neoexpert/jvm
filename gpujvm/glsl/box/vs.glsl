#version 150 core

in vec3 position;
in vec4 color;
in vec2 texcoord;


out vec4 vertexColor;
out vec2 pass_TextureCoord;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec2 shift;

void main() {
	vertexColor = color;
	pass_TextureCoord = texcoord;

	mat4 mvp = projection;
	vec3 newpos=position;
	newpos.x+=shift.x;
	newpos.y+=shift.y;
	gl_Position = mvp * vec4(newpos, 1.0);
}
