package gl;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.classpath.DirClassPath;
import com.cupvm.jvm.classpath.JarClassPath;
import com.cupvm.jvm.method.Method;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.MemoryStack;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnableClientState;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31C.glGetUniformBlockIndex;
import static org.lwjgl.opengl.GL31C.glUniformBlockBinding;
import static org.lwjgl.opengl.GL42C.glMemoryBarrier;
import static org.lwjgl.opengl.GL43.*;
import static org.lwjgl.opengl.GL43C.GL_SHADER_STORAGE_BUFFER;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;
import com.cupvm.jvm._class.Class;


public class GLExecutor {
    // The window handle
    private long window;
    private boolean mousedown=false;
    private int width=1024;
    private int height=768;
    private int shaderProgram;
    private int uniProjection;
    public static int fbtexture;
    private AClass mainClass;
    private boolean dumpmem=false;

    public void run() {
        try{
            ClassLoader.addToClasspath(new JarClassPath(new File("./minijdk/target/minijdk.jar")));

        }catch(IOException e){
            throw new RuntimeException(e);
        }
        ClassLoader.addToClasspath(new DirClassPath(new File("./gpujvm")));
        init();

        GLUtil.setupDebugMessageCallback();
        loop();

        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(new GLFWErrorCallback() {
            @Override
            public void invoke(int error, long description) {
                System.err.println("GLFW error [" + Integer.toHexString(error) + "]: " + GLFWErrorCallback.getDescription(description));
            }
        });
    }

    private double mousex;
    private double mousey;
    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);




        // Create the window
        window = glfwCreateWindow(2, 2, "Hello World!", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");




        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        //glfwSwapInterval(1);

        // Make the window visible
        //glfwShowWindow(window);
        GL.createCapabilities();
        int computeShader = glCreateShader(GL_COMPUTE_SHADER);
        glShaderSource(computeShader, readFile("gpujvm/glsl/jvm/jvm.glsl"));
        glCompileShader(computeShader);

        int status = glGetShaderi(computeShader, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(computeShader));
        }
        int vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, readFile("gpujvm/glsl/jvm/vs.glsl"));
        glCompileShader(vertexShader);

        status = glGetShaderi(vertexShader, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(vertexShader));
        }

        int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, readFile("gpujvm/glsl/jvm/fs.glsl"));
        glCompileShader(fragmentShader);

        status = glGetShaderi(fragmentShader, GL_COMPILE_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetShaderInfoLog(fragmentShader));
        }

        shaderProgram = glCreateProgram();
        //glAttachShader(shaderProgram, vertexShader);
        //glAttachShader(shaderProgram, fragmentShader);
        glAttachShader(shaderProgram, computeShader);
        glBindFragDataLocation(shaderProgram, 0, "fragColor");
        glLinkProgram(shaderProgram);

        status = glGetProgrami(shaderProgram, GL_LINK_STATUS);
        if (status != GL_TRUE) {
            throw new RuntimeException(glGetProgramInfoLog(shaderProgram));
        }


    }


    private void loop() {
        checkGLError();
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.

        // Set the clear color
        glClearColor(0.0f, 0.0f, 1.0f, 0.0f);


        int fbid = glGenFramebuffers();
        glBindFramebuffer(GL_FRAMEBUFFER,fbid);

        {
            int colorbufId = glGenRenderbuffers();
            // setup renderbuffepr
            glBindRenderbuffer(GL_RENDERBUFFER, colorbufId);
            glRenderbufferStorage(GL_RENDERBUFFER,GL_R32I , 4, 4);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorbufId);
            int colorbuf2Id = glGenRenderbuffers();
            // setup renderbuffepr
            glBindRenderbuffer(GL_RENDERBUFFER, colorbuf2Id);
            glRenderbufferStorage(GL_RENDERBUFFER,GL_R32I , 4, 4);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_RENDERBUFFER, colorbuf2Id);

        }
        checkGLError();
        mainClass = ClassLoader.getClass("Main");
        ArrayList<Method> ms=new ArrayList<>();
        ms.add((Method) mainClass.getMethod("main([Ljava/lang/String;)V"));
        ms.add((Method) mainClass.getMethod("test(I)V"));
        ms.add((Method) mainClass.getMethod("print(I)V"));
        ms.add((Method) mainClass.getMethod("main2(I)V"));

        int codememsize=0;
        HashMap<String , Integer> mtable=new HashMap<>();
        HashMap<Integer , Method> mindex=new HashMap<>();
        for(Method m:ms){
            mtable.put(m.signature,codememsize);
            mindex.put(codememsize,m);
            if(m.isNative){
                codememsize+=2;
                continue;
            }
           codememsize+=m.code.length;
           codememsize+=3;
        }
        int[] codedump=new int[codememsize];
        int j=0;
        int ssboCode;
        {
            ssboCode = glGenBuffers();
            glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER, ssboCode);
            try (MemoryStack stack = MemoryStack.stackPush()) {
                IntBuffer verticesb = stack.mallocInt(codememsize);
                for(Method method:ms){
                    if(method.isNative){
                        verticesb.put(method.args.length);
                        codedump[j++]=method.args.length;
                        verticesb.put(-1);
                        codedump[j++]=-1;
                        continue;
                    }
                    int[] code = CodeProcessor.optimize(method.code, (Class) method.getMyClass(),mtable);
                    verticesb.put(method.args.length);
                    codedump[j++]=method.args.length;
                    verticesb.put(method.getMaxLocals());
                    codedump[j++]=method.getMaxLocals();
                    verticesb.put(method.getMaxStack());
                    codedump[j++]=method.getMaxStack();
                    for (int op : code) {
                        verticesb.put(op);
                        codedump[j++]=op;
                    }
                }
                verticesb.flip();
                glBufferData(GL43.GL_SHADER_STORAGE_BUFFER, verticesb, GL_STATIC_DRAW);
            }
            glBindBufferBase(GL43.GL_SHADER_STORAGE_BUFFER, 2, ssboCode);
            glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER, 0);
        }
        checkGLError();

        int ssboMem;
        {
            ssboMem = glGenBuffers();
            glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER,ssboMem);
            try (MemoryStack stack = MemoryStack.stackPush()) {
                IntBuffer verticesb = stack.mallocInt(1024);
                //first frame offset
                verticesb.put(1);
                //prev frame offset
                verticesb.put(0);
                //method_info_offset
                verticesb.put(0);
                //pc offset for first method
                verticesb.put(3);
                //stackpos offset for first method
                verticesb.put(1+4+ms.get(0).getMaxLocals());
                for(int i=0;i<1019;i++)
                    verticesb.put(0);
                verticesb.flip();
                glBufferData(GL43.GL_SHADER_STORAGE_BUFFER, verticesb,GL_STATIC_DRAW);
            }
            glBindBufferBase(GL43.GL_SHADER_STORAGE_BUFFER,3,ssboMem);
            glBindBuffer(GL43.GL_SHADER_STORAGE_BUFFER,0);
        }
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

        //int ul = glGetUniformBlockIndex(shaderProgram,"Lights");
        //glUniformBlockBinding(shaderProgram,ul,0);


        glUseProgram(shaderProgram);
        checkGLError();
        glDrawBuffers(new int[]{GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1});
        checkGLError();
        int vao = generateVAO(shaderProgram);
        //glBindBufferBase(GL_SHADER_STORAGE_BUFFER,2,ssboCode);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER,3,ssboMem);
        checkGLError();
        IntBuffer mappedbuffer = null;
        checkGLError();
        int[] mem=new int[1024];
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        checkGLError();
        while ( !glfwWindowShouldClose(window) ) {
            {
                glBindFramebuffer(GL_FRAMEBUFFER, fbid);
                checkGLError();

                glViewport(0, 0, 4, 4);
                checkGLError();
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT| GL_STENCIL_BUFFER_BIT); // clear the framebuffer
                checkGLError();
                //glEnableClientState(GL_COLOR_ARRAY);
                checkGLError();
            }
            checkGLError();
            glDispatchCompute(2,2,1);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT|GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
            /*
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glReadBuffer(GL_COLOR_ATTACHMENT1);

            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

            int v1 = 0;
            int[] pixels=new int[16];
            glReadPixels(0,0,4,4,GL_RED_INTEGER,GL_UNSIGNED_INT,pixels);
            for(int i:pixels) {
                v1=i;
            }
            //System.out.println(v1-3);

            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            glReadBuffer(GL_COLOR_ATTACHMENT0);

            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            pixels=new int[16];
            glReadPixels(0,0,4,4,GL_RED_INTEGER,GL_UNSIGNED_INT,pixels);
            int cmd = 0;
            for(int i:pixels) {
                cmd=i;
            }

             */
            ByteBuffer bb = glMapBuffer(GL43.GL_SHADER_STORAGE_BUFFER,GL_READ_ONLY);
            checkGLError();
            //ByteBuffer bb = glMapBufferRange(GL43.GL_SHADER_STORAGE_BUFFER,v1+4,args_length,GL_READ_ONLY);

            if (bb != null) {
                mappedbuffer = bb.asIntBuffer();
                mappedbuffer.position(0);
                if(dumpmem)
                    System.out.println("memdump:");
                for (int i = 0; i < mappedbuffer.capacity(); i++) {
                    int v=mappedbuffer.get();
                    mem[i]=v;
                    if(dumpmem&v!=0)
                        System.out.println(i+": "+v);
                }
                glUnmapBuffer(GL43.GL_SHADER_STORAGE_BUFFER);
                checkGLError();
            }
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            checkGLError();
            int cmd=mem[1022];
            int v1=mem[1023];
            switch (cmd){
                case 0:return;
                case 3:
                    System.out.println(v1);
                    break;
                case 4:
                    int args_length = codedump[mem[v1 + 1]];
                    //System.out.println("native method: frame offset: "+v1);
                    //System.out.println("args count: "+args_length);
                    for(int i=0;i<args_length;i++){
                        int v = mem[v1+4+i];
                        System.out.println("o:"+v);
                    }


                    break;
            }
            /*
            glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
            //glfwSwapBuffers(window); // swap the color buffers
            //glClear(GL_COLOR_BUFFER_BIT);

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            //glfwPollEvents();

             */
        }
    }

    private static void checkGLError() {
        int errorCheckValue = glGetError();
        if(errorCheckValue != GL_NO_ERROR)
        {
            errorCheckValue=glGetError();
        }
    }

    public int generateVAO(int shaderProgram){
        int vao = glGenVertexArrays();
        glBindVertexArray(vao);


        try (MemoryStack stack = MemoryStack.stackPush()) {
            FloatBuffer verticesb = stack.mallocFloat(6 * 2);
            verticesb.put(-1f);
            verticesb.put(1f);

            verticesb.put(1f);
            verticesb.put(1f);

            verticesb.put(-1f);
            verticesb.put(-1f);

            verticesb.put(-1f);
            verticesb.put(-1f);

            verticesb.put(1f);
            verticesb.put(1f);

            verticesb.put(1f);
            verticesb.put(-1f);

            verticesb.flip();

            int vbo = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBufferData(GL_ARRAY_BUFFER, verticesb, GL_STATIC_DRAW);
            int posAttrib = glGetAttribLocation(shaderProgram, "position");
            glEnableVertexAttribArray(posAttrib);
            int floatSize = 4;
            glVertexAttribPointer(posAttrib, 2, GL_FLOAT, false, 2 * floatSize, 0);

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }
        return vao;
    }


    private static String readFile(String filename){
        try{
            FileInputStream fis=new FileInputStream(filename);
            BufferedReader buf = new BufferedReader(new InputStreamReader(fis));
            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();
            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }
            return sb.toString();
        }
        catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new GLExecutor().run();
    }

    public int getShaderProgram() {
        return shaderProgram;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
