package kotlin;

public @interface SinceKotlin{
	String version() default "";
}
