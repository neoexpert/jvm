package kotlin;

public class KotlinNullPointerException extends NullPointerException {
	public KotlinNullPointerException(){
		super();
	}
	public KotlinNullPointerException(String message){
		super(message);
	}
}
