package java.security;

public class ProtectionDomain{
	private final CodeSource codeSource;

	public ProtectionDomain(CodeSource codeSource) {
		this.codeSource = codeSource;
	}
	public CodeSource getCodeSource(){
		return codeSource;
	}
}
