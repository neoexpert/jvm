package java.nio;

public abstract class ByteBuffer extends Buffer{
    ByteBuffer(int capacity, int limit, int position, int mark){
    	super(capacity, limit, position, mark);
    }

    public static ByteBuffer wrap(byte[] buf){
        return new NativeByteBuffer(buf);
    }
    public abstract ByteBuffer order(ByteOrder order);
    public abstract ByteBuffer get(byte[] arr);
    public abstract int getInt();
    public abstract float getFloat();
    public abstract short getShort();
    public abstract char getChar();
    public abstract byte get();
    public abstract byte get(int pos);
    public abstract long getLong();
    public abstract double getDouble();
}
