package java.nio;

public class NativeByteBuffer extends ByteBuffer{
    static {
        System.loadLibrary("bytebuffer");
			registerNatives();
    }
    private byte[] buf;
    NativeByteBuffer(byte[] buf){
	    super(buf.length, buf.length, 0, 0);
	    this.buf=buf;
			init(buf);
    }

    private native void init(byte[] buf);
    private static final int BIG_ENDIAN=0;
    private static final int LITTLE_ENDIAN=1;

    private int byteorder=BIG_ENDIAN;
    public ByteBuffer order(ByteOrder order){
   	if(order==ByteOrder.BIG_ENDIAN  )
		this.byteorder=BIG_ENDIAN;
	else
		this.byteorder=LITTLE_ENDIAN;
	setByteOrder(byteorder);
	return this;
    }
    public native void setByteOrder(int order);
    
    public native ByteBuffer get(byte[] arr);
    public native int getInt();
    public native float getFloat();
    public native short getShort();
    public native char getChar();
    public native byte get();
    public native byte get(int pos);
    public native long getLong();
    public native double getDouble();

    public final native int limit();
    public final native ByteBuffer limit(int i);
    public final native int position();
    public final native ByteBuffer position(int i);
    public final native boolean hasRemaining();
    public static native void registerNatives();
}
