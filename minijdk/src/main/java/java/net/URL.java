package java.net;

public class URL {
	private final String spec0;

	public URL(String spec)throws MalformedURLException{
		if(spec==null)
			throw new NullPointerException("spec is null");
		this.spec0=spec;
	}
	public URI toURI(){
		if(spec0==null)
			System.out.println(spec0);
		return new URI(spec0);
	}
}
