package java.net;

public class ServerSocket {
	private native void init(int port);
	public ServerSocket(int port){
		init(port);
	}
	public native Socket accept();
}
