package java.io;
import java.net.URI;

public class File{
	static{
		System.loadLibrary("filesystem");
		registerNatives();
	}
	public static final char separatorChar='/';
	public static final String separator = "/";
	public static final char pathSeparatorChar ='/';
	public static final String pathSeparator = "/";
	private final String path;
	public File(File parent, String path){
		this.path=parent.getAbsolutePath()+"/"+path;
	}
	public File(String parent, String child){
		this.path=parent+"/"+child;
	}
	public File(String path){
		if(path==null)
			throw new NullPointerException();
		this.path=path;
	}
	public File(URI uri){
		this.path=uri.toString();
		if(path==null)
			throw new NullPointerException("path is null");
	}
	public native String getAbsolutePath();
	public URI toURI(){
		return new URI(path);
	}
	public native boolean isDirectory();
	public native String getParent();
	public native boolean exists();
	public native boolean canRead();
	public native boolean canWrite();
	public native boolean canExecute();
	public native String getName();
	public final native long length();
	public static native void registerNatives();
	@Override
	public String toString(){
		return path;
	}
}
