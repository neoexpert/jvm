package java.io;

public class FileOutputStream extends OutputStream{
	static{
		System.loadLibrary("filesystem");
	}
	public FileOutputStream(File f)throws FileNotFoundException{
		open(f);
	}
	public FileOutputStream(String path)throws FileNotFoundException{
		open(new File(path));
	}

	public native void open(File f)throws FileNotFoundException;

	public native void close() throws IOException;
	public native void write(int b) throws IOException;
	public native void write(byte[] b, int off, int len) throws IOException;


	public void flush() throws IOException {
	}
}
