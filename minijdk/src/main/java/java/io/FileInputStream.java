package java.io;

public class FileInputStream extends InputStream{
	private int offset;
	private int fd=-1;
	static{
		System.loadLibrary("filesystem");
	}

	public FileInputStream(String path)throws IOException{
		fd=open(new File(path));
	}

	public FileInputStream(File f)throws IOException{
		fd=open(f);
	}


	@Override
	public int read(byte[] b) throws IOException{
		return read(b,0,b.length);
	}

	public final native void close() throws IOException;
	private native int open(File f)throws IOException;

	public final int read() throws IOException{
		return read0();
	}
	public native int read0() throws IOException;
	public native int read(byte[] b, int off, int len) throws IOException;
}
