package java.io;

public class ByteArrayOutputStream extends OutputStream {
    private byte[] array;
    private int count;
    public ByteArrayOutputStream(){
        array=new byte[64];
    }
    public ByteArrayOutputStream(int initialCapacity){
        array=new byte[initialCapacity];
    }
    @Override
    public void write(int b) throws IOException {
        if(count >=array.length)
            grow(array.length*2);
        array[count++]=(byte) b;
    }
		public synchronized void write(byte[] b, int off, int len) {
        if ((off < 0) || (off > b.length) || (len < 0) ||
            ((off + len) - b.length > 0)) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity(count + len);
        System.arraycopy(b, off, array, count, len);
        count += len;
    }

		private void ensureCapacity(int minCapacity) {
        // overflow-conscious code
        if (minCapacity - array.length > 0)
            grow(minCapacity);
    }


    private void grow(int minCapacity) {
        byte[] newarray=new byte[minCapacity];
        System.arraycopy(array,0,newarray,0,array.length);
        array=newarray;
    }

    public byte[] toByteArray(){
        byte[] newarray=new byte[count];
        System.arraycopy(array,0,newarray ,0,count);
        return newarray;
    }
}
