package java.util;

public class ArrayList<E> extends AbstractList<E> implements List<E>{
	private int size=0;
	private E[] array;
	private int currentCapacity;
	public ArrayList(){
		this(16);	
	}

	@SuppressWarnings("unchecked")
	public ArrayList(int initialsize){
		array=(E[])new Object[initialsize];
		currentCapacity=initialsize;
	}

	public boolean add(E e){
		if(currentCapacity<=size)
			ensureCapacity(size+1);
		array[size++]=e;
		return true;
	}


	public final E get(int index){
		/*
		if(index<0|index>=size) {
			if(index<0)
				throw new IndexOutOfBoundsException("index < 0");
			else
				throw new IndexOutOfBoundsException("index >= size");
		}
		 */
		return array[index];
	}

	public E set(int index, E e){
		array[index]=e;
		return e;
	}

	public void add(int index, E e){
		if(currentCapacity<=size)
			ensureCapacity(size+1);
		System.arraycopy(array, index, array, index + 1,
				size - index);
		array[index] = e;
		++size;
	}

	public E remove(int index){
		E o=array[index];
		int numMoved = size - index - 1;
		if (numMoved > 0)
			System.arraycopy(array, index+1, array, index, numMoved);
		--size;
		return o;
	}


	public boolean remove(Object o){
		int l=currentCapacity;
	    for(int i=0;i<l;++i)
		{
			if(o.equals(array[i])) {
				remove(i);
				return true;
			}
		}
		return false;
	}


	public final int size(){
		return size;	
	}

	public final boolean isEmpty(){
		return size==0;	
	}

	public final boolean contains(Object o){
		for(Object _o:array)
			if(_o.equals(o))
				return true;
		return false;
	}
	public final Iterator<E> iterator(){
		return new Iterator<E>(){
			private int pos=0;
			public final boolean hasNext(){
				return pos<size;
			}
			public final E next(){
				return array[pos++];
			}
			public void remove(){}
		};
	}

	@Override
	public final ListIterator<E> listIterator() {
		return listIterator(0);
	}

	@Override
	public final ListIterator<E> listIterator(int index) {
		return new ListIterator<E>() {
			int pos=index;
			@Override
			public final boolean hasNext() {
				return pos<size;
			}

			@Override
			public final E next() {
				return array[pos++];
			}

			@Override
			public final boolean hasPrevious() {
				return pos>0;
			}

			@Override
			public E previous() {
				return array[--pos];
			}

			@Override
			public int nextIndex() {
				return pos;
			}

			@Override
			public int previousIndex() {
				return pos-1;
			}

			@Override
			public void remove() {

			}

			@Override
			public void set(E o) {
				array[pos]=o;
			}

			@Override
			public void add(E var1) {

			}
		};
	}

	@Override
	public int indexOf(Object o) {
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		return 0;
	}
	@SuppressWarnings("unchecked")
	public void ensureCapacity(int minCapacity){
		//modCount++;
		int oldCapacity = currentCapacity;
		if (minCapacity > oldCapacity) {
			Object[] oldData = array;
			int newCapacity = (oldCapacity * 3)/2 + 1;
			if (newCapacity < minCapacity)
				newCapacity = minCapacity;
			array = (E[])new Object[newCapacity];
			currentCapacity=newCapacity;
			System.arraycopy(oldData, 0, array, 0, size);
		}
	}

	@SuppressWarnings("unchecked")
	public final void clear(){
		size=0;	
		array=(E[])new Object[16];
		currentCapacity=16;
	}

	@Override
    @SuppressWarnings("unchecked")
    public void sort(Comparator<? super E> c) {
        final int expectedModCount = modCount;
        Arrays.sort((E[]) array, 0, size, c);
        if (modCount != expectedModCount) {
            throw new ConcurrentModificationException();
        }
        modCount++;
    }
}
