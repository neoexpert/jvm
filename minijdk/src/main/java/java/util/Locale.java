package java.util;

public class Locale{
	private static Locale def=new Locale("en");
	public static Locale getDefault(){
		return def;
	}
	private final String language;
	private final String country;
	public Locale(String language){
		this(language, "");
	}
	public Locale(String language, String country){
		this.language=language;
		this.country=country.intern();
	}

	@Override
	public String toString(){
		StringBuilder sb=new StringBuilder(language);
		if(country!=""){
			sb.append("_")
				.append(country);
		}

		return sb.toString();
	}
}
