package java.util;


public class LinkedList<E> extends AbstractList<E> implements List<E> {
    private class Node{
        public Node(E e) {
            this.value=e;
        }
        public Node next;
        public Object value;
    }
    private Node first;
    private Node last;
    private int size=0;
    @Override
    public final int size() {
        return size;
    }
    public final E getFirst(){
        return (E) first.value;

    }
    public final E getLast(){
        return (E) last.value;
    }

    @Override
    public final boolean isEmpty() {
        return size==0;
    }

    @Override
    public final boolean contains(Object o) {
        Node n=first;
        while (n!=null) {
            if (o.equals(n.value))
                return true;
            n=n.next;
        }
        return false;
    }

    @Override
    public final Iterator<E> iterator() {
        return new Iterator<E>(){
            private Node n=first;
            public boolean hasNext(){
                return n!=null;
            }
            public E next(){
                E value=(E)n.value;
                n=n.next;
                return value;
            }
            public void remove(){}
        };
    }


    @Override
    public final ListIterator<E> listIterator() {
        return listIterator(0);
    }

    @Override
    public final ListIterator<E> listIterator(int index) {
        Node start=first;
        for (int i=0;true;++i){
            if(i==index)break;
            start=start.next;
        }
        Node finalStart = start;
        return new ListIterator<E>() {
            private Node n= finalStart;
            private Node previousNode;
            @Override
            public final boolean hasNext() {
                return n!=null;
            }

            @Override
            public final E next() {
                E value=(E)n.value;
                previousNode=n;
                n=n.next;
                return value;
            }

            @Override
            public boolean hasPrevious() {
                throw new RuntimeException("not implemented");
            }

            @Override
            public E previous() {
                throw new RuntimeException("not implemented");
            }

            @Override
            public int nextIndex() {
                throw new RuntimeException("not implemented");
            }

            @Override
            public int previousIndex() {
                throw new RuntimeException("not implemented");
            }

            @Override
            public final void remove(){
                if(n==null)
                    previousNode.next=null;
                else
                if(previousNode==first)
                    first=n;
                else
                    previousNode.next=n.next;
                size--;
                if(size==0) {
                    first = null;
                    last=null;
                }
            }

            @Override
            public void set(E var1) {
                throw new RuntimeException("not implemented");
            }

            @Override
            public void add(E var1) {
                throw new RuntimeException("not implemented");
            }
        };
    }

    public final E removeFirst(){
        E value=(E)first.value;
        first=first.next;
        size--;
        if(first==null)
            last=null;
        return value;
    }

    @Override
    public final boolean add(E e) {
        if(first==null){
            first=new Node(e);
            last=first;
            size++;
            return true;
        }
        last.next=new Node(e);
        last=last.next;
        size++;
        return true;
    }


    @Override
    public final boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public final boolean remove(Object o) {
        if(first==null)
            return false;
        if(o.equals(first.value)) {
            first = first.next;
            if(first==null) last=null;
            size--;
            return true;
        }

        Node prev=null;
        Node n=first;
        while (n!=null) {
            if (o.equals(n.value)) {
                if(prev==null) {
                    first = first.next;
                    if(first==null) last=null;
                }
                else {
                    prev.next = n.next;
                    if(prev.next==null)
                        last=prev;
                }
                size--;
                return true;
            }
            prev=n;
            n=n.next;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {
        first=null;
        last=null;
        size=0;
    }

    @Override
    public E get(int index) {
        if(first==null)
            throw new IndexOutOfBoundsException();
        int i=0;
        Node n=first;
        while (i<index){
            n=n.next;
            ++i;
        }
        if(n!=null)
            return (E)n.value;
        return null;
    }

    @Override
    public E set(int index, E e) {
        if(first==null)
            throw new IndexOutOfBoundsException();
        int i=0;
        Node n=first;
        while (i<index){
            n=n.next;
            ++i;
        }
        if(n!=null) {
            n.value=e;
            return (E)n.value;
        }
        return null;
    }

    @Override
    public void add(int i, E e) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public E remove(int i) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

}
