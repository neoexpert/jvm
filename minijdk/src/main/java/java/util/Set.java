package java.util;

public interface Set<E> extends Collection<E> {
		int size();

	boolean retainAll(Collection<?> collection);

	boolean removeAll(Collection<?> collection);

	Iterator<E> iterator();
    boolean remove(Object o);

    boolean containsAll(Collection<?> collection);
	    Object[] toArray();


    boolean add(E o);
		boolean addAll(Collection<? extends E> c);
		boolean contains(Object o);
		void clear();
}

