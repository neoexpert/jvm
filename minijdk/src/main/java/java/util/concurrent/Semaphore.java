package java.util.concurrent;

public class Semaphore{
	public Semaphore(int capacity){
		init(capacity);
	}
	private native void init(int capacity);
	public native void acquire() throws InterruptedException;
	public native void release();

}
