package java.util.concurrent.locks;

public class ReentrantLock{
	public ReentrantLock(){
		init();
	}
	public native void init();
	public native void lock();
	public native void unlock();
}
