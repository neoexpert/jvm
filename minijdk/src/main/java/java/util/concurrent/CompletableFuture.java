package java.util.concurrent;

public class CompletableFuture<V> implements Future{
	public static CompletableFuture<Void> runAsync(Runnable runnable) {
		runnable.run();
		return new CompletableFuture<Void>();
	}
	@Override
	public boolean cancel(final boolean var1){
		return false;
	}

	@Override
	public boolean isCancelled(){
		return false;
	}

	@Override
	public boolean isDone(){
		return false;
	}

	@Override
	public Object get() throws InterruptedException, ExecutionException{
		return null;
	}

	/*
	@Override
	public Object get(final long var1, final TimeUnit var3) throws InterruptedException, ExecutionException, TimeoutException{
		return null;
	}*/
}
