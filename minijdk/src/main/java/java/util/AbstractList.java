package java.util;

public abstract class AbstractList<E> extends AbstractCollection<E> implements List<E>{
		    protected transient int modCount = 0;

	protected AbstractList(){}
	public List<E> subList(int fromIndex, int toIndex){
		return null;
	}
	public void add(int index, E o){
		throw new UnsupportedOperationException();
	}
	public E set(int index, E element) {
        throw new UnsupportedOperationException();
    }
	public boolean addAll(int index, Collection<? extends E> c){
        throw new UnsupportedOperationException();
	}

	public boolean add(E o){
		add(size(), o);
		return true;
	}
	public E remove(int index){
		throw new UnsupportedOperationException();
	}
	public int lastIndexOf(Object o)
	{
		int pos = size();
		ListIterator<E> itr = listIterator(pos);
		while (--pos >= 0)
			if(o.equals(itr.previous()))
				return pos;
		return -1;
	}
	public int indexOf(Object o)
	{
		ListIterator<E> itr = listIterator();
		int size = size();
		for (int pos = 0; pos < size; pos++)
			if (o.equals(itr.next()))
				return pos;
		return -1;
	}
	public Iterator<E> iterator() {
        return listIterator();
    }
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }
	public ListIterator<E> listIterator(final int index) {
        //rangeCheckForAdd(index);

        return new ListItr(index);
    }
	private class Itr implements Iterator<E> {
        /**
         * Index of element to be returned by subsequent call to next.
         */
        int cursor = 0;

        /**
         * Index of element returned by most recent call to next or
         * previous.  Reset to -1 if this element is deleted by a call
         * to remove.
         */
        int lastRet = -1;

        /**
         * The modCount value that the iterator believes that the backing
         * List should have.  If this expectation is violated, the iterator
         * has detected concurrent modification.
         */
        int expectedModCount = modCount;

        public boolean hasNext() {
            return cursor != size();
        }

        public E next() {
            checkForComodification();
            try {
                int i = cursor;
                E next = get(i);
                lastRet = i;
                cursor = i + 1;
                return next;
            } catch (IndexOutOfBoundsException e) {
                checkForComodification();
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                AbstractList.this.remove(lastRet);
                if (lastRet < cursor)
                    cursor--;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException e) {
                throw new ConcurrentModificationException();
            }
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    private class ListItr extends Itr implements ListIterator<E> {
        ListItr(int index) {
            cursor = index;
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public E previous() {
            checkForComodification();
            try {
                int i = cursor - 1;
                E previous = get(i);
                lastRet = cursor = i;
                return previous;
            } catch (IndexOutOfBoundsException e) {
                checkForComodification();
                throw new NoSuchElementException();
            }
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor-1;
        }

        public void set(E e) {
            if (lastRet < 0)
                throw new IllegalStateException();
            checkForComodification();

            try {
                AbstractList.this.set(lastRet, e);
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(E e) {
            checkForComodification();

            try {
                int i = cursor;
                AbstractList.this.add(i, e);
                lastRet = -1;
                cursor = i + 1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }
    }


		@Override
		public boolean equals(Object o) {
			if (o == this)
				return true;
			if (!(o instanceof List))
				return false;
			ListIterator<E> e1 = listIterator();
			ListIterator e2 = ((List) o).listIterator();
			while(e1.hasNext() && e2.hasNext()) {
				E o1 = e1.next();
				Object o2 = e2.next();
				if (!(o1==null ? o2==null : o1.equals(o2)))
					return false;
			}
			return !(e1.hasNext() || e2.hasNext());
		}

		@Override
		public int hashCode() {
			int hashCode = 1;
			for (E e : this)
				hashCode = 31*hashCode + (e==null ? 0 : e.hashCode());
			return hashCode;
		}
}
