package java.util.zip;

import java.io.*;

public class ZipFile {
    static {
        System.loadLibrary("zip");
        registerNatives();
    }
    final long id;
    public static native long open(String path);
    public ZipFile(String path)throws IOException{
        id=open(path);
        if(id==0L)
            throw new FileNotFoundException(path);
    }
    public ZipFile(File f)throws IOException{
        this(f.getAbsolutePath());
    }
    public ZipEntry getEntry(String name){
        if(entryExists(id, name))
            return new ZipEntry(id, name);
        else return null;
    }

    private static native boolean entryExists(long id, String name);

    public InputStream getInputStream(ZipEntry entry){
	    if(entry==null)
		    throw new NullPointerException("ZipEntry is null");
	    return new ByteArrayInputStream(getBytes(id, entry.getName()));
    
    }
    public void close(){
    	close(id);
    }
    private static native byte[] getBytes(long id, String path);
    static native long getSize(long id, String path);
    private static native void registerNatives();
    private static native void close(long id);
    
}
