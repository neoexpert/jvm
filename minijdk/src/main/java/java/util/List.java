package java.util;
import java.util.function.*;

public interface List<E> extends Collection<E> {
	int size();

	boolean isEmpty();

	boolean contains(Object o);

	boolean add(E e);
	    boolean addAll(int index, Collection<? extends E> c);

	E get(int index);

	E set(int index, E e);

	void add(int index, E e);

	E remove(int index);

	boolean removeAll(Collection<?> collection);

	boolean retainAll(Collection<?> collection);

    boolean remove(Object o);

	boolean containsAll(Collection<?> collection);
	List<E> subList(int fromIndex, int toIndex);

	default void replaceAll(UnaryOperator<E> operator) {
        Objects.requireNonNull(operator);
        final ListIterator<E> li = this.listIterator();
        while (li.hasNext()) {
            li.set(operator.apply(li.next()));
        }
    }

	default void sort(Comparator<? super E> c) {
			Collections.sort(this, c);
	}

	void clear();
	Iterator<E> iterator();
	ListIterator<E> listIterator();
	ListIterator<E> listIterator(int index);

	int indexOf(Object o);

	int lastIndexOf(Object o);
}
