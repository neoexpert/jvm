package java.lang;

public class OutOfMemoryError extends Error {
    public OutOfMemoryError() {
    }

    public OutOfMemoryError(String detailMessage) {
        super(detailMessage);
    }
}
