package java.lang;

public final class Void {
	public static final Class<Void> TYPE = (Class<Void>)Class.getPrimitiveClass("V");

	private Void() {
	}
}
