package java.lang;

public class RuntimeException extends Exception{
	public RuntimeException(){}
	public RuntimeException(String detailMessage){
		super(detailMessage);
	}

	public RuntimeException(String detailMessage,Throwable cause){
		super(detailMessage, cause);
	}
	public RuntimeException(Throwable cause){
		super(cause);
	}

}

