package java.lang;

public class Thread implements Runnable{
	public static final int MIN_PRIORITY = 1;

   public static final int NORM_PRIORITY = 5;

   public static final int MAX_PRIORITY = 10;

	private Runnable target;
	private String name;
	ThreadGroup group;
	int priority;
	public Thread(){
		this(null, null, null);	
	}
	public Thread(Runnable target){
		this(null, target, null);	
	}
	public Thread(Runnable target, String name){
		this(null, target, name);
	}
	public Thread(String name){
		this(null, null, name);
	}
	public Thread(ThreadGroup group, Runnable target, String name){
		this.target=target;
		this.name=name;
		init();
		Thread current=Thread.currentThread();
		if(current==null) return;
		if(group==null)
			group=current.group;
		this.group=group;
		priority = current.priority;
		//group.addThread(this);
	}
	private native void init();
	public final native void start();
	public final void setName(String name){
		this.name=name;
	}
	public final void setPriority(int newPriority){
		this.priority=newPriority;
		setPriority0(newPriority);
	}
	public final native void setPriority0(int newPriority);
	public final native void join()throws InterruptedException;
	public static native void sleep(long millis) throws InterruptedException;
	public static native Thread currentThread();
	public final native void interrupt();
	public final native void suspend();
	public final native void resume();
	public static native void yield();
	public static void setDefaultUncaughtExceptionHandler(UncaughtExceptionHandler eh) {
		defaultUncaughtExceptionHandler=eh;
	}
	private static UncaughtExceptionHandler defaultUncaughtExceptionHandler;
	public interface UncaughtExceptionHandler {
		void uncaughtException(Thread thread, Throwable ex);
	}
	public void run(){
		if(target!=null)
			target.run();
	}
	//invoked by JVM
	private static void start(Thread t){
			t.run();
	}
	public native StackTraceElement[] getStackTrace();
}
