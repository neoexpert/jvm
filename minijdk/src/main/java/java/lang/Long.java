package java.lang;

public final class Long extends Number{
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("J");
	public static final long MIN_VALUE = 0x8000000000000000L;
	public static final long MAX_VALUE = 0x7fffffffffffffffL;
	public static Long valueOf(long j){
		if (j >= PrimitivesCache.low && j <= PrimitivesCache.high)
			return PrimitivesCache.longcache[(int)j + (-PrimitivesCache.low)];
		return new Long(j);
	}
	public static long sum(long a, long b) {
			return a + b;
	}
	public static long divideUnsigned(long dividend, long divisor) {
        if (divisor < 0L) { // signed comparison
            // Answer must be 0 or 1 depending on relative magnitude
            // of dividend and divisor.
            //return (compareUnsigned(dividend, divisor)) < 0 ? 0L :1L;
			throw new RuntimeException();
        }

        if (dividend > 0) //  Both inputs non-negative
            return dividend/divisor;
        else {
			throw new RuntimeException();
            /*
             * For simple code, leveraging BigInteger.  Longer and faster
             * code written directly in terms of operations on longs is
             * possible; see "Hacker's Delight" for divide and remainder
             * algorithms.
             */
            //return toUnsignedBigInteger(dividend).
            //    divide(toUnsignedBigInteger(divisor)).longValue();
        }
    }

	public static String toString(Long j){
		return j.toString();
	}

	public static String toString(long j){
		return IntegralToString.longToString(j,10);
	}

	public static String toHexString(long j){
		return IntegralToString.longToString(j,16);
	}

	private final long value;
	Long(long value){
		this.value=value;
	}
	public final int intValue(){
		return (int)value;
	}

	public final short shortValue(){
		return (short)value;
	}
	public final byte byteValue(){
		return (byte)value;
	}
	public final long longValue(){
		return value;
	}

	@Override
	public final float floatValue(){
		return (float)value;
	}

	@Override
	public final double doubleValue(){
		return (double)value;
	}

	@Override
	public final boolean equals(Object o){
		if(o==this)return true;
		if(o instanceof Long)
			return value==((Long)o).value;
		return false;
	}

	@Override
	public final int hashCode(){
		return (int)value;
	}

	@Override
	public final String toString() {
		return toString(longValue());
	}
}
