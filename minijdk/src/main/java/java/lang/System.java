package java.lang;
import java.io.*;
public final class System{
	private System(){}
	static{
		initializeSystemClass();
	}
	 private static void initializeSystemClass() {
		PrintStream myStream = new PrintStream(null) {
			@Override
			public void print(String x) {
				print0(x);
			}
			@Override
			public void println(String x) {
				print0(x);
				print0("\n");
			}
			@Override
			public void print(int x) {
				print0(Integer.toString(x));
			}
			@Override
			public void println(int x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(long x) {
				print0(Long.toString(x));
			}
			@Override
			public void println(long x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(boolean x) {
				print0(Boolean.toString(x));
			}
			@Override
			public void println(boolean x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(float x) {
				print0(Float.toString(x));
			}
			@Override
			public void println(float x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(char x) {
				print0(x);
			}
			@Override
			public void println(char x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(char[] x) {
				print0(new String(x));
			}
			@Override
			public void println(char[] x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(double x) {
				print0(Double.toString(x));
			}
			@Override
			public void println(double x) {
				print(x);
				print0("\n");
			}
			@Override
			public void print(Object x) {
				if(x==null)x="null";
				print0(x.toString());
			}
			@Override
			public void println(Object x) {
				print(x);
				print0("\n");
			}
		};
		if(myStream==null)
			throw new RuntimeException("myStream==null");
		setOut(myStream);
		setErr(myStream);
		setIn(new InputStream(){
			@Override
			public int read()throws IOException{
				return read0();	
			}
		});
	}

	private static native void print0(String x);
	private static native void print0(char c);
	private static native int read0()throws IOException;

	public static PrintStream out;
	public static PrintStream err;
	public static InputStream in;
	public static void setOut(PrintStream newout){
		if(newout!=null)
		out=newout;
	}
	public static void setErr(PrintStream newerr){
		if(newerr!=null)
			err=newerr;
	}
	public static void setIn(InputStream newin){
		if(newin!=null)
			in=newin;
	}
	public static native void gc();
	public static native long currentTimeMillis();
	public static native long nanoTime();
	public static native void arraycopy(Object src,  int  srcPos, Object dest, int destPos, int length);
	public static int identityHashCode(Object o){
		return o.hashCode();
	}
	public native static String getProperty(String name);
	public native static String getenv(String name);
	public native static void loadLibrary(String name);
}
