package java.lang;

public class NoSuchMethodException extends RuntimeException{
    public NoSuchMethodException() {
    }
    public NoSuchMethodException(String detailMessage) {
        super(detailMessage);
    }
}
