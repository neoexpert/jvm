package java.lang;

public final class StackTraceElement implements java.io.Serializable {
    private String declaringClass;
    private String methodName;
    private String fileName;
    private int lineNumber;

    public StackTraceElement(String className, String methodName,
                             String fileName, int lineNumber) {
        this.declaringClass = className;
        this.methodName     = methodName;
        this.fileName       = fileName;
        this.lineNumber     = lineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getClassName() {
        return declaringClass;
    }

    public String getMethodName() {
        return methodName;
    }

    public boolean isNativeMethod() {
        return lineNumber == -2;
    }

    public String toString() {
	    return new StringBuilder()
		    .append(getClassName())
		    .append(".")
		    .append(methodName)
		    .append(isNativeMethod() ? "(Native Method)" :(fileName != null && lineNumber >= 0 ? "(" + fileName + ":" + lineNumber + ")" :(fileName != null ?  "("+fileName+")" : "(Unknown Source)")))
		    .toString();
    }
}
