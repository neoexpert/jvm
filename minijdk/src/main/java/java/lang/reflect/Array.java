package java.lang.reflect;

public final class Array {

    /**
     * Constructor.  Class Array is not instantiable.
     */
    private Array() {}

    public static native Object newInstance(Class<?> componentType, int length);
        //throws NegativeArraySizeException;
}
