package java.lang;

public final class Character {
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("C");
	public static final int MIN_RADIX=2;
	public static final int MAX_RADIX=36;
    public native static Character valueOf(char c);
    public native static String toString(char c);
    public native final int intValue();
    public native final char charValue();
    @Override
    public final native boolean equals(Object o);
    @Override
    public final native int hashCode();
	public static boolean isWhitespace(char c){
			switch(c){
					case ' ':
					case '\t':
					case '\n':
							return true;
			}
			return false;
	}
	public String toString(){
		return toString(charValue());
	}
}
