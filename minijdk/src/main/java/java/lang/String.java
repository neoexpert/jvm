package java.lang;

public final class String implements CharSequence, Comparable<String>{
	final char[] value;
	private int hash;
	public String(byte[] arr){
		value=init(arr);
	}

	private native char[] init(byte[] arr);

	public String(char[] value){
		this.value=value;
	}

	public String(String original) {
		this.value = original.value;
		this.hash = original.hash;
	}

	public String(char[] value, int offset, int count){
		this.value=new char[count];
		System.arraycopy(value,offset,this.value,0,count);
	}

	public int compareTo(String anotherString) {
        int len1 = value.length;
        int len2 = anotherString.value.length;
        int lim = Math.min(len1, len2);
        char v1[] = value;
        char v2[] = anotherString.value;

        int k = 0;
        while (k < lim) {
            char c1 = v1[k];
            char c2 = v2[k];
            if (c1 != c2) {
                return c1 - c2;
            }
            k++;
        }
        return len1 - len2;
    }

	public native final boolean contains(CharSequence s);

	public int hashCode(){
		int h = hash;
		if (h == 0 && value.length > 0) {
			char[] val = value;

			for (int i = 0; i < value.length; i++) {
				h = 31 * h + val[i];
			}
			hash = h;
		}
		return h;
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof String) {
			String anotherString = (String) o;
			int n = value.length;
			if (n == anotherString.value.length) {
				char[] v1 = value;
				char[] v2 = anotherString.value;
				int i = 0;
				while (n-- != 0) {
					if (v1[i] != v2[i])
						return false;
					++i;
				}
				return true;
			}
		}
		return false;
	}

	public int lastIndexOf(String str) {
		return lastIndexOf(str, length());
	}
	public final native int lastIndexOf(String str, int fromIndex);
	public final native String toLowerCase();
	public final native String toUpperCase();



	public final int length(){
		return value.length;
	}

	public final char[] toCharArray(){
		int len=value.length;
		char[] result = new char[len];
		System.arraycopy(value, 0, result, 0, len);
		return result;
	}
	public final char charAt(int index){
		return value[index];
	}
	public native final String substring(int index);
	public native final String substring(int beginIndex, int endIndex);
	public native final int indexOf(int i);
	public native final int indexOf(int ch, int fromIndex);
	public native final boolean startsWith(String prefix);
	public native final String intern();
	public final void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin){
		if (srcBegin < 0 || srcBegin > srcEnd || srcEnd > value.length)
			throw new IndexOutOfBoundsException();
		System.arraycopy(value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
	}

	public final CharSequence subSequence(int beginIndex, int endIndex) {
        return this.substring(beginIndex, endIndex);
    }
	@Override
	public final String toString() {
		return this;
	}
	public static String valueOf(char[] array){
		return new String(array);
	}
	public static String valueOf(int i){
		return Integer.valueOf(i).toString();
	}
	public static String valueOf(char c){
		return Character.valueOf(c).toString();
	}
	public static String valueOf(Object obj) {
		return (obj == null) ? "null" : obj.toString();
	}
	public native boolean matches(String regex);
	public native String replaceAll(String regex, String replacement);
	public String replace(char oldChar, char newChar){
		int len=value.length;
		char[] newValue=new char[len];
		for(int i=0;i<len;++i){
			char c=value[i];
			if(c==oldChar)
				newValue[i]=newChar;
			else
				newValue[i]=c;
		}
		return new String(newValue);
	}
	public static String format(String format, Object... args) {
		StringBuilder sb=new StringBuilder();
		char[] chars = format.toCharArray();
		for(int i=0,j=0;i<chars.length;++i){
			char c=chars[i];
			if(c=='%')
				switch(chars[i+1]){
					case 'd':
					case 'f':
					case 's':
					case 'c':
						sb.append(args[j++]);
						i+=2;
						break;
					case 'n':
						sb.append('\n');
						i+=2;
						break;
				}
			sb.append(c);
		}
		return sb.toString();
	}

}
