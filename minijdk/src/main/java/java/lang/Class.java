package java.lang;
import java.net.*;
import java.security.*;
import java.lang.reflect.*;

public class Class<T>{
	private String code_source;
	private String name;
	private Class(){
	}
	public ProtectionDomain getProtectionDomain(){
		try{
			return new ProtectionDomain(new CodeSource(new URL(code_source)));
		}
		catch(MalformedURLException e){
			return null;
		}
	}
	public native Class getComponentType();
	public String getName(){
		return name;
	}
	public native String getSimpleName();
	public boolean desiredAssertionStatus(){
		return false;
	}
	public native static Class<?> forName(String type)throws ClassNotFoundException;
	public static Class<?> getPrimitiveClass(String type){
		try{
			return forName(type);
		}
		catch(ClassNotFoundException e){
			return null;
		}
	}

	public native Class<? super T> getSuperclass();
	public native Field[] getFields();
	public native ClassLoader getClassLoader();
	public native Constructor<T> getConstructor(Class<?> ... parameterTypes) throws NoSuchMethodException, SecurityException;
	public native boolean isArray();
	public native boolean isInstance(Object value);
	public T newInstance() throws InstantiationException, IllegalAccessException{
		try{
			return getConstructor().newInstance();
		}
		catch(ReflectiveOperationException e){
			throw new InstantiationException(e);
		}
	}
}
