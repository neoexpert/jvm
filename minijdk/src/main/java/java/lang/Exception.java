package java.lang;

public class Exception extends Throwable{
	public Exception(){}
	public Exception(String detailMessage){
		super(detailMessage);
	}
	public Exception(String detailMessage,Throwable cause){
		super(detailMessage, cause);
	}
	public Exception(Throwable cause){
		super(cause);
	}

}
