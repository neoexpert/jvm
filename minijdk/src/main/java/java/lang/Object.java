package java.lang;

public class Object{
	private static int hashCounter=1;
	private int hash;
	public String toString(){
		return "java/lang/Object";
	}

	public int hashCode(){
		if(hash==0)
			hash=hashCounter++;
		return hash;
	}

	public boolean equals(Object o){
		return o==this;	
	}
	protected native Object clone() throws CloneNotSupportedException;
	public native Class getClass();
	public final native void wait()
                throws InterruptedException;
	public final native void notify();
	public final native void notifyAll();
}
