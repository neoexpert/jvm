package java.lang;
public final class Short extends Number{
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("S");
	public static final short MIN_VALUE = -32768;
  public static final short MAX_VALUE = 32767;

	public static Short valueOf(short s){
		if (s >= PrimitivesCache.low && s <= PrimitivesCache.high)
			return PrimitivesCache.shortcache[s + (-PrimitivesCache.low)];
		return new Short(s);
	}
	public native static String toString(short s);

	private final short value;
	Short(short value){
		this.value=value;
	}

	@Override
	public final int intValue(){
		return value;
	}
	@Override
	public final long longValue(){
		return value;
	}
	@Override
	public final float floatValue(){
		return (float)value;
	}
	@Override
	public final double doubleValue(){
		return (double)value;
	}
	@Override
	public final short shortValue(){
		return value;
	}
	@Override
	public final byte byteValue(){
		return (byte)value;
	}

	@Override
	public final boolean equals(Object o){
		if(o==this)return true;
		if(o instanceof Short)
			return value==((Short)o).value;
		return false;
	}
	@Override
	public final int hashCode(){
		return value;
	}
}
