package java.lang;

public final class Integer extends Number{
	public static final int MIN_VALUE = 0x80000000;
	public static final int MAX_VALUE = 0x7fffffff;
	public static final int SIZE = 32;

	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("I");
	static{
	
	}
	public static int sum(int a, int b) {
			return a + b;
	}
	public static Integer valueOf(int i){
		if (i >= PrimitivesCache.low && i <= PrimitivesCache.high)
			return PrimitivesCache.intcache[i + (-PrimitivesCache.low)];
		return new Integer(i);
	}


	public static Integer valueOf(String s)throws NumberFormatException{
		return valueOf(parseInt(s,10));	
	}
	public static int parseInt(String s) throws NumberFormatException{
		return parseInt(s,10);
	}

	public native static int parseInt(String s, int radix) throws NumberFormatException;
	public static String toString(int i){
		return IntegralToString.intToString(i,10);
	}
	public static String toHexString(int i){
		return IntegralToString.intToString(i,16);
	}
	private final int value;
	Integer(int value){
		this.value=value;
	}

	@Override
	public final int intValue(){
		return value;
	}

	@Override
	public final long longValue(){
		return value;
	}

	@Override
	public  final float floatValue(){
		return value;
	}

	@Override
	public  final double doubleValue(){
		return value;
	}

	public final  byte byteValue(){
		return (byte)value;
	}
	public final short shortValue(){
		return (short)value;
	}

	@Override
	public final String toString() {
		return toString(intValue());
	}

	@Override
	public final boolean equals(Object o){
		if(o==this)return true;
		if(o instanceof Integer)
			return value==((Integer)o).value;
		return false;
	}

	@Override
	public  final int hashCode(){
		return value;
	}
	public static int reverse(int i) {
			i = (i & 1431655765) << 1 | i >>> 1 & 1431655765;
			i = (i & 858993459) << 2 | i >>> 2 & 858993459;
			i = (i & 252645135) << 4 | i >>> 4 & 252645135;
			return reverseBytes(i);
	}

	public static int signum(int i) {
			return i >> 31 | -i >>> 31;
	}

	public static int reverseBytes(int i) {
			return i << 24 | (i & '\uff00') << 8 | i >>> 8 & '\uff00' | i >>> 24;
	}
}
