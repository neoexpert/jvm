package java.lang;

public final class Double extends Number {
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("D");
	public static final double POSITIVE_INFINITY = 1.0 / 0.0;

    public static final double NEGATIVE_INFINITY = -1.0 / 0.0;

    public static final double NaN = 0.0d / 0.0;
    public native static Double valueOf(double d);
    public native static String toString(Double d);
    public String toString(){
    	return toString(doubleValue());
    }
    public native final int intValue();
	public native static long doubleToLongBits(double value);
	public native static boolean isNaN(double d);
	public native static boolean isInfinite(double d);

    @Override
    public native final long longValue();

    @Override
    public native final float floatValue();

    public native final double doubleValue();
    @Override
    public final native boolean equals(Object o);
    @Override
    public final native int hashCode();
}
