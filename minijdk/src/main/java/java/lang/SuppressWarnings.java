package java.lang;

public @interface SuppressWarnings
{
	/**
	 * The list of warnings to suppress.
	 *
	 * It is valid to list a name more than once.  Unrecognized names
	 * are not a compile-time error.  At the present there is no
	 * standard for the names to be recognized by compilers; consult
	 * your compiler's documentation for this information.
	 */
	String[] value ();
}
