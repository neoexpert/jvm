package java.lang;

public final class Boolean{
	public static final Class<Integer>  TYPE = (Class<Integer>) Class.getPrimitiveClass("Z");
	public static final Boolean TRUE=new Boolean(true);
	public static final Boolean FALSE=new Boolean(false);
	public static String toString(boolean b){
		return b?"true":"false";
	}
	private final boolean value;

	public Boolean(boolean value){
		this.value=value;
	}

	public static Boolean valueOf(boolean value){
		return value?TRUE:FALSE;
	}

	public static Boolean valueOf(String value){
		return value.equals("true")?TRUE:FALSE;
	}

	public final String toString(){
		return value?"true":"false";
	}


	public final boolean booleanValue(){
		return value;
	}
}
