package java.lang.invoke;


public class LambdaMetafactory {
	static{
		System.loadLibrary("invoke");
	}
	public native static CallSite metafactory(
			MethodHandles.Lookup caller,
			String invokedName,
			MethodType invokedType,
			MethodType samMethodType,
			MethodHandle implMethod,
			MethodType instantiatedMethodType)
		throws LambdaConversionException;/*{
			if(caller==null)
				caller=MethodHandles.lookup();
			return new CallSite(caller.findStatic(LambdaMetafactory.class, "implRunnable", MethodType.methodType(Runnable.class, Object.class))){};
		}*/

	private static Runnable implRunnable(Object o){
		return new Runnable(){
			@Override
			public void run(){
				System.out.println("hello");
			}
		};
	}
}
