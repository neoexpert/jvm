package java.lang.invoke;

public final
class MethodType{
	Class<?> rtype;
	private Class<?>[] ptypes;
	private MethodType(Class<?> rtype, Class<?>[] ptypes) {
		//checkRtype(rtype);
		//checkPtypes(ptypes);
		this.rtype = rtype;
		this.ptypes = ptypes;
	}

	public static MethodType methodType(Class<?> rtype) {
		return new MethodType(rtype, new Class[0]);
	}

	public static MethodType methodType(Class<?> rtype, Class<?>[] ptypes) {
		return new MethodType(rtype, ptypes);
	}    
	public static MethodType methodType(Class<?> type, Class<?> parameter0) {
		return methodType(type, new Class<?>[] { parameter0 });
	}
}
