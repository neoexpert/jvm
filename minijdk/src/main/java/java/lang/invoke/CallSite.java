package java.lang.invoke;

abstract public class CallSite {
	public CallSite(MethodHandle mh){
		this.target=mh;
	}
	private final MethodHandle target;
	public MethodHandle getTarget(){
		return target;
	}
}
