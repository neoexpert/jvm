package java.lang;
import java.util.*;

public class ThreadGroup{
	static ThreadGroup root = new ThreadGroup();
	private static Thread mainThread = new Thread(root, null, "main");
	final ThreadGroup parent;
	final String name;
	private int maxpri;
 
	private ThreadGroup(){
		name = "main";
		parent = null;
		maxpri = Thread.MAX_PRIORITY;
		init();
	}

	public ThreadGroup(String name){
		this(Thread.currentThread().group, name);
		init();
	}
	public ThreadGroup(ThreadGroup parent, String name)
  {
    //parent.checkAccess();
    this.parent = parent;
    this.name = name;
    maxpri = parent.maxpri;
    //daemon_flag = parent.daemon_flag;
		synchronized (parent)
		{
			//if (parent.groups == null)
			//	throw new IllegalThreadStateException();
		}
		init();
  }

	private final native void init();
	final native void addGroup(ThreadGroup group);
	final native void removeGroup(ThreadGroup group);
	final native void addThread(Thread thread);
	final native void removeThread(Thread thread);
}
