package java.lang;
import java.util.*;

public class Throwable{
	private String stacktraceAsString;
	private StackTraceElement[] stacktrace; 
	public final void printStackTrace(){
		System.err.println("Exception occured: ");
		System.err.println(getClass().getName());
		if(detailMessage!=null)
			System.err.println(detailMessage);
		if(stacktrace==null){
			System.err.println("no StackTrace provided");
			return;
		}
		for(StackTraceElement e:stacktrace){
			System.err.println(e.toString());
		}
	}
	public final native void fillInStackTrace();

	private String detailMessage="";
	private Throwable cause;

	public Throwable(){
		fillInStackTrace();
	}
	public Throwable(String message){
		fillInStackTrace();
		detailMessage = message;
	}
	public Throwable(String detailMessage,Throwable cause){
		fillInStackTrace();
		this.detailMessage = detailMessage;
		this.cause=cause;
	}
	public Throwable(Throwable cause){
		fillInStackTrace();
		this.cause=cause;
	}

	public String getMessage(){
		return detailMessage;
	}
	private List<Throwable> suppressedExceptions=new ArrayList<>();
	public final synchronized void addSuppressed(Throwable exception) {
		if (exception == this)
			throw new IllegalArgumentException("Self-suppression not permitted");

		if (exception == null)
			throw new NullPointerException("Cannot suppress a null exception");

		if (suppressedExceptions == null) // Suppressed exceptions not recorded
			return;

		suppressedExceptions.add(exception);
	}
	public void setStackTrace(StackTraceElement[] stackTrace){
		this.stacktrace=stackTrace;
	}
	public StackTraceElement[] getStackTrace(){
		return this.stacktrace;
	}
}
