package java.lang;

public class IllegalAccessException extends RuntimeException {
    public IllegalAccessException() {
        super();
    }
    public IllegalAccessException(String s) {
        super(s);
    }
}
