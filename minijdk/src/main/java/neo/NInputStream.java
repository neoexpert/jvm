package neo;

import java.io.*;

public class NInputStream extends InputStream {
	@Override
	public native int read();
	@Override
	public native int read(byte[] b, int off, int len) throws IOException;
}
