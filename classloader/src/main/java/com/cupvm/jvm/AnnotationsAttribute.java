package com.cupvm.jvm;

import com.cupvm.jvm.constants.Constant;

import java.nio.ByteBuffer;

public class AnnotationsAttribute {
	public final String[] annotations;
	public AnnotationsAttribute(final Attribute a, final Constant[] constantPool){
		ByteBuffer bb=ByteBuffer.wrap(a.info);
		int num_annotations=bb.getShort();
		annotations=new String[num_annotations];
		for(int i=0;i<num_annotations;++i){
			final short typeIndex = bb.getShort();
			final Constant c = constantPool[typeIndex];
			annotations[i]=c.str;
			final short numElementValuePairs = bb.getShort();
			break;
		}
	}
}
