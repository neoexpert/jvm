package com.cupvm.jvm._abstract;

import com.cupvm.jvm.LineNumberEntry;
import com.cupvm.jvm.MethodIntf;
import com.cupvm.jvm.method.MethodHead;
import com.cupvm.jvm.method.MethodType;

import java.util.ArrayList;
import java.util.List;

public abstract class AMethod extends AccessAble implements MethodIntf{
	protected MethodType mt;
	protected int id;
	public final int getModifiers() {
		return access_flags;
	}
	public final AClass clazz;
	protected String name;
	public char[] args;
	public int argsLength;
	public String nativeSignature =null;
	public String signature;
	protected String type;
	public boolean isNative;
	public int max_stack;
	public int max_locals;
	public MethodHead mh;
	//protected int paramscount;


	protected AMethod(AClass cl){
		this.clazz =cl;
	}

	protected AMethod(String signature, AClass cl){
		this.signature=signature;
		this.clazz =cl;
		args=null;
		access_flags = 0;
		type=null;
		this.mt=null;
	}

	protected AMethod(String signature, AClass cl, String nativeSignature){
		this.signature=signature;
		this.clazz =cl;
		isNative=true;
		this.nativeSignature = nativeSignature;
		args=null;
		access_flags = 0;
		type=null;
	}

	protected AMethod(String name, String type, AClass cl){
	    this.type=type;
		this.signature=name+type;
		this.clazz =cl;
		this.mt=new MethodType(type);
		mh =new MethodHead(cl.getName(),name, signature, mt);
		this.args= mh.mt.args;
		access_flags = 0;
	}

	public final boolean isAbstract() {
		return (access_flags & ACC_ABSTRACT) != 0;
	}

	public final boolean isStatic() {
		return (access_flags & ACC_STATIC) != 0;
	}

	public static void parseParams(String type, MethodType mt){
		//int paramscount = 0;
		type = type.substring(1);
		ArrayList<Character> params = new ArrayList<>(16);
		loop:while (true) {
			char c = type.charAt(0);
			switch (c) {
				case ')':
					mt.rtype=type.charAt(1);
					switch(mt.rtype){
						case 'V':
							mt.rsize=0;
							break;
						case 'D':
						case 'J':
							mt.rsize=2;
							break;
						default:
							mt.rsize=1;
					}
					break loop;
				case 'Z':
				case 'B':
				case 'C':
				case 'S':
				case 'I':
				case 'F':
					params.add(c);
					break;
				case 'D':
				case 'J':
					params.add(c);
					params.add(c);
					break;
				case 'L':
					params.add(c);
					type = type.substring(type.indexOf(';') + 1);
					continue;
				case '[':
					params.add(c);
					char at = type.charAt(1);
					while(at=='['){
						type=type.substring(1);
						at = type.charAt(1);

					}
					if(at=='L')
						type = type.substring(type.indexOf(';') + 1);
					else
						type = type.substring(2);
					continue;
			}
			type = type.substring(1);
		}
		//this.paramscount = paramscount;
		char[] args=new char[params.size()];
		for(int i=0;i<args.length;++i){
			args[i]=params.get(i);
		}
		mt.args=args;
	}

	public final String getNativeSignature(){
		return nativeSignature;
	}

	public final String getName() {
		return name;
	}

	public final AClass getMyClass() {
		return clazz;
	}

	@Override
	public final String toString() {
		return signature;
	}
	//public JNIMethod nativeHandler;

	public final void declareNative(){
		if(nativeSignature !=null)return;
		isNative=true;
		nativeSignature = (clazz.getName() + "." + signature).intern();
		clazz.nativeMethods.add(this);
	}

	@Override
	public String getSignature() {
		return signature;
	}

	public final void setID(int id){
		this.id=id;
	}

	@Override
	public final int getID() {
		return id;
	}

	@Override
	public List<? extends LineNumberEntry> getLineNumbers() {
		return null;
	}

	@Override
	public int getCodeLength() {
		return -1;
	}

	@Override
	public int getNextLineNumber(int pc) {
		return -1;
	}
}
