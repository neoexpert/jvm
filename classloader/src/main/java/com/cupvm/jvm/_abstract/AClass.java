package com.cupvm.jvm._abstract;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.method.*;

import java.util.*;

public abstract class AClass extends AccessAble {
	private static final Logger logger= ConsoleLogger.get();
	String codeSource;
	final ArrayList<AField> declaredFields=new ArrayList<>();
	public final HashMap<String, AField> fields=new HashMap<>();
	public final HashMap<String, AField> instanceFields=new HashMap<>();
	public final HashMap<String, AField> instanceRefs=new HashMap<>();
	public final HashMap<String, AField> staticFields=new HashMap<>();
	public final HashMap<String, AField> staticRefs=new HashMap<>();
	protected final HashMap<String, Integer> methodindex=new HashMap<>();
	protected final HashMap<Integer, Method> methodsbyid=new HashMap<>();
	protected final ArrayList<AMethod> nativeMethods=new ArrayList<>();
	protected final String name;
	protected final ClassLoader classLoader;
	public int refsCount=0;
	protected Number myoref;
	protected final ArrayList<Integer> references = new ArrayList<>();
	//private final ArrayList<String> static_references=new ArrayList<>();
	//public boolean hasStaticReferences;
	private String sourceFileName;

	public AClass(final String name, ClassLoader classLoader){
		this.name=name;
		this.classLoader=classLoader;
	}
	public final ClassLoader getClassLoader(){
		return classLoader;
	}

	public void setCodeSource(String codeSource){
		this.codeSource=codeSource;
	}

	public String getCodePath(){
		return codeSource;
	}

	public final boolean isSuper() {
		return (access_flags & ACC_SUPER) != 0;
	}

	public final boolean isInterface() {
		return (access_flags & ACC_INTERFACE) != 0;
	}

	public final boolean isAbstract() {
		return (access_flags & ACC_ABSTRACT) != 0;
	}


	public final boolean isAnnotation() {
		return (access_flags & ACC_ANNOTATION) != 0;
	}

	public final boolean isEnum() {
		return (access_flags & ACC_ENUM) != 0;
	}

	@Override
	public final boolean equals(Object obj) {
		if (obj instanceof AClass)
			return ((AClass) obj).getName().equals(getName());
		return false;
	}

	public final Number getMyInstanceRef() {
		return myoref;
	}
	public final void setMyInstenceRef(Number oref) {
		this.myoref = oref;
	}

	public boolean isInitialized;


	public boolean isInstance(AClass cl){
		return false;
	}

	public final String getName(){
		return name;
	}





	public final ArrayList<Integer> getReferenceNames() {
		return references;
	}

	protected AClass[] NO_INTERFACES=new AClass[0];
	public AClass[] getInterfaces(){
		return NO_INTERFACES;
	}

	public final String getSourceFileName(){
		return sourceFileName;
	}




	public HashMap<String,AField> getInstanceFields(){
		return instanceFields;
	}
	public HashMap<String,AField> getInstanceRefs(){
		return instanceRefs;
	}

	public final void addField(AField f){
		declaredFields.add(f);
		fields.put(f.getName(), f);
	}

	public void addMethod(final Method m){}

	public final void setSourceFileName(String sourceFilaName){
		this.sourceFileName=sourceFilaName;
	}

	public boolean isPrimitive(){
		return false;
	}

	public void createNatives(String ... signatures){
		for(String signature:signatures){
			AMethod m= getDeclaredMethod(signature);
			if(m==null){
			
				logger.warn("could not declare native Method: "+getName()+"."+signature);
				continue;
			}
			if(m.isNative)continue;
			m.isNative=true;
			m.nativeSignature =getName()+"."+signature;
			//AMethod m=new AMethod(signature, this, getName()+"."+signature){};
			nativeMethods.add(m);
		}	
	}

	public Number[] instanceTemplate;

	//@Override
	public boolean isInitialized(){
		return isInitialized;
	}

	//@Override
	public List<? extends MethodIntf> getDeclaredMethods() {
		ArrayList<AMethod> methods=new ArrayList<>();
		methods.addAll(methodsbyid.values());
		return methods;
	}

	public Map<Integer, Method> getMethods() {
		return methodsbyid;
	}

	public final HashMap<String, AField> getFields(){
		return fields;
	}

	public AField getDeclaredField(String name){
		return fields.get(name);
	}

	//@Override
	public MethodIntf getMethodByID(int methodID) {
		return methodsbyid.get(methodID);
	}

	//@Override
	public final ArrayList<AField> getDeclaredFields() {
		return declaredFields;
	}

	//@Override
	public AClass getSuperClass() {
		return null;
	}

	public abstract AMethod getMethod(String signature);
	public abstract int getID();
    public abstract AMethod getMethodHead(String s);
	public abstract AMethod getDeclaredMethod(String signature);

	public ArrayList<AMethod> getNativeMethods() {
		return nativeMethods;
	}
}
