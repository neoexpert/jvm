package com.cupvm.jvm.method;

import com.cupvm.jvm.Attribute;
import com.cupvm.jvm.constants.Constant;

import java.nio.ByteBuffer;

public class CodeAttribute extends Attribute {

	private static final boolean printparams=false;
	private LineNumberTableAttribute line_number_table;
	public final LineNumberTableAttribute getLineNumberTableAttribute(){
		return line_number_table;
	}

	private LocalVariableTableAttribute local_variable_table;

	private LocalVariableTypeTableAttribute local_variable_type_table;


	public CodeAttribute(Attribute a, Constant[] constantPool) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		max_stack = buf.getChar();
		max_locals = buf.getChar();
		int code_length = buf.getInt();
		byte[] code = new byte[code_length];
		short[] scode=new short[code_length];
		buf.get(code);
		for(int i=0;i<code_length;++i)
			scode[i]=(short)(code[i]&0xFF);
		this.code=scode;
		this.bcode=code;
		//for (int i = 0; i < code_length; ++i)
		//	code[i] = buf.get();
		char exception_table_length = buf.getChar();

		MException[] exception_table = new MException[exception_table_length];
		this.exception_table=exception_table;
		for (int i = 0; i < exception_table_length; ++i) {
			exception_table[i] = new MException(buf);
		}
		char attributes_count = buf.getChar();
		//attrs = new Attribute[attributes_count];
		for (int ix = 0; ix < attributes_count; ++ix) {
			Attribute attr = new Attribute(buf);
			int i = attr.attribute_name_index;
			String name = constantPool[i].str;
			switch(name){
				case "LineNumberTable":
					line_number_table = new LineNumberTableAttribute(attr);
					break;
				case "LocalVariableTable":
					local_variable_table = new LocalVariableTableAttribute(attr);
					break;
				case "LocalVariableTypeTable":
					local_variable_type_table = new LocalVariableTypeTableAttribute(attr);
					break;
			}

		}
		//delete info
		info=null;
	}
	public final MException[] getExceptionTable() {
		return exception_table;
	}
	public final int max_stack;
	public final int max_locals;
	public final short[] code;
	public final byte[] bcode;
	private int[] params;
	int code_type;
	public final Object getCode(){
		return code;
	}

	public final int getCurrentLine(int pc) {
		return line_number_table.getLineNumber(pc);
	}

	final MException checkException(int pc) {
		for (MException e : exception_table)
			if (e.start_pc <= pc && e.end_pc >= pc)
				return e;
		return null;
	}

	private final MException[] exception_table;
	//Attribute[] attrs;


	String cs;


	private static int i(int b1, int b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(int b1, int b2, int b3, int b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private static String byteToHex(int b) {
		StringBuilder sb=new StringBuilder();
		int v = b & 0xFF;
		sb.append(hexArray[v >>> 4]);
		sb.append(hexArray[v & 0x0F]);

		return sb.toString();
	}


}
