package com.cupvm.jvm.method;
import java.util.*;

import com.cupvm.jvm.OPs;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.field.FieldHead;

public class Preprocessor implements OPs {
		private Method m;
		private short[] code;
		private byte[] processed;
		private ArrayList<StackMapEntry> stackMapTable;
		private Constant[] cpool;
	public Preprocessor(final Method m){
		this.m=m;
		this.code = m.code;
		stackMapTable=new ArrayList<>();
		processed=new byte[code.length];
		cpool=m.clazz.constantPool;
	}
	public static void processcode(Method m){
		Preprocessor cpf=new Preprocessor(m);
		cpf.processcode(0, 0, null, null);
		m.stackMap=cpf.stackMapTable;
	}
private int processcode(int pc, int stackpos, byte[] lv, byte[] opstack){
	Method m=this.m;
	short[] code=this.code;
	int length=code.length;
	byte[] processed=this.processed;
	if(processed[pc]==1)return 0;
	short index;
	short jump16;
	Constant c;
	if(lv==null)
		lv = new byte[m.max_locals];
	else{
		byte[] lv_copy = new byte[m.max_locals];
		System.arraycopy(lv,0,lv_copy,0,lv.length);
		lv = lv_copy;
	}
	if(opstack==null)
		opstack = new byte[m.max_stack];
	else{
		byte[] opstack_copy = new byte[m.max_stack];
		System.arraycopy(opstack,0,opstack_copy,0,opstack.length);
		opstack = opstack_copy;
	}

	byte i1,i2,i3,i4;
	MethodType mt=m.mh.mt;
	if(pc==0){
		int argsoffset=0;
		if(!m.isStatic()){
			lv[argsoffset++]=1;
		}
		for(int i=0; i<mt.args.length;++i){
			char arg=mt.args[i];
			switch(arg){
				case 'L':
				case '[':
					lv[argsoffset++]=1;
					break;
				default:
					lv[argsoffset++]=0;
					break;
			}
		}
		addStackEntry(pc, lv, opstack);
	}
	mainLoop:
	do {
		if(stackpos<0||stackpos>m.max_stack){
			throw new RuntimeException("error, operand stackoverflow "+stackpos);
		}
		if(processed[pc]==1){
			return 0;
		}
		processed[pc]=1;
		switch (code[pc]) {
			case NOP:
				pc += 1;
				continue;
			case ACONST_NULL:
			case ICONST_M1:
			case ICONST_0:
			case ICONST_1:
			case ICONST_2:
			case ICONST_3:
			case ICONST_4:
			case ICONST_5:
			case FCONST_0:
			case FCONST_1:
			case FCONST_2:
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case LCONST_0:
			case LCONST_1:
			case DCONST_0:
			case DCONST_1:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case BIPUSH:
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case SIPUSH:
				opstack[stackpos++]=0;
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif*/
				pc += 3;
				continue;
			case LDC:
				c=cpool[code[pc+1]];
				switch(c.tag){
					default:
						opstack[stackpos++]=0;
						break;
					case Constant.CString:
					case Constant.CClass:
						opstack[stackpos++]=1;
						addStackEntry(pc, lv, opstack);
				}
				pc += 2;
				continue;
			case LDC_W:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif*/
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c=cpool[index];
				switch(c.tag){
					default:
						opstack[stackpos++]=0;
						break;
					case Constant.CString:
					case Constant.CClass:
						opstack[stackpos++]=1;
						addStackEntry(pc, lv, opstack);
				}
				pc += 3;
				continue;
			case LDC2_W:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				pc += 3;
				continue;
			case ILOAD:
			case FLOAD:
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case LLOAD:
			case DLOAD:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case ALOAD:
				opstack[stackpos++]=1;
				pc += 2;
				continue;
			case ILOAD_0:
			case FLOAD_0:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_1:
			case FLOAD_1:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_2:
			case FLOAD_2:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_3:
			case FLOAD_3:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_0:
			case DLOAD_0:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_1:
			case DLOAD_1:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_2:
			case DLOAD_2:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_3:
			case DLOAD_3:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ALOAD_0:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_1:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_2:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_3:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case IALOAD:
			case FALOAD:
			case BALOAD:
			case CALOAD:
			case SALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LALOAD:
			case DALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case AALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ISTORE:
			case FSTORE:
				lv[code[pc+1]]=0;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case LSTORE:
			case DSTORE:
				lv[code[pc+1]]=0;
				lv[code[pc+1]+1]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case ASTORE_0:
				lv[0]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_1:
				lv[1]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_2:
				lv[2]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_3:
				lv[3]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE:
				lv[code[pc+1]]=1;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case LSTORE_0:
			case DSTORE_0:
				lv[0]=0;
				lv[1]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_1:
			case DSTORE_1:
				lv[1]=0;
				lv[2]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_2:
			case DSTORE_2:
				lv[2]=0;
				lv[3]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_3:
			case DSTORE_3:
				lv[3]=0;
				lv[4]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_0:
			case FSTORE_0:
				lv[0]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_1:
			case FSTORE_1:
				lv[1]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_2:
			case FSTORE_2:
				lv[2]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_3:
			case FSTORE_3:
				lv[3]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case IASTORE:
			case FASTORE:
			case AASTORE:
			case CASTORE:
			case SASTORE:
			case BASTORE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LASTORE:
			case DASTORE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case POP:
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case POP2:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case DUP:
				opstack[stackpos]=opstack[stackpos-1];
				++stackpos;
				pc += 1;
				continue;
			case DUP_X1:
				i1 = opstack[stackpos-1];
				i2 = opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				++stackpos;
				pc += 1;
				continue;
			case DUP_X2:
				i1 = opstack[stackpos-1];
				i2 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				++stackpos;
				pc += 1;
				continue;
			case DUP2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X1:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i2;
				opstack[stackpos-1]=i3;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i4 = opstack[stackpos-3];
				i3 = opstack[stackpos-4];
				opstack[stackpos-4]=i1;
				opstack[stackpos-3]=i2;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i4;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case SWAP:
				i1=opstack[stackpos-1];
				i2=opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				pc += 1;
				continue;
			case IADD:
			case FADD:
			case ISUB:
			case FSUB:
			case IMUL:
			case FMUL:
			case IDIV:
			case FDIV:
			case IREM:
			case FREM:
			case ISHL:
			case ISHR:
			case IUSHR:
			case IAND:
			case IOR:
			case IXOR:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case LADD:
			case DADD:
			case LSUB:
			case DSUB:
			case LMUL:
			case DMUL:
			case LDIV:
			case DDIV:
			case LREM:
			case DREM:
			case LAND:
			case LOR:
			case LXOR:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case LSHL:
			case LSHR:
			case LUSHR:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case INEG:
			case LNEG:
			case FNEG:
			case DNEG:
				pc += 1;
				continue;
			case IINC:
				pc += 3;
				continue;
			case I2L:
			case I2D:
			case F2L:
			case F2D:
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case L2I:
			case L2F:
			case D2I:
			case D2F:
				opstack[--stackpos]=0;
				pc+=1;
				continue;
			case I2F:
			case L2D:
			case F2I:
			case D2L:
			case I2B:
			case I2C:
			case I2S:
				pc += 1;
				continue;
			case LCMP:
			case DCMPL:
			case DCMPG:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case FCMPL:
			case FCMPG:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case IFEQ:
			case IFNE:
			case IFLT:
			case IFGE:
			case IFGT:
			case IFLE:
				opstack[--stackpos]=0;

				jump16 = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				/*
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						int res=processcode(pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc+=3;
				continue;
			case IF_ICMPEQ:
			case IF_ICMPNE:
			case IF_ICMPLT:
			case IF_ICMPGE:
			case IF_ICMPGT:
			case IF_ICMPLE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				jump16 = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				//code[pc+1]=jump16;
				/*
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case IF_ACMPEQ:
			case IF_ACMPNE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				jump16 = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				/*
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case GOTO:
				jump16 = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				/*
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					/*
					if(pc+jump16<length){
						int res=processcode(m, cpool, pc+3, stackpos, pc+jump16, lv, opstack); 
						if(res!=0) return res;
					}*/
					pc+=jump16;
					continue;
				//}
				//else return 0;
				//pc+=3;
				//continue;
			case JSR:
				opstack[stackpos++]=0;
				++pc;
				continue;
			case RET:
				opstack[--stackpos]=0;
				++pc;
				continue;
			case TABLESWITCH:
				opstack[--stackpos]=0;
				int origpc=pc;
        pc = (pc + 4)&0xfffffffc;
				int default_offset = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
				/*
#ifdef LITTLE_ENDIAN
				default_offset =__builtin_bswap32(default_offset);
					*((int32_t*)(code+pc))=
						default_offset;
#endif
*/
					res=processcode(origpc+default_offset , stackpos, lv, opstack); 
					if(res!=0) return res;

				pc += 4;
				int low = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
				/*
#ifdef LITTLE_ENDIAN
				low=__builtin_bswap32(low);
					*(int32_t*)(code+pc)=
						low;
#endif
*/
				pc += 4;
				int high = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
				/*
#ifdef LITTLE_ENDIAN
				high=__builtin_bswap32(high);
					*(int32_t*)(code+pc)=
						high;
#endif
*/
				pc += 4;
				int pc2=pc;
				for(int i=low;i<=high;++i){
					//printf("case %d\n",i);
					pc=pc2 + (i - low) * 4;
					int jump32 = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);

					/*
#ifdef LITTLE_ENDIAN
					jump32 =__builtin_bswap32(jump32);
					*((int32_t*)(code+pc))=jump32;
#endif
*/
					res=processcode(origpc+jump32, stackpos, lv, opstack); 
					if(res!=0) return res;
				}
				pc+=8;
				
				return 0;
			case LOOKUPSWITCH:
				opstack[--stackpos]=0;
				origpc=pc;
        			pc = (pc + 4)&0xfffffffc;
				default_offset = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
				/*
#ifdef LITTLE_ENDIAN
				default_offset =__builtin_bswap32(default_offset);
					*((int32_t*)(code+pc))=
						default_offset;
#endif
*/
					res=processcode(origpc+default_offset, stackpos, lv, opstack); 
					if(res!=0) return res;
					pc += 4;

					//unsigned
					int len = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
					/*
#ifdef LITTLE_ENDIAN
					len =__builtin_bswap32(len);
					*((int32_t*)(code+pc))=
						len;
#endif
*/
					pc += 4;
					for (int i = 0; i < len; ++i) {
						int key = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
						/*
#ifdef LITTLE_ENDIAN
						key =__builtin_bswap32(key);
						*((int32_t*)(code+pc))=
							key;
#endif
*/
						pc += 4;
						int value = ((code[pc]) << 24) | ((code[pc+1]) << 16) | ((code[pc + 2]) << 8) | (code[pc + 3]);
						/*
#ifdef LITTLE_ENDIAN
						value =__builtin_bswap32(value);
						*((int32_t*)(code+pc))=
							value;
						int res=processcode(cpf, origpc+value, stackpos, lv, opstack); 
						if(res!=0) return res;
#endif
*/
						pc += 4;
					}
				return 0;
			case IRETURN:
			case FRETURN:
			case ARETURN:
				opstack[--stackpos]=0;
				pc += 1;
				return 0;
			case LRETURN:
			case DRETURN:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				return 0;
			case RETURN:
				pc += 1;
				return 0;
			case GETSTATIC:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				addStackEntry(pc, lv, opstack);
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c=cpool[index];
				FieldHead fh=(FieldHead)c.data;
				switch(fh.type){
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					default:
						throw new RuntimeException("unknown field type: %c\n"+fh.type);
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case PUTSTATIC:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				addStackEntry(pc, lv, opstack);
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c=cpool[index];
				fh=(FieldHead)c.data;
				//fprintln(stderr, fh->classname);
				//fprintf(stderr, "%c\n",fh->type->str[0]);
				switch(fh.type){
					case 'J':
					case 'D':
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'L':
					case '[':
					case 'Z':
						opstack[--stackpos]=0;
						break;
					default:
						throw new RuntimeException("unknown field type: %c\n"+fh.type);
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case GETFIELD:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				//fprintf(stderr, "PRE GETFIELD index: %d\n",index);

				addStackEntry(pc, lv, opstack);
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c=cpool[index];
				fh=(FieldHead)c.data;
				opstack[--stackpos]=0;
				switch(fh.type){
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					default:
						throw new RuntimeException("unknown field type: %c\n"+fh.type);
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case PUTFIELD:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				addStackEntry(pc, lv, opstack);

				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c=cpool[index];
				fh=(FieldHead)c.data;
				//fprintln(stderr, fh->classname);
				//fprintf(stderr, "%c\n",fh->type->str[0]);
				opstack[--stackpos]=0;
				switch(fh.type){
					case 'J':
					case 'D':
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'L':
					case '[':
					case 'Z':
						opstack[--stackpos]=0;
						break;
					default:
						throw new RuntimeException("unknown field type: %c\n"+fh.type);
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case INVOKEVIRTUAL:
			case INVOKESPECIAL:
				opstack[--stackpos]=0;
			case INVOKESTATIC:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c = cpool[index];
				MethodHead mh=(MethodHead)c.data;
				mt=mh.mt;

				//uint8_t returnType=c->ivalue;
				//signature->str[signature->length-1];
				/*
				fprint(stderr,mh->classname);
				fprintf(stderr,".");
				fprintln(stderr,mh->signature);
				fprintf(stderr, "argsCount: %d\n",mt->argsLength);
				*/
				for(int i=0;i<mt.args.length;++i){
					opstack[--stackpos]=0;
				}
				switch(mt.rtype){
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					case 'V':
						break;
					default:
						throw new RuntimeException("unknown return type: "+mt.rtype);

				}
				//fprintf(stderr, "%c\n",mt->returnType);
				pc += 3;
				addStackEntry(pc, lv, opstack);
				continue;
			case INVOKEINTERFACE:
				opstack[--stackpos]=0;
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				c = cpool[index];
				mh=(MethodHead)c.data;
				mt=mh.mt;
				/*
				fprint(stderr,mh->classname);
				fprintf(stderr,".");
				fprintln(stderr,mh->signature);
				*/
				for(int i=0;i<mt.args.length;++i){
					opstack[--stackpos]=0;
				}
				switch(mt.rtype){
					case 'L':
						opstack[stackpos++]=1;
						break;
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					case 'V':
						break;
					default:
						throw new RuntimeException("unknown return type: "+mt.rtype);


				}

				pc += 5;
				addStackEntry(pc, lv, opstack);
				continue;
			case INVOKEDYNAMIC:
				pc+=5;
				continue;
			case NEW:
				addStackEntry(pc, lv, opstack);
				opstack[stackpos++]=1;
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				pc += 3;
				continue;
			case NEWARRAY:
				addStackEntry(pc, lv, opstack);
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 2;
				continue;
			case ANEWARRAY:
				addStackEntry(pc, lv, opstack);
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 3;
				continue;
			case ARRAYLENGTH:
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ATHROW:
				//stackpos=0;
				addStackEntry(pc, lv, opstack);
				pc += 1;
				return 0;
			case CHECKCAST:
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				pc += 3;
				continue;
			case INSTANCEOF:
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/
				pc += 3;
				continue;
			case MONITORENTER:
			case MONITOREXIT:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case WIDE:
				pc += 1;
				switch (code[pc]) {
					case ILOAD:
					case FLOAD:
						opstack[stackpos++]=0;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
*/
						pc += 3;
						continue;
					case LLOAD:
					case DLOAD:
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
*/
						pc += 3;
						continue;
					case ALOAD:
						opstack[stackpos++]=1;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
*/
						pc += 3;
						continue;
					case ISTORE:
					case FSTORE:
						opstack[--stackpos]=0;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endi
*/
						pc += 3;
						continue;
					case LSTORE:
					case DSTORE:
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
*/
						pc += 3;
						continue;
					case ASTORE:
						opstack[--stackpos]=0;
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
*/
						index = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
						lv[index]=1;
						pc += 3;
						continue;
					case IINC:
						/*
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;

						index=*((uint16_t*)(code+pc+3));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+3))=index;
#endif
*/
						pc += 5;
						continue;

				}

				throw new RuntimeException("wide error");
				//continue;
			case MULTIANEWARRAY:
				addStackEntry(pc, lv, opstack);
				{
				int dimensions=code[pc + 3];
				//int32_t * sizes = malloc(4*dimensions);
				for (int i= 0; i < dimensions; ++i) {
					opstack[--stackpos]=0;
				}
				opstack[stackpos++]=1;
				}
				/*
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				pc += 4;
				continue;
			case IFNULL:
			case IFNONNULL:
				opstack[--stackpos]=0;
				jump16 = (short) (((code[pc + 1]) << 8) | (code[pc + 2]));
				/*
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
*/

				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(pc+jump16, stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case GOTO_W:
			case JSR_W:
				//pc +=((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
					//((code[pc + 3]) << 8) | (code[pc + 4]) ;
					pc+=5;
				continue;
			default:
				pc += 1;
				//printf("bigtolittle unsupported opcode: %d\n", code[pc]);
				throw new RuntimeException("unsupported instruction");
		}
	}while (pc<length);
	//free(lv);free(opstack);
	return 0;
}

void addStackEntry(int pc, byte[] lv, byte[] opstack){
	StackMapEntry sme = new StackMapEntry();
	sme.pc=pc;
	sme.opstack = new byte[m.max_stack];
	System.arraycopy(opstack,0,sme.opstack,0,opstack.length);
	sme.lv = new byte[m.max_locals];
	System.arraycopy(lv,0,sme.lv,0,lv.length);
/*
#ifdef VERBOSED
	fprintln(stderr, m->signature);
	fprintf(stderr, "pc = %d: \n",pc);
	fprintf(stderr, "lv_map: ");
	for(int i=0;i<m->max_lv;++i){
		fprintf(stderr, "%d, ", lv[i]);
	}
	fprintf(stderr, "\nopstack_map: ");
	for(int i=0;i<m->max_stack;++i){
		fprintf(stderr, "%d, ", opstack[i]);
	}
	fprintf(stderr, "\n");

	fprintf(stderr, "before:\n");
	//printStackMapTable(m, stackMapTable);
#endif
*/
	for(int i=0;i<stackMapTable.size();++i){
		StackMapEntry _sme=stackMapTable.get(i);
		if(_sme.pc>=sme.pc){
			stackMapTable.add(i, sme);
			/*
#ifdef VERBOSED
			fprintf(stderr, "after addAt:\n");
			//printStackMapTable(m, stackMapTable);
#endif
*/
			return;
		}

	}
	stackMapTable.add(sme);
			return;
}

}
