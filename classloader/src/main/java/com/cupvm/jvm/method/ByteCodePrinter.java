package com.cupvm.jvm.method;


import com.cupvm.jvm.OPs;

public class ByteCodePrinter implements OPs {

	private static final boolean printparams=false;

	private static String parseCode(short[] code) {
		StringBuilder cs = new StringBuilder();
		int pc = 0;
		while (pc < code.length) {
			//StringBuilder cs = new StringBuilder();
			pc += parseOp(pc, code, cs);
			cs.append("\n");
			//ops.add(cs.toString());
		}
		return cs.toString();
	}

	public static int parseOp(int pc, short[] code, StringBuilder cs) {
		//short[] code= (short[]) _code;
		short op = code[pc];
		switch (op) {
			case (short) NOP:
				cs.append("nop");
				return 1;
			case (byte) ACONST_NULL:
				cs.append("aconst_null");
				return 1;
			case (short) ICONST_M1:
				cs.append("iconst_m1");
				return 1;
			case (short) ICONST_0:
				cs.append("iconst_0");
				return 1;
			case (short) ICONST_1:
				cs.append("iconst_1");
				return 1;
			case (short) ICONST_2:
				cs.append("iconst_2");
				return 1;
			case (short) ICONST_3:
				cs.append("iconst_3");
				return 1;
			case (short) ICONST_4:
				cs.append("iconst_4");
				return 1;
			case (short) ICONST_5:
				cs.append("iconst_5");
				return 1;
			case (short) LCONST_0:
				cs.append("lconst_0");
				return 1;
			case (short) LCONST_1:
				cs.append("lconst_1");
				return 1;
			case (short) FCONST_0:
				cs.append("fconst_0");
				return 1;
			case (short) FCONST_1:
				cs.append("fconst_1");
				return 1;
			case (short) FCONST_2:
				cs.append("fconst_2");
				return 1;
			case (short) DCONST_0:
				cs.append("dconst_0");
				return 1;
			case (short) DCONST_1:
				cs.append("dconst_1");
				return 1;
			case (short) BIPUSH:
				cs.append("bipush ");
				if(printparams)
					cs.append(code[pc + 1]);
				return 2;
			case (short) SIPUSH:
				cs.append("sipush ");
				if(printparams)
					cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) LDC:
				cs.append("ldc ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) LDC_W:
				cs.append("ldc_w ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) LDC2_W:
				cs.append("ldc2_w ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) ILOAD:
				cs.append("iload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) LLOAD:
				cs.append("lload ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 1] + 1);
				return 2;
			case (short) FLOAD:
				cs.append("fload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) DLOAD:
				cs.append("dload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) ALOAD:
				cs.append("aload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) ILOAD_0:
				cs.append("iload_0");
				return 1;
			case (short) ILOAD_1:
				cs.append("iload_1");
				return 1;
			case (short) ILOAD_2:
				cs.append("iload_2");
				return 1;
			case (short) ILOAD_3:
				cs.append("iload_3");
				return 1;
			case (short) LLOAD_0:
				cs.append("lload_0");
				return 1;
			case (short) LLOAD_1:
				cs.append("lload_1");
				return 1;
			case (short) LLOAD_2:
				cs.append("lload_2");
				return 1;
			case (short) LLOAD_3:
				cs.append("lload_3");
				return 1;
			case (short) FLOAD_0:
				cs.append("fload_0");
				return 1;
			case (short) FLOAD_1:
				cs.append("fload_1");
				return 1;
			case (short) FLOAD_2:
				cs.append("fload_2");
				return 1;
			case (short) FLOAD_3:
				cs.append("fload_3");
				return 1;
			case (short) DLOAD_0:
				cs.append("dload_0");
				return 1;
			case (short) DLOAD_1:
				cs.append("dload_1");
				return 1;
			case (short) DLOAD_2:
				cs.append("dload_2");
				return 1;
			case (short) DLOAD_3:
				cs.append("dload_3");
				return 1;
			case (short) ALOAD_0:
				cs.append("aload_0");
				return 1;
			case (short) ALOAD_1:
				cs.append("aload_1");
				return 1;
			case (short) ALOAD_2:
				cs.append("aload_2");
				return 1;
			case (short) ALOAD_3:
				cs.append("aload_3");
				return 1;
			case (short) IALOAD:
				cs.append("iaload");
				return 1;
			case (short) LALOAD:
				cs.append("laload");
				return 1;
			case (short) FALOAD:
				cs.append("faload");
				return 1;
			case (short) DALOAD:
				cs.append("daload");
				return 1;
			case (short) AALOAD:
				cs.append("aaload");
				return 1;
			case (short) BALOAD:
				cs.append("baload");
				return 1;
			case (short) CALOAD:
				cs.append("caload");
				return 1;
			case (short) SALOAD:
				cs.append("saload");
				return 1;
			case (short) ISTORE:
				cs.append("istore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) LSTORE:
				cs.append("lstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) FSTORE:
				cs.append("fstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) DSTORE:
				cs.append("dstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) ASTORE:
				cs.append("astore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) ISTORE_0:
				cs.append("istore_0");
				return 1;
			case (short) ISTORE_1:
				cs.append("istore_1");
				return 1;
			case (short) ISTORE_2:
				cs.append("istore_2");
				return 1;
			case (short) ISTORE_3:
				cs.append("istore_3");
				return 1;
			case (short) LSTORE_0:
				cs.append("lstore_0");
				return 1;
			case (short) LSTORE_1:
				cs.append("lstore_1");
				return 1;
			case (short) LSTORE_2:
				cs.append("lstore_2");
				return 1;
			case (short) LSTORE_3:
				cs.append("lstore_3");
				return 1;

			case (short) FSTORE_0:
				cs.append("fstore_0");
				return 1;
			case (short) FSTORE_1:
				cs.append("fstore_1");
				return 1;
			case (short) FSTORE_2:
				cs.append("fstore_2");
				return 1;
			case (short) FSTORE_3:
				cs.append("fstore_3");
				return 1;
			case (short) DSTORE_0:
				cs.append("dstore_0");
				return 1;
			case (short) DSTORE_1:
				cs.append("dstore_1");
				return 1;
			case (short) DSTORE_2:
				cs.append("dstore_2");
				return 1;
			case (short) DSTORE_3:
				cs.append("dstore_3");
				return 1;
			case (short) ASTORE_0:
				cs.append("astore_0");
				return 1;
			case (short) ASTORE_1:
				cs.append("astore_1");
				return 1;
			case (short) ASTORE_2:
				cs.append("astore_2");
				return 1;
			case (short) ASTORE_3:
				cs.append("astore_3");
				return 1;
			case (short) IASTORE:
				cs.append("iastore");
				return 1;
			case (short) LASTORE:
				cs.append("lastore");
				return 1;
			case (short) FASTORE:
				cs.append("fastore");
				return 1;
			case (short) DASTORE:
				cs.append("dastore");
				return 1;
			case (short) AASTORE:
				cs.append("aastore");
				return 1;
			case (short) BASTORE:
				cs.append("bastore");
				return 1;
			case (short) CASTORE:
				cs.append("castore");
				return 1;
			case (short) SASTORE:
				cs.append("sastore");
				return 1;
			case (short) POP:
				cs.append("pop");
				return 1;
			case (short) POP2:
				cs.append("pop2");
				return 1;
			case (short) DUP:
				cs.append("dup");
				return 1;
			case (short) DUP_X1:
				cs.append("dup_x1");
				return 1;
			case (short) DUP_X2:
				cs.append("dup_x2");
				return 1;
			case (short) DUP2:
				cs.append("dup2");
				return 1;
			case (short) DUP2_X1:
				cs.append("dup2_x1");
				return 1;
			case (short) DUP2_X2:
				cs.append("dup2_x2");
				return 1;
			case (short) SWAP:
				cs.append("swap");
				return 1;
			case (short) IADD:
				cs.append("iadd");
				return 1;
			case (short) LADD:
				cs.append("ladd");
				return 1;
			case (short) FADD:
				cs.append("fadd");
				return 1;
			case (short) DADD:
				cs.append("dadd");
				return 1;
			case (short) ISUB:
				cs.append("isub");
				return 1;
			case (short) LSUB:
				cs.append("lsub");
				return 1;
			case (short) FSUB:
				cs.append("fsub");
				return 1;
			case (short) DSUB:
				cs.append("dsub");
				return 1;
			case (short) IMUL:
				cs.append("imul");
				return 1;
			case (short) LMUL:
				cs.append("lmul");
				return 1;
			case (short) FMUL:
				cs.append("fmul");
				return 1;
			case (short) DMUL:
				cs.append("dmul");
				return 1;
			case (short) IDIV:
				cs.append("idiv");
				return 1;
			case (short) LDIV:
				cs.append("ldiv");
				return 1;
			case (short) FDIV:
				cs.append("fdiv");
				return 1;
			case (short) DDIV:
				cs.append("ddiv");
				return 1;
			case (short) IREM:
				cs.append("irem");
				return 1;
			case (short) LREM:
				cs.append("lrem");
				return 1;
			case (short) FREM:
				cs.append("frem");
				return 1;
			case (short) DREM:
				cs.append("drem");
				return 1;
			case (short) INEG:
				cs.append("ineg");
				return 1;
			case (short) LNEG:
				cs.append("lneg");
				return 1;
			case (short) FNEG:
				cs.append("fneg");
				return 1;
			case (short) DNEG:
				cs.append("dneg");
				return 1;
			case (short) ISHL:
				cs.append("ishl");
				return 1;
			case (short) LSHL:
				cs.append("lshl");
				return 1;
			case (short) ISHR:
				cs.append("ishr");
				return 1;
			case (short) LSHR:
				cs.append("lshr");
				return 1;
			case (short) IUSHR:
				cs.append("iushr");
				return 1;
			case (short) LUSHR:
				cs.append("lushr");
				return 1;
			case (short) IAND:
				cs.append("iand");
				return 1;
			case (short) LAND:
				cs.append("land");
				return 1;
			case (short) IOR:
				cs.append("ior");
				return 1;
			case (short) LOR:
				cs.append("lor");
				return 1;
			case (short) IXOR:
				cs.append("ixor");
				return 1;
			case (short) LXOR:
				cs.append("lxor");
				return 1;
			case (short) IINC:
				cs.append("iinc ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) I2L:
				cs.append("i2l ");
				return 1;
			case (short) I2F:
				cs.append("i2f");
				return 1;
			case (short) I2D:
				cs.append("i2d ");
				return 1;
			case (short) L2I:
				cs.append("l2i ");
				return 1;
			case (short) L2F:
				cs.append("l2f ");
				return 1;
			case (short) L2D:
				cs.append("l2d ");
				return 1;
			case (short) F2I:
				cs.append("f2i ");
				return 1;
			case (short) F2L:
				cs.append("f2l ");
				return 1;
			case (short) F2D:
				cs.append("f2d ");
				return 1;
			case (short) D2I:
				cs.append("d2i ");
				return 1;
			case (short) D2L:
				cs.append("d2l ");
				return 1;
			case (short) D2F:
				cs.append("d2f ");
				return 1;
			case (short) I2B:
				cs.append("i2b ");
				return 1;
			case (short) I2C:
				cs.append("i2c ");
				return 1;
			case (short) I2S:
				cs.append("i2s ");
				return 1;
			case (short) LCMP:
				cs.append("lcmp ");
				return 1;
			case (short) FCMPL:
				cs.append("fcmpl ");
				return 1;
			case (short) FCMPG:
				cs.append("fcmpg ");
				return 1;
			case (short) DCMPL:
				cs.append("dcmpl ");
				return 1;
			case (short) DCMPG:
				cs.append("dcmpg ");
				return 1;
			case (short) IFEQ:
				cs.append("ifeq ");
				return 3;
			case (short) IFNE:
				cs.append("ifne ");
				return 3;
			case (short) IFLT:
				cs.append("iflt ");
				return 3;
			case (short) IFGE:
				cs.append("ifge ");
				return 3;
			case (short) IFGT:
				cs.append("ifgt ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (short) IFLE:
				cs.append("ifle ");
				return 3;
			case (short) IF_ICMPEQ:
				cs.append("if_icmpeq ");
				return 3;
			case (short) IF_ICMPNE:
				cs.append("if_icmpne ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (short) IF_ICMPLT:
				cs.append("if_icmplt ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (short) IF_ICMPGE:
				cs.append("if_icmpge ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) IF_ICMPGT:
				cs.append("if_icmpgt ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) IF_ICMPLE:
				cs.append("if_icmple ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) IF_ACMPEQ:
				cs.append("if_acmpeq ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) IF_ACMPNE:
				cs.append("if_acmpne ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) GOTO:
				cs.append("goto ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) JSR:
				cs.append("jsr ");
				if(printparams)
					cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) RET:
				cs.append("ret ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) TABLESWITCH:
				cs.append("tableswitch ");
				int pc2 = pc + (4 - pc % 4);
				//int default_offset=pc(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				int low = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				int high = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				int n = high - low + 1;
				pc2 += n * 4;
				return pc2 - pc + 1;
			case (short) LOOKUPSWITCH:
				cs.append("lookupswitch ");
				pc2 = pc + (4 - pc % 4);

				//int default_offset=pc(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				n = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				for (pc = 0; pc < n; ++pc) {
					//int key = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					//int value = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
				}
				return pc2 - pc + 1;
			case (short) IRETURN:
				cs.append("ireturn");
				return 1;
			case (short) LRETURN:
				cs.append("lreturn");
				return 1;
			case (short) FRETURN:
				cs.append("freturn");
				return 1;
			case (short) DRETURN:
				cs.append("dreturn");
				return 1;
			case (short) ARETURN:
				cs.append("areturn");
				return 1;
			case (short) RETURN:
				cs.append("return");
				return 1;
			case (short) GETSTATIC:
				cs.append("getstatic ");
				if(printparams)
						cs.append((short)i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) PUTSTATIC:
				cs.append("putstatic ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) GETFIELD:
				cs.append("getfield ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) PUTFIELD:
				cs.append("putfield ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) INVOKEVIRTUAL:
				cs.append("invokevirtual ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) INVOKESPECIAL:
				cs.append("invokespecial ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) INVOKESTATIC:
				cs.append("invokestatic ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2])).append(" ");
				return 3;
			case (short) INVOKEINTERFACE:
				cs.append("invokeinterface ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2])).append(" ");
				return 5;
			case (short) INVOKEDYNAMIC:
				cs.append("invokedynamic ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2])).append(" ");
				return 5;
			case (short) NEW:
				cs.append("new ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (short) NEWARRAY:
				cs.append("newarray ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (short) ANEWARRAY:
				cs.append("anewarray ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) ARRAYLENGTH:
				cs.append("arraylength ");
				return 1;
			case (short) ATHROW:
				cs.append("athrow");
				return 1;
			case (short) CHECKCAST:
				cs.append("checkcast");
				return 3;
			case (short) INSTANCEOF:
				cs.append("instanceof ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2]));
				return 3;
			case (short) MONITORENTER:
				cs.append("monitorenter");
				return 1;
			case (short) MONITOREXIT:
				cs.append("monitorexit");
				return 1;
			case (short) WIDE:
				cs.append("wide");
				if (code[pc + 1] == 0x3A)
					return 5;
				else
					return 3;
			case (short) MULTIANEWARRAY:
				cs.append("multianewarray");
				return 4;
			case (short) IFNULL:
				cs.append("ifnull");
				return 3;
			case (short) IFNONNULL:
				cs.append("ifnonnull ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (short) GOTO_W:
				cs.append("goto_w ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]));
				return 5;
			case (short) JSR_W:
				cs.append("jsr_w ");
				if(printparams)
						cs.append(i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]));
				return 5;
			case (short) DEBUGGER:
				cs.append("breakpoint");
				return 1;
			default:
				cs.append(byteToHex(code[pc]));
		}
		return 1;
	}

	private static int i(int b1, int b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(int b1, int b2, int b3, int b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private static String byteToHex(int b) {
		StringBuilder sb=new StringBuilder();
		int v = b & 0xFF;
		sb.append(hexArray[v >>> 4]);
		sb.append(hexArray[v & 0x0F]);

		return sb.toString();
	}
}
