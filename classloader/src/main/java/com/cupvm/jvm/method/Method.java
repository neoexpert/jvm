package com.cupvm.jvm.method;

import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm.constants.Constant;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Method extends AMethod {
	public ArrayList<StackMapEntry> stackMap;
	private boolean polymorphicSignature;
	private CodeAttribute codeattr;

	public Method(String name, String type, AClass dclazz) {
		super(name,type,dclazz);
		id=0;
		this.constantPool=null;
		this.clazz=null;
	}

	/*public Method(String signature, AClass cl){
		super(signature,cl);
	}*/
	public final Constant[] constantPool;
	public final Class clazz;
	public Method(ByteBuffer buf, Class clazz){
		super(clazz);
		this.clazz = clazz;
		Constant[] constantPool=clazz.constantPool;
		this.constantPool=constantPool;
		access_flags = buf.getShort();

		int name_index = buf.getChar();
		int descriptor_index = buf.getChar();
		name = constantPool[name_index].str;
		Constant mtype=constantPool[descriptor_index];
		type = mtype.str;
		signature=(name+type).intern();
		if(mtype.data!=null)
			this.mt= (MethodType) mtype.data;
		else {
			this.mt = new MethodType(type);
			mtype.data=mt;
		}
		this.mh =new MethodHead(clazz.getName(), name, signature, mt);
		this.args= mt.args;
		argsLength= mt.args.length;

		isNative=(access_flags & ACC_NATIVE) != 0;
		if(isNative)
			declareNative();
		readAttributes(buf, constantPool);
		int attributes_count = buf.getChar();
		for (int ix = 0; ix < attributes_count; ++ix) {
			Attribute a = new Attribute(buf);

			String name =
					constantPool[a.attribute_name_index].str;
			switch(name){
				case "Code":
					codeattr = new CodeAttribute(a, constantPool);
					max_locals=codeattr.max_locals;
					max_stack=codeattr.max_stack;
					code=codeattr.code;
					break;
				default:
					super.processAttribute(name, a, constantPool);
			}
		}
		//attrs=new Attribute[attributes_count];
	}

	public int getMaxStack() {
		return max_stack;
	}
	public byte[] getByteCode(){
		return codeattr.bcode;
	}

	public int getMaxLocals() {
		return max_locals;
	}
	public void setMaxLocals(int max_locals){
		this.max_locals=max_locals;
	}
	public void setMaxStack(int max_stack){
		this.max_stack=max_stack;
	}



	public int getCurrentLine(int pc) {
		return codeattr.getCurrentLine(pc);
	}

	public MException checkException(int pc) {
		return codeattr.checkException(pc);
	}




	public void write(DataOutput raf) throws IOException {
		raf.writeShort(access_flags);
		//raf.writeChar(name_index);
		//raf.writeChar(descriptor_index);
		raf.writeChar(attrs.length);
		for (Attribute a : attrs) {
			a.write(raf);
		}
	}



	//	method_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}
	Attribute[] attrs;


	//@Override
	//public int getParameterCount() {
	//	return paramscount;
	//}


	public int invoked=0;
	public short[] code;

	public CodeAttribute getCodeAttr(){
		return codeattr;
	}

	public String getSignature() {
		return signature;
	}

	public String getType() {
		return type;
	}

	public void setCode(short[] code) {
	    this.code=code;
	}

	@Override
	public List<? extends LineNumberEntry> getLineNumbers() {
		LineNumberTableAttribute lnt=codeattr.getLineNumberTableAttribute();
		if(lnt==null)return null;
		Map<Integer, Integer> table=lnt.getTable();
		ArrayList<LineNumberEntry> lineNumbers=new ArrayList();
		Set<Map.Entry<Integer, Integer>> entries = table.entrySet();
		for(Map.Entry<Integer, Integer> entry:entries){
		    final int pc=entry.getKey();
		    final int lineNr=entry.getValue();
			lineNumbers.add(new LineNumberEntry() {
				@Override
				public int getPC() {
					return pc;
				}

				@Override
				public int getLineNumber() {
					return lineNr;
				}
			});
		}
		return lineNumbers;
	}

	@Override
	public int getCodeLength() {
		return code.length;
	}

	@Override
	public int getNextLineNumber(int pc) {
	    return getCurrentLine(pc);
	}
}
