package com.cupvm.jvm.method;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoader;

public class MethodHead{
	public final String classname;
	public final String signature;
	public final MethodType mt;
	public boolean isNative;
	public boolean isFinal;
	//public boolean isFromSuper;
	public boolean nativeInline;
	private final int hashcode;
	public final String name;
	public String nativeSignature;
	public boolean isLoaded;
	public AClass clazz;

	public MethodHead(String classname, String name, String signature, MethodType mt){
		this.classname=classname;
		this.signature=signature;
		this.mt=mt;
		this.name=name;
		this.hashcode= classname.hashCode()+signature.hashCode();
	}
	@Override
	public final boolean equals(final Object o){
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		final MethodHead that = (MethodHead) o;
		return classname.equals(that.classname) &&
				signature.equals(that.signature);
	}

	@Override
	public final int hashCode(){
		return hashcode;
	}
}
