package com.cupvm.jvm._class;

import com.cupvm.jvm.OPs;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.method.Method;

import java.util.ArrayList;

public class BootstrapMethod implements ReferenceKind{
    public String hclazz;
    public String mclazz;
    public String mname;
    public String mtype;
    public int referenceKind;
    public String desc;

    private Number oref;
    public Method method;

    int bootstrap_method_ref;
    public Constant[] arguments;

    public Number resolve(String name) {
        if(oref==null) {
            DClass dclazz = new DClass("R");
            if(method.isStatic()){
                int args=method.args.length;
                Method helper = new Method(name , desc, dclazz);
                ArrayList<Short> code=new ArrayList<>();
                for(int i=0;i<args;i++) {
                    switch(i) {
                        case 0:
                            code.add(OPs.ALOAD_1); //aload_1
                            break;
                        case 1:
                            code.add(OPs.ALOAD_2); //aload_2
                            break;
                        case 2:
                            code.add(OPs.ALOAD_3); //aload_3
                            break;
                        default:
                            code.add(OPs.ALOAD); //aload
                            code.add((short) (i+1)); //aload
                            break;
                    }
                }
                code.add(OPs.INVOKESTATIC); //invokestatic
                code.add((short)0);
                code.add((short)0);
                code.add(OPs.RETURN); //return
                short[] scode=new short[code.size()];
                for(int i=0;i< scode.length;i++)
                    scode[i]=code.get(i);
                helper.setMaxLocals(1+args);
                helper.setMaxStack(args);
                helper.setCode(scode);
                dclazz.addMethod("theMethod", method );
                dclazz.methods.add(method);
                method=helper;
            }

            dclazz.setMethod(method);
            throw new RuntimeException("look here");
            /*
            Instance ci = new Instance(dclazz);
            this.oref = Heap.add(ci);

             */
        }
        return oref;
    }

}

