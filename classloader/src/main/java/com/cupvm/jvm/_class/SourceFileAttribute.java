package com.cupvm.jvm._class;

import com.cupvm.jvm.Attribute;
import com.cupvm.jvm.constants.Constant;

import java.nio.ByteBuffer;

public class SourceFileAttribute extends Attribute {
	private final String sourcefile;

	protected SourceFileAttribute(Attribute a, Constant[] constantPool) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		int index = buf.getChar();
		sourcefile=
		constantPool[index].str;
	}

	public final String getSourceFileName() {
		return sourcefile;
	}
}
