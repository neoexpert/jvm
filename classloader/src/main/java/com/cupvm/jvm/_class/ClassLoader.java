package com.cupvm.jvm._class;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.Attribute;
import com.cupvm.jvm.NArray;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm.constants.Constant;
import com.cupvm.jvm.field.Field;
import com.cupvm.jvm.field.FieldHead;
import com.cupvm.jvm.method.Method;
import com.cupvm.jvm.method.MethodHead;
import com.cupvm.jvm.method.MethodType;
import com.cupvm.jvm.method.Preprocessor;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.cupvm.jvm.Globals.*;

public abstract class ClassLoader{
	protected final HashMap<String, Integer> classindex = new HashMap<>();
	protected final ArrayList<AClass> classes=new ArrayList<>();
	private final HashMap<String, NArray> array_classes = new HashMap<>();
	private final Map<String, Number> primitive_classes = new HashMap<>();
	private static int idcounter=1;
	public static String logPrefix="";
	public static String logPostfix="\u001b[0m";
	protected static final Logger logger= ConsoleLogger.get();
	public static final int HEAD = 0xcafebabe;
	private boolean verbose;

	protected static void print(final String ... messages){
		System.out.print(logPostfix);
		for(String message:messages)
			System.out.print(message);
		System.out.print(logPostfix);
	}

	public abstract AClass loadClass(String name);
	protected abstract AClass loadClass(String classname, ClassLoader classLoader);
	public Number getPrimitiveClass(String classname) {
		switch (classname) {
			case "I":
			case "Z":
			case "B":
			case "C":
			case "D":
			case "F":
			case "J":
			case "S":
			    /*
				Number oref = primitive_classes.get(classname);
				if (oref == null) {
					JClass ci = new JClass(clazzClazz, classname);
					oref = Heap.add(ci);
					primitive_classes.put(classname, oref);
				}
				return oref;
			     */
		}
		throw new RuntimeException("wrong primitive class");
	}

	public AClass getArrayClass(String a) {
		NArray ac = array_classes.get(a);
		if (ac == null) {
			ac = new NArray(a.charAt(1));
			/*
			JClass ci = new JClass(clazzClazz, a);
			Number oref = Heap.addPermanent(ci);

			ac.setMyInstenceRef(oref);
			 */
			//ci.fields.put("name", a);
			array_classes.put(a, ac);
		}
		return ac;
	}

	public void reset() {
		classes.clear();
		classes.add(null);
		classindex.clear();
	}

	protected Class loadClass(InputStream raw, int size, String code_source, ClassLoader classLoader) throws IOException{
		if(verbose)
			print("\r[▏ ] \r");

		if(size <= 0)
			size = 16384;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream(size);

		int nRead;
		byte[] bytes = new byte[size];

		while((nRead = raw.read(bytes, 0, bytes.length)) != -1){
			buffer.write(bytes, 0, nRead);
		}
		raw.close();

		Class clazz = defineClass(buffer.toByteArray(), code_source, classLoader);
		return clazz;
	}

	public Class defineClass(byte[] array, String codeSource, ClassLoader classLoader){
		Class clazz=defineClass(array, classLoader);
		clazz.setCodeSource(codeSource);
		return clazz;
	}

	public Class defineClass(byte[] array){
		return defineClass(array,this);
	}

	public void setStaticCreator(StaticCreator sc){
		this.srf=sc;
	}

	public Number createString(String str) {
		if(srf==null)
			return null;
		return srf.createStringInstance(str);
	}

	public interface StaticCreator{
		int createStaticRef(Number v);

		int createStatic(Number v);

		Number createStringInstance(String str);
	}
	public StaticCreator srf;

	private Class defineClass(byte[] array, ClassLoader classLoader){
		boolean verbose = this.verbose;
		ByteBuffer buf = ByteBuffer.wrap(array);
		if (verbose)
			print("\r[▎ ] \r");
		if (buf.order(ByteOrder.BIG_ENDIAN).getInt() != HEAD){
			throw new RuntimeException("not a valid class file");
		}
		int minor = buf.getChar(), ver = buf.getChar();

		final int constant_pool_count = buf.getChar();

		LinkedList<Constant> strings_to_resolve =
				new LinkedList<>();
		LinkedList<Constant> fields_to_resolve =
				new LinkedList<>();
		LinkedList<Constant> methods_to_resolve =
				new LinkedList<>();
		if (verbose)
			print("\r[▍ ] \r");

		final Constant[] constantPool = new Constant[constant_pool_count + 1];
		for (int ix = 1; ix < constant_pool_count; ++ix) {
			Constant c = new Constant(buf);
			switch (c.tag) {
				case Constant.CLong:
				case Constant.CDouble:
					constantPool[ix] = c;
					++ix;
					break;
				case Constant.CString:
					strings_to_resolve.add(c);
					break;
				case Constant.CFieldRef:
					fields_to_resolve.add(c);
					break;
				case Constant.CMethodRef:
				case Constant.CInterfaceMethodRef:
					methods_to_resolve.add(c);
					break;
			}
			constantPool[ix] = c;
		}
		if (verbose)
			print("\r[▌ ] \r");

		int access_flags = buf.getChar();

		int this_class = buf.getChar();

		final String name = constantPool[constantPool[this_class].index1].str;
		int super_class = buf.getChar();
		Class superClass;
		if (super_class == 0)
			superClass=null;
		else{
			Constant c = constantPool[super_class];
			c = constantPool[c.index1];
			superClass = (Class) loadClass(c.str);
		}

		final int interfaces_count = buf.getChar();


		if (verbose)
			print("\r[▋ ] \r");

		int[] interfaces = new int[interfaces_count];
		for (int ix = 0; ix < interfaces_count; ++ix) {
			interfaces[ix] = buf.getChar();
		}

		int fields_count = buf.getChar();
		if (verbose)
			print("\r[▊ ] \r");

		AClass[] ics=new AClass[interfaces_count];
		final Class clazz=new Class(name, idcounter++, constantPool, this_class, superClass, ics, classLoader);
		clazz.setAccessFlags(access_flags);
		if(superClass!=null){
			clazz.refsCount += superClass.refsCount;
			clazz.instanceRefs.putAll(superClass.instanceRefs);
		}
		for (int ix = 0; ix < fields_count; ++ix){
			Field f = new Field(buf, clazz);
			clazz.addField(f);
			if (f.isStatic()){
				if (f.isObject()) {
					clazz.staticRefs.put(f.getName(),f);
					///*
					Number v = f.getDefaultValue(classLoader);
					if(v==null)
						v = aconst_null;
					if(srf!=null) {
						f.index = srf.createStaticRef(v);
					}
					//*/
				}
				else{
					clazz.staticFields.put(f.getName(),f);
					///*
					Number v = f.getDefaultValue(classLoader);
					if(v==null)
						v = iconst_0;
					if(srf!=null)
						f.index=srf.createStatic(v);
					//*/
					//statics.put(f.getName(), new ValueHolder(f.getDefaultValue()));
				}

			}
			else{
				if(f.isObject()){
					f.index=clazz.refsCount;
					clazz.instanceRefs.put(f.getName(),f);
					clazz.refsCount++;
				}
				else
					clazz.instanceFields.put(f.getName(),f);
			}
		}
		clazz.createInstanceTemplate();

		if (verbose)
			print("\r[█ ] \r");

		final int methods_count = buf.getChar();

		for (Constant mc : methods_to_resolve) {
			int cref = mc.index1;
			Constant cc=constantPool[cref];
			cc=constantPool[cc.index1];
			int mref = mc.index2;

			int nid = constantPool[mref].index1;
			int tid = constantPool[mref].index2;

			String mname = constantPool[nid].str;
			Constant mtype=constantPool[tid];
			MethodType mt;
			if(mtype.data==null) {
				mt = new MethodType(mtype.str);
				mtype.data=mt;
			}
			else mt= (MethodType) mtype.data;
			mc.str = (mname + mtype.str).intern();
			mc.data=new MethodHead(cc.str, mname, mc.str, mt);
		}

		LinkedList<Method> code_to_process=new LinkedList<>();
		for (int ix = 0; ix < methods_count; ++ix) {
			Method m = new Method(buf, clazz);
			clazz.addMethod(m);
			if(m.code!=null){
				//code_to_process.add(m);
			}
		}
		if (verbose)
			print("\r[█▏] \r");
		int attributes_count = buf.getChar();
		String sourceFileName = null;
		BootstrapMethodsAttribute bootstrapMethods = null;
		for (int ix = 0; ix < attributes_count; ++ix) {
			Attribute a = new Attribute(buf);
			switch (a.getName(constantPool)) {
				case "SourceFile":
					sourceFileName = new SourceFileAttribute(a, constantPool).getSourceFileName();
					break;
				case "BootstrapMethods":
					bootstrapMethods = new BootstrapMethodsAttribute(a, constantPool);
					break;
			}

			//attrs.add(a);
		}
		if (verbose)
			print("\r[█▎] \r");
		clazz.setSourceFileName(sourceFileName);

		while (buf.hasRemaining())
			logger.warn((buf.position() + " -> " + buf.get()));

		classindex.put(name, classes.size());
		classes.add(clazz);
		if (verbose)
			print("\r[█▍] \r");

		if (verbose)
			print("\r[█▌] \r");
		for(Constant c:strings_to_resolve){
			c.str = constantPool[c.index1].str;
		}
		if (verbose)
			print("\r[OK] \n");

		for (int ix = 0; ix < interfaces_count; ++ix) {
			AClass intf = clazz.getClass(interfaces[ix]);
			if(intf==null)
				logger.warn("interface not found: ", clazz.getClassName(interfaces[ix]));
			ics[ix]=intf;
		}


		for(Constant c:fields_to_resolve){
			Constant cref  = constantPool[c.index1];
			Constant cname = constantPool[cref.index1];
			String clazzName=cname.str;
			Constant ntc = constantPool[c.index2];
			Constant fname = constantPool[ntc.index1];
			String fieldName=fname.str;
			Constant ftype = constantPool[ntc.index2];
			c.data=new FieldHead(clazzName, fname.str, ftype.str);
			c.type=clazzName;
			c.str=fname.str;
			AClass tclazz= loadClass(clazzName);
			loop:while(true){
				if(tclazz==null)
					throw new RuntimeException("could not resolve field: "+clazzName+"."+fieldName);
				Field f=(Field)tclazz.getDeclaredField(fieldName);
				if(f!=null){
					c.type=tclazz.getName();
					c.clazz = tclazz;
					break;
				}
				for(AClass i:tclazz.getInterfaces()){
					f=(Field)i.fields.get(fieldName);
					if(f!=null){
						c.type=i.getName();
						c.clazz = i;
						break loop;
					}
				}
				tclazz=tclazz.getSuperClass();
			}
		}
		for(Method m:code_to_process){
			Preprocessor.processcode(m);
		}
		if(bootstrapMethods!=null)
			clazz.resolveMethods(bootstrapMethods);
		return clazz;
	}


	public abstract InputStream getResource(String path);

}
