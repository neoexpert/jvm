package com.cupvm.jvm._class;

import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm.method.Method;

public class MethodHandleClass extends AClass{
	private Method method;

	public MethodHandleClass(ClassLoader classLoader){
		super("_java/lang/invoke/MethodHandle", classLoader);
		instanceTemplate=new Number[0];
	}

	@Override
	public boolean isInstance(final AClass cl){
		return false;
	}


	@Override
	public AMethod getMethod(final String signature){
		if(signature.startsWith("invokeExact")){
			AClass stringClass = classLoader.loadClass("java/lang/String");
			return stringClass.getMethod("substring(I)Ljava/lang/String;");
		}
		return null;
	}

	@Override
	public AMethod getDeclaredMethod(final String signature){
		return null;
	}

	@Override
	public AClass[] getInterfaces(){
		return new AClass[0];
	}

	@Override
	public AMethod getMethodHead(final String s){
		return null;
	}


	@Override
	public int getID(){
		return 0;
	}

	public Method getMethod(){
		return method;
	}

	public void setMethod(final Method m){
		this.method=m;
	}
}
