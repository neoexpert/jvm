package com.cupvm.jvm._class;

import com.cupvm.jvm.Attribute;
import com.cupvm.jvm.constants.Constant;

import java.nio.ByteBuffer;

public class BootstrapMethodsAttribute extends Attribute {
    final BootstrapMethod[] methods;

    protected BootstrapMethodsAttribute(Attribute a, Constant[] constantPool) {
        super(a);
        ByteBuffer buf = ByteBuffer.wrap(info);
        int num_bootstrap_methods  = buf.getChar();
        methods=new BootstrapMethod[num_bootstrap_methods];
        for(int i=0;i<num_bootstrap_methods;++i){
            BootstrapMethod m=new BootstrapMethod();
            m.bootstrap_method_ref=buf.getChar();
            Constant mhandle = constantPool[m.bootstrap_method_ref];
            m.referenceKind=mhandle.index1;
            Constant mref=constantPool[mhandle.index2];
            Constant mclazz=constantPool[mref.index1];
            m.mclazz=constantPool[mclazz.index1].str;
            Constant mnameandtype=constantPool[mref.index2];
            m.mname=constantPool[mnameandtype.index1].str;
            m.mtype=constantPool[mnameandtype.index2].str;
            int num_bootstrap_arguments=buf.getChar();
            m.arguments=new Constant[num_bootstrap_arguments];
            for(int j=0;j<num_bootstrap_arguments;++j){
                int arg=buf.getChar();
                m.arguments[j]=constantPool[arg];
            }
            methods[i]=m;
        }

    }
}
