package com.cupvm.jvm._class;

import com.cupvm.jvm._abstract.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;

public class SystemClassLoader extends ClassLoaderImpl {

	public SystemClassLoader() {
		MethodHandleClass mhc=new MethodHandleClass(this);
		addClass(mhc);
	}







	public void addClass(AClass clazz){
		classindex.put(clazz.getName(), classes.size());
		classes.add(clazz);
	}

	public void loadAll(String[] names){
		for(final String classname:names){
			loadClass(classname);
		}
	}

	public void loadAllAsync(String[] names){
		ArrayList<CompletableFuture<Void>> list=new ArrayList<>();
		for(final String classname:names){
			final CompletableFuture<Void> cf = CompletableFuture.runAsync(new Runnable(){
				@Override
				public void run(){
					logger.debug("debug: ",classname);
					SystemClassLoader.this.loadClass(classname);
				}
			});
			list.add(cf);
		}
		for(CompletableFuture<Void> cf:list){
			try{
				cf.get();
			}catch(Exception e){
				throw new RuntimeException(e);
			}
		}

	}



	public String tostring() {
		StringBuilder sb=new StringBuilder();
		sb.append("classes_loaded: ")
			.append(classes.size())
			.append("\n");
		return sb.toString();
	}
}
