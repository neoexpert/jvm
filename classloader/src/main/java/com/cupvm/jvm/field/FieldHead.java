
package com.cupvm.jvm.field;

public class FieldHead{
	public final String desc;
	public final String clazzName;
	public final String name;
	public char type;
	public final boolean wide;

	public FieldHead(String clazzName, String name, String desc){
		this.clazzName=clazzName;
		this.name=name;
		this.desc=desc;
		this.type=desc.charAt(0);
		wide=(type=='J'||type=='D');
	}
}
