package com.cupvm.jvm.field;

import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm.constants.Constant;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Field extends AField {
	private ConstantValueAttribute constant_value;
	public final Number getDefaultValue(ClassLoader classLoader) {
		switch (desc) {
			case "Z":
				return Globals.iconst_0;
			case "I":
			case "S":
			case "B":
			case "C":
				if (constant_value != null)
					return constant_value.value.value;
				else return Globals.iconst_0;
			case "J":
				if (constant_value != null)
					return constant_value.value.value;
				else return Globals.lconst_0;
			case "D":
				if (constant_value != null)
					return constant_value.value.value;
				else return Globals.dconst_0;
			case "F":
				if (constant_value != null)
					return constant_value.value.value;
				else return Globals.fconst_0;
			case "Ljava/lang/String;":
				if (constant_value != null) {
					String str = clazz.constantPool[constant_value.value.index1].str;
					return classLoader.createString(str);
				} else return null;
		}
		return null;
	}

	@Override
	public final String getDesc() {
		return desc;
	}


	@Override
	public final String parseType() {
		String type = desc;

		if (type.charAt(0) == '[') {
			return parseType(type.substring(1) + "[]");
		}
		return parseType(type);
	}

	@Override
	public final boolean isObject() {
		char type=desc.charAt(0);
		return type=='L'||type=='[';
	}

	private String parseType(String type) {
		if (type.length() == 1) {
			return parsePrimitiveType(type);
		}
		if (type.charAt(0) == 'L')
			return type.substring(1, type.indexOf(";"));
		return type;
	}

	private static String parsePrimitiveType(String type) {
		switch (type) {
			case "I":
				return "int";
			case "Z":
				return "boolean";
			case "B":
				return "byte";
			case "C":
				return "char";
			case "D":
				return "double";
			case "F":
				return "float";
			case "J":
				return "long";
			case "S":
				return "short";
			default:
				throw new RuntimeException("unknown primitive: " + type);
		}
	}


	private String parseModifiers() {
		StringBuilder sb = new StringBuilder();

		if (isPublic())
			sb.append("public ");
		if (isPrivate())
			sb.append("private ");
		if (isProtected())
			sb.append("protected ");
		if (isStatic())
			sb.append("static ");
		if (isFinal())
			sb.append("final ");
		if (isVolatile())
			sb.append("volatile ");
		if (isTransient())
			sb.append("transient ");
		if (isSynthetic())
			sb.append("synthetic ");
		if (isEnum())
			sb.append("enum ");
		return sb.toString();
	}

	@Override
	public final Number getType(ClassLoader classLoader) {
		if (desc.startsWith("L")) {
			String classname = desc.substring(1, desc.indexOf(";"));
			return classLoader.loadClass(classname).getMyInstanceRef();
		}
		if (desc.startsWith("[")) {
			return classLoader.getArrayClass(desc).getMyInstanceRef();
		}
		if(desc.length()==1)
			return classLoader.getPrimitiveClass(desc);
		return 0;
	}


	private Class clazz;

	@Override
	public final AClass getMyClass() {
		return clazz;
	}


	private short id;


	//private ArrayList<Attribute> attrs;

	private String name;

	private String desc;

	//	field_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}

	public Field() {
	}

	public Field(ByteBuffer buf, Class clazz) {
		this.clazz = clazz;
		Constant[] constantPool=clazz.constantPool;
		access_flags = buf.getChar();
		char name_index = buf.getChar();

		name = constantPool[name_index].str;

		char descriptor_index = buf.getChar();
		//int di=(int)descriptor_index;

		desc = constantPool[descriptor_index].str;
		setSignature(name+ " "+desc);


		char attributes_count = buf.getChar();
		//attrs = new ArrayList<>(attributes_count);
		for (int ix = 0; ix < attributes_count; ++ix) {
			Attribute a = new Attribute(buf);
			String name = constantPool[a.attribute_name_index].str;
			switch(name){
				case "ConstantValue":
				constant_value = new ConstantValueAttribute(a, constantPool);
				break;
				default:
					super.processAttribute(name, a,constantPool);
					break;
			}

			//attrs.add(a);
		}
	}

	@Override
	public void write(DataOutput raf) throws IOException {
		raf.writeChar(access_flags);
		//raf.writeChar(name_index);
		//t raf.writeChar(descriptor_index);

		//raf.writeChar(attrs.size());
		/*for (Attribute a : attrs) {

			a.write(raf);
		}*/
	}

	public final void setName(String name) {
		this.name = name;
	}


	public final String getName() {
		return name;
	}

	public final String getNameAndDesc() {
		return name + desc;
	}





	@Override
	public final String toString() {
		StringBuilder s = new StringBuilder(parseModifiers());
		s.append("\nField: ").append(name).append("\n");
		s.append("access_flags: ").append(access_flags).append("\n");
		return s + "\n";
	}
}
