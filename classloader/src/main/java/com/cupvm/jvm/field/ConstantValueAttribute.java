package com.cupvm.jvm.field;

import com.cupvm.jvm.Attribute;
import com.cupvm.jvm.constants.Constant;

import java.nio.ByteBuffer;

/*
ConstantValue_attribute {
        u2 attribute_name_index;
        u4 attribute_length;
        u2 constantvalue_index;
        }

 */
public class ConstantValueAttribute extends Attribute {
	public final Constant value;

	public ConstantValueAttribute(Attribute a, Constant[] constantPool) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		int constand_value_index = buf.getChar();
		value = constantPool[constand_value_index];
		info=null;

	}
}
