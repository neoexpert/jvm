package com.cupvm.jvm;

public interface ArrayTypes {
    byte T_OBJECT = 3;
    byte T_BOOLEAN = 4;
    byte T_CHAR = 5;
    byte T_FLOAT = 6;
    byte T_DOUBLE = 7;
    byte T_BYTE = 8;
    byte T_SHORT = 9;
    byte T_INT = 10;
    byte T_LONG = 11;
}
