package com.cupvm.jvm.classpath;
import java.io.*;

public abstract class ClassPath implements Closeable{
	public abstract ClassSource getClassSource(String s);
	public abstract InputStream getResource(String s);
	public abstract void close() throws IOException;
}
