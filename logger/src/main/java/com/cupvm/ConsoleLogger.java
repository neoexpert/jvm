package com.cupvm;

public class ConsoleLogger implements Logger{
	public static int color;
	private String logPrefix="";
	private static String logPostfix="\u001b[0m";

	public static Logger get(){
		return new ConsoleLogger();
	}

	public static Logger get(int color){
		return new ConsoleLogger().setColor(color);
	}

	private static final String INFO="[\u001b[34mINFO\u001b[0m] ";
	private static final String WARN="[\u001b[33mWARN\u001b[0m] ";
	private static final String ERROR="[\u001b[31mERROR\u001b[0m] ";
	private static final String DEBUG="[\u001b[32mDEBUG\u001b[0m] ";
	public void info(String message){
		System.out.print(INFO);
		System.out.print(logPrefix);
		System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void info(Throwable message) {
		System.out.print(INFO);
		System.out.print(logPrefix);
		System.out.print(message);
		message.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	@Override
	public void info(String message, Throwable throwable) {
		System.out.print(INFO);
		System.out.print(logPrefix);
		System.out.print(message);
		throwable.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	public void info(String ...  messages){
		System.out.print(INFO);
		System.out.print(logPrefix);
		for(String message:messages)
			System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void info(final String message, final Object ... messages){
		System.out.print(INFO);
		System.out.print(logPrefix);
		System.out.print(message);
		for(Object msg:messages)
			System.out.print(msg);
		System.out.println(logPostfix);
	}

	@Override
	public void warn(Throwable message) {
		System.out.print(WARN);
		System.out.print(logPrefix);
		System.out.print(message);
		message.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	public void warn(String message){
		System.out.print(WARN);
		System.out.print(logPrefix);
		System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void warn(String message, Throwable throwable) {
		System.out.print(WARN);
		System.out.print(logPrefix);
		System.out.print(message);
		throwable.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	public void warn(String ...  messages){
		System.out.print(WARN);
		System.out.print(logPrefix);
		for(String message:messages)
			System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void warn(final String message, final Object... messages){
		System.out.print(WARN);
		System.out.print(logPrefix);
		System.out.print(message);
		for(Object msg:messages)
			System.out.print(msg);
		System.out.println(logPostfix);
	}

	public void error(String message){
		System.out.print(ERROR);
		System.out.print(logPrefix);
		System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void error(Throwable message) {
		System.out.print(ERROR);
		System.out.print(logPrefix);
		System.out.print(message);
		message.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	@Override
	public void error(String message, Throwable throwable) {
		System.out.print(ERROR);
		System.out.print(logPrefix);
		System.out.print(message);
		throwable.printStackTrace(System.out);
		System.out.println(logPostfix);
	}

	public void error(String ...  messages){
		System.out.print(ERROR);
		System.out.print(logPrefix);
		for(String message:messages)
			System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void error(final String message, final Object... messages){
		System.out.print(ERROR);
		System.out.print(logPrefix);
		System.out.print(message);
		for(Object msg:messages)
			System.out.print(msg);
		System.out.println(logPostfix);
	}

	@Override
	public void debug(String message){
		System.out.print(DEBUG);
		System.out.print(logPrefix);
		System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void debug(String ...  messages){
		System.out.print(DEBUG);
		System.out.print(logPrefix);
		for(String message:messages)
			System.out.print(message);
		System.out.println(logPostfix);
	}

	@Override
	public void debug(final String message, final Object ... messages){
		System.out.print(DEBUG);
		System.out.print(logPrefix);
		System.out.print(message);
		for(Object msg:messages)
			System.out.print(msg);
		System.out.println(logPostfix);
	}
	@Override
	public void log(String message){
			System.out.println(message);
	}

	@Override
	public void log(String ... messages){
		for(String msg:messages)
			System.out.print(msg);
		System.out.println();
	}

	@Override
	public void log(String message, Object ... messages){
		System.out.print(message);
		for(Object msg:messages)
			System.out.print(msg);
		System.out.println();
	}

	@Override
	public Logger setColor(int color){
		switch(color){
			case RED:
				logPrefix="\u001b[31m";
				break;
			case GREEN:
				logPrefix="\u001b[32m";
				break;
			case BLUE:
				logPrefix="\u001b[34m";
				break;
		}
		return this;
	}

	@Override
	public void reset(){
		logPrefix="";
	}


}
