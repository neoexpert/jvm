package com.cupvm;

public class LogFilter implements Logger{
	private final Logger logger;
	private boolean debug=false;
	private int filterLevel=INFO;
	public LogFilter(Logger logger){
		this.logger=logger;
	}

	public void setLevelFilter(int level){
		this.filterLevel=level;
	}

	public void setDebug(boolean debug){
		this.debug=debug;
	}

	public void info(String message){
		if(filterLevel>=INFO)
			logger.info(message);
	}

	@Override
	public void info(Throwable message) {
		if(filterLevel>=INFO)
			logger.info(message);
	}

	@Override
	public void info(String message, Throwable throwable) {
		if(filterLevel>=INFO)
			logger.info(message, throwable);
	}

	public void info(String ...  messages){
		if(filterLevel>=INFO)
			logger.info(messages);
	}

	@Override
	public void info(final String message, final Object ... messages){
		if(filterLevel>=INFO)
			logger.info(message, messages);
	}

	@Override
	public void warn(Throwable message) {
		if(filterLevel>=WARNING)
			logger.warn(message);
	}

	public void warn(String message){
		if(filterLevel>=WARNING)
			logger.warn(message);
	}

	@Override
	public void warn(String message, Throwable throwable) {
		if(filterLevel>=WARNING)
			logger.warn(message, throwable);
	}

	public void warn(String ...  messages){
		if(filterLevel>=WARNING)
			logger.warn(messages);
	}

	@Override
	public void warn(final String message, final Object... messages){
		if(filterLevel>=WARNING)
			logger.warn(message, messages);
	}

	public void error(String message){
		if(filterLevel>=ERROR)
			logger.error(message);
	}

	@Override
	public void error(Throwable message) {
		if(filterLevel>=ERROR)
			logger.error(message);
	}

	@Override
	public void error(String message, Throwable throwable) {
		if(filterLevel>=WARNING)
			logger.error(message, throwable);
	}

	public void error(String ...  messages){
		if(filterLevel>=ERROR)
			logger.error(messages);
	}

	@Override
	public void error(final String message, final Object... messages){
		if(filterLevel>=ERROR)
			logger.error(message, messages);
	}

	@Override
	public void debug(String message){
		if(debug)
			logger.debug(message);
	}

	@Override
	public void debug(String ...  messages){
		if(debug)
			logger.debug(messages);
	}

	@Override
	public void debug(final String message, final Object ... messages){
		if(debug)
			logger.debug(message, messages);
	}
	@Override
	public void log(String message){
		logger.log(message);
	}

	@Override
	public void log(String ... messages){
		logger.log(messages);
	}

	@Override
	public void log(String message, Object ... messages){
		logger.log(message, messages);
	}

	@Override
	public Logger setColor(final int color){
		logger.setColor(color);
		return this;
	}

	@Override
	public void reset(){
		logger.reset();
	}


}
