package com.cupvm;
import java.util.*;

public class LoggerImpl implements Logger{
	private static LoggerImpl logger;
	public static LoggerImpl get(){
		if(logger==null)
			logger=new LoggerImpl();
		return logger;
	}
	private LogFilter consoleLogger;
	public LoggerImpl(){
		this.consoleLogger=new LogFilter(new ConsoleLogger());
		appender.add(consoleLogger);
	}

	public LogFilter getConsoleLogger(){
		return consoleLogger;
	}

	private ArrayList<Logger> appender=new ArrayList<>();

	public void addAppender(Logger logger){
		appender.add(logger);
	}

	public void info(String message){
		for(Logger logger:appender)
			logger.info(message);
	}

	@Override
	public void info(Throwable message) {
		for(Logger logger:appender)
			logger.info(message);
	}

	@Override
	public void info(String message, Throwable throwable) {
		for(Logger logger:appender)
			logger.info(message, throwable);
	}

	public void info(String ...  messages){
		for(Logger logger:appender)
			logger.info(messages);
	}

	@Override
	public void info(final String message, final Object ... messages){
		for(Logger logger:appender)
			logger.info(message, messages);
	}

	@Override
	public void warn(Throwable message) {
		for(Logger logger:appender)
			logger.warn(message);
	}

	public void warn(String message){
		for(Logger logger:appender)
			logger.warn(message);
	}

	@Override
	public void warn(String message, Throwable throwable) {
		for(Logger logger:appender)
			logger.warn(message, throwable);
	}

	public void warn(String ...  messages){
		for(Logger logger:appender)
			logger.warn(messages);
	}

	@Override
	public void warn(final String message, final Object... messages){
		for(Logger logger:appender)
			logger.warn(message, messages);
	}

	public void error(String message){
		for(Logger logger:appender)
			logger.error(message);
	}

	@Override
	public void error(Throwable message) {
		for(Logger logger:appender)
			logger.error(message);
	}

	@Override
	public void error(String message, Throwable throwable) {
		for(Logger logger:appender)
			logger.error(message, throwable);

	}

	public void error(String ...  messages){
		for(Logger logger:appender)
			logger.error(messages);
	}

	@Override
	public void error(final String message, final Object... messages){
		for(Logger logger:appender)
			logger.error(message, messages);
	}

	@Override
	public void debug(String message){
		for(Logger logger:appender)
			logger.debug(message);
	}

	@Override
	public void debug(String ...  messages){
		for(Logger logger:appender)
			logger.debug(messages);
	}

	@Override
	public void debug(final String message, final Object ... messages){
		for(Logger logger:appender)
			logger.debug(message, messages);
	}
	@Override
	public void log(String message){
		for(Logger logger:appender)
			logger.log(message);
	}

	@Override
	public void log(String ... messages){
		for(Logger logger:appender)
			logger.log(messages);
	}

	@Override
	public void log(String message, Object ... messages){
		for(Logger logger:appender)
			logger.log(message, messages);
	}

	@Override
	public Logger setColor(final int color){
		for(Logger logger:appender)
			logger.setColor(color);
		return this;
	}

	@Override
	public void reset(){
		for(Logger logger:appender)
			logger.reset();
	}
}
