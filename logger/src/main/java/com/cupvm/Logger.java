package com.cupvm;

public interface Logger{
	int FATAL=0;
	int ERROR=1;
	int WARNING=2;
	int INFO=3;

	int RED=0xFF0000;
	int GREEN=0x00FF00;
	int BLUE=0x0000FF;
	void info(String message);
	void info(Throwable message);
	void info(String message, Throwable throwable);
	void info(String ... messages);
	void info(String message, Object ... messages);
	void warn(Throwable message);
	void warn(String message);
	void warn(String message, Throwable throwable);
	void warn(String ... messages);
	void warn(String message, Object ... messages);
	void error(String message);
	void error(Throwable message);
	void error(String message, Throwable throwable);
	void error(String ... messages);
	void error(String message, Object ... messages);
	void debug(String message);
	void debug(String ... messages);
	void debug(String message, Object ... messages);
	void log(String message);
	void log(String ... messages);
	void log(String message, Object ... messages);
	Logger setColor(int color);
	void reset();

}
