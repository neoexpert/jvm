package com.cupvm.jvm;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jvm.classpath.JarClassPath;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.SystemClassLoader;
import com.cupvm.jvm.instance.arrays.ObjectArrayInstance;
import com.cupvm.jvm.natives.java.lang.*;
import com.cupvm.jvm.natives.java.lang.invoke.*;
import com.cupvm.jvm.instance.natives.*;
import java.io.*;
import java.util.zip.*;
import java.net.*;
import com.cupvm.jni.*;
import com.cupvm.debugger.*;
import com.cupvm.jvm.natives.java.util.concurent.locks.NReentrantLock;


public class Main{
	private static Logger logger= ConsoleLogger.get();
	public final static String[] dependecies= new String[]{
			"java/lang/NullPointerException",
			"java/lang/Thread",
			"java/lang/System"
	};
	public static long startTime;
	private static JVM jvm;

	public static void init(){
		JNI jni = new JNI();
		MyThread.setJNI(jni);

		SystemClassLoader classLoader = jvm.getClassLoader();
		final AClass java_lang_String = classLoader.loadClass("java/lang/String");
		JVM.stringClass=java_lang_String;
		JVM.classClass=classLoader.loadClass("java/lang/Class");
		jni.addAll(NString.gen(java_lang_String));
		jni.registerNatives(java_lang_String);

		jni.addAll(NObject.gen());
		AClass objclazz = classLoader.loadClass("java/lang/Object");
		jni.registerNatives(objclazz);
		jni.addAll(NThread.gen());
		AClass threadClass = classLoader.loadClass("java/lang/Thread");
		jni.registerNatives(threadClass);
		jni.addAll(NThrowable.gen());
		AClass throwableClazz = classLoader.loadClass("java/lang/Throwable");
		jni.registerNatives(throwableClazz);
		jni.addAll(NSystem.gen());
		AClass systemClazz = classLoader.loadClass("java/lang/System");
		jni.registerNatives(systemClazz);

		AClass java_lang_Long=classLoader.loadClass("java/lang/Long");
		AClass java_lang_Integer=classLoader.loadClass("java/lang/Integer");
		AClass java_lang_Short=classLoader.loadClass("java/lang/Short");

		AClass java_lang_Float=classLoader.loadClass("java/lang/Float");
		AClass java_lang_Double=classLoader.loadClass("java/lang/Double");
		AClass java_lang_Character=classLoader.loadClass("java/lang/Character");
		JCharacter.init(java_lang_Character);
		jni.addAll(NPrimitives.gen(classLoader));

		java_lang_Integer.createNatives(
				"valueOf(I)Ljava/lang/Integer;",
				"intValue()I",
				"shortValue()S",
				"byteValue()B",
				"longValue()J",
				"floatValue()F",
				"doubleValue()D",
				"equals(Ljava/lang/Object;)Z",
				"hashCode()I",
				"toString(I)Ljava/lang/String;");
		java_lang_Short.createNatives(
				"valueOf(S)Ljava/lang/Short;",
				"intValue()I",
				"shortValue()S",
				"byteValue()B",
				"longValue()J",
				"floatValue()F",
				"doubleValue()D",
				"equals(Ljava/lang/Object;)Z",
				"hashCode()I",
				"toString(S)Ljava/lang/String;");
		java_lang_Long.createNatives(
				"valueOf(J)Ljava/lang/Long;",
				"intValue()I",
				"shortValue()S",
				"byteValue()B",
				"longValue()J",
				"floatValue()F",
				"doubleValue()D",
				"equals(Ljava/lang/Object;)Z",
				"hashCode()I",
				"toString(J)Ljava/lang/String;");
		jni.registerNatives(java_lang_Integer);
		jni.registerNatives(java_lang_Short);
		jni.registerNatives(java_lang_Long);
		jni.registerNatives(java_lang_Float);
		jni.registerNatives(java_lang_Double);
		jni.registerNatives(java_lang_Character);

		jni.addAll(NClass.gen());
		jni.registerNatives(classLoader.loadClass("java/lang/Class"));

		AClass java_lang_StringBuilder=classLoader.loadClass("java/lang/StringBuilder");
		java_lang_StringBuilder.createNatives(
				"<init>()V",
				"<init>(Ljava/lang/String;)V",
				"<init>(I)V",
				"append(Ljava/lang/String;)Ljava/lang/StringBuilder;",
				"append(I)Ljava/lang/StringBuilder;",
				"append(J)Ljava/lang/StringBuilder;",
				"append(F)Ljava/lang/StringBuilder;",
				"append(D)Ljava/lang/StringBuilder;",
				"append(C)Ljava/lang/StringBuilder;",
				"toString()Ljava/lang/String;");
		jni.addAll(NStringBuilder.gen());
		jni.registerNatives(java_lang_StringBuilder);

		jni.addAll(NReentrantLock.gen());
		jni.registerNatives(classLoader.loadClass("java/util/concurrent/locks/ReentrantLock"));

		//jni.addAll(NMethodHandle.gen());
		initializeMethodHandles();
		jni.addAll(NMethodHandlesLookup.gen());
		jni.registerNatives(classLoader.loadClass("java/lang/invoke/MethodHandles$Lookup"));

		AClass mhClass=classLoader.loadClass("java/lang/invoke/MethodHandle");
		jni.registerNatives(mhClass);
		classLoader.loadAll(dependecies);

		if(JVM.verbose)
			logger.info("system class initialized");
		//ClassLoader.getClass("java/lang/NullPointerException",true,null);
		classLoader.loadClass("java/lang/Thread");
		//ClassLoader.getClass("java/lang/System",true,null);

		//initializeSystemClass();
		System.gc();
		//initOutput();
		//NOutput.setOut(System.out);

		//Class.getClass("java/io/File");
		//Class.getClass("java/io/NFile$PathStatus");
	
	}

	private static void initializeMethodHandles(){
	}

	public static void initializeSystemClass() {
		//AClass c = Class.getClass("java/lang/System",true);

		//t=c.createInitMethod();
		//MyThread t = new MyThread(c, "main",true);
		//t.setMethodToRun("main([Ljava/lang/String;)V");


		//t.setMethodToRun("initializeSystemClass()V");
		//t.run();
	}

	private static void printUsage() {
		logger.log("Usage: java [options] <main-class>");
		logger.log("       java [options] -jar <jar-file>");
		logger.log("");
		logger.log("       options:");
		logger.log("           -verbose");
		logger.log("           log gc stats:");
		logger.log("           -gcstats");
		logger.log("           colored JVM logging:");
		logger.log("               -red");
		logger.log("               -green");
		logger.log("               -blue");
	}

	public static void main(String[] args)throws Exception{
		if(args==null){
			printUsage();
			return;
		}
		if(args.length==0){
			printUsage();
			return;
		}
		jvm=new JVM();
		SystemClassLoader classLoader = jvm.getClassLoader();
		String mainClass = null;
		String jarfile = null;
		int args_offset=0;
		loop:for(int i=0;i<args.length;++i){
			String arg=args[i];
			if(arg.charAt(0)=='-'){
				switch (arg){
					case "-jar":
						jarfile = args[i+1];
						args_offset=i+2;
						break loop;
					case "-verbose":
						JVM.verbose=true;
						startTime=System.currentTimeMillis();
						break;
					case "-debugger":
						logger.info("debugger");
						JDWP jdwp=new JDWP(null, null);
						jdwp.startasync();
						break;
					case "-red":
						JVM.setLogColor(ConsoleLogger.RED);
						logger=ConsoleLogger.get(ConsoleLogger.RED);
						SystemClassLoader.logPrefix="\u001b[31m";
						break;
					case "-green":
						JVM.setLogColor(ConsoleLogger.GREEN);
						logger=ConsoleLogger.get(ConsoleLogger.GREEN);
						SystemClassLoader.logPrefix="\u001b[32m";
						break;
					case "-blue":
						JVM.setLogColor(ConsoleLogger.BLUE);
						logger=ConsoleLogger.get(ConsoleLogger.BLUE);
						SystemClassLoader.logPrefix="\u001b[34m";
						break;
					case "-gcstats":
						Heap.printgcstats=true;
						break;
				}
			}
			else{
				mainClass=arg;
				args_offset=i+1;
				break;
			}

		}
		if(JVM.verbose)
			logger.info("starting JVM...");

		String cp = ".";

		if (jarfile != null){
			ZipFile jar = new ZipFile(jarfile);
			ZipEntry e = jar.getEntry("META-INF/MANIFEST.MF");
			if (e == null) {
				throw new RuntimeException("META-INF/MANIFEST.MF not found");
			}
			InputStream is = jar.getInputStream(e);
			mainClass=parseManifest(is);
			if(mainClass!=null)
				jvm.getClassLoader().addJar(new File(jarfile));
			/*
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line=reader.readLine())!=null){
				if (line.startsWith("Main-Class: ")) {
					mainClass = line.substring("Main-Class: ".length());
					mainClass = mainClass.replaceAll("\\.", "/");
					Class.setJar(new File(jarfile));
					break;
				}
			}
			*/
		}
		if(mainClass==null){
			printUsage();
			return;
		}
		String s = new File(cp).getAbsolutePath();
		classLoader.addToClasspath(s);
		URL url= JVM.class.getProtectionDomain()
			.getCodeSource()
			.getLocation();
		File rt = new File(url.toURI());
		if (rt.isDirectory()){
			rt = new File(rt.getParent(), "../../jvm_with_jdk/target/java.jar");
			JarClassPath jcp = new JarClassPath(rt, "rt/");
			classLoader.addToClasspath(jcp);
			JarClassPath jcp2 = new JarClassPath(rt);
			classLoader.addToClasspath(jcp2);
			//Class.addToClasspath(rt.getAbsolutePath() + "/rt/");
			//Class.addToClasspath(rt.getAbsolutePath());

		}
		else {
			JarClassPath jcp = new JarClassPath(rt, "rt/");
			classLoader.addToClasspath(jcp);
		}
		//Class.addJar("rt.jar");
		//Class.addJar("charsets.jar");
		if(JVM.verbose)
			logger.info("before init...");
		init();



		ObjectArrayInstance ai=new ObjectArrayInstance(args.length-args_offset);
		AClass stringClass = classLoader.loadClass("java/lang/String");
		for(int i=args_offset;i<args.length;++i){
			ai.refs[i-args_offset]= NString.createInstance(args[i]);
		}
		Number aref = Heap.add(ai);
		if(JVM.verbose){
			long duration=System.currentTimeMillis()-startTime;
			logger.info("jvm initialization took:");
			printDuration(duration);
		}
		jvm.runMainClass(mainClass, aref);
		if(JVM.verbose){
			logger.log("\nsummary: ");
			long duration=System.currentTimeMillis()-startTime;
			printDuration(duration);
			logger.log(Heap.tostring());
			logger.log(classLoader.tostring());
		}
	}

	private static void printDuration(long duration){
		StringBuilder sb=new StringBuilder();	
		if(duration>3600000){
			sb.append(duration/3600000)
				.append(" hours ")
				.append((duration/60000)%60)
				.append(" minutes");
		}
		else if(duration>1200000)
			sb.append(duration/60000)
				.append(" minutes");
		else if(duration>5000)
			sb.append(duration/1000)
				.append(" seconds");
		else
			sb.append(duration)
				.append(" milliseconds");
		logger.log(sb.toString());
	}
	private static String parseManifest(InputStream is) throws IOException {
		String line;
		while ((line=readLine(is))!=null){
			if (line.startsWith("Main-Class: ")) {
				//mainClass = mainClass.replaceAll("\\.", "/");
				return line.substring("Main-Class: ".length());
			}
		}
		return null;
	}
	
	private static String readLine(InputStream is) throws IOException {
		StringBuilder line=new StringBuilder();
		int b=is.read();
		if(b<0)return null;
		while (b>0){
			char c= (char) b;
			if(c=='\r'|c=='\n')
				break;
			if(c=='.')c='/';
			line.append(c);
			b=is.read();
		}
		return line.toString();
	}
}
