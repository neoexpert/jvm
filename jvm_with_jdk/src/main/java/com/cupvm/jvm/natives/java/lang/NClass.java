package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.instance.natives.*;
import com.cupvm.jvm.instance.arrays.ObjectArrayInstance;

import java.util.*;
import java.io.*;

public class NClass {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Class.forName(Ljava/lang/String;)Ljava/lang/Class;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				String name=((JString)Heap.get(lv[0])).s;
				cF.push(Heap.add(new JClass(name)));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Class.getClassLoader()Ljava/lang/ClassLoader;",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(JClassLoader.get(t.classLoader,MyThread.jni));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/ClassLoader.getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;",1) {

			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				ClassLoader classLoader = t.classLoader;
				String path=((JString)Heap.get(lv[1])).s;
				InputStream is=classLoader.getResource(path);
				if(is==null)
					cF.push(0);
				else
					cF.push(Heap.add(new NInputStream(classLoader,is)));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Class.getFields()[Ljava/lang/reflect/Field;",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new ObjectArrayInstance(0)));
				return false;
			}
		});
		return methods;
	}
}
