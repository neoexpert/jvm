package com.cupvm.jvm.natives.java.util.concurent;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.ThreadInterface;

public class NAtomicLong {
	public static boolean invoke(String methodname, NativeFrame cF, Object[] lv, ThreadInterface t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "VMSupportsCS8()Z":
				VMSupportsCS8(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void VMSupportsCS8(NativeFrame cF, Object[] lv, ThreadInterface t) {
		cF.push(0);
	}


	private static void registerNatives(NativeFrame cF, Object[] lv, ThreadInterface t) {
	}


}
