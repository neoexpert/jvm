package com.cupvm.jvm.natives.java.lang;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm._class.Class;
import com.cupvm.jvm.instance.natives.JString;
import com.cupvm.jvm.instance.natives.NInputStream;
import com.cupvm.jvm.natives.java.io.NFile;
import com.cupvm.jvm.natives.java.io.NFileInputStream;
import com.cupvm.jvm.natives.java.nio.NByteBuffer;
import com.cupvm.jvm.natives.java.util.zip.NZipFile;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

import static com.cupvm.jvm.Globals.aconst_null;

public class NSystem {
	private static final Logger logger= ConsoleLogger.get(JVM.logColor);
	public static final HashSet<String> loadedlibs=new HashSet<>();
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/System.loadLibrary(Ljava/lang/String;)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				ClassLoader classLoader = t.classLoader;
				JString name=(JString)Heap.get(lv[0]);
				String libname=name.s;
				if(JVM.verbose)
					logger.info("loading library: ",libname);
				if(loadedlibs.contains(libname)){
					if(JVM.verbose)
						logger.info(libname," library was already loaded");
					return false;
				}
				loadedlibs.add(libname);
				switch(libname){
					case "bytebuffer":
						Class bbc=(Class)classLoader.loadClass("java/nio/NativeByteBuffer");
						bbc.createNatives(
								"init([B)V",
								"order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;",
								"limit()I",
								"position()I",
								"capacity()I",
								"limit(I)Ljava/nio/ByteBuffer;",
								"position(I)Ljava/nio/ByteBuffer;",
								"hasRemaining()Z");
						MyThread.addNativeMethods(NByteBuffer.gen());
							MyThread.jni.registerNatives(bbc);
						return false;
					case "filesystem":
						MyThread.addNativeMethods(NFile.gen());
						MyThread.addNativeMethods(NFileInputStream.gen());
						MyThread.jni.registerNatives(classLoader.loadClass("java/io/File"));
						MyThread.jni.registerNatives(classLoader.loadClass("java/io/FileInputStream"));
						return false;
					case "zip":
						MyThread.addNativeMethods(NZipFile.gen());
						MyThread.addNativeMethods(NInputStream.gen());
						MyThread.jni.registerNatives(classLoader.loadClass("java/util/zip/ZipFile"));
						MyThread.jni.registerNatives(classLoader.loadClass("java/util/zip/ZipEntry"));
						MyThread.jni.registerNatives(classLoader.loadClass("neo/NInputStream"));
						return false;
					case "math":
						MyThread.addNativeMethods(NMath.gen());
						MyThread.jni.registerNatives(classLoader.loadClass("java/lang/Math"));
						return false;
					default:
						throw new RuntimeException("library not found: "+libname);

				}
			}
		});
		methods.add(new JNIMethod("java/lang/System.arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V",5) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				System.arraycopy(Heap.get(lv[0]).helper, lv[1].intValue(), Heap.get(lv[2]).helper, lv[3].intValue(), lv[4].intValue());
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/System.getProperty(Ljava/lang/String;)Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString jString= (JString) Heap.get(lv[0]);
				String value=System.getProperty(jString.s);
				if(value==null)
					cF.push(aconst_null);
				else
					cF.push(Heap.add(new JString( value)));
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.getenv(Ljava/lang/String;)Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString jString= (JString) Heap.get(lv[0]);
				String value=System.getenv(jString.s);
				if(value==null)
					cF.push(aconst_null);
				else
					cF.push(Heap.add(new JString(value)));
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.print0(Ljava/lang/String;)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JString jString= (JString) Heap.get(lv[0]);
				//if(jString.s.contains("was not a"))
				//	getClass();
				System.out.print(jString.s);
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.print0(C)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				System.out.print((char)lv[0].intValue());
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.read0()I",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				try{
					cF.push(System.in.read());
				}catch(IOException e){
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.gc()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Heap.gc();
				return false;
			}});
		methods.add(new JNIMethod("java/lang/System.currentTimeMillis()J",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.pushl(Long.valueOf(System.currentTimeMillis()));
				return false;
			}});
		return methods;
	}



}
