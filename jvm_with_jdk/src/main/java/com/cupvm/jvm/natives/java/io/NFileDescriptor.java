package com.cupvm.jvm.natives.java.io;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.ThreadInterface;

public class NFileDescriptor {
	public static boolean invoke(String methodname, NativeFrame cF, Object[] lv, ThreadInterface t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, t);
				return false;
			case "initIDs()V":
				initIDs(cF, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void initIDs(NativeFrame cF, ThreadInterface t) {
	}

	private static void registerNatives(NativeFrame cF, ThreadInterface t) {
	}
}
