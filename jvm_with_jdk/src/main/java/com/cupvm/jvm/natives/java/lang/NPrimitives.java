package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.SystemClassLoader;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.*;
import java.util.*;

import static com.cupvm.jvm.Globals.*;

public class NPrimitives{
	private static AClass javaLangInteger;
	private static AClass javaLangLong;
	private static AClass javaLangShort;
	private static AClass javaLangFloat;
	private static AClass javaLangDouble;

	private static class Cache{
		static Number[] intcache;
		static Number[] longcache;
		static Number[] shortcache;
		static final int low = -512;
		static final int high = 511;

		private static void init(SystemClassLoader classLoader){
			AClass javaLangInteger=classLoader.loadClass("java/lang/Integer");
			NPrimitives.javaLangInteger=javaLangInteger;
			AClass javaLangLong=classLoader.loadClass("java/lang/Long");
			NPrimitives.javaLangLong=javaLangLong;
			AClass javaLangShort=classLoader.loadClass("java/lang/Short");
			NPrimitives.javaLangShort=javaLangShort;
			NPrimitives.javaLangFloat=classLoader.loadClass("java/lang/Float");
			NPrimitives.javaLangDouble=classLoader.loadClass("java/lang/Double");

			int length=(high - low) + 1;
			intcache = new Number[length];
			longcache = new Number[length];
			shortcache = new Number[length];
			int j = low;
			for(int k = 0; k < length; ++k){
				intcache[k] = Heap.addPermanent(new JInteger(javaLangInteger, j));
				longcache[k] = Heap.addPermanent(new JLong(javaLangLong, j));
				shortcache[k] = Heap.addPermanent(new JShort(javaLangShort, (short)j));
				++j;
			}
		}
	}
    public static LinkedList<JNIMethod> gen(SystemClassLoader classLoader) {
			Cache.init(classLoader);
        LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Integer.valueOf(I)Ljava/lang/Integer;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				int i=(int)lv[0];
				if (i >= Cache.low && i <= Cache.high){
					cF.push(Cache.intcache[i + (-Cache.low)]);
					return false;
				}
				cF.push(Heap.add(new JInteger(javaLangInteger, i)));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.intValue()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JInteger i= (JInteger) Heap.get(lv[0]);
				cF.push(i.value);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.byteValue()B",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JInteger i= (JInteger) Heap.get(lv[0]);
				cF.push(Integer.valueOf((byte)i.value));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.longValue()J",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JInteger i= (JInteger) Heap.get(lv[0]);
				cF.push(Long.valueOf(i.value));
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.doubleValue()D",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JInteger i= (JInteger) Heap.get(lv[0]);
				cF.push(Integer.valueOf(i.value).doubleValue());
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.shortValue()S",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JInteger i= (JInteger) Heap.get(lv[0]);
				cF.push(Integer.valueOf((short)i.value));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.toString(I)Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new JString(Integer.toString(lv[0].intValue()))));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.equals(Ljava/lang/Object;)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Number me=lv[0];
				Number it=lv[1];
				if(me==it){
					cF.push(iconst_1);
					return false;
				}
				if(it== aconst_null){
					cF.push(iconst_0);
					return false;
				}
				Instance i1 = Heap.get(it);
				if(i1.clazz!=javaLangInteger){
					cF.push(iconst_0);
					return false;
				}
				JInteger $this=(JInteger)Heap.get(me);
				JInteger other=(JInteger)i1;
				if($this.value==other.value)
					cF.push(iconst_1);
				else
					cF.push(iconst_0);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.hashCode()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(((JInteger) Heap.get(lv[0])).value);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.valueOf(J)Ljava/lang/Long;",2) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				long l=(long)lv[0];
				if (l >= Cache.low && l <= Cache.high){
					cF.push(Cache.longcache[(int)l + (-Cache.low)]);
					return false;
				}
				cF.push(Heap.add(new JLong(javaLangLong, l)));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.hashCode()I",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.get(lv[0]).hashCode());
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.equals(Ljava/lang/Object;)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Number me=lv[0];
				Number it=lv[1];
				if(me==it){
					cF.push(iconst_1);
					return false;
				}
				Instance i1 = Heap.get(it);
				if(i1.clazz!=javaLangLong){
					cF.push(iconst_0);
					return false;
				}
				if(Heap.get(me).equals(i1))
					cF.push(iconst_1);
				else
					cF.push(iconst_0);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.longValue()J",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JLong i= (JLong) Heap.get(lv[0]);
				cF.push(i.value);
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.intValue()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JLong j= (JLong) Heap.get(lv[0]);
				cF.push(Integer.valueOf((int)j.value));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.floatValue()F",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JLong j= (JLong) Heap.get(lv[0]);
				cF.push(Float.valueOf((float)j.value));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.doubleValue()D",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JLong i= (JLong) Heap.get(lv[0]);
				cF.push(Long.valueOf(i.value).doubleValue());
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Long.toString(J)Ljava/lang/String;",2) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new JString(Long.toString((long) lv[0]))));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Float.valueOf(F)Ljava/lang/Float;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new JFloat(javaLangFloat, (float) lv[0])));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Float.floatValue()F",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JFloat f=(JFloat)Heap.get(lv[0]);
				cF.push(f.value);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Float.intValue()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JFloat f=(JFloat)Heap.get(lv[0]);
				cF.push((int)f.value);

				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Float.longValue()J",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JFloat i= (JFloat) Heap.get(lv[0]);
				cF.push(Float.valueOf(i.value).longValue());
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Float.isNaN(F)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Float.isNaN(lv[0].floatValue())?1:0);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Double.valueOf(D)Ljava/lang/Double;",2) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                cF.push(Heap.add(new JDouble(javaLangDouble,(double) lv[0])));
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/Double.doubleValue()D",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                JDouble d= (JDouble) Heap.get(lv[0]);
                cF.push(d.value);
				cF.push(null);
                return false;
            }
        });
		methods.add(new JNIMethod("java/lang/Double.longValue()J",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JDouble d= (JDouble) Heap.get(lv[0]);
				cF.push(Double.valueOf(d.value).longValue());
				cF.push(null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Short.valueOf(S)Ljava/lang/Short;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				short i=lv[0].shortValue();
				if (i >= Cache.low && i <= Cache.high){
					cF.push(Cache.shortcache[i + (-Cache.low)]);
					return false;
				}

				cF.push(Heap.add(new JShort(javaLangShort, i)));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Short.intValue()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JShort i= (JShort) Heap.get(lv[0]);
				cF.push(i.value);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Short.toString(S)Ljava/lang/String;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new JString(Short.toString(lv[0].shortValue()))));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Short.equals(Ljava/lang/Object;)Z",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Number me=lv[0];
				Number it=lv[1];
				if(me==it){
					cF.push(iconst_1);
					return false;
				}
				if(it== aconst_null){
					cF.push(iconst_0);
					return false;
				}
				Instance i1 = Heap.get(it);
				if(i1.clazz!=javaLangShort){
					cF.push(iconst_0);
					return false;
				}
				JShort $this=(JShort)Heap.get(me);
				JShort other=(JShort)i1;
				if($this.value==other.value)
					cF.push(iconst_1);
				else
					cF.push(iconst_0);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Integer.toString(I)Ljava/lang/String;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                cF.push(Heap.add(new JString(Integer.toString((Integer) lv[0]))));
                return false;
            }
        });
		methods.add(new JNIMethod("java/lang/Character.valueOf(C)Ljava/lang/Character;",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(Heap.add(new JCharacter((char)(int)lv[0])));
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Character.charValue()C",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				JCharacter c= (JCharacter) Heap.get(lv[0]);
				cF.push((int)c.value);
				return false;
			}
		});
		return methods;
	}


}
