package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.*;
import com.cupvm.jvm.executor.ByteCodeExecutor;
import com.cupvm.jvm.instance.Instance;
import java.util.LinkedList;

public class NThrowable {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Throwable.fillInStackTrace()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance ci= Heap.get(lv[0]);
				ByteCodeExecutor.fillInStackTrace(t,ci,t.getCurrentThread());
				return false;
			}
		});
		return methods;
	}






}
