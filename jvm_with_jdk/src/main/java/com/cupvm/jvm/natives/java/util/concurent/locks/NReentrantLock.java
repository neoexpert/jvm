package com.cupvm.jvm.natives.java.util.concurent.locks;
import java.util.concurrent.locks.ReentrantLock;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm.instance.*;
import java.util.LinkedList;

public class NReentrantLock{
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/util/concurrent/locks/ReentrantLock.init()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				c.helper=new ReentrantLock();
				return false;
			}
		});
		methods.add(new JNIMethod("java/util/concurrent/locks/ReentrantLock.lock()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				((ReentrantLock)c.helper).lock();
				return false;
			}
		});
		methods.add(new JNIMethod("java/util/concurrent/locks/ReentrantLock.unlock()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance c=Heap.get(lv[0]);
				((ReentrantLock)c.helper).unlock();
				return false;
			}
		});
		return methods;
	}
}
