package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.*;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.executor.ByteCodeExecutor;
import com.cupvm.jvm.instance.*;

import java.util.LinkedList;

import static com.cupvm.jvm.Globals.aconst_null;

public class NObject {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Object.hashCode()I",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(lv[0]);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Object.clone()Ljava/lang/Object;",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				cF.push(aconst_null);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Object.getClass()Ljava/lang/Class;",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i = Heap.get(lv[0]);
				Number myoref = i.clazz.getMyInstanceRef();
				if(myoref==null) {
					AClass clazz = i.clazz;
					myoref = ByteCodeExecutor.createClassClassInstance(clazz.getClassLoader(), clazz);
				}
				cF.push(myoref);
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Object.wait()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				/*if(t!=i.owner)
					throw new IllegalMonitorStateException();
					*/
				synchronized(i){
					try{
						i.lock.unlock();
						i.wait();
						i.lock.lock();
					}
					catch(InterruptedException e){

					}
				}
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Object.notify()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				synchronized(i){
					i.notify();
				}
				return false;
			}
		});
		methods.add(new JNIMethod("java/lang/Object.notifyAll()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance i=Heap.get(lv[0]);
				synchronized(i){
					i.notifyAll();
				}
				return false;
			}
		});
		return methods;
	}
}
