package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.JString;

import java.util.LinkedList;

public class NStringBuilder {
    public static LinkedList<JNIMethod> gen() {
        LinkedList<JNIMethod> methods = new LinkedList<>();
        methods.add(new JNIMethod("java/lang/StringBuilder.<init>()V",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ci.helper=new StringBuilder();
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.<init>(Ljava/lang/String;)V",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                JString jString = (JString) Heap.get(lv[1]);
                ci.helper=new StringBuilder(jString.s);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.<init>(I)V",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ci.helper=new StringBuilder(lv[1].intValue());
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.append(Ljava/lang/String;)Ljava/lang/StringBuilder;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                JString jString = (JString) Heap.get(lv[1]);
                StringBuilder sb= (StringBuilder) ci.helper;
                sb.append(jString.s);
                cF.push(oref);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.append(I)Ljava/lang/StringBuilder;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                StringBuilder sb= (StringBuilder) ci.helper;
                sb.append(lv[1].intValue());
                cF.push(oref);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.append(F)Ljava/lang/StringBuilder;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                StringBuilder sb= (StringBuilder) ci.helper;
                sb.append(lv[1].floatValue());
                cF.push(oref);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.append(J)Ljava/lang/StringBuilder;",2) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                StringBuilder sb= (StringBuilder) ci.helper;
                sb.append(lv[1]);
                cF.push(oref);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.append(C)Ljava/lang/StringBuilder;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                StringBuilder sb= (StringBuilder) ci.helper;
                sb.append((char)lv[1].intValue());
                cF.push(oref);
                return false;
            }
        });
        methods.add(new JNIMethod("java/lang/StringBuilder.toString()Ljava/lang/String;",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Number oref= lv[0];
                Instance ci= Heap.get(oref);
                StringBuilder sb= (StringBuilder) ci.helper;
                cF.push(Heap.add(new JString(sb.toString())));
                return false;
            }
        });
        return methods;
    }
}
