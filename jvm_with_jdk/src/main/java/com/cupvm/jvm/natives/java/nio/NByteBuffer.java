package com.cupvm.jvm.natives.java.nio;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.arrays.ByteArrayInstance;
import com.cupvm.jvm.instance.natives.JString;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;

public class NByteBuffer {
    public static LinkedList<JNIMethod> gen(){
        LinkedList<JNIMethod> methods = new LinkedList<>();
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.registerNatives()V",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.init([B)V",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteArrayInstance ai= (ByteArrayInstance) Heap.get(lv[1]);
                ci.helper= ByteBuffer.wrap(ai.arr);
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                Instance orderi= Heap.get(lv[1]);
                JString order= (JString) Heap.get(orderi.getRef("order"));
                ByteBuffer bb = (ByteBuffer) ci.helper;
                switch(order.s){
                    case "BIG_ENDIAN":
                        bb.order(ByteOrder.BIG_ENDIAN);
                        break;
                    case "LITTLE_ENDIAN":
                        bb.order(ByteOrder.LITTLE_ENDIAN);
                        break;
                }
                cF.push(lv[0]);
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.get()B",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                cF.push(Integer.valueOf(((ByteBuffer) Heap.get(lv[0]).helper).get()));
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.get([B)Ljava/nio/ByteBuffer;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteArrayInstance ai= (ByteArrayInstance) Heap.get(lv[1]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
								bb.get(ai.arr);
                cF.push(lv[0]);
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getFloat()F",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push(bb.getFloat());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getDouble()D",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.pushl(bb.getDouble());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getLong()J",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.pushl(bb.getLong());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getInt()I",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push(bb.getInt());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getShort()S",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci=  Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push((int)bb.getShort());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.getChar()C",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci=  Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push((int)bb.getChar());
                return false;
            }
        });

        methods.add(new JNIMethod("java/nio/NativeByteBuffer.limit()I",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci=  Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push(bb.limit());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.position()I",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push(bb.position());
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.position(I)Ljava/nio/ByteBuffer;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                Buffer bb = (Buffer) ci.helper;
                bb.position(lv[1].intValue());
                cF.push(lv[0]);
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.limit(I)Ljava/nio/ByteBuffer;",1) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                Buffer bb = (Buffer) ci.helper;
                bb.limit(lv[1].intValue());
                cF.push(lv[0]);
                return false;
            }
        });
        methods.add(new JNIMethod("java/nio/NativeByteBuffer.hasRemaining()Z",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                ByteBuffer bb = (ByteBuffer) ci.helper;
                cF.push(bb.hasRemaining()?1:0);
                return false;
            }
        });
        return methods;
    }
}
