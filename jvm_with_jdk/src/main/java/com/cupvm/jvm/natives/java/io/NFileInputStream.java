package com.cupvm.jvm.natives.java.io;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.*;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.JString;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import static com.cupvm.jvm.Globals.iconst_0;

public class NFileInputStream {
	public static LinkedList<JNIMethod> gen() {
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/io/FileInputStream.registerNatives()V",0) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				return false;
			}
		});
		methods.add(new JNIMethod("java/io/FileInputStream.open(Ljava/io/File;)I",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance ci = Heap.get(lv[0]);
				Instance fileci = Heap.get(lv[1]);
				JString path = (JString) Heap.get(fileci.getRef("path"));
				try {
					ci.helper= new FileInputStream(path.s);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				cF.push(iconst_0);
				return false;
			}
		});
		methods.add(new JNIMethod("java/io/FileInputStream.open(Ljava/io/File;)V",1) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance ci = Heap.get(lv[0]);
				Instance fileci = Heap.get(lv[1]);
				JString path = (JString) Heap.get(fileci.getRef("path"));
				try {
					ci.helper= new FileInputStream(path.s);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				return false;
			}
		});
		methods.add(new JNIMethod("java/io/FileInputStream.read([BII)I",3) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance ci = Heap.get( lv[0]);
				Instance bytearr =  Heap.get( lv[1]);
				FileInputStream fis= (FileInputStream) ci.helper;
				try {
					cF.push(fis.read((byte[])bytearr.helper,(int)lv[2],(int)lv[3]));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
		});
		methods.add(new JNIMethod("java/io/FileInputStream.close()V",3) {
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
				Instance ci = Heap.get( lv[0]);
				FileInputStream fis= (FileInputStream) ci.helper;
				try {
				    fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}
		});
		return methods;
	}

}
