package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.JNIEnv;

import java.util.LinkedList;

public class NMath{
	public static LinkedList<JNIMethod> gen(){
		LinkedList<JNIMethod> methods = new LinkedList<>();
		methods.add(new JNIMethod("java/lang/Math.random()D",0){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				cF.push(Math.random());
				cF.push(null);
				return false;
			}
		});
		return methods;
	}
}
