package com.cupvm.jvm.natives.java.lang;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.ThreadInterface;
import com.cupvm.jvm.Heap;

public class NRuntime {
	public static boolean invoke(String methodname, NativeFrame cF, Object[] lv, ThreadInterface t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "gc()V":
				Heap.gc();
				return false;
			case "availableProcessors()I":
				availableProcessors(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static void availableProcessors(NativeFrame cF, Object[] lv, ThreadInterface t) {
		cF.push(1);
	}

	private static void registerNatives(NativeFrame cF, Object[] lv, ThreadInterface t) {
	}
}
