package com.cupvm.jvm.natives.java.util.zip;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm.MyThread;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.executor.ByteCodeExecutor;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm.instance.arrays.ByteArrayInstance;
import com.cupvm.jvm.instance.natives.JString;
import com.cupvm.jvm.instance.natives.NInputStream;
import com.cupvm.jvm.natives.NativeInputStream;
import com.cupvm.jvm.natives.java.lang.NString;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static com.cupvm.jvm.Globals.*;

public class NZipFile {
	private static long idCounter=1;
    private static HashMap<Long, ZipFile> openedZips=new HashMap<>();
	public static LinkedList<JNIMethod> gen(){
		LinkedList<JNIMethod> methods=new LinkedList<>();
		methods.add(new JNIMethod("java/util/zip/ZipFile.registerNatives()V",0){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				return false;
			}});
		methods.add(new JNIMethod("java/util/zip/ZipFile.open(Ljava/lang/String;)J",1){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				JString path= (JString) Heap.get(lv[0]);
				try {
					Long id=idCounter++;
					openedZips.put(id,new ZipFile(path.s));
					cF.pushl(id);
				} catch (IOException e) {
					e.printStackTrace();
					cF.pushl(0);
				}
				return false;
            }});
		methods.add(new JNIMethod("java/util/zip/ZipFile.entryExists(JLjava/lang/String;)Z",3){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				ZipFile zip = openedZips.get(lv[0]);
				ZipEntry entry=zip.getEntry(((JString)Heap.get(lv[2])).s);
				if(entry==null)
					cF.push(iconst_0);
				else
					cF.push(iconst_1);
				return false;
			}});
		methods.add(new JNIMethod("java/util/zip/ZipFile.getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;",1){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				Instance ci= Heap.get(lv[0]);
				JString name= (JString) Heap.get(lv[1]);
				ZipFile zf= (ZipFile) ci.helper;
				Instance zEntry=new Instance(t.getClass("java/util/zip/ZipEntry"));
				ZipEntry entry = zf.getEntry(name.s);
				zEntry.helper=entry;
				if(entry==null)
					cF.push(aconst_null);
				else
					cF.push(Heap.add(zEntry));
				return false;
			}});
		methods.add(new JNIMethod("java/util/zip/ZipFile.getBytes(JLjava/lang/String;)[B",3){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				ZipFile zip = openedZips.get(lv[0]);
				ZipEntry entry=zip.getEntry(((JString)Heap.get(lv[2])).s);
				if(entry==null){
					cF.push(aconst_null);
					return false;
				}
				try {
					byte[] arr=new byte[(int) entry.getSize()];
					InputStream is = zip.getInputStream(entry);
					int readen=0;
					while(readen<arr.length)
						readen+=is.read(arr,readen,arr.length-readen);
					ByteArrayInstance bai=new ByteArrayInstance(arr);
					cF.push(Heap.add(bai));
				} catch (IOException e) {
					e.printStackTrace();
					cF.push(aconst_null);
				}
				return false;
			}});
		methods.add(new JNIMethod("java/util/zip/ZipFile.getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;",1){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				Instance ci= Heap.get(lv[0]);
				Instance entry= Heap.get(lv[1]);
				ZipEntry zEntry = (ZipEntry) entry.helper;
				ZipFile zf= (ZipFile) ci.helper;
				try {
					InputStream is = zf.getInputStream(zEntry);
					cF.push(Heap.add(new NInputStream(t.classLoader, is)));
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				return false;
			}});
		methods.add(new JNIMethod("java/util/zip/ZipFile.getSize(JLjava/lang/String;)J",3){
			@Override
			public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t){
				ZipFile zip = openedZips.get(lv[0]);
				ZipEntry entry=zip.getEntry(((JString)Heap.get(lv[2])).s);
				if(entry==null)
					cF.pushl(0L);
				else
					cF.pushl(entry.getSize());
				return false;
			}});
		return methods;
	}




	private static int addr = 1;
	private static int eaddr = 1;
	private static Map<Integer, ZipFile> files = new HashMap<>();
	private static Map<Integer, ZipEntry> entries = new HashMap<>();



}
