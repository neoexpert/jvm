package com.cupvm.jvm.natives.java.io;

import com.cupvm.jni.JNIMethod;
import com.cupvm.jni.NativeFrame;
import com.cupvm.jvm.Heap;
import com.cupvm.jvm.JNIEnv;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.natives.JString;

import java.io.File;
import java.util.LinkedList;

public class NFile {
    public static LinkedList<JNIMethod> gen(){
        LinkedList<JNIMethod> methods=new LinkedList<>();
        methods.add(new JNIMethod("java/io/File.registerNatives()V",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                return false;
            }
        });
        methods.add(new JNIMethod("java/io/File.isDirectory()Z",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                JString path= (JString) Heap.get(ci.getRef("path"));
                cF.push(new File(path.s).isDirectory()?1:0);
                return false;
            }
        });
        methods.add(new JNIMethod("java/io/File.exists()Z",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                JString path= (JString) Heap.get(ci.getRef("path"));
                cF.push(new File(path.s).exists()?1:0);
                return false;
            }
        });
        methods.add(new JNIMethod("java/io/File.length()J",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                JString path= (JString) Heap.get(ci.getRef("path"));
                cF.push(new File(path.s).length());
                cF.push(0);
                return false;
            }
        });
        methods.add(new JNIMethod("java/io/File.getAbsolutePath()Ljava/lang/String;",0) {
            @Override
            public final boolean invoke(NativeFrame cF, Number[] lv, JNIEnv t) {
                Instance ci= Heap.get(lv[0]);
                JString path= (JString) Heap.get(ci.getRef("path"));
                cF.push(Heap.add(new JString(new File(path.s).getAbsolutePath())));
                return false;
            }
        });
        return methods;
    }
}
