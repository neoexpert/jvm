package com.cupvm.jvm.natives;

import com.cupvm.jni.NativeFrame;
import com.cupvm.jni.ThreadInterface;
import com.cupvm.jvm.*;
import com.cupvm.jvm._abstract.AClass;
import com.cupvm.jvm._abstract.AField;
import com.cupvm.jvm._abstract.AMethod;
import com.cupvm.jvm._class.ClassLoader;
import com.cupvm.jvm.instance.Instance;
import com.cupvm.jvm.instance.arrays.ByteArrayInstance;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class NativeInputStream extends AClass {
	private final InputStream is;

	public NativeInputStream(ClassLoader classLoader, InputStream inputStream) {
		super("NativeInputStream", classLoader );
		this.is = inputStream;
	}

	public static boolean invoke(String methodname, NativeFrame cF, Object[] lv, ThreadInterface t) {
		switch (methodname) {
			case "read([BII)I":
				read(cF, lv, t);
				return false;
			case "available()I":
				avaliable(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void avaliable(NativeFrame cF, Object[] lv, ThreadInterface t) {
		int oref = (int) lv[0];
		Instance nis = Heap.get(oref);
		NativeInputStream _is = (NativeInputStream) nis.clazz;
		try {
			int r = _is.is.available();
			cF.push(r);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static void read(NativeFrame cF, Object[] lv, ThreadInterface t) {
		int oref = (int) lv[0];
		Instance nis = Heap.get(oref);
		int baoref = (int) lv[1];
		int i1 = (int) lv[2];
		int i2 = (int) lv[3];
		NativeInputStream _is = (NativeInputStream) nis.clazz;
		ByteArrayInstance ai = (ByteArrayInstance) Heap.get(baoref);
		byte[] ba =  ai.arr;
		try {
			int r = _is.is.read(ba, i1, i2);
			cF.push(r);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}


	@Override
	public boolean isInstance(AClass cl) {
		return false;
	}




	@Override
	public AMethod getMethod(String signature) {
		return new NativeMethod(this, signature);
	}




	@Override
	public AMethod getDeclaredMethod(String nameAndDesc) {
		return null;
	}


	@Override
	public AClass[] getInterfaces() {
		return new AClass[0];
	}

	@Override
	public AMethod getMethodHead(String s) {
		return null;
	}


	@Override
	public HashMap<String, AField> getInstanceFields(){
		return null;
	}


	public int getID(){
		return 0;
	}
}
