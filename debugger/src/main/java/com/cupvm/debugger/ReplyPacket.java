package com.cupvm.debugger;
import java.io.*;


public class ReplyPacket {
	public final int length;
	private final int id;
	private final byte flags;
	private final short error_code;
	private final byte[] data;

	public ReplyPacket(int id, short error_code, byte[] data){
		this.length=11+data.length;
		this.id=id;
		this.flags= (byte) 0x80;
		this.error_code=error_code;
		this.data=data;
	}

	public ReplyPacket(DataInputStream dis) throws IOException {
		length=dis.readInt();
		id=dis.readInt();
		flags=dis.readByte();
		error_code=dis.readShort();
		data=new byte[length-11];
		dis.read(data);
	}

	public ReplyPacket(int id, short error_code) {
		this(id,error_code,new byte[0]);
	}

	public void send(DataOutputStream dos) throws IOException {
		dos.writeInt(length);
		dos.writeInt(id);
		dos.writeByte(flags);
		dos.writeShort(error_code);
		dos.write(data);
	}

	public boolean isReply() {
		return flags==(byte)0x80;
	}
}
