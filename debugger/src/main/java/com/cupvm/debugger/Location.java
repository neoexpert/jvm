package com.cupvm.debugger;

public class Location {
    public final int classID;
    public final int methodID;
    public final long pc;

    public Location(int classID, int methodID, long pc) {
        this.classID=classID;
        this.methodID=methodID;
        this.pc=pc;
    }
}
