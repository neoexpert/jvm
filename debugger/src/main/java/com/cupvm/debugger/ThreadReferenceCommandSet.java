package com.cupvm.debugger;

public interface ThreadReferenceCommandSet {
    int THREAD_NAME=1;
    int THREAD_STATUS=4;
    int THREAD_GROUP=5;
    int THREAD_FRAMES=6;
    int THREAD_FRAME_COUNT=7;

    //THREAD_STATUS:
    int ZOMBIE	=0;
    int RUNNING	=1;
    int SLEEPING=	2;
    int MONITOR	=3;
    int WAIT	=4;
    //SUSPEND_STATUS:
    int SUSPEND_STATUS_SUSPENDED=1;
}
