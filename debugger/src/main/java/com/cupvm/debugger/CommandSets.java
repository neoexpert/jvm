package com.cupvm.debugger;

public interface CommandSets {
	int VM_COMMAND_SET = 1;
	int REFERENCETYPE=2;
	int CLASS_TYPE=3;
	int METHOD_COMMAND_SET=6;
	int OBJECT_REFERENCE=9;
	int THREAD_REFERENCE=11;
	int THREADGROUPREFERENCE=12;
	int EVENT_REQUEST = 15;
	int STACK_FRAME=16;
	short NOT_IMPLEMENTED = 99;
}
