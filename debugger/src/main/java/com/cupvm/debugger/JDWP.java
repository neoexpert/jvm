package com.cupvm.debugger;

import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import com.cupvm.debugger.event.EventHandler;
import com.cupvm.debugger.event.EventModifier;
import com.cupvm.jni.ThreadInterface;
import com.cupvm.jvm.*;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class JDWP implements
		CommandSets,
		VMCommandSet,
		ReferenceTypeCommandSet,
		ClassTypeCommandSet,
		MethodCommandSet,
		EventRequestCommandSet,
		StackFrameCommandSet,
		ThreadReferenceCommandSet,
		ObjectReferenceCommandSet,
		ThreadGroupReferenceCommandSet,
		Constants,
		ErroCodes {
	private static final Logger logger= ConsoleLogger.get();
	private final JVMIntf jvm;
	private final ClassLoaderIntf classLoader;
	private final Map<Integer, Integer> eventHandlers=new HashMap<>();
	private final EventHandler eventHandler;
	private int eventRequestID=1;
	private ServerSocket serverSocket;
	private DataOutputStream socketOut;
	private Runnable onDisconnect;

	public JDWP(JVMIntf jvm, EventHandler eventHandler){
		this.jvm=jvm;
		this.eventHandler=eventHandler;
		this.classLoader=jvm.getClassLoader();
	}

	public void close() throws IOException {
	    vmDeath();
		if(serverSocket==null)
			throw new IOException("can't close: serverSocket is null");
	    serverSocket.close();
	}

	public void singleStep(int requestID, int clazzid, int methodid, int pc) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos=new DataOutputStream(baos);
			//suspend policy
			dos.writeByte(2);
			//events in set
			dos.writeInt(1);
			//envent kind
			dos.writeByte(SINGLE_STEP);
			dos.writeInt(requestID);
			//threadID
			dos.writeInt(-2);
			//location
			dos.writeByte(1);
			dos.writeInt(clazzid);
			dos.writeInt(methodid);
			dos.writeLong(pc);
			dos.flush();
			CommandPacket event=new CommandPacket((byte)0,(byte)64,(byte)100,baos.toByteArray());
			synchronized(this.socketOut){
				event.write(this.socketOut);
				this.socketOut.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void breakpoint(int requestID, int clazzid, int methodid, int pc) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos=new DataOutputStream(baos);
			//suspend policy
			dos.writeByte(2);
			//events in set
			dos.writeInt(1);
			//envent kind
			dos.writeByte(BREAKPOINT);
			dos.writeInt(requestID);
			//threadID
			dos.writeInt(-2);
			//location
            dos.writeByte(1);
			dos.writeInt(clazzid);
			dos.writeInt(methodid);
			dos.writeLong(pc);
			dos.flush();
			CommandPacket event=new CommandPacket((byte)0,(byte)64,(byte)100,baos.toByteArray());
			synchronized(this.socketOut){
				event.write(this.socketOut);
				this.socketOut.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void vmDeath() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream dos=new DataOutputStream(baos);
			//suspend policy
			dos.writeByte(2);
			//events in set
			dos.writeInt(1);
			//envent kind
			dos.writeByte(BREAKPOINT);
			dos.flush();
			CommandPacket event=new CommandPacket((byte)0,(byte)64,(byte)100,baos.toByteArray());
			synchronized(this.socketOut){
				event.write(this.socketOut);
				this.socketOut.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setOnDisconnectListener(Runnable runnable) {
		this.onDisconnect=runnable;
	}




	public static void main(String[] args) throws IOException {
		//JVMIntf jvm=new JVMIntf();
		//new JDWP(jvm).start();
		new JDWP(null,null).start();
	}

	public void startasync(){
		Thread t=new Thread(() -> {
			try {
				start();
			} catch (IOException e) {
				e.printStackTrace();
				jvm.dispose();
			}
			finally {
				onDisconnect.run();
			}
		});
		t.setName("JDWP-Thread");
		t.setDaemon(true);
		t.start();
	}

	public void start()throws IOException{
		this.serverSocket = new ServerSocket(5005,0, InetAddress.getLoopbackAddress());
		Socket clientSocket = serverSocket.accept();
		OutputStream out=clientSocket.getOutputStream();
		InputStream is=clientSocket.getInputStream();
		byte[] handshake=new byte[14];
		is.read(handshake);
		out.write(handshake);
		DataInputStream dis=new DataInputStream(is);
		socketOut = new DataOutputStream(out);
		while (true) {
			CommandPacket cmd = new CommandPacket(dis);
			synchronized (socketOut) {
				ReplyPacket r = processCommand(cmd);
				r.send(socketOut);
				socketOut.flush();
			}
		}
	}

	private ReplyPacket processCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command_set){
			case VM_COMMAND_SET:
				return processVMCommand(cmd);
			case REFERENCETYPE:
				return processReferenceTypeCommand(cmd);
			case CLASS_TYPE:
				return processClassTypeCommand(cmd);
			case METHOD_COMMAND_SET:
				return processMethodCommand(cmd);
			case OBJECT_REFERENCE:
				return processObjectReferenceCommand(cmd);
			case THREAD_REFERENCE:
				return processThreadReferenceCommand(cmd);
			case THREADGROUPREFERENCE:
				return processThreadGroupReference(cmd);
			case EVENT_REQUEST:
				return processEventRequest(cmd);
			case STACK_FRAME:
				return processStackFrame(cmd);
			default:
				throw new RuntimeException("Unknown Command Set: "+cmd.command_set);
		}
	}


	private ReplyPacket processVMCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case VERSION:
				logger.info("cmd: version");
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				String desc="0.0";
				writeString(dos,desc);
				//major
				dos.writeInt(0);
				//minor
				dos.writeInt(0);
				//vm version
				String vm_version="0.0";
				writeString(dos, vm_version);

				String vm_name="neoVM";
				writeString(dos, vm_name);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CLASSES_BY_SIGNATURE:
				DataInputStream dis = cmd.getDataInputStream();
				String signature=readString(dis);
				logger.info("cmd: CLASSES_BY_SIGNATURE: ",signature);
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				String className=signature.substring(1,signature.length()-1);
				logger.info("retrieving class: ",className);

				ClassIntf clazz=classLoader.getLoadedClass(className);
				if(clazz==null)
					dos.writeInt(0);
				else{
					dos.writeInt(1);
					if(clazz.isInterface())
						dos.writeByte(INTERFACE);
					else
						dos.writeByte(CLASS);
					//referenceTypeID typeID
					//string signature
					dos.writeInt(clazz.getID());
					//status
					//if(clazz.isInitialized())
						dos.writeInt(VERIFIED | PREPARED | INITIALIZED);
					//else
						//dos.writeInt(PREPARED);
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case IDSIZES:
				logger.info("cmd: id_sizes");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				//fieldIDSize
				dos.writeInt(4);
				//methodIDSize
				dos.writeInt(4);
				//objectIDSize
				dos.writeInt(4);
				//frameIDSize
				dos.writeInt(4);
				//referenceTypeIDSize
				dos.writeInt(4);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case ALL_CLASSES:
				logger.info("cmd: all classes");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				List<? extends ClassIntf> classes = classLoader.getLoadedClasses();
				int size=classes.size();
				dos.writeInt(size);
				for(int i=0;i<size;++i){
					clazz=classes.get(i);
					//refTypeTag
					if(clazz.isInterface())
						dos.writeByte(INTERFACE);
					else
						dos.writeByte(CLASS);
					//referenceTypeID typeID
					dos.writeInt(clazz.getID());
					signature=clazz.getName();
					writeSignature(dos, signature);
					//status
					//if(clazz.isInitialized())
					dos.writeInt(VERIFIED | PREPARED | INITIALIZED);
					//else
					//	dos.writeInt(PREPARED);
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case ALL_THREADS:
				logger.info("cmd: all threads");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				List<? extends ThreadInterface> threads = jvm.getThreads();
				size=threads.size();
				logger.info("threads size: ",size);
				dos.writeInt(size);
				for(int i=0;i<size;++i){
					logger.info("threadid: ",i);
					dos.writeInt(-i-2);
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case TOPLEVELTHREADGRPOUPS:
				logger.info("cmd: TOPLEVELTHREADGRPOUPS");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				//count
				dos.writeInt(1);
				//threadGroupID: group:
				dos.writeInt(-1);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case DISPOSE:
				logger.info("cmd: dispose");
				jvm.dispose();
				return new ReplyPacket(cmd.id,(short) 0);
			case SUSPEND:
				logger.info("cmd: suspend");
				jvm.suspend();
				return new ReplyPacket(cmd.id,(short) 0);
			case RESUME:
				logger.info("cmd: resume");
				jvm.resume();
				return new ReplyPacket(cmd.id,(short) 0);
			case CREATE_STRING:
				dis = cmd.getDataInputStream();
				String s=readString(dis);
				logger.info("cmd: CREATE STRING: "+s);
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				dos.writeInt(0);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CAPABILITIES:
				logger.info("cmd: CAPABILITIES");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				dos.writeBoolean(false);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CLASSPATHS:
				logger.info("cmd: CLASSPATHS");
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				String baseDir="./";
				byte[] arr=baseDir.getBytes();
				dos.writeInt(arr.length);
				dos.write(arr);
				dos.writeInt(0);
				dos.writeInt(0);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case HOLD_EVENTS:
				logger.info("cmd: hold events");
				eventHandler.holdEvents();
				return new ReplyPacket(cmd.id,(short) 0);
			case RELEASE_EVENTS:
				logger.info("cmd: release events");
				eventHandler.releaseEvents();
				return new ReplyPacket(cmd.id,(short) 0);
			default:
				throw new RuntimeException("Unknown VMCommand: "+cmd.command);
		}
	}

	private ReplyPacket processReferenceTypeCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case SIGNATURE:
				DataInputStream dis = cmd.getDataInputStream();
				int refTypeId = dis.readInt();
				logger.info("cmd: SIGNATURE FOR: "+refTypeId);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(baos);
				switch (refTypeId){
					case -1:
						writeSignature(dos,"java/lang/ThreadGroup");
						break;
					case -2:
						writeSignature(dos,"java/lang/Thread");
						break;
					default:
						ClassIntf clazz = classLoader.getClassByID(refTypeId);
						writeSignature(dos,clazz.getName());
						break;
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CLASSLOADER:
				dis = cmd.getDataInputStream();
				refTypeId = dis.readInt();
				logger.info("cmd: CLASSLOADER FOR: "+refTypeId);
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				dos.writeInt(0);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case FIELDS:
				logger.info("cmd: FIELDS");
				dis = cmd.getDataInputStream();
				refTypeId = dis.readInt();
				ClassIntf clazz = classLoader.getClassByID(refTypeId);
				List<? extends FieldIntf> fields = clazz.getDeclaredFields();
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				int size=fields.size();
				dos.writeInt(size);
				for(int i=0;i<size;++i){
					FieldIntf f = fields.get(i);
					dos.writeInt(f.getID());
					writeString(dos, f.getName());
					writeString(dos, f.getSignature());
					dos.writeInt(f.getAccessFlags());
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case METHODS:
				logger.info("cmd: METHODS");
				dis = cmd.getDataInputStream();
				refTypeId = dis.readInt();
				clazz = classLoader.getClassByID(refTypeId);
				if(clazz==null) {
					logger.error("no class found for ID: ", refTypeId);
					return new ReplyPacket(cmd.id,(short) ErroCodes.INVALID_OBJECT);
				}
				List<? extends MethodIntf> methods = clazz.getDeclaredMethods();
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				size=methods.size();
				dos.writeInt(size);
				for(int i=0;i<size;++i){
					MethodIntf m = methods.get(i);
					dos.writeInt(m.getID());
					writeString(dos, m.getName());
					writeString(dos, m.getSignature());
					dos.writeInt(m.getAccessFlags());
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case SOURCE_FILE:
				logger.info("cmd: SOURCE_FILE");
				dis = cmd.getDataInputStream();
				refTypeId=dis.readInt();
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				clazz=classLoader.getClassByID(refTypeId);
				writeString(dos, clazz.getSourceFileName());
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case STATUS:
				logger.info("cmd: STATUS");
				dis = cmd.getDataInputStream();
				refTypeId=dis.readInt();
				logger.info("refTypeID: "+refTypeId);
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				clazz=classLoader.getClassByID(refTypeId);
				//if(clazz.isInitialized())
					dos.writeInt(VERIFIED | PREPARED | INITIALIZED);
				//else
				//	dos.writeInt(PREPARED);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case INTERFACES:
				logger.info("cmd: INTERFACES");
				dis = cmd.getDataInputStream();
				refTypeId=dis.readInt();
				baos=new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				clazz=classLoader.getClassByID(refTypeId);
				ClassIntf[] intfs = clazz.getInterfaces();
				dos.writeInt(intfs.length);
				for(ClassIntf intf:intfs)
					dos.writeInt(intf.getID());
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());

			default:
				throw new RuntimeException("Unknown ReferenceTypeCommand: "+cmd.command);
		}
	}
	private ReplyPacket processClassTypeCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case SUPERCLASS:
				logger.info("cmd: SUPEECLASS");
				DataInputStream dis = cmd.getDataInputStream();
				ByteArrayOutputStream baos=new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				int classID = dis.readInt();
				ClassIntf clazz = classLoader.getClassByID(classID);
				if(clazz==null)
					clazz = classLoader.getClassByID(classID);
				clazz=clazz.getSuperClass();
				if(clazz==null)
					dos.writeInt(0);
				else
					dos.writeInt(clazz.getID());
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			default:
				throw new RuntimeException("Unknown ClassTypeTypeCommand: "+cmd.command);
		}
	}

	private ReplyPacket processEventRequest(CommandPacket cmd) throws IOException {
		switch (cmd.command) {
			case SET_EVENT_REQUEST:
				DataInputStream dis = cmd.getDataInputStream();
				byte eventKind=dis.readByte();
				byte suspendPolicy=dis.readByte();
				logger.info("cmd: EVENT_REQUEST eventKind: "+eventKind+", suspendPolicy: "+suspendPolicy+" requestID: "+eventRequestID);
				int modifiers=dis.readInt();
				EventModifier[] mods=new EventModifier[modifiers];
				for(int i=0;i<modifiers;++i){
					byte modKind = dis.readByte();
					logger.info("modKind: "+modKind);
					EventModifier mod=new EventModifier(modKind);
					mods[i]=mod;
					switch (modKind) {
						case 1:
							mod.count=dis.readInt();
							break;
						case 2:
							mod.exprID = dis.readInt();
							break;
						case 3:
							//threadID
							mod.threadID = dis.readInt();
							break;
						case 4:
							//referenceTypeID
							mod.refTypeID = dis.readInt();
							break;
						case 5:
							//String
							mod.classPattern=readString(dis);
							break;
						case 6:
							//String
							mod.clazzexclude = readString(dis);
							break;
						case 7:
							//location
							mod.loc=readLocation(dis);
							break;
						case 8:
							//referenceTypeID
							mod.exception = dis.readInt();
							mod.caught=dis.readBoolean();
							mod.uncaught=dis.readBoolean();
							break;
						case 9:
							//referenceTypeID
							mod.declating = dis.readInt();
							mod.fieldID = dis.readInt();
							break;
						case 10:
							//threadID
							mod.thread = dis.readInt();
							mod.size = dis.readInt();
							mod.depth = dis.readInt();
							break;
						case 11:
							//objectID
							mod.instance = dis.readInt();
							break;
						case 12:
							//objectID
							//string
                            mod.s=readString(dis);
							break;
					}
				}
				ByteArrayOutputStream baos=new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				dos.writeInt(eventRequestID);
				eventHandlers.put(eventRequestID, (int) eventKind);
				eventHandler.addEventListener(suspendPolicy, eventKind, eventRequestID, mods);
				/*
				switch(eventKind) {
					case SINGLE_STEP:
						//jvm.addEventListener(eventKind, eventRequestID);
						break;
					case BREAKPOINT:
					    Location loc= (Location) mods[0].loc;
						ClassIntf clazz = classLoader.getClassByID(loc.classID);
						MethodIntf method = clazz.getMethodByID(loc.methodID);
						logger.info(method.getSignature());
						jvm.addBreakPoint(eventRequestID, loc.classID, loc.methodID, (int) loc.pc);
						break;
					case CLASS_PREPARE:
					    final int requestID=eventRequestID;
					    String signature=null;
					    if(mods.length==1)
					    	signature= (String) mods[0].classPattern;
					    jvm.setOnClassPrepareListener(signature, new Consumer<ClassIntf>(){
							@Override
							public void accept(ClassIntf clazz) {
								try {
									ByteArrayOutputStream baos = new ByteArrayOutputStream();
									DataOutputStream edos=new DataOutputStream(baos);
									//suspend policy
									edos.writeByte(0);
									//events in set
									edos.writeInt(1);
									//envent kind
									edos.writeByte(CLASS_PREPARE);
									edos.writeInt(requestID);
									//threadID
									edos.writeInt(2);
									if(clazz.isInterface())
										edos.writeByte(INTERFACE);
									else
										edos.writeByte(CLASS);
									//referenceTypeID typeID
									edos.writeInt(clazz.getID());
									writeSignature(edos, clazz.getName());
									//status
									//if(clazz.isInitialized())
									edos.writeInt(VERIFIED | PREPARED | INITIALIZED);
									edos.flush();

									CommandPacket event=new CommandPacket((byte)0,(byte)64,(byte)100,baos.toByteArray());
									synchronized(JDWP.this.socketOut){
										event.send(JDWP.this.socketOut);
										JDWP.this.socketOut.flush();
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						});
						break;
					case CLASS_LOAD:
						break;
					case CLASS_UNLOAD:
						break;
					case EXCEPTION:
						break;
					case THREAD_START:
						break;
					case THREAD_DEATH:
						break;
					default:
					    throw new RuntimeException("unknown eventKind: "+eventKind);
				}

				 */
				eventRequestID++;
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CLEAR_EVENT_REQUEST:
				logger.info("cmd: CLEAR_EVENT");
				dis=cmd.getDataInputStream();
				eventKind=dis.readByte();
				int requestID = dis.readInt();
				eventHandlers.remove(requestID);
				eventHandler.removeEventListener(eventKind, requestID);
				return new ReplyPacket(cmd.id,(short) 0);
			default:
				throw new RuntimeException("Unknown EventRequest: "+cmd.command);
		}
	}
	private ReplyPacket processStackFrame(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case THIS_OBJECT:
				logger.info("THIS_OBJECT");
				DataInputStream dis = cmd.getDataInputStream();
				int threadID=dis.readInt();
				int frameID=dis.readInt();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				ThreadIntf thread = jvm.getThreadByID(threadID);
				List<? extends FrameIntf> frames = thread.getFrames();
				FrameIntf frame = frames.get(frameID);
				Object[] lv=frame.getLocalVariables();
				dos.writeInt((Integer) lv[0]);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());

			default:
				throw new RuntimeException("Unknown StackFrame Command: "+cmd.command);
		}
	}


	private ReplyPacket processThreadGroupReference(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case THREAD_GROUP_NAME:
				DataInputStream dis = cmd.getDataInputStream();
				int threadGroupID=dis.readInt();
				logger.info("THREADGROUP_NAME for: "+threadGroupID);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				writeString(dos, "BrowserThreadGroup");
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case CHILDREN:
				dis = cmd.getDataInputStream();
				threadGroupID=dis.readInt();
				logger.info("THREADGROUP_CHILDREN for: "+threadGroupID);
				baos = new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				//children count
				dos.writeInt(1);
				//child id
				dos.writeInt(-2);
				//number of active child thread groups
				dos.writeInt(0);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			default:
				throw new RuntimeException("Unknown ThreadGroupReference Command: "+cmd.command);
		}
	}
	private ReplyPacket processThreadReferenceCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case THREAD_NAME:
				DataInputStream dis = cmd.getDataInputStream();
				int threadID=dis.readInt();
				logger.info("cmd: THREAD_NAME for: "+threadID);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos=new DataOutputStream(baos);
				writeString(dos, "BrowserThread");
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case THREAD_STATUS:
				dis = cmd.getDataInputStream();
				threadID=dis.readInt();
				logger.info("cmd: THREAD_STATUS for: "+threadID);
				ThreadIntf thread = jvm.getThreadByID(threadID);
				baos = new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				dos.writeInt(thread.getState());
				dos.writeInt(thread.isSuspended()?1:0);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case THREAD_GROUP:
				dis = cmd.getDataInputStream();
				threadID=dis.readInt();
				logger.info("cmd: THREAD_GROUP for: "+threadID);
				baos = new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				dos.writeInt(-1);
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case THREAD_FRAMES:
				dis = cmd.getDataInputStream();
				threadID=dis.readInt();
				int startFrame = dis.readInt();
				int length = dis.readInt();
				logger.info("cmd: THREAD_FRAMES for: "+threadID);
				thread = jvm.getThreadByID(threadID);
				List<? extends FrameIntf> frames = thread.getFrames();
				baos = new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				//FramesCount
				int size=Math.min(length, frames.size());
				dos.writeInt(size);
				for(int i = startFrame; i<startFrame+size; ++i){
					FrameIntf f=frames.get(i);
					dos.writeInt(f.getID());
					Location location=new Location(f.getClassID(), f.getMethodID(), f.getPC());
					writeLocation(dos, location);
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case THREAD_FRAME_COUNT:
				logger.info("cmd: THREAD_FRAME_COUNT");
				dis = cmd.getDataInputStream();
				threadID=dis.readInt();
				thread = jvm.getThreadByID(threadID);
				frames = thread.getFrames();
				baos = new ByteArrayOutputStream();
				dos=new DataOutputStream(baos);
				//FramesCount
				dos.writeInt(frames.size());
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			default:
				throw new RuntimeException("Unknown ThreadReference Command: "+cmd.command);
		}
	}
	private ReplyPacket processMethodCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case LINE_TABLE:
				logger.info("cmd: LINE_TABLE");
				DataInputStream dis = cmd.getDataInputStream();
				int classID = dis.readInt();
				ClassIntf clazz = classLoader.getClassByID(classID);
				int methodID = dis.readInt();
				MethodIntf method = clazz.getMethodByID(methodID);
				List<? extends LineNumberEntry> lineNumbers = method.getLineNumbers();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(baos);
				dos.writeLong(0L);
				dos.writeLong(method.getCodeLength());
				dos.writeInt(lineNumbers.size());
				for(LineNumberEntry line:lineNumbers){
					dos.writeLong(line.getPC());
					dos.writeInt(line.getLineNumber());
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case VARIABLE_TABLE:
				logger.info("cmd: VARIABLE_TABLE");
				dis = cmd.getDataInputStream();
				classID = dis.readInt();
				clazz = classLoader.getClassByID(classID);
				methodID = dis.readInt();
				method = clazz.getMethodByID(methodID);
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				return new ReplyPacket(cmd.id,(short) ABSENT_INFORMATION,baos.toByteArray());
			default:
				throw new RuntimeException("Unknown Method Command: "+cmd.command);
		}
	}

	private ReplyPacket processObjectReferenceCommand(CommandPacket cmd) throws IOException {
		switch (cmd.command){
			case REFERENCE_TYPE:
				DataInputStream dis = cmd.getDataInputStream();
				int objectID=dis.readInt();
				logger.info("cmd: REFERENCE_TYPE: "+objectID);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(baos);
				switch (objectID) {
					case -1:
						dos.writeByte(CLASS);
						ClassIntf threadGroupClass = classLoader.getLoadedClass("java/lang/ThreadGroup");
						dos.writeInt(threadGroupClass.getID());
						break;
					case -2:
						dos.writeByte(CLASS);
						ClassIntf threadClass = classLoader.getLoadedClass("java/lang/Thread");
						dos.writeInt(threadClass.getID());
						break;
					default:
						dos.writeByte(CLASS);
						dos.writeInt(0);
						break;
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			case ISCOLLECTED:
				logger.info("cmd: ISCOLLECTED");
				dis = cmd.getDataInputStream();
				objectID=dis.readInt();
				baos = new ByteArrayOutputStream();
				dos = new DataOutputStream(baos);
				switch (objectID) {
					case -1:
						dos.writeBoolean(false);
						break;
					case -2:
						dos.writeBoolean(false);
						break;
					default:
						dos.writeBoolean(false);
						break;
				}
				return new ReplyPacket(cmd.id,(short) 0,baos.toByteArray());
			default:
				throw new RuntimeException("Unknown ObjectReference Command: "+cmd.command);
		}
	}
	public static void writeSignature(DataOutputStream dos, String s)throws IOException{
		byte[] s_ba=s.getBytes();
		dos.writeInt(s_ba.length+2);
		dos.writeByte('L');
		dos.write(s_ba);
		dos.writeByte(';');
	}
	public static void writeString(DataOutputStream dos, String s)throws IOException{
		byte[] s_ba=s.getBytes();
		dos.writeInt(s_ba.length);
		dos.write(s_ba);

	}
	public static String readString(DataInputStream dis)throws IOException{
		int length=dis.readInt();
		byte[] s_ba=new byte[length];
		dis.read(s_ba);
		return new String(s_ba);
	}

	public Location readLocation(DataInputStream dis) throws IOException {
		byte type = dis.readByte();
		switch (type){
			case 1:
				int classID = dis.readInt();
				int methodID = dis.readInt();
				long pc = dis.readLong();
				return new Location(classID, methodID, pc);
		}
		return null;
	}
	private void writeLocation(DataOutputStream dos, Location location) throws IOException {
		dos.writeByte(1);
		dos.writeInt(location.classID);
		dos.writeInt(location.methodID);
		dos.writeLong(location.pc);
	}
}
