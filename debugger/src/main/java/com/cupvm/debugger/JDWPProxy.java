package com.cupvm.debugger;

import java.io.*;
import com.cupvm.ConsoleLogger;
import com.cupvm.Logger;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class JDWPProxy{
	private static final Logger logger= ConsoleLogger.get();
    private final Proxy proxy;
    private ServerSocket serverSocket;
    private DataOutputStream socketOut;
    private Runnable onDisconnect;
    private Socket clientSocket;

    public void sendEvent(ReplyPacket result) throws IOException {
        synchronized (socketOut) {
            result.send(socketOut);
            socketOut.flush();
        }
    }

    public interface Proxy {
        ReplyPacket processCommand(CommandPacket cmd) throws IOException;
    }

    public JDWPProxy(Proxy proxy) {
        this.proxy=proxy;
    }

		private volatile boolean closed=false;
    public void close() throws IOException {
			closed=true;
        synchronized (this) {
            if (serverSocket == null)
                throw new IOException("can't close: serverSocket is null");
            if (clientSocket != null)
                clientSocket.close();
            serverSocket.close();
        }
    }


    public void setOnDisconnectListener(Runnable runnable) {
        this.onDisconnect=runnable;
    }



    public void startasync(){
        Thread t=new Thread(() -> {
            try {
                start();
            } catch (IOException e) {
							if(closed)
								logger.info("jdwp closed");
							else
								e.printStackTrace();
            }
            finally {
                try {
                    serverSocket.close();
                } catch (IOException e) {
										logger.info("IOException caught later: ");
                    e.printStackTrace();
                }
                onDisconnect.run();
            }
        });
        t.setName("JDWP-Thread");
        t.setDaemon(true);
        t.start();
    }

    public void start()throws IOException{
        DataInputStream dis;
        synchronized(this) {
            this.serverSocket = new ServerSocket(5005, 0, InetAddress.getLoopbackAddress());
        }
        this.clientSocket = serverSocket.accept();
        OutputStream out = clientSocket.getOutputStream();
        InputStream is = clientSocket.getInputStream();
        byte[] handshake = new byte[14];
        is.read(handshake);
        out.write(handshake);
        dis = new DataInputStream(is);
        socketOut = new DataOutputStream(out);
        while (true) {
            CommandPacket cmd = new CommandPacket(dis);
            ReplyPacket r = proxy.processCommand(cmd);
            synchronized (socketOut) {
                if(r==null){
                    clientSocket.close();
                    return;
                }
                r.send(socketOut);
                socketOut.flush();
            }
        }
    }

}
