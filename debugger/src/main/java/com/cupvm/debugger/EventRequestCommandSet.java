package com.cupvm.debugger;

public interface EventRequestCommandSet {
    int SET_EVENT_REQUEST = 1;
    int CLEAR_EVENT_REQUEST = 2;
    //EventKind:
    int SINGLE_STEP	=1;
    int BREAKPOINT	=2;
    int FRAME_POP	=3;
    int EXCEPTION	=4;
    int USER_DEFINED	=5;
    int THREAD_START	=6;
    int THREAD_DEATH	=7;
    int CLASS_PREPARE	=8;
    int CLASS_UNLOAD	=9;
    int CLASS_LOAD	=10;
    int FIELD_ACCESS	=20;
    int FIELD_MODIFICATION	=21;
    int EXCEPTION_CATCH	=30;
    int METHOD_ENTRY	=40;
    int METHOD_EXIT	=41;
    int METHOD_EXIT_WITH_RETURN_VALUE	=42;
    int MONITOR_CONTENDED_ENTER	=43;
    int MONITOR_CONTENDED_ENTERED	=44;
    int MONITOR_WAIT	=45;
    int MONITOR_WAITED	=46;
    int VM_START	=90;
    int VM_DEATH	=99;
    int VM_DISCONNECTED=100;
}
