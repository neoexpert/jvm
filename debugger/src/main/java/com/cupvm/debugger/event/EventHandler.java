package com.cupvm.debugger.event;

public interface EventHandler {
    void addEventListener(byte suspendPolicy, byte eventKind, int requestID, EventModifier[] mods);
    void removeEventListener(byte eventKind, int requestID);
    void holdEvents();
    void releaseEvents();
}
