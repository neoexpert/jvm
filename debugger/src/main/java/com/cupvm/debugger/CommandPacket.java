package com.cupvm.debugger;
import java.io.*;

public class CommandPacket{
	private static int id_counter;
	private final int length;
	public final int id;
	private final byte flags;
	private final byte[] data;
	public final byte command_set;
	public final byte command;

	public CommandPacket(byte flags, byte command_set, byte command, byte[] data){
		this.length=11+data.length;
		this.id=id_counter++;
		this.flags=flags;
		this.command_set=command_set;
		this.command=command;
		this.data=data;
	}

	public CommandPacket(DataInputStream dis) throws IOException {
		length=dis.readInt();
		id=dis.readInt();
		flags=dis.readByte();
		command_set=dis.readByte();
		command=dis.readByte();
		data=new byte[length-11];
		dis.read(data);
	}

	public void write(DataOutputStream dos) throws IOException {
		dos.writeInt(length);
		dos.writeInt(id);
		dos.writeByte(flags);
		dos.writeByte(command_set);
		dos.writeByte(command);
		dos.write(data);
	}

	public DataInputStream getDataInputStream() {
		ByteArrayInputStream bais=new ByteArrayInputStream(data);
		return new DataInputStream(bais);
	}

	public byte[] toArray() {
		ByteArrayOutputStream baos=new ByteArrayOutputStream(length);
		try {
			write(new DataOutputStream(baos));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}
}
