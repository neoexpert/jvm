package com.cupvm.jvm;

import com.cupvm.jni.ThreadInterface;

import java.util.List;

public interface JVMIntf{
	ClassLoaderIntf getClassLoader();
	List<? extends ThreadInterface> getThreads();
	void suspend();
	void resume();
	ThreadIntf getThreadByID(int threadID);
	void dispose();
}
