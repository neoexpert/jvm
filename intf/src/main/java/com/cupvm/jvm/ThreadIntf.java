package com.cupvm.jvm;

import java.util.List;

public interface ThreadIntf {
    List<? extends FrameIntf> getFrames();
    int getState();
    boolean isSuspended();
}
