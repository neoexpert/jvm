package com.cupvm.jvm;

public interface FieldIntf {
    int getID();
    String getName();
    String getSignature();
    int getAccessFlags();
}
