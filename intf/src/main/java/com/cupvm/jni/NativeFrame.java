package com.cupvm.jni;

import java.util.HashSet;

public interface NativeFrame{
	void push(Number v);
	void pushl(Number v);
	Number pop();

	Object getMethod();

	int getCurrentLine();

	void checkReference(HashSet<Number> refs);
}
