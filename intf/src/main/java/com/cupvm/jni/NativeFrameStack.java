package com.cupvm.jni;

import java.util.Iterator;

public interface NativeFrameStack {
    NativeFrame get(int i);
    int size();
    Iterator<NativeFrame> iterator();
}
